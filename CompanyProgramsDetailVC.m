

//
//  CompanyProgramsDetailVC.m
//  PerfectGraduate
//
//  Created by netset on 6/3/15.
//  Copyright (c) 2015 netset. All rights reserved.
//




#define FONT_SIZE 14.0f
#define CELL_CONTENT_WIDTH 320.0f
#define CELL_CONTENT_MARGIN 10.0f

#import "CompanyProgramsDetailVC.h"
#import "AppDelegate.h"

@interface CompanyProgramsDetailVC ()
{
    NSMutableArray*arrayForBool;
    NSArray *arrData;
    NSMutableDictionary *dicPrograms;
    NSArray *arrProgramAllKeys;
}

@end

@implementation CompanyProgramsDetailVC

-(void)viewWillAppear:(BOOL)animated
{
    stringNotifiction = @"";
    stringForNotifyEditCompany = @"";
    
    if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"] )
    {
        self.imageRedStar.hidden = YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(redStarShow)
                                                 name:@"redStarShow"
                                               object:nil];
}

-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, _imageRedStar.frame.origin.y, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    
    self.imageRedStar.hidden = YES;
    
    dicPrograms = [[NSMutableDictionary alloc] init];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.navigationController.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = leftItem;
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    self.navigationController.navigationBar.tintColor =[UIColor whiteColor];
    
    arrayForBool = [[NSMutableArray alloc]init];
    
    arrData =[[_dicData allValues] objectAtIndex:0];
    
    if (![[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Awards"])
    {
        for (int i=0; i<[[[_dicData allValues] objectAtIndex:0] count]; i++)
        {
            [arrayForBool addObject:[NSNumber numberWithBool:NO]];
        }
    }
    
    if ([[[_dicData allKeys] objectAtIndex:0] isEqualToString:@"Application Due Dates"])
    {
        
        NSArray *arrDupRemoved = [[NSSet setWithArray:[arrData valueForKey:@"program_id"]] allObjects];
        
        for (int k=0; k<arrDupRemoved.count; k++)
        {
            NSMutableArray*arrFilteredData = [[NSMutableArray alloc] init];
            
            NSString *strProgName;
            
            strProgName = nil;
            
            for (int i=0; i<[[arrData valueForKey:@"program_id"] count]; i++)
                
            {
                if ([[arrDupRemoved objectAtIndex:k] isEqual:[[arrData valueForKey:@"program_id"] objectAtIndex:i]])
                {
                    [arrFilteredData addObject:[arrData objectAtIndex:i]];
                    strProgName = [[arrData valueForKey:@"program"]objectAtIndex:i];
                }
            }
            
            [dicPrograms setObject:arrFilteredData forKey:strProgName];
        }
        
        arrProgramAllKeys = [dicPrograms allKeys];
    }
    
    else if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Awards"])
    {
        
        NSMutableArray *arrDuplicateRemoved = [[NSMutableArray alloc]init];
        
        for (id obj in [arrData valueForKey:@"awards"])
        {
            if (![arrDuplicateRemoved containsObject:obj]) {
                [arrDuplicateRemoved addObject:obj];
            }
        }
        for (int k=0; k<arrDuplicateRemoved.count; k++)
        {
            NSMutableArray*arrFilteredData = [[NSMutableArray alloc] init];
            NSString *strProgName;
            strProgName = nil;
            for (int i=0; i<[[arrData valueForKey:@"awards"] count]; i++)
                
            {
                if ([[arrDuplicateRemoved objectAtIndex:k] isEqual:[[arrData valueForKey:@"awards"] objectAtIndex:i]])
                {
                    [arrFilteredData addObject:[arrData objectAtIndex:i]];
                    strProgName = [[arrData valueForKey:@"awards"]objectAtIndex:i];
                }
            }
            
            [dicPrograms setObject:arrFilteredData forKey:strProgName];
        }
        
        arrProgramAllKeys = [dicPrograms allKeys];
    }
    
    [self addNavTitles];
}


-(void)addNavTitles
{
    UIView  *textView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 200, 44)];
    
    UILabel *labelTitleName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 0, CGRectGetWidth(textView.frame), 22)];
    labelTitleName.backgroundColor = [UIColor clearColor];
    labelTitleName.font = [UIFont fontWithName:@"OpenSans" size:18.0f];
    labelTitleName.textAlignment = NSTextAlignmentCenter;
    labelTitleName.textColor = [UIColor whiteColor];
    labelTitleName.text = _strCompanyName;
    
    labelTitleName.adjustsFontSizeToFitWidth = YES;
    [labelTitleName setMinimumScaleFactor:8.0/[UIFont labelFontSize]];
    
    /*----UILabel Sub-title Name-------*/
    UILabel * labelSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 22, CGRectGetWidth(textView.frame), 18)];
    labelSubTitle.backgroundColor = [UIColor clearColor];
    labelSubTitle.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
    labelSubTitle.textAlignment = NSTextAlignmentCenter;
    labelSubTitle.textColor = [UIColor whiteColor];
    labelSubTitle.text = [[_dicData allKeys] objectAtIndex:0];
    [textView addSubview:labelTitleName];
    [textView addSubview:labelSubTitle];
    self.navigationItem.titleView=textView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - UITableViewDataSource------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Application Due Dates"]||[[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Awards"])
    {
        return [arrProgramAllKeys count];
    }
    else
    {
        return [arrData count];
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (![[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Awards"])
    {
        if ([[arrayForBool objectAtIndex:section] boolValue])
        {
            
            if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Application Due Dates"])
            {
                return [[dicPrograms valueForKey:[arrProgramAllKeys objectAtIndex:section]] count];
                
            }
            
            else if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Target Disciplines"])
            {
                return [[[arrData valueForKey:@"sub_discipline"] objectAtIndex:section] count];
                
            }
            else if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Frequently Asked Questions"])
                
            {
                return 1;
            }
        }
    }
    else
    {
        return [[dicPrograms valueForKey:[arrProgramAllKeys objectAtIndex:section]] count];
    }
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellId";
    /*remove cell line in table view*/
    [self.tableViewCompnyInfo setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    tableView.backgroundColor = [UIColor whiteColor];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
        
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.backgroundColor = [UIColor whiteColor];
        
        cell.textLabel.highlightedTextColor = [UIColor whiteColor];
        
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    
    else
    {
        UIView *subview;
        while ((subview= [[[cell contentView]subviews]lastObject])!=nil)
            [subview removeFromSuperview];
    }
    
    if ([[[_dicData allKeys] objectAtIndex:0] isEqualToString:@"Application Due Dates"])
    {
        cell.separatorInset = UIEdgeInsetsZero;
        
        UILabel *staticLabelState = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width/3, 15)];
        
        staticLabelState.text = @"Location";
        
        staticLabelState.textAlignment = NSTextAlignmentCenter;
        
        staticLabelState.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
        
        staticLabelState.textColor = [UIColor blueColor];
        
        UILabel *labelStateName = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, (tableView.frame.size.width/3), 30)];
        
        labelStateName.textAlignment = NSTextAlignmentCenter;
        
        if ([[[[dicPrograms valueForKey:[arrProgramAllKeys objectAtIndex:indexPath.section]] valueForKey:@"state"] objectAtIndex:indexPath.row] isEqualToString:@"no"])
        {
            labelStateName.text = _strCountryame;
        }
        else
        {
            labelStateName.text = [[[dicPrograms valueForKey:[arrProgramAllKeys objectAtIndex:indexPath.section]] valueForKey:@"state"] objectAtIndex:indexPath.row];
        }
        labelStateName.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
        
        UILabel *staticStartDate = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(labelStateName.frame), 0, tableView.frame.size.width/3, CGRectGetHeight(staticLabelState.frame))];
        
        staticStartDate.textAlignment = NSTextAlignmentCenter;
        
        staticStartDate.text = @"Start Date";
        
        staticStartDate.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
        
        staticStartDate.textColor = [UIColor blueColor];
        
        UILabel *labelStartDate = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(labelStateName.frame), 15, tableView.frame.size.width/3, CGRectGetHeight(labelStateName.frame))];
        
        labelStartDate.textAlignment = NSTextAlignmentCenter;
        
        NSDateFormatter* startDateFormatter = [[NSDateFormatter alloc] init];
        
        startDateFormatter.dateFormat = @"yyyy-MM-dd";
        
        NSDate *yourDate = [startDateFormatter dateFromString:[[[dicPrograms valueForKey:[arrProgramAllKeys objectAtIndex:indexPath.section]] valueForKey:@"start_date"] objectAtIndex:indexPath.row]];
        
        NSDateFormatter* myStartDateFormatter = [[NSDateFormatter alloc] init];
        myStartDateFormatter.dateFormat = @"dd-MMM-yyyy";
        labelStartDate.text = [[myStartDateFormatter stringFromDate:yourDate] uppercaseString];
        labelStartDate.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
        
        UILabel *staticEndDate = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(labelStartDate.frame), 0, tableView.frame.size.width/3, CGRectGetHeight(staticLabelState.frame))];
        staticEndDate.textAlignment = NSTextAlignmentCenter;
        
        staticEndDate.text = @"End Date";
        staticEndDate.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
        staticEndDate.textColor = [UIColor blueColor];
        
        UILabel *labelEndDate = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(labelStartDate.frame), 15, tableView.frame.size.width/3, CGRectGetHeight(labelStateName.frame))];
        labelEndDate.textAlignment = NSTextAlignmentCenter;
        
        if (indexPath.row!=0)
        {
            labelStateName.frame = CGRectMake(0, 3, (tableView.frame.size.width/3), 35);
            labelStartDate.frame = CGRectMake(CGRectGetMaxX(labelStateName.frame), 3, tableView.frame.size.width/3, CGRectGetHeight(labelStateName.frame));
            labelEndDate.frame = CGRectMake(CGRectGetMaxX(labelStartDate.frame), 3, tableView.frame.size.width/3, CGRectGetHeight(labelStateName.frame));
        }
        
        if ([[[[dicPrograms valueForKey:[arrProgramAllKeys objectAtIndex:indexPath.section]] valueForKey:@"close_date"] objectAtIndex:indexPath.row] isEqualToString:@"OPEN UNTIL FILLED"]||[[[[dicPrograms valueForKey:[arrProgramAllKeys objectAtIndex:indexPath.section]] valueForKey:@"close_date"] objectAtIndex:indexPath.row] isEqualToString:@"Open Until Filled"])
        {
            labelEndDate.text = @"Open Until Filled";
        }
        else if ([[[[dicPrograms valueForKey:[arrProgramAllKeys objectAtIndex:indexPath.section]] valueForKey:@"close_date"] objectAtIndex:indexPath.row] isEqualToString:@"no"])
        {
            labelEndDate.text = @"NA";
        }
        else
        {
            NSDateFormatter* endDateFormatter = [[NSDateFormatter alloc] init];
            endDateFormatter.dateFormat = @"yyyy-MM-dd";
            NSDate *endDate = [endDateFormatter dateFromString:[[[dicPrograms valueForKey:[arrProgramAllKeys objectAtIndex:indexPath.section]] valueForKey:@"close_date"] objectAtIndex:indexPath.row]];
            NSDateFormatter* myEndDateFormatter = [[NSDateFormatter alloc] init];
            myEndDateFormatter.dateFormat = @"dd-MMM-yyyy";
            labelEndDate.text = [[myEndDateFormatter stringFromDate:endDate] uppercaseString];
        }
        
        labelEndDate.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
        
        [cell.contentView addSubview:labelStateName];
        [cell.contentView addSubview:labelStartDate];
        [cell.contentView addSubview:labelEndDate];
        [cell.contentView addSubview:staticEndDate];
        [cell.contentView addSubview:staticLabelState];
        [cell.contentView addSubview:staticStartDate];
        
        if (indexPath.row==0)
        {
            staticEndDate.hidden = NO;
            staticLabelState.hidden = NO;
            staticStartDate.hidden = NO;
        }
        else
        {
            staticEndDate.hidden = YES;
            staticLabelState.hidden = YES;
            staticStartDate.hidden = YES;
        }
    }
    else if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Target Disciplines"])
    {
        if ([[[[[arrData valueForKey:@"sub_discipline"] objectAtIndex:indexPath.section] valueForKey:@"name"] objectAtIndex:indexPath.row]isEqualToString:@"<null>"])
        {
            
        }
        else
        {
            cell.textLabel.text =[NSString stringWithFormat:@"              %@",[[[[arrData valueForKey:@"sub_discipline"] objectAtIndex:indexPath.section] valueForKey:@"name"] objectAtIndex:indexPath.row]];
            
            cell.textLabel.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
            
            cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
        }
    }
    
    else if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Awards"])
        
    {
        if ([[[[dicPrograms valueForKey:[arrProgramAllKeys objectAtIndex:indexPath.section]] valueForKey:@"description"] objectAtIndex:indexPath.row] isEqualToString:@"Nostar"])
        {
            cell.textLabel.text = @"Honourable mention";
        }
        else
        {
            cell.textLabel.text = [[[dicPrograms valueForKey:[arrProgramAllKeys objectAtIndex:indexPath.section]] valueForKey:@"description"] objectAtIndex:indexPath.row];
        }
        
        cell.textLabel.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
    }
    
    else if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Frequently Asked Questions"])
        
    {
        NSString *text = [[arrData valueForKey:@"answer"] objectAtIndex:indexPath.section];
        
        CGSize constraint = CGSizeMake(tableView.frame.size.width-10, 20000.0f);
        
        CGSize size = [text sizeWithFont:[UIFont fontWithName:@"OpenSans" size:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByTruncatingMiddle];
        
        
        
        UILabel  *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width-10, MAX(size.height, 44.0f))];
        [label setLineBreakMode:NSLineBreakByTruncatingMiddle];
        [label setMinimumFontSize:14];
        [label setNumberOfLines:0];
        [label setFont:[UIFont fontWithName:@"OpenSans" size:FONT_SIZE]];
        [label setTag:1];
        
        label.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        
        [[cell contentView] addSubview:label];
        
        label.textAlignment = NSTextAlignmentLeft;
        
        if (!label)
            
            label = (UILabel*)[cell viewWithTag:1];
        
        [label setText:text];
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark--UITableViewDelegate--

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    /*---Added one UIVIEW programmatically in table view header--*/
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableViewCompnyInfo.frame.size.width, 60)];
    headerView.tag = section;
    headerView.backgroundColor= [UIColor whiteColor];
    
    UILabel *headingLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, self.view.frame.size.width-20-50, headerView.frame.size.height)];
    
    
    if ([[[_dicData allKeys] objectAtIndex:0] isEqualToString:@"Application Due Dates"])
    {
        headingLabel.text = [arrProgramAllKeys objectAtIndex:section];
    }
    else if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Target Disciplines"])
    {
        headingLabel.text = [[arrData valueForKey:@"name"] objectAtIndex:section];
    }
    
    else if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Awards"])
    {
        int xValue = 120;
        
        if ([[arrProgramAllKeys objectAtIndex:section] isEqualToString:@"1"])
        {
            headingLabel.text = @"Honourable mention";
            xValue=180;
        }
        if ([[arrProgramAllKeys objectAtIndex:section] isEqualToString:@"3"])
        {
            
            headingLabel.text = @"Silver Award";
            
            xValue=120;
        }
        if ([[arrProgramAllKeys objectAtIndex:section] isEqualToString:@"2"])
        {
            headingLabel.text = @"Gold Award";
            
            xValue=120;
        }
        
        for (int k=0; k<[[[dicPrograms valueForKey:[arrProgramAllKeys objectAtIndex:section]] valueForKey:@"awards"] count]; k++)
        {
            UIImageView *imageAwards = [[UIImageView alloc] initWithFrame:CGRectMake(xValue, 22.5f, 15, 15)];
            
            xValue = xValue+20;
            
            if ([[arrProgramAllKeys objectAtIndex:section] isEqualToString:@"2"])
            {
                imageAwards.image = [UIImage imageNamed:@"yellow-cup"];
            }
            else if ([[arrProgramAllKeys objectAtIndex:section] isEqualToString:@"3"])
            {
                imageAwards.image = [UIImage imageNamed:@"silverCup"];
            }
            else if ([[arrProgramAllKeys objectAtIndex:section] isEqualToString:@"1"])
            {
                imageAwards.image = [UIImage imageNamed:@"honour"];
            }
            
            [headerView addSubview:imageAwards];
        }
    }
    
    else if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Frequently Asked Questions"])
        
    {
        UIFont *myFont = [UIFont fontWithName:@"OpenSans" size:18.0f];
        
        NSAttributedString *attributedText = [[NSAttributedString alloc]initWithString:[[arrData valueForKey:@"question"] objectAtIndex:section] attributes:@{NSFontAttributeName:myFont}];
        
        CGRect rect = [attributedText boundingRectWithSize:(CGSize){300, CGFLOAT_MAX}
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        CGSize size = rect.size;
        CGFloat height=0;
        height=size.height;
        
        if (height<=50)
        {
            headerView.frame = CGRectMake(0, 0, tableView.frame.size.width, 50);
            headingLabel.frame = CGRectMake(40,headerView.frame.origin.y, headerView.frame.size.width-60, 50);
        }
        else
        {
            headerView.frame = CGRectMake(0, 0, tableView.frame.size.width, height);
            headingLabel.frame = CGRectMake(40,headerView.frame.origin.y, headerView.frame.size.width-60, height);
        }
        headingLabel.text = [[arrData valueForKey:@"question"] objectAtIndex:section];
    }
    
    /*UIlabel heading label*/
    headingLabel.backgroundColor = [UIColor clearColor];
    headingLabel.font = [UIFont fontWithName:@"OpenSans" size:15.0f];
    headingLabel.textAlignment = NSTextAlignmentLeft;
    headingLabel.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    CALayer *headerLayer = [[CALayer alloc] init];
    headerLayer.backgroundColor = [[UIColor lightGrayColor]CGColor];
    headerLayer.frame = CGRectMake(0, CGRectGetMaxY(headingLabel.frame), CGRectGetWidth(headerView.frame), 1);
    
    CALayer *headerLayer1 = [[CALayer alloc] init];
    
    
    headerLayer1.backgroundColor = [[UIColor lightGrayColor]CGColor];
    
    headerLayer1.frame = CGRectMake(0, CGRectGetMinY(headingLabel.frame)+1, CGRectGetWidth(headerView.frame), 0.5f);
    
    if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Frequently Asked Questions"])
        
    {
        BOOL manyCells  = [[arrayForBool objectAtIndex:section] boolValue];
        
        UIImageView *upDownArrow = [[UIImageView alloc] initWithImage:manyCells ? [UIImage imageNamed:@"-ve"] : [UIImage imageNamed:@"add"]];
        
        upDownArrow.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        
        upDownArrow.frame = CGRectMake(self.view.frame.origin.x+10, self.view.frame.origin.y+16,20,20);
        
        UITapGestureRecognizer *headerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
        
        [headerView addSubview:upDownArrow];
        
        [headerView addGestureRecognizer:headerTapped];
    }
    
    else  if (![[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Awards"])
        
    {
        BOOL manyCells  = [[arrayForBool objectAtIndex:section] boolValue];
        
        UIImageView *upDownArrow = [[UIImageView alloc] initWithImage:manyCells ? [UIImage imageNamed:@"-ve"] : [UIImage imageNamed:@"add"]];
        
        upDownArrow.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        
        upDownArrow.frame = CGRectMake(self.view.frame.origin.x+10, self.view.frame.origin.y+20,20,20);
        
        UITapGestureRecognizer  *headerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
        
        [headerView addSubview:upDownArrow];
        
        [headerView addGestureRecognizer:headerTapped];
    }
    
    else
    {
        [headingLabel setFrame:CGRectMake(15,headerView.frame.origin.y, self.view.frame.size.width-20-50, 60)];
        
    }
    
    [headerView addSubview:headingLabel];
    
    if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Awards"])
    {
        UIImageView *titleImage = [[UIImageView alloc] initWithFrame: CGRectMake(0.0,  1,  headerView.frame.size.width , 0.5f)];
        titleImage.backgroundColor = [UIColor lightGrayColor];
        
        [headerView addSubview:titleImage];
    }
    else
    {
        
        UIImageView *titleImage = [[UIImageView alloc] initWithFrame: CGRectMake(0.0,  1,  headerView.frame.size.width , 0.5f)];
        titleImage.backgroundColor = [UIColor lightGrayColor];
        
        [headerView addSubview:titleImage];
        
    }
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footer  = [[UIView alloc] initWithFrame:CGRectZero];
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Frequently Asked Questions"])
    {
        UIFont *myFont = [UIFont fontWithName:@"OpenSans" size:18.0f];
        
        NSAttributedString *attributedText = [[NSAttributedString alloc]initWithString:[[arrData valueForKey:@"question"]objectAtIndex:section]attributes:@{NSFontAttributeName:myFont}];
        
        CGRect rect = [attributedText boundingRectWithSize:(CGSize){300, CGFLOAT_MAX}
                                                   options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        
        CGSize size = rect.size;
        
        CGFloat height=0;
        
        height=size.height;
        
        if (height<=50)
        {
            return 55;
        }
        else
        {
            return height;
        }
    }
    return 60;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Application Due Dates"])
    {
        return 70;
    }
    
    else  if (![[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Awards"])
    {
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue])
        {
            if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Frequently Asked Questions"])
            {
                NSString *text = [[arrData valueForKey:@"answer"] objectAtIndex:indexPath.section];
                
                CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
                
                CGSize size = [text sizeWithFont:[UIFont fontWithName:@"OpenSans" size:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
                
                CGFloat height = MAX(size.height, 44.0f);
                
                return height + (CELL_CONTENT_MARGIN * 2);
                
            }
            
            else
            {
                return 40;
            }
        }
    }
    else if ([[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Awards"])
    {
        return 30;
    }
    return 0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark --HeaderTapped--

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    if (![[[_dicData allKeys]objectAtIndex:0] isEqualToString:@"Awards"])
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
        
        if (indexPath.row == 0)
        {
            
            BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
            
            collapsed = !collapsed;
            
            [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:collapsed]];
            
            NSRange range = NSMakeRange(indexPath.section, 1);
            
            NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
            
            [_tableViewCompnyInfo reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end




