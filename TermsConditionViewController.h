//
//  TermsConditionViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 2/2/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "CircleProgressBar.h"

@interface TermsConditionViewController : UIViewController
{

}

- (IBAction)checkBoxButtonAction:(id)sender;
- (IBAction)submitButtonAction:(id)sender;
@property(strong,nonatomic) NSDictionary* dicUserInfoFromScPg;
@property(strong,nonatomic) NSArray* arrSocietiesFromScPg;
@property (strong, nonatomic) IBOutlet UIView *viewTerms;

@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton;
@property (weak, nonatomic) IBOutlet UIButton *buttonSubmit;
@property (weak, nonatomic) IBOutlet UIButton *termsConditionCloseButton;
@property (weak, nonatomic) IBOutlet UITextView *termsConditionsTextView;
- (IBAction)termsConditionCloseButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet CircleProgressBar *circleProgressBar;

@end
