//
//  LocationFilterViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 2/6/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "LocationFilterViewController.h"
#import "FilterTableViewCell.h"
#import "AFNetworking.h"
#import "PortfolioViewController.h"
#import "DisciplineViewController.h"
#import "LocationFilterViewController.h"
#import "MessagesViewController.h"
#import "SalaryGuideViewController.h"
#import "LocationFilterSearchViewController.h"
#import "ApiClass.h"

@interface LocationFilterViewController ()<senddataProtocol>
{
    NSIndexPath *oldCheckedData ;
    NSMutableArray *mutableArraySelectStates;
    BOOL searchEnableBool;
    NSArray *arrayData;
    UILabel*labelSubTitle;
    Database *dummy;
}

@end

@implementation LocationFilterViewController
@synthesize searchBarLocation;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imageRedStar.hidden = YES;
    isCancelClicked = NO;
    if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
    {
        [arrStateIds removeAllObjects];
        
        [arrCountryIds removeAllObjects];
        
        arrStateIds = [arrayDisciplineStateId mutableCopy];
        
        arrCountryIds = [arrayDisciplineCountryId mutableCopy];
    }
    
    if ([stringCheckDisciplines isEqualToString:@"fromSubDisciplines"])
    {
        [arrStateIds removeAllObjects];
        
        [arrCountryIds removeAllObjects];
        
        arrStateIds =  [arraySubDisciStateId mutableCopy];
        
        arrCountryIds = [arraySubDisciCountryId mutableCopy];
    }
    
    if(arrStateIds.count==arrayData.count){
        isAllLocations = YES;
    }
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, _imageRedStar.frame.origin.y, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    
    dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    isInternetOff = NO;
    
    [_buttonSearch setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    
    _buttonSearch.backgroundColor = [UIColor clearColor];
    
    _labelSearchVw.textColor=[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    self.viewBottomBar.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]
                                 initWithTitle:@"Cancel"
                                 style:UIBarButtonItemStylePlain
                                 target:self
                                 action:@selector(leftButtonAction)];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]
                                  initWithTitle:@"Select"
                                  style:UIBarButtonItemStylePlain
                                  target:self
                                  action:@selector(rightButtonAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
    self.navigationItem.leftBarButtonItem = leftItem;
    
    [leftItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                      [UIFont fontWithName:@"OpenSans-Bold" size:14], NSFontAttributeName,
                                      [UIColor whiteColor], NSForegroundColorAttributeName,
                                      nil]
                            forState:UIControlStateNormal];
    
    [rightItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                       [UIFont fontWithName:@"OpenSans-Bold" size:14], NSFontAttributeName,
                                       [UIColor whiteColor], NSForegroundColorAttributeName,
                                       nil]
                             forState:UIControlStateNormal];
    
    [self.navigationItem setHidesBackButton:TRUE];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:20], NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    CGRect rect = searchBarLocation.frame;
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, rect.size.height-2,rect.size.width, 2)];
    lineView.backgroundColor = [UIColor whiteColor];
    [searchBarLocation addSubview:lineView];
    
    for (UIView *subView in searchBarLocation.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews)
        {
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                searchBarTextField.textColor = [UIColor colorWithRed:0.0f/255.0f green:122.0f/255.0f blue:255.0f/255.0f alpha:1];
                break;
            }
        }
    }
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor lightGrayColor]];
    
    mutableArraySelectStates=[[NSMutableArray alloc] init];
    
    [[self tableViewFilter] reloadData];
    
    [self addNavTitles];
    NewCountryIdArr = [arrCountryIds mutableCopy];
    NewStateIdArr = [arrStateIds mutableCopy];
    
    [self RefreshStateData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    stringNotifiction = @"";
    
    [super viewWillAppear:animated];
    
    stringForNotifyEditCompany = @"";
    
    if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"] )
    {
        self.imageRedStar.hidden = YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self reachabilityCheck];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(redStarShow)
                                                 name:@"redStarShow"
                                               object:nil];
    
    [self SetHeader];
    [[NSUserDefaults standardUserDefaults] setObject:arrayData forKey:@"OriginalStateData"];
    NSArray *arrayCountryName = (NSArray*)[dummy loaddataCountries:@"Select * From COUNTRIES" strForscreen:@"countiesList"];
    [[NSUserDefaults standardUserDefaults]setObject:arrayCountryName forKey:@"OriginalCountryData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

-(void)RefreshStateData{
    NSArray *arrFinalS;
    NSArray *arrFinalC;
    if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"] || [stringCheckDisciplines isEqualToString:@"fromSubDisciplines"]){
        arrFinalS = [[NSUserDefaults standardUserDefaults] objectForKey:@"FinalStateDataDisp"];
        arrFinalC = [[NSUserDefaults standardUserDefaults] objectForKey:@"FinalCountryDataDisp"];
    }else{
        arrFinalS = [[NSUserDefaults standardUserDefaults] objectForKey:@"FinalStateDataSearch"];
        arrFinalC = [[NSUserDefaults standardUserDefaults] objectForKey:@"FinalCountryDataSearch"];
    }
    arrTempStateData = [[NSMutableArray alloc]initWithArray:arrFinalS];
    arrTempCountryData = [[NSMutableArray alloc]initWithArray:arrFinalC];
    [self SetHeader];
    [_tableViewFilter reloadData];
}

-(void)SetHeader{
    NSArray *arrOriginalS = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalStateData"];
    NSArray *arrOriginalC = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalCountryData"];
    if(arrOriginalS.count == arrTempStateData.count && arrOriginalC.count == arrTempCountryData.count){
        self.buttonSelectAllStates.selected = YES;
    }else{
        self.buttonSelectAllStates.selected = NO;
    }
    
    if((arrOriginalS.count == arrTempStateData.count && arrOriginalC.count == arrTempCountryData.count) || (arrTempStateData.count == 0 && arrTempCountryData.count == 0) || (arrOriginalS.count == 0 && arrOriginalC.count == 0)){
        labelSubTitle.text = @"All Locations";
    }else{
        NSString *strIds = @"";
        for (NSDictionary *dict in arrTempStateData) {
            if([dict objectForKey:@"abr"]){
                NSString *strName = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"abr"]] removeNull];
                if([strIds isEqualToString:@""]){
                    strIds = [NSString stringWithFormat:@"%@",strName];
                }else{
                    strIds = [NSString stringWithFormat:@"%@,%@",strIds,strName];
                }
            }
        }
        for (NSDictionary *dictc in arrTempCountryData) {
            if([dictc objectForKey:@"abr"]){
                NSString *strName = [[NSString stringWithFormat:@"%@",[dictc objectForKey:@"abr"]] removeNull];
                if([strIds isEqualToString:@""]){
                    strIds = [NSString stringWithFormat:@"%@",strName];
                }else{
                    strIds = [NSString stringWithFormat:@"%@,%@",strIds,strName];
                }
            }
        }
        
        labelSubTitle.text = strIds;
    }
}

-(void)setAllLocation{
    NSArray *arrOriginalS = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalStateData"];
    NSArray *arrOriginalC = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalCountryData"];
    if(arrOriginalS.count == arrTempStateData.count && arrOriginalC.count == arrTempCountryData.count){
        [arrTempStateData removeAllObjects];
        [arrTempCountryData removeAllObjects];
    }else{
        arrTempStateData = [[NSMutableArray alloc]initWithArray:arrOriginalS];
        arrTempCountryData = [[NSMutableArray alloc]initWithArray:arrOriginalC];
    }
    [self CheckAllLocation];
    
}

-(void)CheckAllLocation{
    NSArray *arrOriginalS = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalStateData"];
    NSArray *arrOriginalC = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalCountryData"];
    if(arrOriginalS.count == arrTempStateData.count && arrOriginalC.count == arrTempCountryData.count){
        isAllLocationSelected = YES;
    }else{
        isAllLocationSelected = NO;
    }
    [self SetHeader];
}

-(void)setFinalData{
    if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"] || [stringCheckDisciplines isEqualToString:@"fromSubDisciplines"]){
        [[NSUserDefaults standardUserDefaults] setObject:arrTempCountryData forKey:@"FinalCountryDataDisp"];
        [[NSUserDefaults standardUserDefaults] setObject:arrTempStateData forKey:@"FinalStateDataDisp"];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:arrTempCountryData forKey:@"FinalCountryDataSearch"];
        [[NSUserDefaults standardUserDefaults] setObject:arrTempStateData forKey:@"FinalStateDataSearch"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)sendDataToStateLoc:(NSMutableArray *)array{
    arrTempCountryData = array;
    [self SetHeader];
    [_tableViewFilter reloadData];
}

-(void)showSelectedInt:(FilterTableViewCell *)cell indexPath:(NSIndexPath *)indexPath
{
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"allLocation"]isEqualToString:@"allLocation"])
    {
        [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
    }
    if (searchEnableBool)
    {
        if ([[arrStateIds valueForKey:@"id"] containsObject:[[mutableArraySelectStates valueForKey:@"id"] objectAtIndex:indexPath.row]])
        {
            [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
        }
    }
    else
    {
        if ([[arrStateIds valueForKey:@"id"] containsObject:[[arrayData valueForKey:@"id"] objectAtIndex:indexPath.row]])
        {
            [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
        }
        else
            
        {
            [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
            
        }
    }
    if(isAllLocationSelected){
        [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
    }else{
        NSArray *arrStates = arrayData;
        if(searchEnableBool){
            arrStates = mutableArraySelectStates;
        }
        NSString *strId = [[NSString stringWithFormat:@"%@",[[arrStates objectAtIndex:indexPath.row] objectForKey:@"id"]] removeNull];
        NSArray *arrIds = [arrTempStateData valueForKey:@"id"];
        if ([arrIds containsObject:strId]) {
            [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
        }else{
            [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
        }
    }
}

-(void)setNavTitleText
{
    
    if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
    {
        if (isAllCountrySelected&&arrStateIds.count==arrayData.count)
        {
            isAllLocations = YES;
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocationDes"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }else{
            isAllLocations = NO;
            
            [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationDes"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"allLocationDes"]isEqualToString:@"allLocation"])
        {
            labelSubTitle.text = [NSString stringWithFormat:@"All Locations"];
            
            [_buttonSelectAllStates setSelected:YES];
        }
        
        else
        {
            [_buttonSelectAllStates setSelected:NO];
            
            if (arrCountryIds.count>0&&arrStateIds.count>0)
            {
                labelSubTitle.text = [NSString stringWithFormat:@"%@, %@",[[arrStateIds valueForKey:@"abr"] componentsJoinedByString:@", "],[[arrCountryIds valueForKey:@"abr"] componentsJoinedByString:@", "]];
            }
            else if (arrStateIds.count>0)
            {
                labelSubTitle.text = [[arrStateIds valueForKey:@"abr"] componentsJoinedByString:@", "];
            }
            
            else if (arrCountryIds.count>0)
                
            {
                labelSubTitle.text = [[arrCountryIds valueForKey:@"abr"] componentsJoinedByString:@", "];
            }
            
            else
            {
                labelSubTitle.text = @"";
            }
        }
    }
    
    else if ([stringCheckDisciplines isEqualToString: @"fromSubDisciplines"])
    {
        if (isAllCountrySelected&&arrStateIds.count==arrayData.count)
        {
            isAllLocations = YES;
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocationSBDes"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }else{
            isAllLocations = NO;
            
            [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationSBDes"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"allLocationSBDes"]isEqualToString:@"allLocation"])
        {
            labelSubTitle.text = [NSString stringWithFormat:@"All Locations"];
            
            [_buttonSelectAllStates setSelected:YES];
        }
        else
        {
            [_buttonSelectAllStates setSelected:NO];
            if(arrStateIds.count==[arrayData count]&&[arrCountryIds count]==[arrayCountryData count]){
                [[NSUserDefaults standardUserDefaults] setValue:@"allLocation" forKey:@"allLocation"];
                labelSubTitle.text = @"All Locations";
            }
            else if (arrCountryIds.count>0&&arrStateIds.count>0)
            {
                
                labelSubTitle.text = [NSString stringWithFormat:@"%@, %@",[[arrStateIds valueForKey:@"abr"] componentsJoinedByString:@", "],[[arrCountryIds valueForKey:@"abr"] componentsJoinedByString:@", "]];
                
            }
            
            else if (arrStateIds.count>0)
            {
                labelSubTitle.text = [[arrStateIds valueForKey:@"abr"] componentsJoinedByString:@", "];
            }
            else if (arrCountryIds.count>0)
            {
                labelSubTitle.text = [[arrCountryIds valueForKey:@"abr"] componentsJoinedByString:@", "];
            }
            else
            {
                labelSubTitle.text = @"";
            }
        }
    }
    
    else
    {
        if (isAllCountrySelectedSearch&&arrStateIds.count==arrayData.count)
        {
            isAllLocations = YES;
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocation"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }else{
            isAllLocations = NO;
            
            [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocation"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"allLocation"]isEqualToString:@"allLocation"])
        {
            labelSubTitle.text = [NSString stringWithFormat:@"All Locations"];
            
            [_buttonSelectAllStates setSelected:YES];
        }
        
        else
            
        {
            if(arrStateIds.count==arrayData.count && arrCountryIds.count==arrayCountryData.count){
                labelSubTitle.text = [NSString stringWithFormat:@"All Locations"];
                [_buttonSelectAllStates setSelected:YES];
            }else{
                [_buttonSelectAllStates setSelected:NO];
                if (arrCountryIds.count>0&&arrStateIds.count>0)
                {
                    labelSubTitle.text = [NSString stringWithFormat:@"%@, %@",[[arrStateIds valueForKey:@"abr"] componentsJoinedByString:@", "],[[arrCountryIds valueForKey:@"abr"] componentsJoinedByString:@", "]];
                }
                
                else if (arrStateIds.count>0)
                {
                    labelSubTitle.text = [[arrStateIds valueForKey:@"abr"] componentsJoinedByString:@", "];
                }
                
                else if (arrCountryIds.count>0)
                    
                {
                    labelSubTitle.text = [[arrCountryIds valueForKey:@"abr"] componentsJoinedByString:@", "];
                }
                
                else
                {
                    labelSubTitle.text = @"";
                }
            }
            
            
            
        }
        
    }
}

#pragma mark- AddNavTitles
-(void)addNavTitles
{
    UIView  *textView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    
    UILabel *labelTitleName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 0, CGRectGetWidth(textView.frame), 22)];
    labelTitleName.backgroundColor = [UIColor clearColor];
    labelTitleName.font = [UIFont fontWithName:@"OpenSans" size:18.0f];
    labelTitleName.textAlignment = NSTextAlignmentCenter;
    labelTitleName.textColor = [UIColor whiteColor];
    labelTitleName.text = @"Location Filter";
    
    /*----UILabel Sub-title Name-------*/
    labelSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 22, CGRectGetWidth(textView.frame), 18)];
    labelSubTitle.backgroundColor = [UIColor clearColor];
    labelSubTitle.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
    labelSubTitle.textAlignment = NSTextAlignmentCenter;
    labelSubTitle.textColor = [UIColor whiteColor];
    
    [textView addSubview:labelTitleName];
    [textView addSubview:labelSubTitle];
    self.navigationItem.titleView=textView;
}

#pragma mark- sendDataToPreviousView
-(void)sendDataBack:(NSArray *)array
{
    arrCountries = [[NSArray alloc] initWithArray:array];
}

#pragma mark- reachabilityCheck

-(void)reachabilityCheck
{
    if ([[[dummy loaddataStates:@"Select * From STATES"] copy] count]>0)
    {
        [self loadStatesOffline];
    }
    
    else if([Utilities CheckInternetConnection])
    {
        NSLog(@"connected");
        
        NSArray *arrAllStates = [[NSArray alloc] initWithArray:[dummy loaddataStates:@"Select * From STATES"]];
        
        if (arrAllStates.count>0)
        {
            [self loadStatesOffline];
        }
        else
        {
            [self getFilteredStates];
        }
    }
    else
    {
        [self loadStatesOffline];
        
    }
}

#pragma mark- loadStatesOffline

-(void)loadStatesOffline
{
   // isInternetOff = YES;
    
    NSArray *arrAllStates = [[NSArray alloc] initWithArray:[dummy loaddataStates:@"Select * From STATES"]];
    
    NSMutableArray *arrSortedStates = [[NSMutableArray alloc] init];
    
    NSLog(@"getcountrystates offline");
    
    for (int i=0; i<[arrAllStates count]; i++)
    {
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"countryId"] integerValue]==[[[arrAllStates valueForKey:@"country_id"] objectAtIndex:i] integerValue])
        {
            [arrSortedStates addObject:[arrAllStates objectAtIndex:i]];
        }
    }
    
    arrayData = [arrSortedStates copy];
    
    arrayData = [arrayData sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES]]];
    [[NSUserDefaults standardUserDefaults] setObject:arrayData forKey:@"OriginalStateData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self SetHeader];
    [_tableViewFilter reloadData];
    
}


#pragma mark- filterStateWebService0

-(void)getFilteredStates
{
    NSString *strCountyIds;
    
    strCountyIds = [[NSUserDefaults standardUserDefaults] objectForKey:@"countryId"];
    
    NSString* urlStr = [NSString stringWithFormat:@"%@getcountrystates",baseUrl];
    
    NSDictionary *dicParam = @{@"cid":strCountyIds};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         isInternetOff = NO;
         NSLog(@"getcountrystates after filtered");
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             arrayData = [[NSArray alloc] initWithArray:[responseObject objectForKey:@"data"]];
             NSLog(@"array data is %@", arrayData);
             arrayData = [arrayData sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES]]];
             [[NSUserDefaults standardUserDefaults] setObject:arrayData forKey:@"OriginalStateData"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             [self SetHeader];
             [_tableViewFilter reloadData];
             
             {
                 NSArray *arraySt = [[NSArray alloc] initWithArray:[responseObject objectForKey:@"data"]];
                 if ([dummy deleteTable:@"delete from STATES"])
                 {
                     for (int i=0; i<[arraySt count]; i++)
                     {
                         NSString *query;
                         query = [NSString stringWithFormat: @"INSERT INTO STATES (id,name,country_id,abr) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                                  ,[[arraySt valueForKey:@"id"] objectAtIndex:i]
                                  ,[[arraySt valueForKey:@"name"] objectAtIndex:i]
                                  ,[[arraySt valueForKey:@"country_id"] objectAtIndex:i]
                                  ,[[arraySt valueForKey:@"abr"] objectAtIndex:i]];
                         
                         const char *query_stmt = [query UTF8String];
                         [dummy  runQueries:query_stmt strForscreen:@"StatesList" ];
                     }
                 }
             }
         }
     }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self loadStatesOffline];
         
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             //[[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         
     }];
    
}

#pragma mark- Cancel Button Action

- (void)leftButtonAction
{
    isCancelClicked = YES;
    arrCountryIds = NewCountryIdArr;
    arrStateIds = NewStateIdArr;
    [[NSUserDefaults standardUserDefaults]setObject:arrCountryIds forKey:@"arryCountry"];
    [[NSUserDefaults standardUserDefaults]setObject:arrStateIds forKey:@"arryStates"];
    if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
    {
        stringCheckSubDesc = @"Desc";
        
        if (arrStateIds.count==0 && arrCountryIds.count==0)
        {
            stringFilterScreen = @"nothing";
            
            [arrayDisciplineStateId removeAllObjects];
            
            [arrayDisciplineCountryId removeAllObjects];
        }
    }
    
    else if (  [stringCheckDisciplines isEqualToString: @"fromSubDisciplines"])
    {
        stringCheckSubDesc = @"subDesc";
        
        if (arrStateIds.count==0 && arrCountryIds.count==0)
        {
            stringFilterScreen = @"nothing";
            
            [arraySubDisciStateId removeAllObjects];
            [arraySubDisciCountryId removeAllObjects];
        }
    }
    
    else
    {
        
        if (arrStateIds.count==0 && arrCountryIds.count==0)
        {
            stringFilterScreen = @"nothing";
        }
        
        else
        {
            stringFilterScreen = @"filterScreen";
        }
        
    }
    
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark- Done Button Action

-(void)localllyFilterMethod
{
    NSMutableArray *arrayComp = [[NSMutableArray alloc]init];
    
    arrayComp = [[[NSUserDefaults standardUserDefaults]objectForKey:@"allCompanies"]mutableCopy];
    
    if ([stringAddRemove isEqualToString: @"remove"])
    {
        stringAddRemove = @"";
        
        [arrayFilterContry removeAllObjects];
        
        BOOL isAllcountry = isAllCountrySelected;
        if (!([stringCheckDisciplines isEqualToString:@"fromDisciplines"] || [stringCheckDisciplines isEqualToString: @"fromSubDisciplines"]))
        {
            isAllcountry = isAllCountrySelectedSearch;
        }
        if (isAllcountry&&arrStateIds.count==arrayData.count)
        {
            isAllLocations = YES;
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocation"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        else
        {
            if (arrStateIds.count>0)
            {
                NSMutableArray *arrayRegState =[NSMutableArray new];
                
                for (int i=0; i<arrayComp.count; i++)
                {
                    NSDictionary *dicCompanyDetail = arrayComp [i];
                    
                    if ([[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"<null>"]||[[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@""]||[[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"(null)"])
                    {
                        
                    }
                    
                    else
                    {
                        arrayRegState = [[[dicCompanyDetail valueForKey:@"regstates"] componentsSeparatedByString:@","] mutableCopy];
                        
                        for (int c=0; c<arrStateIds.count; c++)
                        {
                            if ([arrayRegState containsObject:[arrStateIds valueForKey:@"name" ][c]])
                            {
                                if ([[arrayFilterContry valueForKey:@"id"] containsObject:[arrayComp valueForKey:@"id"][i]])
                                {
                                    
                                }
                                else
                                {
                                    [arrayFilterContry addObject:arrayComp[i]];
                                }
                            }
                        }
                    }
                }
                
                NSLog(@"arrayFilterContry is %@", arrayFilterContry);
            }
            if (arrCountryIds.count>0)
            {
                NSMutableArray *arrayRegState =[NSMutableArray new];
                
                for (int i=0; i<arrayComp.count; i++)
                {
                    NSDictionary *dicCompanyDetail = arrayComp [i];
                    
                    if ([[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@"<null>"]||[[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@""]||[[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@"(null)"])
                    {
                        
                    }
                    
                    else
                    {
                        arrayRegState = [[[dicCompanyDetail valueForKey:@"internationl"] componentsSeparatedByString:@","] mutableCopy];
                        
                        NSMutableArray *arrayTemp = [NSMutableArray new];
                        
                        for (int d=0; d<arrayRegState.count; d++)
                        {
                            NSString *string = [arrayRegState objectAtIndex:d];
                            
                            NSArray *array = [string componentsSeparatedByString:@" ("];
                            
                            [arrayTemp addObject:array[0]];
                        }
                        
                        [arrayRegState removeAllObjects];
                        
                        arrayRegState = [NSMutableArray new];
                        
                        arrayRegState = [arrayTemp mutableCopy];
                        
                        for (int c=0; c<arrCountryIds.count; c++)
                        {
                            if ([arrayTemp containsObject:[arrCountryIds[c] objectForKey:@"name"]])
                            {
                                if ([[arrayFilterContry valueForKey:@"id"] containsObject:[arrayComp valueForKey:@"id"][i]])
                                {
                                    
                                }
                                
                                else
                                {
                                    [arrayFilterContry addObject:arrayComp[i]];
                                }
                            }
                        }
                    }
                }
                
                NSLog(@"arrayFilterContry is %@", arrayFilterContry);
            }
            
            [[NSUserDefaults standardUserDefaults]setObject:arrayFilterContry forKey:@"filterConties"];
        }
    }
    
    else
    {
        BOOL isAllcountry = isAllCountrySelected;
        if (!([stringCheckDisciplines isEqualToString:@"fromDisciplines"] || [stringCheckDisciplines isEqualToString: @"fromSubDisciplines"]))
        {
            isAllcountry = isAllCountrySelectedSearch;
        }
        if (isAllcountry&&arrStateIds.count==arrayData.count)
        {
            isAllLocations = YES;
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocation"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        else
        {
            
            NSMutableArray *arrayRegState =[NSMutableArray new];
            
            for (int i=0; i<arrayComp.count; i++)
            {
                NSDictionary *dicCompanyDetail = arrayComp [i];
                
                if ([[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"<null>"]||[[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@""]||[[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"(null)"])
                {
                    
                }
                
                else
                {
                    arrayRegState = [[[dicCompanyDetail valueForKey:@"regstates"] componentsSeparatedByString:@","] mutableCopy];
                    
                    for (int c=0; c<arrStateIds.count; c++)
                    {
                        if ([arrayRegState containsObject:[arrStateIds valueForKey:@"name" ][c]])
                        {
                            if ([[arrayFilterContry valueForKey:@"id"] containsObject:[arrayComp valueForKey:@"id"][i]])
                            {
                                
                            }
                            else
                            {
                                [arrayFilterContry addObject:arrayComp[i]];
                            }
                        }
                    }
                }
            }
            
            NSLog(@"arrayFilterContry is %@", arrayFilterContry);
            
            [[NSUserDefaults standardUserDefaults]setObject:arrayFilterContry forKey:@"filterConties"];
            
        }
        
    }
    
    if (arrStateIds.count==0 && arrCountryIds.count==0)
    {
        stringFilterScreen = @"nothing";
    }
    else
    {
        stringFilterScreen = @"filterScreen";
    }
}


-(void)localllyFilterMethodDiscipline
{
    NSMutableArray *arrayComp = [[NSMutableArray alloc]init];
    
    arrayComp = [[[NSUserDefaults standardUserDefaults]objectForKey:@"allCompanies"]mutableCopy];
    
    if ([stringAddRemove isEqualToString: @"remove"])
    {
        stringAddRemove = @"";
        
        [arrayFilterContry removeAllObjects];
        
        BOOL isAllcountry = isAllCountrySelected;
        if (!([stringCheckDisciplines isEqualToString:@"fromDisciplines"] || [stringCheckDisciplines isEqualToString: @"fromSubDisciplines"]))
        {
            isAllcountry = isAllCountrySelectedSearch;
        }
        if (isAllcountry&&arrStateIds.count==arrayData.count)
        {
            isAllLocations = YES;
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocation"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        else
        {
            if (arrStateIds.count>0)
            {
                NSMutableArray *arrayRegState =[NSMutableArray new];
                
                for (int i=0; i<arrayComp.count; i++)
                {
                    NSDictionary *dicCompanyDetail = arrayComp [i];
                    
                    if ([[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"<null>"]||[[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@""]||[[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"(null)"])
                    {
                        
                    }
                    
                    else
                    {
                        arrayRegState = [[[dicCompanyDetail valueForKey:@"regstates"] componentsSeparatedByString:@","] mutableCopy];
                        
                        for (int c=0; c<arrStateIds.count; c++)
                        {
                            if ([arrayRegState containsObject:[arrStateIds valueForKey:@"name" ][c]])
                            {
                                if ([[arrayFilterContry valueForKey:@"id"] containsObject:[arrayComp valueForKey:@"id"][i]])
                                {
                                    
                                }
                                else
                                {
                                    [arrayFilterContry addObject:arrayComp[i]];
                                }
                            }
                        }
                    }
                }
                
                NSLog(@"arrayFilterContry is %@", arrayFilterContry);
            }
            if (arrCountryIds.count>0)
            {
                NSMutableArray *arrayRegState =[NSMutableArray new];
                
                for (int i=0; i<arrayComp.count; i++)
                {
                    NSDictionary *dicCompanyDetail = arrayComp [i];
                    
                    if ([[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@"<null>"]||[[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@""]||[[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@"(null)"])
                    {
                        
                    }
                    
                    else
                    {
                        arrayRegState = [[[dicCompanyDetail valueForKey:@"internationl"] componentsSeparatedByString:@","] mutableCopy];
                        
                        NSMutableArray *arrayTemp = [NSMutableArray new];
                        
                        for (int d=0; d<arrayRegState.count; d++)
                        {
                            NSString *string = [arrayRegState objectAtIndex:d];
                            
                            NSArray *array = [string componentsSeparatedByString:@" ("];
                            
                            [arrayTemp addObject:array[0]];
                        }
                        
                        [arrayRegState removeAllObjects];
                        
                        arrayRegState = [NSMutableArray new];
                        
                        arrayRegState = [arrayTemp mutableCopy];
                        
                        for (int c=0; c<arrCountryIds.count; c++)
                        {
                            if ([arrayTemp containsObject:[arrCountryIds[c] objectForKey:@"name"]])
                            {
                                if ([[arrayFilterContry valueForKey:@"id"] containsObject:[arrayComp valueForKey:@"id"][i]])
                                {
                                    
                                }
                                
                                else
                                {
                                    [arrayFilterContry addObject:arrayComp[i]];
                                }
                            }
                        }
                    }
                }
                
                NSLog(@"arrayFilterContry is %@", arrayFilterContry);
            }
            
            [[NSUserDefaults standardUserDefaults]setObject:arrayFilterContry forKey:@"filterContiesDiscipline"];
        }
    }
    
    else
    {
        BOOL isAllcountry = isAllCountrySelected;
        if (!([stringCheckDisciplines isEqualToString:@"fromDisciplines"] || [stringCheckDisciplines isEqualToString: @"fromSubDisciplines"]))
        {
            isAllcountry = isAllCountrySelectedSearch;
        }
        if (isAllcountry&&arrStateIds.count==arrayData.count)
        {
            isAllLocations = YES;
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocation"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        else
        {
            
            NSMutableArray *arrayRegState =[NSMutableArray new];
            
            for (int i=0; i<arrayComp.count; i++)
            {
                NSDictionary *dicCompanyDetail = arrayComp [i];
                
                if ([[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"<null>"]||[[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@""]||[[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"(null)"])
                {
                    
                }
                
                else
                {
                    arrayRegState = [[[dicCompanyDetail valueForKey:@"regstates"] componentsSeparatedByString:@","] mutableCopy];
                    
                    for (int c=0; c<arrStateIds.count; c++)
                    {
                        if ([arrayRegState containsObject:[arrStateIds valueForKey:@"name" ][c]])
                        {
                            if ([[arrayFilterContry valueForKey:@"id"] containsObject:[arrayComp valueForKey:@"id"][i]])
                            {
                                
                            }
                            else
                            {
                                [arrayFilterContry addObject:arrayComp[i]];
                            }
                        }
                    }
                }
            }
            
            NSLog(@"arrayFilterContry is %@", arrayFilterContry);
            
            [[NSUserDefaults standardUserDefaults]setObject:arrayFilterContry forKey:@"filterContiesDiscipline"];
            
        }
    }
    
    if (arrStateIds.count==0 && arrCountryIds.count==0)
    {
        stringFilterScreen = @"nothing";
    }
    else
    {
        stringFilterScreen = @"filterScreen";
    }
}


-(void)filterStateCompany
{
    NSMutableArray *arrayStateFilterCoun = [NSMutableArray new];
    
    for (int i=0; i<arrayCompaniesList.count; i++)
    {
        NSDictionary *dicCompanyDetail = arrayCompaniesList [i];
        
        if ([[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"<null>"]||[[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@""]||[[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"(null)"])
        {
            
        }
        else
        {
            NSArray *arrayRegState = [[[dicCompanyDetail valueForKey:@"regstates"] componentsSeparatedByString:@","] mutableCopy];
            
            for (int c=0; c<arrayDisciplineStateId.count; c++)
            {
                if ([arrayRegState containsObject:[arrayDisciplineStateId valueForKey:@"name" ][c]])
                {
                    if ([arrayStateFilterCoun containsObject:arrayCompaniesList [i]])
                    {
                        
                    }
                    else
                    {
                        [arrayStateFilterCoun addObject:arrayCompaniesList[i]];
                    }
                }
            }
        }
    }
    
    NSLog(@"arrayStateFilterCoun is %@",arrayStateFilterCoun);
    
    arrayFilterState = [arrayStateFilterCoun mutableCopy];
    
    [[NSUserDefaults standardUserDefaults]setObject:arrayFilterState forKey:@"filter"];
}

-(void)resetLocalValues
{
    NSMutableArray *arrayTemp = [[NSMutableArray alloc]init];
    
    arrayTemp = [[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountrySubDiscipline"];
    
    if (arrayTemp.count==0)
    {
        [[NSUserDefaults standardUserDefaults]setObject:arraySubDisciCountryId forKey:@"arryCountrySubDiscipline"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arryCountrySubDiscipline"];
        
        [[NSUserDefaults standardUserDefaults]setObject:arraySubDisciCountryId forKey:@"arryCountrySubDiscipline"];
    }
    
    NSLog(@"user default is %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountrySubDiscipline"]);
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    arraySubDisciCountryId = [[NSMutableArray alloc]init];
    
    arraySubDisciCountryId = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountrySubDiscipline"]];
    
    NSMutableArray *arrayTemp1 = [[NSMutableArray alloc]init];
    
    arrayTemp1 = [[NSUserDefaults standardUserDefaults]objectForKey:@"arryStatesDiscipline"];
    
    if (arrayTemp1.count==0)
    {
        [[NSUserDefaults standardUserDefaults]setObject:arraySubDisciStateId forKey:@"arryStatesSubDiscipline"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arryStatesSubDiscipline"];
        
        [[NSUserDefaults standardUserDefaults]setObject:arraySubDisciStateId forKey:@"arryStatesSubDiscipline"];
    }
    
    NSLog(@"user default is %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"arryStatesSubDiscipline"]);
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    arraySubDisciStateId = [[NSMutableArray alloc]init];
    
    arraySubDisciStateId = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryStatesSubDiscipline"]];
    
    NSMutableArray *arrayTemp3 = [[NSMutableArray alloc]init];
    
    arrayTemp3 = [[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountryDiscipline"];
    
    if (arrayTemp3.count==0)
    {
        [[NSUserDefaults standardUserDefaults]setObject:arrayDisciplineCountryId forKey:@"arryCountryDiscipline"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arryCountryDiscipline"];
        
        [[NSUserDefaults standardUserDefaults]setObject:arrayDisciplineCountryId forKey:@"arryCountryDiscipline"];
        
    }
    
    NSLog(@"user default is %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountryDiscipline"]);
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    arrayDisciplineCountryId = [[NSMutableArray alloc]init];
    
    arrayDisciplineCountryId = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountryDiscipline"]];
    
    NSMutableArray *arrayTemp2 = [[NSMutableArray alloc]init];
    
    arrayTemp2 = [[NSUserDefaults standardUserDefaults]objectForKey:@"arryStatesDiscipline"];
    
    if (arrayTemp2.count==0)
    {
        [[NSUserDefaults standardUserDefaults]setObject:arrayDisciplineStateId forKey:@"arryStatesDiscipline"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arryStatesDiscipline"];
        
        [[NSUserDefaults standardUserDefaults]setObject:arrayDisciplineStateId forKey:@"arryStatesDiscipline"];
    }
    
    NSLog(@"user default is %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"arryStatesDiscipline"]);
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    arrayDisciplineStateId = [[NSMutableArray alloc]init];
    
    arrayDisciplineStateId = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryStatesDiscipline"]];
}


- (void)rightButtonAction
{
    
    if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
    {
        stringCheckSubDesc = @"Desc";
        
        BOOL isAllcountry = isAllCountrySelected;
        if (!([stringCheckDisciplines isEqualToString:@"fromDisciplines"] || [stringCheckDisciplines isEqualToString: @"fromSubDisciplines"]))
        {
            isAllcountry = isAllCountrySelectedSearch;
        }
        if (isAllcountry&&arrStateIds.count==arrayData.count)
        {
            isAllLocations = YES;
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocationDes"];
            
            [arrayDisciplineStateId removeAllObjects];
            
            arrayDisciplineStateId = [arrStateIds mutableCopy];
            
            [arrayDisciplineCountryId removeAllObjects];
            
            arrayDisciplineCountryId = [arrCountryIds mutableCopy];
            
            [arraySubDisciStateId removeAllObjects];
            
            arraySubDisciStateId = [arrStateIds mutableCopy];
            
            [arraySubDisciCountryId removeAllObjects];
            
            arraySubDisciCountryId = [arrCountryIds mutableCopy];
            
        }
        
        else  if (arrStateIds.count==0 && arrCountryIds.count==0)
        {
            stringFilterScreen = @"nothing";
            
            [arrayDisciplineStateId removeAllObjects];
            
            [arrayDisciplineCountryId removeAllObjects];
        }
        
        else
        {
            [arrayDisciplineStateId removeAllObjects];
            
            arrayDisciplineStateId = [arrStateIds mutableCopy];
            
            [arrayDisciplineCountryId removeAllObjects];
            
            arrayDisciplineCountryId = [arrCountryIds mutableCopy];
            
            [arraySubDisciStateId removeAllObjects];
            
            arraySubDisciStateId = [arrStateIds mutableCopy];
            
            [arraySubDisciCountryId removeAllObjects];
            
            arraySubDisciCountryId = [arrCountryIds mutableCopy];
            
            stringFilterScreen = @"filterScreen";
            
            [arrCountryIds removeAllObjects];
            
            [arrStateIds removeAllObjects];
        }
        
        [self resetLocalValues];
    }
    
    else if ([stringCheckDisciplines isEqualToString: @"fromSubDisciplines"])
    {
        stringCheckSubDesc = @"subDesc";
        
        BOOL isAllcountry = isAllCountrySelected;
        if (!([stringCheckDisciplines isEqualToString:@"fromDisciplines"] || [stringCheckDisciplines isEqualToString: @"fromSubDisciplines"]))
        {
            isAllcountry = isAllCountrySelectedSearch;
        }
        if (isAllcountry&&arrStateIds.count==arrayData.count)
        {
            isAllLocations = YES;
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocationSBDes"];
            
            [arraySubDisciStateId removeAllObjects];
            
            arraySubDisciStateId = [arrStateIds mutableCopy];
            
            [arraySubDisciCountryId removeAllObjects];
            
            arraySubDisciCountryId = [arrCountryIds mutableCopy];
            
            [arrayDisciplineStateId removeAllObjects];
            
            arrayDisciplineStateId = [arrStateIds mutableCopy];
            
            [arrayDisciplineCountryId removeAllObjects];
            
            arrayDisciplineCountryId = [arrCountryIds mutableCopy];
            
        }
        
        else  if (arrStateIds.count==0 && arrCountryIds.count==0)
        {
            stringFilterScreen = @"nothing";
            
            stringCheckSubDesc = @"Desc";
            
            [arraySubDisciStateId removeAllObjects];
            
            [arraySubDisciCountryId removeAllObjects];
        }
        
        else
        {
            [arraySubDisciStateId removeAllObjects];
            
            arraySubDisciStateId = [arrStateIds mutableCopy];
            
            [arraySubDisciCountryId removeAllObjects];
            
            arraySubDisciCountryId = [arrCountryIds mutableCopy];
            
            [arrayDisciplineStateId removeAllObjects];
            
            arrayDisciplineStateId = [arrStateIds mutableCopy];
            
            [arrayDisciplineCountryId removeAllObjects];
            
            arrayDisciplineCountryId = [arrCountryIds mutableCopy];
            
            stringFilterScreen = @"filterScreen";
        }
        
        
        [arrCountryIds removeAllObjects];
        
        [arrStateIds removeAllObjects];
        
        [self resetLocalValues];
    }
    else
    {
        [self localllyFilterMethod];
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:arrCountryIds forKey:@"arryCountry"];
    [[NSUserDefaults standardUserDefaults]setObject:arrStateIds forKey:@"arryStates"];
    [self setFinalData];
    [self.navigationController popViewControllerAnimated:NO];
    
}


-(void)filterLocallyDiscipline
{
    
    NSMutableArray *arrayResponseData = [NSMutableArray new];
    
    arrayResponseData = [[[NSUserDefaults standardUserDefaults] objectForKey:@"allDiscipline"] mutableCopy];
    
    NSMutableArray *arrayFilter = [NSMutableArray new];
    
    NSMutableArray *arrayFinalfilter = [[NSMutableArray alloc]init];
    
    NSMutableArray *arrayTemp = [[NSMutableArray alloc]init];
    
    NSArray *arrayCom;
    
    NSMutableArray *arrayAllDiscipline = [NSMutableArray new];
    
    arrayAllDiscipline = [arrayResponseData mutableCopy];
    
    arrayTemp = [arrayAllDiscipline mutableCopy];
    
    for (int i=0; i<arrayAllDiscipline.count; i++)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        
        dict = [arrayAllDiscipline[i] mutableCopy];
        
        NSMutableArray *array = [NSMutableArray new];
        
        array = [[dict valueForKey:@"subdiscipline"] mutableCopy];
        
        NSMutableArray *arraySelected;
        
        NSMutableArray *arraySelctedDiscipline;
        
        arrayFinalfilter = [NSMutableArray new];
        
        for (int c=0; c<array.count; c++)
        {
            arraySelected = [NSMutableArray new];
            
            arraySelctedDiscipline = [NSMutableArray new];
            
            arrayCom = [[array[c] valueForKey:[array[c] valueForKey:@"name"]] valueForKey:@"allcompanys"];
            
            NSLog(@"array com is %@", arrayCom);
            
            if (arrayCom.count>0)
            {
                NSMutableArray *arrayComp = [[NSMutableArray alloc]init];
                
                arrayComp = [[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]mutableCopy];
                
                for (int d=0; d<arrayComp.count; d++)
                {
                    if ([[arrayCom valueForKey:@"company_id"]containsObject:[arrayComp[d] valueForKey:@"id"]])
                    {
                        if ([arraySelected containsObject:arrayComp[d]])
                        {
                            
                        }
                        else
                        {
                            [arraySelected addObject:arrayComp[d]];
                            
                            int index = (int) [[arrayCom valueForKey:@"company_id"]indexOfObject:[arrayComp[d] valueForKey:@"id"]];
                            
                            [arraySelctedDiscipline addObject:arrayCom[index]];
                        }
                    }
                }
            }
            
            NSLog(@"array arraySelctedDiscipline %@", [arraySelctedDiscipline valueForKey:@"company_id"]);
            
            NSLog(@"array arraySelected %@", [arraySelected valueForKey:@"id"]);
            
            {
                NSMutableArray *arrayRegState =[NSMutableArray new];
                
                arrayFilterContry = [[NSMutableArray alloc]init];
                
                for (int i=0; i<arraySelected.count; i++)
                {
                    NSDictionary *dicCompanyDetail = arraySelected [i];
                    
                    if (arrayDisciplineStateId.count>0)
                    {
                        if ([[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"<null>"]||[[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@""]||[[dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"(null)"])
                        {
                            
                        }
                        else
                        {
                            arrayRegState = [[[dicCompanyDetail valueForKey:@"regstates"] componentsSeparatedByString:@","] mutableCopy];
                            
                            for (int c=0; c<arrayDisciplineStateId.count; c++)
                            {
                                if ([arrayRegState containsObject:[arrayDisciplineStateId valueForKey:@"name" ][c]])
                                {
                                    if ([[arrayFilter valueForKey:@"id"] containsObject:[arraySelected valueForKey:@"id"][i]])
                                    {
                                        
                                    }
                                    else
                                    {
                                        [arrayFilter addObject:arraySelctedDiscipline[i]];
                                    }
                                }
                            }
                        }
                    }
                    if (arrayDisciplineCountryId.count>0)
                        
                    {
                        if ([[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@"<null>"]||[[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@""]||[[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@"(null)"])
                        {
                            
                        }
                        
                        else
                        {
                            arrayRegState = [[[dicCompanyDetail valueForKey:@"internationl"] componentsSeparatedByString:@","] mutableCopy];
                            
                            NSMutableArray *arrayTemp = [NSMutableArray new];
                            
                            for (int d=0; d<arrayRegState.count; d++)
                            {
                                NSString *string = [arrayRegState objectAtIndex:d];
                                
                                NSArray *array = [string componentsSeparatedByString:@" ("];
                                
                                [arrayTemp addObject:array[0]];
                            }
                            
                            [arrayRegState removeAllObjects];
                            
                            arrayRegState = [NSMutableArray new];
                            
                            arrayRegState = [arrayTemp mutableCopy];
                            
                            for (int c=0; c<arrayDisciplineStateId.count; c++)
                            {
                                if ([arrayRegState containsObject:[arrayDisciplineCountryId valueForKey:@"name" ][c]])
                                {
                                    if ([[arrayFilter valueForKey:@"id"] containsObject:[arraySelected valueForKey:@"id"][i]])
                                    {
                                        
                                    }
                                    else
                                    {
                                        [arrayFilter addObject:arraySelctedDiscipline[i]];
                                    }
                                }
                            }
                        }
                    }
                }
                NSLog(@"arrayFilterContry is %@", arrayFilter);
                
                arrayCom = nil;
                
                arrayCom = [arrayFilter copy];
                
                NSMutableDictionary *dictTemp = [NSMutableDictionary new];
                
                dictTemp = [array[c] mutableCopy ];
                
                NSLog(@"value is d=f %@",[dictTemp valueForKey:@"name"] );
                
                [dictTemp removeObjectForKey:[dictTemp valueForKey:@"name"] ] ;
                
                
                NSDictionary *dictVal = @{@"allcompanys":arrayCom,@"companys":[NSString stringWithFormat:@"%lu",  (unsigned long)arrayCom.count]};
                
                [dictTemp setObject:dictVal forKey:[dictTemp valueForKey:@"name"]];
                
                NSLog(@"dict is %@",dictTemp);
                
                [array replaceObjectAtIndex:c withObject:dictTemp];
                
                [arrayFinalfilter addObject:array[c]];
            }
            
            [dict removeObjectForKey:@"subdiscipline"];
            [dict setObject:arrayFinalfilter forKey:@"subdiscipline"];
            
            NSLog(@"arry final %@", arrayFinalfilter);
            
            [arrayAllDiscipline replaceObjectAtIndex:i withObject:dict];
        }
    }
    
    NSLog(@"filter arrayAllDiscipline is %@",arrayAllDiscipline);
    
    [[NSUserDefaults standardUserDefaults]setObject:arrayAllDiscipline forKey:@"localFilterData"];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- UISearchBarDelegate
- (void)searchBar:(UISearchBar *)searchBarC textDidChange:(NSString *)searchText
{
    [mutableArraySelectStates removeAllObjects];
    
    if(searchBarC.text.length<1)
    {
        searchEnableBool=NO;
    }
    
    else
    {
        searchEnableBool=YES;
        
        for (NSString *sc in [arrayData valueForKey:@"name"])
        {
            NSRange  scrange=[sc rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (scrange.location != NSNotFound)
                
            {
                for ( int i=0; i<[arrayData count]; i++)
                {
                    if([[[arrayData valueForKey:@"name"]objectAtIndex:i] isEqual:sc])
                        
                    {
                        [mutableArraySelectStates addObject:[arrayData objectAtIndex:i]];
                    }
                }
            }
        }
    }
    
    [_tableViewFilter reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBarController
{
    [searchBarController resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar endEditing:YES];
    [searchBar resignFirstResponder];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    searchBarLocation.showsCancelButton = YES;
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    searchBarLocation.showsCancelButton = NO;
    return YES;
}

#pragma mark- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (searchEnableBool)
    {
        return [mutableArraySelectStates count];
    }
    else
    {
        return [arrayData count]+1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FilterCell";
    
    FilterTableViewCell *cell = (FilterTableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[FilterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:CellIdentifier];
    }
    
    if (searchEnableBool)
    {
        if (indexPath.row ==[mutableArraySelectStates count]+1)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.buttonCheckBoxOutlet.hidden = NO;
            cell.labelRegionName.text = @"International";
            
            
            
            
            if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"allLocation"]isEqualToString:@"allLocation"])
            {
                [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
            }
            else
            {
                if ([[arrStateIds valueForKey:@"id"] containsObject:[[arrayData valueForKey:@"id"] objectAtIndex:indexPath.row]])
                {
                    [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
                }
                else
                {
                    [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
                }
            }
            
            if(isAllLocationSelected){
                [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
            }else{
                NSString *strId = [[NSString stringWithFormat:@"%@",[[mutableArraySelectStates objectAtIndex:indexPath.row] objectForKey:@"id"]] removeNull];
                NSArray *arrIds = [arrTempStateData valueForKey:@"id"];
                if ([arrIds containsObject:strId]) {
                    [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
                }else{
                    [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
                }
            }
            
        }
        else
        {
            cell.buttonCheckBoxOutlet.hidden = NO;
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            cell.labelRegionName.text = [[mutableArraySelectStates valueForKey:@"name" ] objectAtIndex:indexPath.row];
            
            [self showSelectedInt:cell indexPath:indexPath];
        }
    }
    else
    {
        if (indexPath.row == [arrayData count])
        {
            cell.labelRegionName.text = @"International";
            cell.buttonCheckBoxOutlet.hidden = NO;
            
            if (isAllLocations== YES)
            {
                [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
            }
            else
            {
                if (arrCountryIds.count>0)
                {
                    [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
                }
                else
                {
                    [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
                }
            }
            if(isAllLocationSelected){
                [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
            }else{
                if (arrTempCountryData.count>0)
                {
                    [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
                }
                else
                {
                    [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
                }
            }
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.buttonCheckBoxOutlet.hidden = NO;
            cell.labelRegionName.text=[[arrayData valueForKey:@"name"] objectAtIndex:indexPath.row];
            
            [self showSelectedInt:cell indexPath:indexPath];
        }
    }
    
    cell.labelRegionName.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    cell.labelRegionName.font = [UIFont fontWithName:@"OpenSans" size:15.0f];
    
    self.tableViewFilter.contentInset = UIEdgeInsetsMake(-1.0f, -1.0f, 0.0f, 0.0);
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == [arrayData count])
    {
        SelectCountryViewController *selectCountry = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectCountryView"];
        selectCountry.delegate = self;
        selectCountry.arrTempCountryData = arrTempCountryData;
        [self.navigationController pushViewController:selectCountry animated:NO];
    }
    
    else
    {
        
        if (searchEnableBool)
        {
            if ([arrStateIds containsObject:[mutableArraySelectStates objectAtIndex:indexPath.row]])
            {
                isAllLocations = NO;
                
                stringAddRemove = @"remove";
                
                [arrStateIds removeObject:[mutableArraySelectStates objectAtIndex:indexPath.row]];
                
                if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
                {
                    [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationDes"];
                }
                
                else if ([stringCheckDisciplines isEqualToString: @"fromSubDisciplines"])
                {
                    [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationSBDes"];
                }
                
                else
                {
                    [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocation"];
                }
                
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            else
            {
                
                [arrStateIds addObject:[mutableArraySelectStates objectAtIndex:indexPath.row]];
            }
        }
        else
        {
            if ([arrStateIds count]>0)
            {
                if ([[arrStateIds valueForKey:@"id"] containsObject:[[arrayData valueForKey:@"id"] objectAtIndex:indexPath.row]])
                {
                    isAllLocations = NO;
                    
                    stringAddRemove = @"remove";
                    
                    [arrStateIds removeObjectAtIndex:[[arrStateIds valueForKey:@"id"] indexOfObject:[[arrayData valueForKeyPath:@"id"] objectAtIndex:indexPath.row]]];
                    
                    if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
                    {
                        [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationSBDes"];
                        
                        [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationDes"];
                    }
                    
                    else if ([stringCheckDisciplines isEqualToString: @"fromSubDisciplines"])
                    {
                        [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationSBDes"];
                        
                        [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationDes"];
                    }
                    
                    else
                    {
                        [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocation"];
                    }
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    if (arrStateIds.count==0)
                        
                    {
                        stringSelectedUnselected = @"AllDataGet";
                    }
                }
                else
                {
                    [arrStateIds addObject:[arrayData  objectAtIndex:indexPath.row]];
                }
            }
            else
            {
                [arrStateIds addObject:[arrayData  objectAtIndex:indexPath.row]];
            }
        }
        
        if (arrCountryIds.count>0)
        {
            NSString *string = [NSString stringWithFormat:@"%@, %@",[[arrStateIds valueForKey:@"abr"] componentsJoinedByString:@", "],[[arrCountryIds valueForKey:@"abr"] componentsJoinedByString:@", "]];
            
            char charF = [string characterAtIndex:0 ];
            
            NSString *new=[NSString stringWithFormat:@"%c",charF];
            
            NSString *charStr=[NSString stringWithFormat:@"%@",new];
            
            if ([charStr isEqualToString:@","])
            {
                string = [string substringFromIndex:1];
                
                NSLog(@"string is %@", string);
            }
            
            labelSubTitle.text = string;
        }
        else
        {
            labelSubTitle.text = [[arrStateIds valueForKey:@"abr"] componentsJoinedByString:@", "];
        }
        
        if ([_buttonSelectAllStates isSelected])
        {
            if (arrStateIds.count<arrayData.count)
            {
                [_buttonSelectAllStates setSelected:NO];
            }
        }
    }
    
    
    
    BOOL isAllcountry = isAllCountrySelected;
    if (!([stringCheckDisciplines isEqualToString:@"fromDisciplines"] || [stringCheckDisciplines isEqualToString: @"fromSubDisciplines"]))
    {
        isAllcountry = isAllCountrySelectedSearch;
    }
    if (isAllcountry&&arrStateIds.count==arrayData.count)
    {
        isAllLocations = YES;
        
        if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
        {
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocationDes"];
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocationSBDes"];
        }
        else if ([stringCheckDisciplines isEqualToString: @"fromSubDisciplines"])
        {
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocationSBDes"];
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocationDes"];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocation"];
        }        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    else
    {
        isAllLocations = NO;
        
        if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
        {
            [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationDes"];
        }
        else if ([stringCheckDisciplines isEqualToString: @"fromSubDisciplines"])
        {
            [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationSBDes"];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocation"];
        }
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"AllLocation"]==YES || isAllLocations)
    {
        labelSubTitle.text = [NSString stringWithFormat:@"All Locations"];
        
        [_buttonSelectAllStates setSelected:YES];
    }
    
    if (indexPath.row != [arrayData count]) {
        NSArray *arrStates = arrayData;
        if(searchEnableBool){
            arrStates = mutableArraySelectStates;
        }
        
        BOOL isStateFound=NO;
        NSInteger stateIndex=0;
        NSDictionary *dictSel=[arrStates objectAtIndex:indexPath.row];

        for (int i=0; i<arrTempStateData.count; i++) {
            NSDictionary *dictS=[arrTempStateData objectAtIndex:i];
            
            if ([[NSString stringWithFormat:@"%@",[dictS objectForKey:@"id"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictSel objectForKey:@"id"]]])
            {
                if ([[NSString stringWithFormat:@"%@",[dictS objectForKey:@"abr"]] isEqualToString:[NSString stringWithFormat:@"%@",[dictSel objectForKey:@"abr"]]])
                {
                    isStateFound=YES;
                    stateIndex=i;
                    if ([arrTempStateData containsObject:[arrStates objectAtIndex:indexPath.row]]) {
                        NSLog(@"bug detected ok...");
                    }else{

                        NSLog(@"bug detected...");
                    }

                }
            }
        }
        
        if (isStateFound) {
            if ((arrTempStateData.count-1)>=stateIndex) {
                [arrTempStateData removeObjectAtIndex:stateIndex];
            }
        }
        else
        {
            [arrTempStateData addObject:dictSel];
        }
        
//        if ([arrTempStateData containsObject:[arrStates objectAtIndex:indexPath.row]]) {
//            [arrTempStateData removeObject:[arrStates objectAtIndex:indexPath.row]];
//        }else{
//            [arrTempStateData addObject:[arrStates objectAtIndex:indexPath.row]];
//        }
        [self CheckAllLocation];
    }
    [tableView reloadData];
}

-(NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return nil;
}

- (IBAction)checkBtn:(id)sender event :(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:_tableViewFilter];
    NSIndexPath *indexPath = [_tableViewFilter indexPathForRowAtPoint:currentTouchPosition];
    FilterTableViewCell *cell = (FilterTableViewCell *)[_tableViewFilter cellForRowAtIndexPath:indexPath];
    UIImage *img = [cell.buttonCheckBoxOutlet imageForState:UIControlStateNormal];
    
    if([img isEqual:[UIImage imageNamed:@"withou_tick"]])
        [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
    else
        [cell.buttonCheckBoxOutlet setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
}


#pragma mark- selfMadeButtonActions

- (IBAction)buttonSelectAllStatesAction:(id)sender
{
    [arrStateIds removeAllObjects];
    [arrCountryIds removeAllObjects];
    
    if ([sender isSelected])
    {
        isAllLocations = NO;
        
        
        
        [sender setSelected:NO];
        
        labelSubTitle.text = @"";
        
        if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
        {
            isAllCountrySelected = NO;
            [[NSUserDefaults standardUserDefaults] setBool:isAllCountrySelected forKey:@"AllCountrySelected"];
            [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationDes"];
            
            [arrayDisciplineStateId removeAllObjects];
            
            [arrayDisciplineCountryId removeAllObjects];
            
            [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationSBDes"];
            
            [arraySubDisciStateId removeAllObjects];
            [arraySubDisciCountryId removeAllObjects];
        }
        
        else if ([stringCheckDisciplines isEqualToString: @"fromSubDisciplines"])
        {
            isAllCountrySelected = NO;
            [[NSUserDefaults standardUserDefaults] setBool:isAllCountrySelected forKey:@"AllCountrySelected"];
            [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationDes"];
            
            [arrayDisciplineStateId removeAllObjects];
            
            [arrayDisciplineCountryId removeAllObjects];
            
            [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocationSBDes"];
            
            [arraySubDisciStateId removeAllObjects];
            [arraySubDisciCountryId removeAllObjects];
        }
        
        else
        {
            isAllCountrySelectedSearch = NO;
            [[NSUserDefaults standardUserDefaults] setBool:isAllCountrySelectedSearch forKey:@"AllCountrySelectedSearch"];
            [[NSUserDefaults standardUserDefaults]setValue:@"Nolocation" forKey:@"allLocation"];
        }
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        stringSelectedUnselected = @"AllDataGet";
        
    }
    
    else
    {
        isAllLocations = YES;
        
        
        if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
        {
            isAllCountrySelected = YES;
            [[NSUserDefaults standardUserDefaults] setBool:isAllCountrySelected forKey:@"AllCountrySelected"];
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocationDes"];
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocationSBDes"];
        }
        else if ([stringCheckDisciplines isEqualToString: @"fromSubDisciplines"])
        {
            isAllCountrySelected = YES;
            [[NSUserDefaults standardUserDefaults] setBool:isAllCountrySelected forKey:@"AllCountrySelected"];
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocationDes"];
            
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocationSBDes"];
        }
        else
        {
            isAllCountrySelectedSearch = YES;
            [[NSUserDefaults standardUserDefaults] setBool:isAllCountrySelectedSearch forKey:@"AllCountrySelectedSearch"];
            [[NSUserDefaults standardUserDefaults]setValue:@"allLocation" forKey:@"allLocation"];
        }
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [arrStateIds addObjectsFromArray:arrayData];
        [arrCountryIds addObjectsFromArray:arrayCountryData];
        [sender setSelected:YES];
        
        stringSelectedUnselected = @"";
        
        labelSubTitle.text = @"All Locations";
    }
    [self setAllLocation];
    [[self tableViewFilter] reloadData];
}


#pragma mark- tabBarButtonAction

- (IBAction)buttonActionPortfolio:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    arrCountryIds = NewCountryIdArr;
    arrStateIds = NewStateIdArr;
    isCancelClicked = YES;
    [[NSUserDefaults standardUserDefaults]setObject:arrCountryIds forKey:@"arryCountry"];
    [[NSUserDefaults standardUserDefaults]setObject:arrStateIds forKey:@"arryStates"];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[PortfolioViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    PortfolioViewController *portfolioView =[self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
    [self.navigationController pushViewController:portfolioView animated:NO];
}


- (IBAction)buttonActionDiscip:(id)sender;
{
    arrCountryIds = NewCountryIdArr;
    arrStateIds = NewStateIdArr;
    isCancelClicked = YES;
    [[NSUserDefaults standardUserDefaults]setObject:arrCountryIds forKey:@"arryCountry"];
    [[NSUserDefaults standardUserDefaults]setObject:arrStateIds forKey:@"arryStates"];
    if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
    {
        
    }
    else if ([stringCheckDisciplines isEqualToString: @"fromSubDisciplines"])
    {
        
    }
    
    else
    {
        fromLoadDisc = YES;
    }
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[DisciplineViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    DisciplineViewController *disciplineVw = [self.storyboard instantiateViewControllerWithIdentifier:@"disciplineView"];
    [self.navigationController pushViewController:disciplineVw animated:NO];
}

- (IBAction)buttonSearch:(id)sender;
{
    arrCountryIds = NewCountryIdArr;
    arrStateIds = NewStateIdArr;
    isCancelClicked = YES;
    [[NSUserDefaults standardUserDefaults]setObject:arrCountryIds forKey:@"arryCountry"];
    [[NSUserDefaults standardUserDefaults]setObject:arrStateIds forKey:@"arryStates"];
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[LocationFilterSearchViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    LocationFilterSearchViewController  *searchVw = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVw"];
    [self.navigationController pushViewController:searchVw animated:NO];
}

- (IBAction)buttonMessages:(id)sender;
{
    arrCountryIds = NewCountryIdArr;
    arrStateIds = NewStateIdArr;
    isCancelClicked = YES;
    [[NSUserDefaults standardUserDefaults]setObject:arrCountryIds forKey:@"arryCountry"];
    [[NSUserDefaults standardUserDefaults]setObject:arrStateIds forKey:@"arryStates"];
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[MessagesViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    MessagesViewController  *messageVw = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    [self.navigationController pushViewController:messageVw animated:NO];
}

- (IBAction)buttonActionSalary:(id)sender;
{
    arrCountryIds = NewCountryIdArr;
    arrStateIds = NewStateIdArr;
    isCancelClicked = YES;
    [[NSUserDefaults standardUserDefaults]setObject:arrCountryIds forKey:@"arryCountry"];
    [[NSUserDefaults standardUserDefaults]setObject:arrStateIds forKey:@"arryStates"];
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[SalaryGuideViewController class]])
            
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    SalaryGuideViewController *salaryVw = [self.storyboard instantiateViewControllerWithIdentifier:@"salaryVw"];
    [self.navigationController pushViewController:salaryVw animated:NO];
}


@end
