//
//  PortfolioViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 2/2/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PortfolioTableCell.h"
#import "Utilities.h"
#import "Database.h"
#import "GamificationVC.h"

@interface PortfolioViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITabBarControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *protfolioTableView;
@property (strong, nonatomic) IBOutlet UIButton *companyBtn;
@property (strong, nonatomic) IBOutlet UIButton *awardsBtn;
@property (strong, nonatomic) IBOutlet UIButton *duebtn;
@property (strong, nonatomic) IBOutlet UIView *viewBottomBar;
@property (strong, nonatomic) IBOutlet UIButton *buttonPortfolio;
@property (strong, nonatomic) IBOutlet UIButton *buttonDiscipline;
@property (strong, nonatomic) IBOutlet UIButton *buttonSearch;
@property (strong, nonatomic) IBOutlet UIButton *buttonMessages;
@property (strong, nonatomic) IBOutlet UIButton *buttonSalaryguide;
@property (strong, nonatomic) IBOutlet UILabel *labelPortfolio;
@property (strong, nonatomic) IBOutlet UILabel *labelDiscipline;
@property (strong, nonatomic) IBOutlet UILabel *labelSearchVw;
@property (strong, nonatomic) IBOutlet UILabel *labelSalaryGuide;
@property (strong, nonatomic) IBOutlet UILabel *labelMessages;
@property (strong, nonatomic) IBOutlet UILabel *labelCompany;
@property (strong, nonatomic) IBOutlet UILabel *labelDueDate;
@property(assign) float latitude;
@property(assign) float longitude;

@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;
@property(strong,nonatomic) NSArray *arrayData;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) IBOutlet UITableView *tblStudentSociety;
@property (strong, nonatomic) IBOutlet UIScrollView *PortScrollView;

- (IBAction)btnSortAction:(id)sender;
- (IBAction)buttonActionPortfolio:(id)sender;
- (IBAction)buttonActionDiscip:(id)sender;
- (IBAction)buttonSearch:(id)sender;
- (IBAction)buttonMessages:(id)sender;
- (IBAction)buttonActionSalary:(id)sender;

@end




//-(void)newMethod
//{
//    progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(135,110, 40, 20)];
//    
//    progressLabel.text = @"0";
//    
//    countForLabel = 0;
//    
//    [viewFront addSubview:progressLabel];
//    
//    imgviewCircle1=[[UIImageView alloc]initWithFrame:CGRectMake(135 , 100, 40, 41)];
//    
//    [viewFront addSubview:imgviewCircle1];
//    
//    imgviewCircle1.image=[UIImage imageNamed:@"question_progress_background.png"];
//    
//    imgviewCircle1.backgroundColor=[UIColor clearColor];
//    
//    circle=[CAShapeLayer layer];
//    circle.path=[UIBezierPath bezierPathWithArcCenter:CGPointMake(20, 21) radius:18 startAngle:2*M_PI*0-M_PI_2 endAngle:2*M_PI*1-M_PI_2 clockwise:YES].CGPath;
//    circle.fillColor=[UIColor clearColor].CGColor;
//    circle.strokeColor=[UIColor clearColor].CGColor;
//    
//    circle.lineWidth=10;
//    
//    circle.strokeColor=[UIColor redColor].CGColor;
//    
//    animation=[CABasicAnimation animationWithKeyPath:@"strokeEnd"];
//    animation.duration=35;
//    animation.removedOnCompletion=NO;
//    animation.fromValue=@(0);
//    animation.toValue=@(1);
//    animation.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
//    [circle addAnimation:animation forKey:@"drawCircleAnimation"];
//    
//    [imgviewCircle1.layer addSublayer:circle];
//    
//    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(labelTextAccordingTimerForAnswer) userInfo:nil  repeats:YES];
//}
//-(void)labelTextAccordingTimerForAnswer
//{
//    if (countForLabel== 96)
//    {
//        countForLabel = countForLabel +4;
//    }
//    else if (countForLabel== 100)
//    {
//        [self.timer invalidate];
//        
//        self.timer = nil;
//    }
//    else
//    {
//        countForLabel = countForLabel +3;
//    }
//    
//    progressLabel.text = [NSString stringWithFormat:@"%d", countForLabel];
//}



