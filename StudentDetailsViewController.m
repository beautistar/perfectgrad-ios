//
//  StudentDetailsViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 1/30/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "StudentDetailsViewController.h"
#import "TermsConditionViewController.h"
#import "AFNetworking.h"
#import "ViewController.h"
#import "DisciplineViewController.h"
#import "PortfolioViewController.h"
#import "SWRevealViewController.h"
#import "SocietySelectionVC.h"
#import "ApiClass.h"

@interface StudentDetailsViewController ()
{
    NSMutableDictionary *dict;
    NSDictionary *dictionary;
    NSArray *positionNameArray;
    NSArray *genderNameArray;
    NSArray *stateNameArray;
    NSArray *universityNameArray;
    NSMutableArray *subdisciplineNameArray;
    NSMutableArray *subdiscipline2NameArray;
    NSArray *societyNameArray;
    NSMutableArray *arrayDisciplinesNames;
    NSMutableArray *arrayCountry;
    
    NSString *strCountryId,*idStrState,*strUniId,*strDisciplineId,*stringPositionId,*strSubDiscipId,*strDiscipline2Id,*strSubDiscip2Id,*strSelectedDiscp, *strCountryName;
    NSString *selectArrayString;
    UIActivityIndicatorView*spinner;
    Database *dummy;
    BOOL checkClick;
}

@end

@implementation StudentDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imageRedStar.hidden = YES;
    
    arrayCountry = [[NSMutableArray alloc] init];
    subdisciplineNameArray = [[NSMutableArray alloc] init];
    subdiscipline2NameArray = [[NSMutableArray alloc] init];
    dummy=[[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    _textFieldCountry.inputView = _pickerView;
    
    self.view_Picker.backgroundColor =  [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    _pickerView.userInteractionEnabled = YES;
    arrayDisciplinesNames=[[NSMutableArray alloc]init];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    /*---Set the image in navigation bar----*/
    
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    /*Set the font size in navigaton bar item*/
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:18.0f],     NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
    
    /*Set the title name in navigation bar*/
    self.navigationItem.title = @"Student Details";
    
    /*set the picker view background colour */
    _pickerView.backgroundColor =[UIColor colorWithRed:233.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.0f];
    
    /* Initialize Data of gender name */
    genderNameArray = @[@"Female",@"Male"];
    
    _buttonSubmit.enabled = NO;
    
    _buttonSubmit.layer.cornerRadius = 5.0f;
    _buttonSubmit.layer.borderWidth = 2.0f;
    
    /*Hide back button of navigation bar*/
    if ([strFromButton isEqualToString:@"tempLogin"])
    {
        _buttonSubmit.layer.borderColor = [[UIColor lightGrayColor]  CGColor];
        [_buttonSubmit setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.navigationItem.hidesBackButton = YES;
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        _viewTabBar.hidden = YES;
        
        [[[UIAlertView alloc]initWithTitle:@"Before we begin..." message:@"\nSelect from the following student details.\n\nThe selected details will help companies connect with you faster." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
    }
    
    else if ([strFromButton isEqualToString:@"menu"])
    {
        _buttonSubmit.layer.borderColor = [[UIColor colorWithRed:189.0f/255.0f green:189.0f/255.0f blue:189.0f/255.0f alpha:1.0f]  CGColor];
        [_buttonSubmit setTitleColor:[UIColor colorWithRed:189.0f/255.0f green:189.0f/255.0f blue:189.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        _buttonSubmit.enabled = NO;
        [_buttonSubmit setTitle:@"Update" forState:UIControlStateNormal];
        
        [self.navigationItem setHidesBackButton:NO];
        UIView *viewAddBtnsLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 40)];
        
        UIButton * buttonMenu = [[UIButton alloc]initWithFrame:CGRectMake(-3, 4, 30, 30) ];
        
        [buttonMenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
        
        buttonMenu.backgroundColor = [UIColor clearColor];
        
        [buttonMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        
        [viewAddBtnsLeft addSubview:buttonMenu];
        
        self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:viewAddBtnsLeft];
        
        _viewTabBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bottom-nav"]];
        _viewTabBar.hidden = YES;
        
    }
    
    else
    {
        _buttonSubmit.layer.borderColor = [[UIColor lightGrayColor]  CGColor];
        [_buttonSubmit setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        _viewTabBar.hidden = YES;
        [self.navigationItem setHidesBackButton:YES];
    }
    
    self.navigationController.navigationBar.tintColor  =[UIColor whiteColor];
    
    for (int i=1; i<10; i++)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:i];
        [self setrightViewFortextField:textField];
    }
}


-(void)resignKeyboardOnMenuClick
{
    for (int i=1; i<10; i++)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:i];
        [textField resignFirstResponder];
    }
    
    [self.pickerView setHidden:YES];
    [self.view_Picker setHidden:YES];
    [self viewChange:65];
}


- (void)setrightViewFortextField:(UITextField *)textField
{
    UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetHeight(textField.frame), CGRectGetHeight(textField.frame))];
    imgV.contentMode = UIViewContentModeCenter;
    imgV.image = [UIImage imageNamed:@"FIRST"];
    textField.rightView = imgV;
    textField.rightViewMode = UITextFieldViewModeAlways;
    
    UIImageView *imgVLeft = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 10, 5)];
    textField.leftView = imgVLeft;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    CALayer *layer1 = [[CALayer alloc]init];
    layer1.frame = CGRectMake(0, 0, CGRectGetWidth(textField.frame)-CGRectGetHeight(textField.frame)+1, CGRectGetHeight(textField.frame));
    layer1.borderColor = [[UIColor lightGrayColor] CGColor];
    layer1.borderWidth = 1.0;
    [textField.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc]init];
    layer2.frame = CGRectMake(CGRectGetWidth(textField.frame)-CGRectGetHeight(textField.frame), 0,CGRectGetHeight(textField.frame), CGRectGetHeight(textField.frame));
    layer2.borderColor = [[UIColor lightGrayColor] CGColor];
    layer2.borderWidth = 1.0;
    
    [textField.layer addSublayer:layer2];
}

#pragma mark:-update the data on the View

-(void)viewWillAppear:(BOOL)animated
{
    stringNotifiction = @"";
    
    stringForNotifyEditCompany = @"";
    
    [super viewWillAppear:YES];
    
    for (UIView* view in self.scrollViewContainer.subviews)
    {
        if ([view isKindOfClass:[UILabel class]])
        {
            UILabel*lbl = (UILabel*)view;
            
            lbl.font = [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
        }
    }
    self.pickerView.hidden = YES;
    
    _view_Picker.hidden = YES;
    
    if ([strFromButton isEqualToString:@"menu"])
    {
        if(![Utilities CheckInternetConnection])
        {
            [self placeDataOffline];
            
            _buttonSubmit.layer.borderColor = [[UIColor lightGrayColor]  CGColor];
            [_buttonSubmit setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            _buttonSubmit.enabled = NO;
            _buttonSubmit.userInteractionEnabled = NO;
            
            for (int j=0; j<10; j++)
            {
                UITextField *fld = (UITextField*)[self.view viewWithTag:j+1];
                fld.userInteractionEnabled = NO;
            }
        }
        else
        {
            [self getStudentProfile];
        }
    }
    if(![Utilities CheckInternetConnection])
    {
    }
    else
    {
        [self getAllCountries];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resignKeyboardOnMenuClick)
                                                 name:@"resignKeyboard"
                                               object:nil];
}

-(void)placeDataOffline
{
    NSDictionary *dictionaryOffline = [[NSUserDefaults standardUserDefaults] objectForKey:@"savedetail"];
    
    _genderTextField.text = [dictionaryOffline valueForKey:@"gender"];
    _textFieldCountry.text = [dictionaryOffline valueForKey:@"country"];
    _stateNameTextField.text = [dictionaryOffline valueForKey:@"state"];
    _universityNameTextField.text = [dictionaryOffline valueForKey:@"university"];
    _positionNameTextField.text = [dictionaryOffline valueForKey:@"position"];
    _textFieldDisciplinesName.text = [dictionaryOffline valueForKey:@"discipline"];
    _subDisciplineTextField.text = [dictionaryOffline valueForKey:@"subdiscipline"];
    _textFldDiscipline2.text = [dictionaryOffline valueForKey:@"discilpine2"];
    _textFldSubdiscipline2.text = [dictionaryOffline valueForKey:@"subdiscipline2"];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    [self ShowDataLoader:NO];
    
    stateNameArray = nil;
    universityNameArray = nil;
    positionNameArray = nil;
    subdisciplineNameArray=nil;
    societyNameArray=nil;
    arrayDisciplinesNames=nil;
    dict =nil;
    arrayCountry=nil;
}


#pragma mark- callWebServices

-(void)getStudentProfile
{
    [self ShowDataLoader:YES];
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@getProfile",baseUrl];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             if ([responseObject valueForKeyPath:@"data.country"] == (id)[NSNull null]||[responseObject valueForKeyPath:@"data.state"] == (id)[NSNull null]||[responseObject valueForKeyPath:@"data.university"] == (id)[NSNull null]||[responseObject valueForKeyPath:@"data.program"] == (id)[NSNull null]||[[responseObject valueForKeyPath:@"data.discipline"] count]==0)
             {
                 [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"\nPlease update your profile again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else
             {
                 [[NSUserDefaults standardUserDefaults] setObject:responseObject forKey:@"userProfile"];
             }
             _genderTextField.text = [NSString stringWithFormat:@" %@",[responseObject valueForKeyPath:@"data.gender"]];
             
             if ([responseObject valueForKeyPath:@"data.country"] != (id)[NSNull null])
             {
                 _textFieldCountry.text = [responseObject valueForKeyPath:@"data.country"];
                 strCountryId = [responseObject valueForKeyPath:@"data.country_id"];
             }
             if ([responseObject valueForKeyPath:@"data.state"] != (id)[NSNull null])
             {
                 _stateNameTextField.text = [responseObject valueForKeyPath:@"data.state"];
                 idStrState=[responseObject valueForKeyPath:@"data.state_id"];
             }
             if ([responseObject valueForKeyPath:@"data.university"] != (id)[NSNull null])
             {
                 _universityNameTextField.text = [responseObject valueForKeyPath:@"data.university"];
                 strUniId=[responseObject valueForKeyPath:@"data.uni_id"];
             }
             if ([responseObject valueForKeyPath:@"data.program"] != (id)[NSNull null])
             {
                 _positionNameTextField.text =[responseObject valueForKeyPath:@"data.program"];
                 stringPositionId=[responseObject valueForKeyPath:@"data.graduateProg_id"];
             }
             
             if ([[responseObject valueForKeyPath:@"data.discipline"] count]>0)
             {
                 _textFieldDisciplinesName.text = [[responseObject valueForKeyPath:@"data.discipline.discipline"] objectAtIndex:0];
                 strDisciplineId = [NSString stringWithFormat:@"%@",[[responseObject valueForKeyPath:@"data.discipline.discipline_id"] objectAtIndex:0]];
                 
                 if (![[[responseObject valueForKeyPath:@"data.discipline.sub_discipline"] objectAtIndex:0] isEqualToString:@"no"])
                 {
                     _subDisciplineTextField.text = [[responseObject valueForKeyPath:@"data.discipline.sub_discipline"] objectAtIndex:0];
                     strSubDiscipId = [NSString stringWithFormat:@"%@",[[responseObject valueForKeyPath:@"data.discipline.sub_discipline_id"] objectAtIndex:0]];
                 }
                 else
                 {
                     _textFldSubdiscipline2.text = @"";
                     strSubDiscip2Id = @"0";
                 }
                 
                 if ([[responseObject valueForKeyPath:@"data.discipline"] count]>=2)
                 {
                     if ([responseObject valueForKeyPath:@"data.discipline.discipline"] == (id)[NSNull null])
                     {
                         
                     }
                     else
                     {
                         _textFldDiscipline2.text = [[responseObject valueForKeyPath:@"data.discipline.discipline"] objectAtIndex:1];
                         strDiscipline2Id = [NSString stringWithFormat:@"%@",[[responseObject valueForKeyPath:@"data.discipline.discipline_id"] objectAtIndex:1]];
                         
                         if (![[[responseObject valueForKeyPath:@"data.discipline.sub_discipline"] objectAtIndex:1] isEqualToString:@"no"])
                         {
                             _textFldSubdiscipline2.text = [[responseObject valueForKeyPath:@"data.discipline.sub_discipline"] objectAtIndex:1];
                             strSubDiscip2Id = [NSString stringWithFormat:@"%@",[[responseObject valueForKeyPath:@"data.discipline.sub_discipline_id"] objectAtIndex:1]];
                         }
                         else
                         {
                             _textFldSubdiscipline2.text = @"";
                             strSubDiscip2Id = @"0";
                         }
                     }
                     
                 }
                 
             }
         }
         else
         {
             NSLog(@"wrong result");
         }
     }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         [self placeDataOffline];
         NSLog(@"%@",error.localizedDescription);
     }];
}


-(void)getAllCountries
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"getAllCountries" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             arrayCountry = [responseObject objectForKey:@"data"];
             [self.pickerView reloadAllComponents];
         }
         else{
             NSLog(@"wrong result");
         }
         
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         
         NSLog(@"Error: %@", error.localizedDescription);
     }];
}


-(void)getCountryPostion :(UITextField*)txtFld
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"cid":strCountryId};
    
    [manager POST:@"getposition" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             positionNameArray = [responseObject objectForKey:@"data"];
             if (txtFld) {
                 [self showPicker:txtFld];
             }
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error.localizedDescription);
     }];
}

-(void)getAllStates:(UITextField*)txtFld
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"cid":strCountryId};
    [manager POST:@"getAllStates" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             dictionary = [responseObject objectForKey:@"data"];
             stateNameArray = [responseObject objectForKey:@"data"];
             
             [self getCountryPostion:nil];
             [self.pickerView reloadAllComponents];
             
             if (txtFld)
             {
                 [self showPicker:txtFld];
             }
         }
         else
         {NSLog(@"wrong result");
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error.localizedDescription);
     }];
}

-(void)getAllUniversities:(UITextField*)textFld

{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *parameters = @{@"sid":idStrState};
    
    [manager POST:@"getAllUniversities" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             universityNameArray = [responseObject objectForKey:@"data"];
             
             [self.pickerView reloadAllComponents];
             
             if (textFld)
                 
             {
                 [self showPicker:textFld];
             }
         }
         
         else
         {
             NSLog(@"wrong result");
         }
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error.localizedDescription);
     }];
}

-(void)getAlldisciplines:(UITextField*)txtFld
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    
    [manager POST:@"getAlldisciplines" parameters:@{@"uid":@"",@"cid":strCountryId} success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             arrayDisciplinesNames = [[responseObject objectForKey:@"data"] mutableCopy];
             arrayDisciplinesNames = [[self sortArrayData:(NSArray *)[arrayDisciplinesNames copy]] mutableCopy];
             
             NSDictionary *dicDummy = @{@"name":@"None",@"id":@""};
             [arrayDisciplinesNames insertObject:dicDummy atIndex:0];
             
             [self.pickerView reloadAllComponents];
             if (txtFld)
                 
             {
                 [self showPicker:txtFld];
             }
         }
         
         else
             
         {
             NSLog(@"wrong result");
         }
         
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     
     {
         NSLog(@"Error: %@", error.localizedDescription);
     }];
    
}


-(void)getAllSubdisciplines:(UITextField *)textFld
{
    NSDictionary *parameters = nil;
    
    if ([strSelectedDiscp isEqualToString:@"1"])
    {
        parameters = @{@"did":strDisciplineId};
    }
    else
    {
        parameters = @{@"did":strDiscipline2Id};
    }
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"getAllSubdisciplines" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             if ([strSelectedDiscp isEqualToString:@"1"])
             {
                 subdisciplineNameArray =  [[responseObject objectForKey:@"data"] mutableCopy];
                 
                 subdisciplineNameArray = [[self sortArrayData:(NSArray *)[subdisciplineNameArray copy]] mutableCopy];
                 
                 NSDictionary *dicDummy = @{@"companys":@"",@"date":@"",@"discipline_id":@"",@"name":@"None",@"id":@""};
                 [subdisciplineNameArray insertObject:dicDummy atIndex:0];
                 
             }
             else
             {
                 subdiscipline2NameArray =  [[responseObject objectForKey:@"data"] mutableCopy];
                 
                 subdiscipline2NameArray = [[self sortArrayData:(NSArray *)[subdiscipline2NameArray copy]] mutableCopy];
                 
                 NSDictionary *dicDummy = @{@"companys":@"",@"date":@"",@"discipline_id":@"",@"name":@"None",@"id":@""};
                 [subdiscipline2NameArray insertObject:dicDummy atIndex:0];
                 
             }
             
             [self.pickerView reloadAllComponents];
             
             if (textFld) {
                 [self showPicker:textFld];
             }
         }
         else{
             NSLog(@"No data Related Yet");
         }
         
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error.localizedDescription);
     }];
}
-(void)getAllStudentSocieties{
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"getAllStudentSocieties" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             societyNameArray = [responseObject objectForKey:@"data"];
             [self.pickerView reloadAllComponents];
         }
         else{
             NSLog(@"wrong result");
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error.localizedDescription);
     }];
}

#pragma mark- UpdateStudentDetailWebService

-(void)updateStudentDetail
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    if(!stringPositionId){
        stringPositionId = @"";
    }
    
    if (!strSubDiscipId) {
        strSubDiscipId = @"";
    }
    
    if (!strDiscipline2Id){
        strDiscipline2Id = @"";
    }
    
    if (!strSubDiscip2Id||[strSubDiscip2Id isEqualToString:@"0"])
    {
        strSubDiscip2Id = @"";
    }
    NSDictionary *parameters = @{@"id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"gender":[_genderTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]],@"cid":strCountryId,@"sid":idStrState,@"uid":strUniId,@"gpid":stringPositionId,@"did":strDisciplineId,@"sdid":strSubDiscipId,@"did1":strDiscipline2Id,@"sdid1":strSubDiscip2Id};
    
    NSLog(@"parameters %@", parameters);
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@UpStudent",baseUrl];
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             checkCopmanyApiCall = NO;
             
             [dummy deleteTable:@"delete from COMPANIES"];
             [arrCountryIds removeAllObjects];
             [arrStateIds removeAllObjects];
             
             
             
             if ([strFromButton isEqualToString:@"menu"])
             {
                 if (![strUniId isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"userUniversityId"]])
                 {
                     [self updateSocieties];
                     [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userSocieties"];
                 }
                 else
                 {
                     [self ShowDataLoader:NO];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:_universityNameTextField.text forKey:@"userUniversity"];
                     
                     if (checkClick == YES)
                     {
                         checkClick = NO;
                         
                         [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arryCountry"];
                         [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arryStates"];
                         
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callPortWebService"];
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callMsgWebService"];
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callallCompanyWebService"];
                         
                         [[NSUserDefaults standardUserDefaults]synchronize];
                         
                         UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                         
                         SocietySelectionVC *societiesView = [storyBoard instantiateViewControllerWithIdentifier:@"SocietySelectionVC"];
                         societiesView.isFromScreen = @"uniChange";
                         
                         self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:42.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
                         
                         [self.navigationController pushViewController:societiesView animated:YES];
                     }
                     else
                     {
                         
                         [[NSNotificationCenter defaultCenter]
                          postNotificationName:@"facebookCompnies"
                          object:self];
                         
                         [self performSelectorOnMainThread:@selector(showAlertMethod) withObject:nil waitUntilDone:NO];
                         
                         [[NSUserDefaults standardUserDefaults] setObject:_universityNameTextField.text forKey:@"userUniversity"];
                         
                         PortfolioViewController *portView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
                         
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callPortWebService"];
                         
                         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callallCompanyWebService"];
                         
                         UINavigationController *navPort = [[UINavigationController alloc]initWithRootViewController : portView];
                         
                         [self.revealViewController setFrontViewController:navPort animated:NO];
                         
                         
                     }
                 }
             }
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}

-(void)showAlertMethod
{
    
    [[[UIAlertView alloc]initWithTitle:@"Student Details Updated" message:@"\nYour student details have been successfully updated." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
}

-(void)saveValuesToUserDefault
{
    [[NSUserDefaults standardUserDefaults] setObject:strCountryId forKey:@"countryId"];
    [[NSUserDefaults standardUserDefaults] setObject:_universityNameTextField.text forKey:@"userUniversity"];
    [[NSUserDefaults standardUserDefaults] setObject:strUniId forKey:@"userUniversityId"];
    [[NSUserDefaults standardUserDefaults] setObject:_stateNameTextField.text forKey:@"state"];
    
    NSDictionary *dictDetail = @{@"gender":_genderTextField.text, @"country":_textFieldCountry.text,@"state":_stateNameTextField.text,@"university":_universityNameTextField.text,@"position":_positionNameTextField.text,@"discipline":_textFieldDisciplinesName.text,@"subdiscipline":_subDisciplineTextField.text,@"discilpine2":_textFldDiscipline2.text,@"subdiscipline2":_textFldSubdiscipline2.text};
    
    [[NSUserDefaults standardUserDefaults]setObject:dictDetail forKey:@"savedetail"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSLog(@"student dict values are %@", dictDetail);
    
}

-(void)updateSocieties
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *dicParam =@{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"sid":@"clear"};
    [manager POST:@"addAllSociety" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
             SocietySelectionVC *societiesView = [storyBoard instantiateViewControllerWithIdentifier:@"SocietySelectionVC"];
             societiesView.isFromScreen = @"uniChange";
             [self.navigationController pushViewController:societiesView animated:YES];
         }
         else
         {
             NSLog(@"wrong result");
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}


#pragma mark- callWebServiceAddStudent

-(void)callWebserviceAddStudent
{
    [self ShowDataLoader:YES];
    
    UIDevice *myDevice = [UIDevice currentDevice];
    NSString *deviceName = myDevice.name;
    NSString *deviceOSVersion = myDevice.systemVersion;
    
    deviceName = [deviceName stringByAppendingString:deviceOSVersion];
    
    NSLog(@"deviceName is %@", deviceName);
    
    if(!stringPositionId)
    {
        stringPositionId = @"";
    }
    if (!strSubDiscipId)
    {
        strSubDiscipId = @"";
    }
    if (!strDiscipline2Id)
    {
        strDiscipline2Id = @"";
    }
    if (!strSubDiscip2Id)
    {
        strSubDiscip2Id = @"";
    }
    
    NSDictionary *parameters = nil;
    
    if (!strDeviceToken)
    {
        strDeviceToken = @"";
    }
    
    if ([strFromButton isEqualToString:@"tempLogin"])
    {
        parameters = @{@"gender":_genderTextField.text,@"cid":strCountryId,@"sid":idStrState,@"uid":strUniId,@"gpid":stringPositionId,@"did":strDisciplineId,@"sdid":strSubDiscipId,@"did1":strDiscipline2Id,@"sdid1":strSubDiscip2Id,@"device":strDeviceToken,@"device type":deviceName};
    }
    
    else
    {
        parameters = @{@"id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"gender":_genderTextField.text,@"cid":strCountryId,@"sid":idStrState,@"uid":strUniId,@"gpid":stringPositionId, @"did":strDisciplineId,@"sdid":strSubDiscipId,@"did1":strDiscipline2Id,@"sdid1":strSubDiscip2Id,@"device":strDeviceToken,@"device type":deviceName};
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:_universityNameTextField.text forKey:@"userUniversity"];
    [[NSUserDefaults standardUserDefaults] setObject:strUniId forKey:@"userUniversityId"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SocietySelectionVC *societiesView = [storyBoard instantiateViewControllerWithIdentifier:@"SocietySelectionVC"];
    
    societiesView.dictUserInfoFromUsrPg = parameters;
    
    
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:42.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    [self.navigationController pushViewController:societiesView animated:YES];
}


-(void)JsonResponseaddStudent:(id)response

{
    if ([strFromButton isEqualToString:@"tempLogin"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"user_id"] forKey:@"userId"];
        [[NSUserDefaults standardUserDefaults] setObject:_universityNameTextField.text forKey:@"userUniversity"];
    }
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"studentviewKey"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

/* The number of rows of data*/
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([selectArrayString isEqualToString:@"1"])
    {
        return [genderNameArray count];
    }
    else if ([selectArrayString isEqualToString:@"2"])
    {
        return [arrayCountry count];
    }
    else if ([selectArrayString isEqualToString:@"3"])
    {
        return [stateNameArray count];
    }
    else if ([selectArrayString isEqualToString:@"4"])
    {
        return [universityNameArray count];
    }
    else if ([selectArrayString isEqualToString:@"5"])
    {
        return [positionNameArray count];
    }
    else if ([selectArrayString isEqualToString:@"6"])
    {
        if ([[[arrayDisciplinesNames valueForKey:@"name"] objectAtIndex:0] isEqualToString:@"None"])
        {
            [arrayDisciplinesNames removeObjectAtIndex:0];
        }
        
        return [arrayDisciplinesNames count];
    }
    else if ([selectArrayString isEqualToString:@"7"])
    {
        if (![[[subdisciplineNameArray valueForKey:@"name"] objectAtIndex:0] isEqualToString:@"None"])
        {
        }
        
        return [subdisciplineNameArray count];
    }
    else if ([selectArrayString isEqualToString:@"8"])
    {
        if (![[[arrayDisciplinesNames valueForKey:@"name"] objectAtIndex:0] isEqualToString:@"None"])
        {
            
        }
        return [arrayDisciplinesNames count];
    }
    else if ([selectArrayString isEqualToString:@"9"])
    {
        if (![[[subdiscipline2NameArray valueForKey:@"name"] objectAtIndex:0] isEqualToString:@"None"])
        {
            
        }
        return [subdiscipline2NameArray count];
    }
    return NO;
}


-(NSMutableArray *)sortArrayData:(NSArray *)array
{
    NSArray *aray;
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    aray=[array sortedArrayUsingDescriptors:@[sort]];
    NSMutableArray *arrayM = [[NSMutableArray alloc]init];
    arrayM = [aray mutableCopy];
    
    return arrayM;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;
    
    if ([selectArrayString isEqualToString:@"1"])
    {
        title =genderNameArray[row];
    }
    else if ([selectArrayString isEqualToString:@"2"])
    {
        arrayCountry = [[self sortArrayData:(NSArray *)[arrayCountry copy]] mutableCopy];
        
        title = [arrayCountry valueForKey:@"name"][row];
    }
    else if ([selectArrayString isEqualToString:@"3"])
    {
        stateNameArray = [[self sortArrayData:(NSArray *)[stateNameArray copy]] mutableCopy];
        
        title = [stateNameArray valueForKey:@"name"][row];
    }
    else if ([selectArrayString isEqualToString:@"4"])
    {
        universityNameArray = [[self sortArrayData:(NSArray *)[universityNameArray copy]] mutableCopy];
        
        title = [universityNameArray valueForKey:@"name"][row];
    }
    else if ([selectArrayString isEqualToString:@"5"])
    {
        positionNameArray = [[self sortArrayData:(NSArray *)[positionNameArray copy]] mutableCopy];
        
        title = [positionNameArray valueForKey:@"name"][row];
    }
    else if ([selectArrayString isEqualToString:@"6"])
    {
        
        title = [arrayDisciplinesNames[row]objectForKey:@"name"];
    }
    else if ([selectArrayString isEqualToString:@"7"])
    {
        
        title = [subdisciplineNameArray[row]objectForKey:@"name"];
    }
    else if ([selectArrayString isEqualToString:@"8"])
    {
        title = [arrayDisciplinesNames[row]objectForKey:@"name"];
    }
    else if ([selectArrayString isEqualToString:@"9"])
    {
        
        title = [subdiscipline2NameArray[row]objectForKey:@"name"];
    }
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]}];
    
    return attString;
}

#pragma mark- ChangeViewFrame

-(void)viewChange:(int)frameY
{
    [UIView animateWithDuration:0.5 animations:^
     {
         CGRect frame = self.scrollViewContainer.frame;
         frame.origin.y = frameY;
         self.scrollViewContainer.frame = frame;
     }];
}

#pragma mark- UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(![Utilities CheckInternetConnection])
    {
        
    }
    else
    {
        if (textField.tag==3)
        {
            if (strCountryId)
            {
                [self getAllStates:textField];
                
                if ([strFromButton isEqualToString:@"menu"])
                {
                    if ([textField.text isEqualToString:@""])
                    {
                        
                    }
                    else
                    {
                        
                        [[[UIAlertView alloc]initWithTitle:@"Warning" message:@"\nChanging the location or university will reset your student society selections. \n\nIf you do not want to reset your student society selections, use the menu button to exit the page." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                    }
                }
            }
        }
        else if (textField.tag==4)
        {
            if (idStrState) {
                [self getAllUniversities:textField];
            }
            
            if ([strFromButton isEqualToString:@"menu"])
            {
                if ([textField.text isEqualToString:@""])
                {
                    
                }
                else
                {
                    
                    [[[UIAlertView alloc]initWithTitle:@"Warning" message:@"\nChanging the location or university will reset your student society selections. \n\nIf you do not want to reset your student society selections, use the menu button to exit the page." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                }
            }
        }
        else if (textField.tag==5)
        {
            if (strCountryId) {
                [self getCountryPostion:textField];
            }
        }
        else if (textField.tag==6)
        {
            if (strCountryId) {
                [self getAlldisciplines:textField];
            }
        }
        else if (textField.tag==7)
        {
            strSelectedDiscp=@"1";
            if (strDisciplineId) {
                [self getAllSubdisciplines:textField];
            }
        }
        else if (textField.tag==8)
        {
            if (strCountryId) {
                [self getAlldisciplines:textField];
            }
        }
        else if (textField.tag==9)
        {
            strSelectedDiscp=@"2";
            
            if (strDiscipline2Id)
            {
                [self getAllSubdisciplines:textField];
            }
        }
        else
        {
            if (textField.tag == 2)
            {
                if ([strFromButton isEqualToString:@"menu"])
                {
                    
                    [[[UIAlertView alloc]initWithTitle:@"Warning" message:@"\nChanging the country will reset all pages in the application, including your Portfolio page. \n\nIf you do not want to reset the application, use the menu button to exit the page." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                }
            }
            [self showPicker:textField];
        }
        
        [self.view endEditing:YES];
    }
    
}

#pragma mark- selfCreatedButtonActions

-(void)backBtnAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnActionDone:(id)sender
{
    NSInteger row;
    
    row = [_pickerView selectedRowInComponent:0];
    
    if ([selectArrayString isEqualToString:@"1"])
    {
        _genderTextField.text = [genderNameArray objectAtIndex:row];
    }
    else if ([selectArrayString isEqualToString:@"2"])
    {
        if (arrayCountry.count>0)
        {
            if (![[_textFieldCountry.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:[[arrayCountry valueForKey:@"name"]objectAtIndex:row]])
            {
                _stateNameTextField.text = @"";
                _positionNameTextField.text = @"";
                _universityNameTextField.text = @"";
                _textFieldDisciplinesName.text = @"";
                _textFldDiscipline2.text = @"";
                _textFldSubdiscipline2.text = @"";
                _subDisciplineTextField.text = @"";
                
                strDisciplineId = @"";
                strDiscipline2Id = @"";
                strSubDiscip2Id = @"";
                strSubDiscipId = @"";
                
                strCountryId = [NSString stringWithFormat:@"%@", [[arrayCountry valueForKey:@"id"]objectAtIndex:row]];
                
                [self getAllStates:nil];
            }
            
            if ([[arrayCountry valueForKey:@"name"]objectAtIndex:row])
                
            {
                _textFieldCountry.text =[[arrayCountry valueForKey:@"name"]objectAtIndex:row];
                
                strCountryName = [arrayCountry valueForKey:@"name"][row];
                
                NSLog(@"strCountryName is %@", strCountryName);
                
                [[NSUserDefaults standardUserDefaults] setObject:strCountryName forKey:@"countryName"];
                
                checkClick = YES;
            }
        }
    }
    
    else if ([selectArrayString isEqualToString:@"3"])
        
    {
        if (stateNameArray.count>0)
        {
            checkClick = YES;
            
            if (![[_stateNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:[[stateNameArray valueForKey:@"name"]objectAtIndex:row]])
            {
                idStrState =[NSString stringWithFormat:@"%@", [[stateNameArray valueForKey:@"id"]objectAtIndex:row]];
                
                _universityNameTextField.text = @"";
                _positionNameTextField.text = @"";
                _textFieldDisciplinesName.text = @"";
                _textFldSubdiscipline2.text = @"";
                _textFldDiscipline2.text = @"";
                _subDisciplineTextField.text = @"";
                
                [USERDEFAULT setObject:[[stateNameArray valueForKey:@"latitude"]objectAtIndex:row] forKey:@"LAtitude_State"];
                
                [USERDEFAULT setObject:[[stateNameArray valueForKey:@"longitude"]objectAtIndex:row] forKey:@"LONGITUDE_State"];
                
                [USERDEFAULT synchronize];
                
                NSLog(@"value in cntryLatitude detail %@", PREF(@"LAtitude_State"));
                
                NSLog(@"value in cntryLongitude detail %@", PREF(@"LONGITUDE_State"));
                
                [self getAllUniversities:nil];
            }
            
            if ([[stateNameArray valueForKey:@"name"]objectAtIndex:row])
            {
                _stateNameTextField.text = [[stateNameArray valueForKey:@"name"]objectAtIndex:row];
                
                [[NSUserDefaults standardUserDefaults] setObject:[[stateNameArray valueForKey:@"name"]objectAtIndex:row] forKey:@"stateName"];
            }
        }
    }
    
    else if ([selectArrayString isEqualToString:@"4"])
    {
        if (universityNameArray.count>0)
        {
            checkClick = YES;
            
            if (![_universityNameTextField.text isEqualToString:[[universityNameArray valueForKey:@"name"]objectAtIndex:row]])
            {
                if ([[universityNameArray valueForKey:@"name"]objectAtIndex:row])
                {
                    _universityNameTextField.text =[[universityNameArray valueForKey:@"name"]objectAtIndex:row];
                    
                    strUniId = [NSString stringWithFormat:@"%@",[[universityNameArray valueForKey:@"id"]objectAtIndex:row]];
                    
                    _positionNameTextField.text = @"";
                    _textFieldDisciplinesName.text = @"";
                    _textFldSubdiscipline2.text = @"";
                    _textFldDiscipline2.text = @"";
                    _subDisciplineTextField.text = @"";
                }
            }
        }
    }
    else if ([selectArrayString isEqualToString:@"5"])
    {
        if (positionNameArray.count>0)
        {
            if ([[positionNameArray valueForKey:@"name"] objectAtIndex:row])
            {
                _positionNameTextField.text = [[positionNameArray valueForKey:@"name"] objectAtIndex:row];
                stringPositionId =[NSString stringWithFormat:@"%@", [[positionNameArray valueForKey:@"id"] objectAtIndex:row]];
            }
        }
    }
    else if ([selectArrayString isEqualToString:@"6"])
    {
        if (arrayDisciplinesNames.count>0)
        {
            if (![[_textFieldDisciplinesName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:[[arrayDisciplinesNames objectAtIndex:row]objectForKey:@"name"]])
            {
                strDisciplineId =[NSString stringWithFormat:@"%@",[[arrayDisciplinesNames objectAtIndex:row] objectForKey:@"id"]];
                _subDisciplineTextField.text = @"";
                strSubDiscipId = @"";
            }
            
            if ([[arrayDisciplinesNames objectAtIndex:row]objectForKey:@"name"])
            {
                _textFieldDisciplinesName.text = [[arrayDisciplinesNames objectAtIndex:row]objectForKey:@"name"];
            }
            
            strSelectedDiscp = @"1";
            [self getAllSubdisciplines:nil];
        }
    }
    
    else if ([selectArrayString isEqualToString:@"7"])
    {
        if (subdisciplineNameArray.count>0) {
            if ([[subdisciplineNameArray objectAtIndex:row]objectForKey:@"name"])
            {
                if (![[[subdisciplineNameArray objectAtIndex:row]objectForKey:@"name"] isEqualToString:@"None"])
                {
                    _subDisciplineTextField.text = [[subdisciplineNameArray objectAtIndex:row]objectForKey:@"name"];
                    strSubDiscipId = [[subdisciplineNameArray objectAtIndex:row]objectForKey:@"id"];
                }
                else
                {
                    _subDisciplineTextField.text = @"";
                    strSubDiscipId = @"";
                }
            }
        }
    }
    
    else if ([selectArrayString isEqualToString:@"8"])
    {
        if (arrayDisciplinesNames.count>0)
        {
            if (![[_textFldDiscipline2.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:[[arrayDisciplinesNames objectAtIndex:row]objectForKey:@"name"]])
            {
                strDiscipline2Id =[NSString stringWithFormat:@"%@",[[arrayDisciplinesNames objectAtIndex:row] objectForKey:@"id"]];
                _textFldSubdiscipline2.text = @"";
                _textFldDiscipline2.text = @"";
                strSubDiscip2Id = @"";
            }
            
            if ([[arrayDisciplinesNames objectAtIndex:row]objectForKey:@"name"])
            {
                if (![[[arrayDisciplinesNames objectAtIndex:row]objectForKey:@"name"] isEqualToString:@"None"])
                {
                    _textFldDiscipline2.text = [[arrayDisciplinesNames objectAtIndex:row]objectForKey:@"name"];
                }
                strSelectedDiscp = @"2";
                
                if (![[[arrayDisciplinesNames objectAtIndex:row]objectForKey:@"name"] isEqualToString:@"None"])
                {
                    [self getAllSubdisciplines:nil];
                }
            }
        }
    }
    
    else if ([selectArrayString isEqualToString:@"9"])
    {
        if (subdiscipline2NameArray.count>0)
        {
            if (subdiscipline2NameArray)
            {
                if (![[[subdiscipline2NameArray objectAtIndex:row]objectForKey:@"name"] isEqualToString:@"None"])
                {
                    _textFldSubdiscipline2.text = [[subdiscipline2NameArray objectAtIndex:row]objectForKey:@"name"];
                    strSubDiscip2Id = [[subdiscipline2NameArray objectAtIndex:row]objectForKey:@"id"];
                }
                else
                {
                    _textFldSubdiscipline2.text = @"";
                    strSubDiscip2Id = @"";
                }
            }
        }
    }
    
    if (_textFieldDisciplinesName.text.length!=0&&_positionNameTextField.text.length!=0&&_universityNameTextField.text.length!=0&&_stateNameTextField.text.length!=0&&_textFieldCountry.text.length!=0&&_genderTextField.text.length!=0)
    {
        _buttonSubmit.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]  CGColor];
        [_buttonSubmit setTitleColor:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        _buttonSubmit.enabled = YES;
        
        _buttonSubmit.enabled = YES;
        
        _buttonSubmit.userInteractionEnabled = YES;
    }
    
    [self.pickerView setHidden:YES];
    [self.view_Picker setHidden:YES];
    [self viewChange:65];
}


- (IBAction)genderButtonAction:(id)sender
{
    [self showPicker:sender];
}


-(void)showPicker:(id)sender
{
    if ([sender tag] == 5 || [sender tag] == 6 || [sender tag] == 7 || [sender tag] == 8 || [sender tag] == 9)
    {
        [self viewChange:-130];
    }
    else
    {
        [self viewChange:65];
    }
    
    if (![sender isSelected])
    {
        selectArrayString =[NSString stringWithFormat:@"%ld",(long)[sender tag]];
        
        if ([sender tag]==9)
        {
            if (subdiscipline2NameArray.count>0)
            {
                self.pickerView.hidden = NO;
                _view_Picker.hidden = NO;
                [self.pickerView reloadAllComponents];
                [self.view  bringSubviewToFront:self.pickerView];
            }
        }
        else
        {
            self.pickerView.hidden = NO;
            _view_Picker.hidden = NO;
            [self.pickerView reloadAllComponents];
            [self.view  bringSubviewToFront:self.pickerView];
        }
    }
    if ([sender tag] == 3 )
    {
        if (_textFieldCountry.text.length == 0)
        {
            self.pickerView.hidden = YES;
            _view_Picker.hidden = YES;
        }
    }
    else if ([sender tag] == 4)
    {
        if (_stateNameTextField.text.length == 0)
        {
            self.pickerView.hidden = YES;
            _view_Picker.hidden = YES;
        }
    }
    else if ([sender tag] == 5)
    {
        if (_textFieldCountry.text.length == 0)
        {
            self.pickerView.hidden = YES;
            _view_Picker.hidden = YES;
            [self viewChange:65];
        }
    }
    else if ([sender tag] == 7 )
    {
        if (_textFieldDisciplinesName.text.length == 0)
        {
            self.pickerView.hidden = YES;
            _view_Picker.hidden = YES;
            [self viewChange:65];
        }
    }
    else if ([sender tag] == 9 )
    {
        if ([_textFldDiscipline2.text isEqualToString:@""])
        {
            self.pickerView.hidden = YES;
            _view_Picker.hidden = YES;
            [self viewChange:65];
        }
    }
}

#pragma mark- UIActivityIndicatorView
-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        [self.view.window setUserInteractionEnabled:NO];
        spinner.center = self.view.center;
        spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [spinner startAnimating];
        [self.view addSubview:spinner];
    }
    else
    {
        [spinner stopAnimating];
        [self.view.window setUserInteractionEnabled:YES];
        [spinner removeFromSuperview];
        spinner = nil;
    }
}

#pragma mark- BuildInButtonActions

- (IBAction)btnActionCancel:(id)sender
{
    [_view_Picker setHidden:YES];
    [_pickerView setHidden:YES];
    [self viewChange:65];
}

-(void)locationLatitudeLongitude
{
    //http://maps.google.com/maps/api/geocode/json?sensor=false&address=australia
    
    [self ShowDataLoader:YES];
    
    
    NSString *string = [NSString stringWithFormat:@"%@%@", @"http://maps.google.com/maps/api/geocode/json?sensor=false&address=",[[NSUserDefaults standardUserDefaults]valueForKey:@"countryName"]];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:string parameters: nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"response google is %@", responseObject);
         
         NSDictionary *dictgeom = [[responseObject valueForKeyPath:@"results.geometry"]objectAtIndex:0];
         
         NSDictionary *dicLoc = [dictgeom valueForKey:@"location"];
         
         NSLog(@"dic loc is %@", dicLoc);
         
         float latitude = [[dicLoc valueForKey:@"lat"] floatValue];
         
         float longitude = [[dicLoc valueForKey:@"lng"] floatValue];
         
         NSLog(@"View Controller get Location Logitute : %f",latitude);
         
         NSLog(@"View Controller get Location Latitute : %f",longitude);
         
         [USERDEFAULT removeObjectForKey:LATITUDE_COUNTRY];
         
         [USERDEFAULT removeObjectForKey:LONGITUDE_COUNTRY];
         
         [USERDEFAULT setObject:[dicLoc valueForKey:@"lat"] forKey:LATITUDE_COUNTRY];
         
         [USERDEFAULT setObject:[dicLoc valueForKey:@"lng"] forKey:LONGITUDE_COUNTRY];
         
         NSLog(@"value in cntryLatitude detail %@", PREF(LATITUDE_COUNTRY));
         
         NSLog(@"value in cntryLongitude detail %@", PREF(LONGITUDE_COUNTRY));
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
         }
         
         else if ([[responseObject objectForKey:@"status"] isEqualToString:@"false"])
         {
         }
         
         else
         {
             NSLog(@"wrong result");
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}

- (IBAction)submitDetailsButtonAction:(id)sender
{
    stringPortCompanyScreen = @"";
    
    [self saveValuesToUserDefault];
    
    
    [self locationLatitudeLongitude];
    
    [self viewChange:65];
    
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    [[NSUserDefaults standardUserDefaults]setObject:_textFieldDisciplinesName.text forKey:@"userDiscipline"];
    
    if ([_genderTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0 || [_stateNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0 || [_universityNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0 ||[_positionNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0 || [_textFieldDisciplinesName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0 ||[_textFieldCountry.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)
    {
        [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"\nPlease fill all required the Fields" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
        
        self.pickerView.hidden = YES;
        
        _view_Picker.hidden = YES;
    }
    
    else
    {
        if ([_buttonSubmit.titleLabel.text isEqualToString:@"Update"])
        {
            [self updateStudentDetail];
        }
        else
            [self callWebserviceAddStudent];
    }
}

#pragma mark- UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([strFromButton isEqualToString:@"menu"])
    {
        PortfolioViewController *portView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
        UINavigationController * navPort = [[UINavigationController alloc]initWithRootViewController:portView];
        [self.revealViewController setFrontViewController:navPort animated:NO];
    }
}

@end
