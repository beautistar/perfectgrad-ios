//
//  PortfolioTableCell.h
//  PerfectGraduate
//
//  Created by netset on 3/13/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface PortfolioTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnProfile;

@property (weak, nonatomic) IBOutlet AsyncImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *CompanyImageView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionlabel;
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) IBOutlet UIButton *buttonMessageOutlet;
@property (strong, nonatomic) IBOutlet UILabel *labelDate;
@property (strong, nonatomic) IBOutlet UIButton *labelMessageCount;

@property (strong, nonatomic) IBOutlet UIImageView *imageAwardOne;
@property (strong, nonatomic) IBOutlet UIImageView *imageAwardTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imageAwardThree;
@property (strong, nonatomic) IBOutlet UIImageView *imageAwardFour;
@property (strong, nonatomic) IBOutlet UIImageView *imageAwardFive;
@property (strong, nonatomic) IBOutlet UIButton *btnBackground;



@end
