//
//  CompanyMoreInfoVC.h
//  PerfectGraduate
//
//  Created by netset on 4/9/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyMoreInfoVC : UIViewController<UIWebViewDelegate>

@property (strong,nonatomic) NSArray *arrLink;

@property (strong, nonatomic) IBOutlet UIWebView *webViewSocial;
@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;

@property (nonatomic) int intTag;
@end
