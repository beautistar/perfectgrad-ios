//
//  SalaryGuideTableViewCell.h
//  PerfectGraduate
//
//  Created by NSPL on 2/3/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalaryGuideTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageVwOccupationTitleCell;
@property (strong, nonatomic) IBOutlet UIImageView *imageVwTitleExprnceCell;
@property (strong, nonatomic) IBOutlet UILabel *labelOccuptaionNameCell;
@property (strong, nonatomic) IBOutlet UILabel *labelTitleNameCell;
@property (strong, nonatomic) IBOutlet UILabel *labelExperienceCell;
@property (strong, nonatomic) IBOutlet UILabel *label3To5ExpCell;
@property (strong, nonatomic) IBOutlet UILabel *label0To3ExpCell;
@property (strong, nonatomic) IBOutlet UILabel *label5To7ExpCell;

@end
