

//
//  OpenMapViewController.m
//  PerfectGraduate
//
//  Created by netset on 30/08/16.
//  Copyright © 2016 netset. All rights reserved.
//


#import "OpenMapViewController.h"
#import "PortfolioViewController.h"
#import "DisciplineViewController.h"
#import "SalaryGuideViewController.h"
#import "MessagesViewController.h"
#import "LocationFilterSearchViewController.h"
#import "Globals.h"
#import "AppDelegate.h"

@interface OpenMapViewController ()
{
    float latitude;
    
    float longitude;
}

@end

@implementation OpenMapViewController

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];

}

-(void)viewWillAppear:(BOOL)animated
{
    
    stringForNotifyEditCompany = @"";
    
    if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"] )
    {
     self.imageRedStar.hidden = YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(redStarShow)
                                                 name:@"redStarShow"
                                               object:nil];
}

-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, _imageRedStar.frame.origin.y, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    
    self.imageRedStar.hidden = YES;
    
    _buttonMessages.backgroundColor = [UIColor whiteColor];
    [_buttonMessages setImage:[UIImage imageNamed:@"messagesSelect"] forState:UIControlStateNormal];
    _labelmessag.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];

    self.title = @"Event Location";
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
[self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:18], NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    self.navigationItem.hidesBackButton = YES;
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStyleBordered target:self action:@selector(backBtnAction)];
    
    self.navigationItem.leftBarButtonItem = leftItem;
    
//    _mapViewGoogle.hidden = YES;
    
    [self loadMapView];
}

-(void)backBtnAction
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)loadMapView
{
    // longitude value76.708511
    //latitude value30.713876
    
//     _mapViewGoogle.hidden = NO;
    
    CLLocationCoordinate2D coord;
    
    GMSCameraPosition *position;
    
    if (latitudeEvent==0.0 && longitudeEvent==0.0)
    {
        coord.latitude = [PREF(LATITUDE_COUNTRY)floatValue];
        
        coord.longitude = [PREF(LONGITUDE_COUNTRY)floatValue];
        
      position = [GMSCameraPosition cameraWithLatitude:coord.latitude
                                                                  longitude:coord.longitude
                                                                       zoom:5];
    }
    
    else
    {
    coord.latitude = latitudeEvent;
    
    coord.longitude = longitudeEvent;
        
    position = [GMSCameraPosition cameraWithLatitude:coord.latitude
                                                                  longitude:coord.longitude
                                                                       zoom:15];
    }
      _mapViewGoogle.camera = position;
    
    _mapViewGoogle.mapType = kGMSTypeNormal;
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(coord.latitude, coord.longitude);
    marker.title = [_dictDataMap valueForKey:@"event_venue"];
    marker.appearAnimation = kGMSMarkerAnimationPop;
    
    marker.map = _mapViewGoogle;
}

-(void)locationLatitudeLongitude
{
    //http://maps.google.com/maps/api/geocode/json?sensor=false&address=australia
    
   // [self ShowDataLoader:YES];
    
    // double latitude = 0, longitude = 0;`
    
    NSString *string = [NSString stringWithFormat:@"%@%@", @"http://maps.google.com/maps/api/geocode/json?sensor=false&address=",[_dictDataMap valueForKey:@"event_venue"]];
    
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:string parameters: nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"response google is %@", responseObject);
         
         if ([[responseObject valueForKey:@"results"]count]==0)
         {
             
         }
         
         else
         {
         
         NSDictionary *dictgeom = [[responseObject valueForKeyPath:@"results.geometry"]objectAtIndex:0];
         
         NSDictionary *dicLoc = [dictgeom valueForKey:@"location"];
         
         NSLog(@"dic loc is %@", dicLoc);
         
          latitude = [[dicLoc valueForKey:@"lat"] floatValue];
         
        longitude = [[dicLoc valueForKey:@"lng"] floatValue];
         
         NSLog(@"View Controller get Location Logitute : %f",latitude);
         
         NSLog(@"View Controller get Location Latitute : %f",longitude);
             
              [self loadMapView];
         
        if ([[responseObject objectForKey:@"status"] isEqualToString:@"OK"])
         {
             
         }
         
         else if ([[responseObject objectForKey:@"status"] isEqualToString:@"false"])
         {
         }
         
         else
         {
             NSLog(@"wrong result");
         }
             
         }
     }
     
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
     
     {
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}

#pragma mark- tabBarButtonActions

- (IBAction)buttonActionPortfolio:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[PortfolioViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    PortfolioViewController *portfolioView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
    [self.navigationController pushViewController:portfolioView animated:NO];
}


- (IBAction)buttonActionDiscip:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[DisciplineViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    DisciplineViewController *disciplineVw = [self.storyboard instantiateViewControllerWithIdentifier:@"disciplineView"];
    [self.navigationController pushViewController:disciplineVw animated:NO];
}

- (IBAction)buttonSearch:(id)sender;
{
    self.navigationController.navigationBar.hidden = NO;
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[LocationFilterSearchViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
 LocationFilterSearchViewController  *searchVw = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVw"];
    
    [self.navigationController pushViewController:searchVw animated:NO];
}

- (IBAction)buttonMessages:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[MessagesViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    MessagesViewController  *messageVw = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    [self.navigationController pushViewController:messageVw animated:YES];
    _labelmessag.textColor = [UIColor blueColor];
}

- (IBAction)buttonActionSalary:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[SalaryGuideViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    SalaryGuideViewController *salaryVw = [self.storyboard instantiateViewControllerWithIdentifier:@"salaryVw"];
    [self.navigationController pushViewController:salaryVw animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end



