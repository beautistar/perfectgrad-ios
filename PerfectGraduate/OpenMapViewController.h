

//
//  OpenMapViewController.h
//  PerfectGraduate
//
//  Created by netset on 30/08/16.
//  Copyright © 2016 netset. All rights reserved.
//



#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>

@interface OpenMapViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;

@property (strong, nonatomic) IBOutlet GMSMapView *mapViewGoogle;
@property (strong, nonatomic) IBOutlet UILabel *labelmessag;
@property (strong, nonatomic) IBOutlet UIButton *buttonPortfolio;
@property (strong, nonatomic) IBOutlet UIButton *buttonDiscipline;
@property (strong, nonatomic) IBOutlet UIButton *buttonSearch;
@property (strong, nonatomic) IBOutlet UIButton *buttonMessages;
@property (strong, nonatomic) IBOutlet UIButton *buttonSalaryguide;
@property (strong, nonatomic) IBOutlet UILabel *labelPortfolio;
@property (strong, nonatomic) IBOutlet UILabel *labelDiscipline;
@property (strong, nonatomic) IBOutlet UILabel *labelSearchVw;
@property (strong, nonatomic) IBOutlet UILabel *labelSalaryGuide;
@property(strong,nonatomic)NSDictionary *dictDataMap;


@end




