//
//  ApiClass.h
//  PerfectGraduate
//
//  Created by netset on 22/08/16.
//  Copyright © 2016 netset. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"


@interface ApiClass : NSObject

+(ApiClass*)getSharedInstance;


- (void)PostWebserviceCall:(NSString *)URLString
                parameters:(id)parameters
                   success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

@end
