//
//  ViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 1/30/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginTempUser.h"
#import "InformationVC.h"
#import "SocietyDetailsVc.h"
#import "MenuViewController.h"
#import "CircleProgressBar.h"

@class CircleProgressBar;

typedef enum : NSUInteger {
    CustomizationStateDefault = 0,
    CustomizationStateCustom,
    CustomizationStateCustomAttributed,
} CustomizationState;


@interface ViewController : UIViewController
{
NSDictionary *data;
}
- (IBAction)loginWithFacebookAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *facebookLoginButton;
@property (weak, nonatomic) IBOutlet UIButton *buttonInfo;
@property(nonatomic,retain) NSDictionary *data;
@property (strong, nonatomic) NSDictionary *dictFbCall;
@property (strong, nonatomic) IBOutlet UIButton *buttonTempLogin;
@property (weak, nonatomic) IBOutlet CircleProgressBar *circleProgressBar;

- (IBAction)buttonTempAction:(id)sender;
- (IBAction)buttonInfoAction:(id)sender;

@end

