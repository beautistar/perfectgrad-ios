
//
//  AppDelegate.h
//  PerfectGraduate
//
//  Created by NSPL on 1/30/15.
//  Copyright (c) 2015 netset. All rights reserved.


// http://52.5.162.191/perfect/assets/uploads/companyLogo/images.png

//#define  strImageUrl @"http://perfectgraduate.com/perfect/assets/uploads/"

//http://perfectgraduate.com/perfect/assets/uploads/defaultIcons/Youtube-app-logo.jpg
//http://perfectgraduate.com/perfect/assets/uploads/defaultIcons/Twitter_Icon.png

//http://52.5.162.191/perfect/assets/uploads/defaultsocial/images.png


#define  strImageUrl @"http://www.perfectgraduate.com/perfect/assets/uploads/"

//com.perfectgraduate.iphone
//com.perfectgraduate.glod.rain.iphone

//1538546279756867 oldFBid
//1830594657200956 newFBid

//#define GoogleApiKey @"AIzaSyAf3zXfhDQNW4f0SG4XF8P1u5bfoxWQJtE"
#define GoogleApiKey @"AIzaSyCU27fqRflvMmdO-cQMnBXoFNVrXNYRuJU" //new

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "SWRevealViewController.h"
#import "IQKeyboardManager.h"
#import "MyAppManager.h"

@import GoogleMaps;

NSArray *arrCountries;
NSMutableArray *arrCountryIds;
NSMutableArray *arrStateIds, *arrStates;
NSArray *arrOldStateId, *arrOldCountryId;

BOOL isAllLocations;
BOOL isAllCountrySelected;
BOOL isAllCountrySelectedSearch;

BOOL isInternetOff;

BOOL isCancelClicked;

BOOL showInternetAlert ;
NSString *strBuubleImage;
NSString *strDeviceToken;
NSString *strFromButton, *stringMessageDetail;

extern NSString *stringPortCompanyScreen;

extern BOOL fromLoad, boolFomAppDele;

extern NSArray *arrayCountryData;

extern NSMutableArray *arrayCompanyIdsContain;

extern NSMutableArray *arraySocietyIdsContain;

extern NSString *stringSelectedUnselected;

extern  NSString *stringAdminLogo;

extern  int badge;

extern  NSString *stringNotifiction;

extern NSString *stringFilterScreen;

extern NSMutableDictionary *dictSelectedMessage;

extern NSString *stringLike, *stringReport;

extern float latitudeEvent;

extern float longitudeEvent;

extern BOOL firstLogin;

extern BOOL messageAlrdyLoad;

extern NSString *stringViewGet;

extern BOOL boolNotifyEditCompanyPort;

extern BOOL boolNotifyEditCompanyMessage;

extern BOOL boolNotifyEditCompanyDiscipline;

extern BOOL boolNotifyEditCompanyList;

extern BOOL boolNotifyEditCompanyDetail;

extern NSString *stringForNotifyEditCompany;

extern NSMutableArray *arrayDisciplineCountryId;

extern NSMutableArray *arrayDisciplineStateId;

extern NSMutableArray *arraySubDisciCountryId;

extern NSMutableArray *arraySubDisciStateId;

extern NSString *stringCheckDisciplines;

extern NSString *stringCheckSubDesc;

extern NSString *stringBackScreen;
extern NSString *stringTakeDataFromPreviousScreen;

extern NSString *stringFromDesciplineView;
extern BOOL checkCopmanyApiCall;

extern NSString *stringGlobalScreen ;

BOOL checkSubDisciFilterClick;

extern BOOL fromTermsCondition;

extern NSMutableArray *arrayCompaniesList;

extern NSMutableArray *arrayPortDataGlobal;

extern NSMutableArray *arrayMessageGlobal;

extern NSString *stringMesagFirstCome;

extern NSString *stringFromSocietyMessage;

extern NSString *stringRequestInvite;

extern NSString *stringBlock;

extern NSMutableArray *arraySocietyGlobal;

extern BOOL fromNotification;

extern NSMutableArray *arrayFilterContry;

extern NSMutableArray *arrayFilterState;

extern NSString *stringAddRemove;

extern BOOL fromLoadDisc;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@property (strong, nonatomic) SWRevealViewController *viewController;

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;

@end




