//
//  ApiClass.m
//  PerfectGraduate
//
//  Created by netset on 22/08/16.
//  Copyright © 2016 netset. All rights reserved.
//

#import "ApiClass.h"

@implementation ApiClass

static ApiClass *sharedInstance = nil;


+(ApiClass*)getSharedInstance
{
    if (!sharedInstance)
    {
        sharedInstance = [[super allocWithZone:NULL]init];
    }
    return sharedInstance;
}

- (void)PostWebserviceCall:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
   AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
   manager.responseSerializer = [AFJSONResponseSerializer serializer];
   manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];

   [manager POST:URLString parameters: parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
 {
     if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
     {
         success(nil,responseObject);
     }
     else if ([[responseObject objectForKey:@"status"] isEqualToString:@"false"])
     {
         success(nil,responseObject);
     }
     else
     {
         NSLog(@"wrong result");
     }
 }
      failure:^(AFHTTPRequestOperation *operation, NSError *error)
 {
//     if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
//     {
//         [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
//     }
//     else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
//     {
//         [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
//     }
//     else
//     {
//         [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
//     }
     
      failure(nil,error);
 }];
    
    


}


@end
