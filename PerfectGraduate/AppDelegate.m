
//  AppDelegate.m
//  PerfectGraduate
//
//  Created by NSPL on 1/30/15.
//  Copyright (c) 2015 netset. All rights reserved.

#import "AppDelegate.h"
#import "StudentDetailsViewController.h"
#import "PortfolioViewController.h"
#import "ViewController.h"
#import "SWRevealViewController.h"
#import "MenuViewController.h"
#import "DisciplineViewController.h"
#import "LocationFilterSearchViewController.h"
#import "MessagesViewController.h"
#import "SalaryGuideViewController.h"
#import "ApiClass.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "MessageSubscriptionManager.h"

NSString *stringAdminLogo;

int badge;

NSString *stringNotifiction;

NSArray *arrayCountryData;

NSMutableArray *arrayCompanyIdsContain;

NSString *stringFilterScreen;

NSMutableDictionary *dictSelectedMessage;

NSString *stringSelectedUnselected;

NSString *stringLike, *stringReport;

NSString *stringViewGet;

float latitudeEvent;

float longitudeEvent;

BOOL firstLogin, messageAlrdyLoad;

BOOL boolNotifyEditCompanyPort;

NSString *stringForNotifyEditCompany;

NSMutableArray *arrayDisciplineCountryId;

NSMutableArray *arrayDisciplineStateId;

NSString *stringCheckDisciplines;

BOOL boolNotifyEditCompanyMessage;

BOOL boolNotifyEditCompanyDiscipline;

BOOL boolNotifyEditCompanyList;

BOOL boolNotifyEditCompanyDetail;

NSMutableArray *arraySubDisciCountryId;

NSMutableArray *arraySubDisciStateId;

NSString *stringCheckSubDesc;

BOOL checkSubDisciFilterClick;

NSString *stringBackScreen;

NSString *stringTakeDataFromPreviousScreen;

BOOL checkCopmanyApiCall;

BOOL fromLoad, boolFomAppDele;

BOOL fromTermsCondition;

NSMutableArray *arrayCompaniesList;

NSString *stringFromDesciplineView;

NSMutableArray *arrayPortDataGlobal;

NSString *stringPortCompanyScreen;

NSString *stringGlobalScreen;

NSMutableArray *arrayMessageGlobal;

NSString *stringMesagFirstCome;

NSString *stringFromSocietyMessage;

NSString *stringRequestInvite;

NSString *stringBlock;

NSMutableArray *arraySocietyGlobal;

BOOL fromNotification;

NSMutableArray *arrayFilterContry;

NSString *stringAddRemove;

BOOL fromLoadDisc;

NSMutableArray *arrayFilterState;

@interface AppDelegate ()
{
    Database *dummy;
    
    NSArray *arrayAllCompaniesPort;
    
    UIActivityIndicatorView *spinner;
}

@end

@implementation AppDelegate

@synthesize viewController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//    NSArray *arrKeys=@[@"FinalStateDataDisp",@"FinalCountryDataDisp",@"OriginalStateData",@"OriginalCountryData",@"FinalStateDataSearch",@"FinalCountryDataSearch"];
//    
//    for (NSString *strKey in arrKeys) {
//        if (![defaults objectForKey:strKey]) {
//            [defaults setObject:[[NSMutableArray alloc] init] forKey:strKey];
//        }
//    }
//   
//    [defaults synchronize];
    
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    Database *dbObj=[[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];

    [GMSServices provideAPIKey:GoogleApiKey];
    
    [Fabric with:@[[Crashlytics class]]];
    
    [[MyAppManager sharedManager] setConfiguration];
    
    arraySocietyGlobal = [[NSMutableArray alloc]init];
    
    stringFilterScreen = @"filterScreen";
    stringPortCompanyScreen = @"";
    stringAdminLogo = @"";
    stringMessageDetail =@"";
    strDeviceToken = @"";
    stringRequestInvite= @"";
    
    arrCountryIds = [[NSMutableArray alloc]init];
    arrStateIds = [[NSMutableArray alloc] init];
    
    arraySubDisciStateId = [[NSMutableArray alloc] init];
    arraySubDisciCountryId = [[NSMutableArray alloc] init];
    
    arrayDisciplineStateId = [[NSMutableArray alloc] init];
    arrayDisciplineCountryId = [[NSMutableArray alloc] init];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"AllCountrySelected"]){
        isAllCountrySelected = [[NSUserDefaults standardUserDefaults] boolForKey:@"AllCountrySelected"];
    }
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"AllCountrySelectedSearch"]){
        isAllCountrySelectedSearch = [[NSUserDefaults standardUserDefaults] boolForKey:@"AllCountrySelectedSearch"];
    }
    
    arrayCompaniesList = [[NSMutableArray alloc]init];
    arrayPortDataGlobal = [[NSMutableArray alloc]init];
    arrStates = [[NSMutableArray alloc] init];
    
    arrayMessageGlobal = [[NSMutableArray alloc]init];
    
    arrayFilterContry = [[NSMutableArray alloc]init];
    
    arrayFilterState = [NSMutableArray new];
    
    fromLoad = YES;
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callPortWebService"];
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callMsgWebService"];
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callallCompanyWebService"];
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callDesciplineWebService"];
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"salaryGuide"];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self.window setBackgroundColor:[UIColor whiteColor]];
    
    UIWindow * window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window = window;
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MenuViewController * LeftRevalView = [storyBoard instantiateViewControllerWithIdentifier:@"menuView"];
    UINavigationController * nav1 = [[UINavigationController alloc]initWithRootViewController:LeftRevalView];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        
#if IPHONE_OS_VERSION_MAX_ALLOWED >= IPHONE_8_0 //__IPHONE_8_0 is not defined in old xcode (==0). Then use 80000
        
        //        NSLog(@"registerForPushNotification: For iOS >= 8.0");
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:
         [UIUserNotificationSettings settingsForTypes:
          (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge)
                                           categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
#endif
    }
    else
    {
        //        NSLog(@"registerForPushNotification: For iOS < 8.0");
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"studentviewKey"])
    {
        dummy=[[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [self getAllCompaniesSearch];
        });
        
        
        PortfolioViewController *portfolioView = [storyBoard instantiateViewControllerWithIdentifier:@"portfolio"];
        UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:portfolioView];
        
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:nav1 frontViewController:nav];
        self.viewController = revealController;
        self.window.rootViewController = self.viewController;
        [self.window makeKeyAndVisible];
    }
    
    else if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userId"])
    {
        StudentDetailsViewController *detailsView = [storyBoard instantiateViewControllerWithIdentifier:@"detailsView"];
        UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:detailsView];
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:nav1 frontViewController:nav];
        self.viewController = revealController;
        self.window.rootViewController = self.viewController;
        [self.window makeKeyAndVisible];
    }
    else
    {
        ViewController * loginView = [storyBoard instantiateViewControllerWithIdentifier:@"PushMainView"];
        UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:loginView];
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:nav1 frontViewController:nav];
        self.viewController = revealController;
        self.window.rootViewController = self.viewController;
        [self.window makeKeyAndVisible];
    }
    
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded)
    {
        // If there's one, just open the session silently, without showing the user the login UI
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"]
                                           allowLoginUI:NO
                                      completionHandler:^(FBSession *sessison, FBSessionState state, NSError *error)
         {
             // Handler for session state changes
             // Call this method EACH time the session state changes,
             //  NOT just when the session open
             [self sessionStateChanged:sessison state:state error:error];
         }];
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getAllCompaniesSearch) name:@"facebookCompnies" object:nil];
    
    return YES;
}

#pragma mark - GetAllCompanies

-(void)getAllCompaniesSearch
{
    boolFomAppDele = YES;
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@getAllCompanies",baseUrl];
    
    NSDictionary *dicParam = @{@"id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"get all companis");
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             arrayAllCompaniesPort = [responseObject valueForKeyPath:@"data"];
             
             [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"callallCompanyWebService"];
             
             [[NSUserDefaults standardUserDefaults] synchronize];
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                 
                 [self saveSearchCompanies];
             });
             
             NSArray *arrCompanies = [[NSArray alloc]initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]];
             
             [[NSUserDefaults standardUserDefaults]setObject:arrCompanies forKey:@"allCompanies"];
             
             NSArray *arrayTemp = [arrCompanies copy];
             
             NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
             
             NSArray* sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
             
             NSArray *arraySorted = [[NSArray alloc] initWithArray:[arrayTemp sortedArrayUsingDescriptors:sortDescriptors]];
             
             [arrayCompaniesList removeAllObjects];
             
             arrayCompaniesList = [arraySorted mutableCopy];
             
             NSLog(@"get arrayCompaniesList arrayCompaniesList");
             
             [[NSNotificationCenter defaultCenter]
              postNotificationName:@"hideHud"
              object:self];
         }
         else
         {
             if ([[responseObject objectForKey:@"data"]isEqualToString:@"no result"])
             {
                 
             }
         }
     }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             //[[[UIAlertView alloc]initWithTitle:@"No Internet " message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"No Internet " message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         
         [[NSNotificationCenter defaultCenter]
          postNotificationName:@"hideHud"
          object:self];
     }];
}


-(void)saveSearchCompanies
{
    if ([dummy deleteTable:@"delete from COMPANIES"])
    {
        if ([dummy deleteTable:@"delete from COMPANYLOCATIONS"])
        {
            if ([dummy deleteTable:@"delete from DispSubDispCompaniesTable"])
            {
                if ([dummy deleteTable:@"delete from SearchCompSubDisciplineTable"])
                {
                    if ([dummy deleteTable:@"delete from SearchCompanyAwards"])
                    {
                        if ([dummy deleteTable:@"delete from SearchCompanyDueDate"])
                        {
                            if ([dummy deleteTable:@"delete from SearchQuesAnsTable"])
                            {
                                if ([dummy deleteTable:@"delete from SearchCompSocialLinksTable"])
                                {
                                    for (int i=0; i<[arrayAllCompaniesPort count]; i++)
                                    {
                                        NSString *query = [NSString stringWithFormat: @"INSERT INTO COMPANIES (id,application_due_date, awards,city,company_address,company_name,description,logo,state_id,regstates,country_id,company_intro,country,portfolio_status,message_state,message_status, website,international,unread_message) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")"
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"id"]
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"application_due_date"]
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"awards"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"city"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"company_address"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"company_name"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"description"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"logo"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"state_id"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"regstates"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"country_id"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"company_intro"]
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"country"]
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"portfolio_status"]
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"message_state"]
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"message_status"],
                                                           [[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"website"],[[arrayAllCompaniesPort valueForKey:@"international"] objectAtIndex:i],[[arrayAllCompaniesPort valueForKey:@"unread_message"] objectAtIndex:i]];
                                        
                                        [self saveImages:[NSString stringWithFormat:@"%@%@",[[arrayAllCompaniesPort  objectAtIndex:i] valueForKeyPath:@"id"],[[arrayAllCompaniesPort  objectAtIndex:i] valueForKeyPath:@"logo"]] imageUrl:[[arrayAllCompaniesPort  objectAtIndex:i] valueForKeyPath:@"logo"]];
                                        
                                        const char *query_stmt = [query UTF8String];
                                        [dummy  runQueries:query_stmt strForscreen:@"search"];
                                        
                                        [self saveDueDateSearchCompanies:i];
                                        [self saveCompanyLocations:i];
                                        [self saveCompanyDisciplines:i];
                                        [self saveSearchCompanyAwards:i];
                                        [self saveQuestionsSearchComp:i];
                                        [self saveSearchCompanyLinks:i];
                                        [self saveCompanySubDisciplines:i];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

-(void)saveImages:(NSString *)ImageName imageUrl :(NSString *)imageUrl
{
    ImageName = [ImageName stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    if (imageUrl.length>0)
    {
        if (![self imageCheck:ImageName])
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:ImageName];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,imageUrl]]];
            [imageData writeToFile:savedImagePath atomically:NO];
        }
    }
}

-(BOOL)imageCheck :(NSString *)strName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    if ([contents containsObject:strName])
    {
        return YES;
    }
    return NO;
}


-(void)saveSearchCompanyLinks:(int)index
{
    if ([[[arrayAllCompaniesPort valueForKey:@"links"] objectAtIndex:index]isKindOfClass:[NSNull class]])
    {
        
    }
    else
    {
        for (int k=0; k<[[[arrayAllCompaniesPort valueForKey:@"links"] objectAtIndex:index] count]; k++)
        {
            NSString *subQueryLinks;
            
            subQueryLinks = [NSString stringWithFormat: @"INSERT INTO SearchCompSocialLinksTable (company_id,id,media_link,media_logo,media_type) VALUES (\"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                             ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"links.company_id"] objectAtIndex:k]
                             ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"links.id"] objectAtIndex:k]
                             ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"links.media_link"] objectAtIndex:k]
                             ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"links.media_logo"] objectAtIndex:k]
                             ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"links.media_type"] objectAtIndex:k]];
            
            [self saveImages:[[NSString stringWithFormat:@"%@%@",[[[arrayAllCompaniesPort valueForKeyPath:@"links.id"] objectAtIndex:index] objectAtIndex:k],[[[arrayAllCompaniesPort valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                    imageUrl:[[[arrayAllCompaniesPort valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]];
            
            const char *query_stmt_SocialLinks = [subQueryLinks UTF8String];
            [dummy runQuerySocialLinksForCompanies:query_stmt_SocialLinks strForscreen:@"search"];
        }
    }
}

-(void)saveSearchCompanyAwards:(int)index
{
    for (int k=0; k<[[[arrayAllCompaniesPort objectAtIndex:index] valueForKey:@"allAwards"] count]; k++)
    {
        NSString *subQueryAwards;
        subQueryAwards = [NSString stringWithFormat: @"INSERT INTO SearchCompanyAwards (awards,company_id,description, id) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                          ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"allAwards.awards"] objectAtIndex:k]
                          ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"allAwards.company_id"] objectAtIndex:k]
                          ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"allAwards.description"] objectAtIndex:k]
                          ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"allAwards.id"] objectAtIndex:k]];
        
        const char *query_stmt_Awards= [subQueryAwards UTF8String];
        
        [dummy runSubQueryToSaveAwards:query_stmt_Awards strForscreen:@"SearchVw"];
    }
}

-(void)saveDueDateSearchCompanies:(int)index
{
    for (int j=0; j<[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all"] count]; j++)
    {
        NSString *subQueryDueDate;
        
        subQueryDueDate = [NSString stringWithFormat:@"INSERT INTO SearchCompanyDueDate(close_date,company_id,id,program,program_id,start_date,state,country) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.close_date"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.company_id"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.id"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.program"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.program_id"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.start_date"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.state"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.country"] objectAtIndex:j]];
        
        const char *query_stmt_dueDate = [subQueryDueDate UTF8String];
        
        [dummy runSubQueryToSaveDueDate:query_stmt_dueDate strForscreen:@"Search"];
    }
}

-(void)saveCompanyDisciplines:(int)index
{
    for (int k=0; k<[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines"] count]; k++)
    {
        NSString *subQuery;
        
        subQuery = [NSString stringWithFormat: @"INSERT INTO DispSubDispCompaniesTable (company_id,discipline_id,name,type) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:k]
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.discipline_id"] objectAtIndex:k]
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.name"] objectAtIndex:k],[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.type"] objectAtIndex:k]];
        
        const char *query_stmt_disciplines = [subQuery UTF8String];
        [dummy runQueryDispSubDispForCompanies:query_stmt_disciplines strForscreen:@"searchCompanyDisciplines"];
        [self saveSubDisciplinesPort1:index disciplineTag:k];
        
    }
}

-(void)saveCompanySubDisciplines:(int)index
{
    for (int k=0; k<[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines"] count]; k++)
    {
        NSString *subQuery;
        
        NSArray *arraySubDisciplineId = [[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline.sub_discipline_id"] objectAtIndex:k];
        
        for (int i=0; i<arraySubDisciplineId.count; i++)
        {
            subQuery = [NSString stringWithFormat: @"INSERT INTO DispSubDispDataTable (company_id,discipline_id,name,sub_discipline_id) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                        ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:k]
                        ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.discipline_id"] objectAtIndex:k]
                        ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.name"] objectAtIndex:k],[arraySubDisciplineId objectAtIndex:i]];
            
            const char *query_stmt_disciplines = [subQuery UTF8String];
            [dummy runQueryDispSubDispFordisp:query_stmt_disciplines strForscreen:@"searchCompanyDisciplines"];
        }
    }
}


-(void)saveSubDisciplinesPort1:(int)index disciplineTag:(int)disciplineTag
{
    for (int j=0; j<[[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag] count]; j++)
    {
        NSString *subQuery;
        
        subQuery = [NSString stringWithFormat: @"INSERT INTO SearchCompSubDisciplineTable (discipline_id,name,company_id,sub_discipline_id) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                    ,[[[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"discipline_id"] objectAtIndex:j]
                    ,[[[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"name"] objectAtIndex:j]
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:disciplineTag]
                    ,[[[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"sub_discipline_id"] objectAtIndex:j]
                    ];
        
        const char *query_stmt_PortSubDiscip = [subQuery UTF8String];
        
        [dummy runQuerySubDisciplines:query_stmt_PortSubDiscip strForscreen:@"SubDiscipSearchCompanies"];
    }
}

-(void)saveCompanyLocations:(int)index
{
    for (int k=0; k<[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location"] count]; k++)
    {
        NSString *subQuery = [NSString stringWithFormat: @"INSERT INTO COMPANYLOCATIONS (company_id,id,branch_name, lat,long,location) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\")"
                              ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location.company_id"] objectAtIndex:k]
                              ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location.id"] objectAtIndex:k]
                              ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location.branch_name"] objectAtIndex:k]
                              ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location.lat"] objectAtIndex:k]
                              ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location.long"] objectAtIndex:k]
                              ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location.location"] objectAtIndex:k]];
        
        const char *query_stmtLocation = [subQuery UTF8String];
        [dummy runSubQueryLocations:query_stmtLocation strForscreen:@"COMPANYLOCATIONS"];
    }
}

-(void)saveQuestionsSearchComp:(int)index
{
    for (int k=0; k<[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"quesAns"] count]; k++)
    {
        NSString *subQuery;
        subQuery = [NSString stringWithFormat: @"INSERT INTO SearchQuesAnsTable (answer,company_id,id,question) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"quesAns.answer"] objectAtIndex:k]
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"quesAns.company_id"] objectAtIndex:k]
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"quesAns.id"] objectAtIndex:k]
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"quesAns.question"] objectAtIndex:k]];
        const char *query_stmt_QuesAns = [subQuery UTF8String];
        [dummy runQueryQuesAnsForCompanies:query_stmt_QuesAns strForscreen:@"search"];
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *string = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    strDeviceToken = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"My token is: %@", [strDeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""]);
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"deviceToken"];
    
    [[NSUserDefaults standardUserDefaults]setObject:strDeviceToken forKey:@"deviceToken"];
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    NSLog(@"Notification received: %@", userInfo);
    
    NSDictionary *dict = [userInfo valueForKey:@"aps"];
    
    badge = [[dict valueForKey:@"badge"]intValue];
    
    [[NSUserDefaults standardUserDefaults]setInteger:badge forKey:@"badge"];
    
    NSLog(@"badge is unread %ld", (long)[[NSUserDefaults standardUserDefaults]integerForKey:@"badge"]);
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = [[dict objectForKey:@"badge"] intValue];
    
    if ([[dict valueForKey:@"msg"]isEqualToString:@"Succesfully Register"])
    {
        
    }
    
    else if ([[dict valueForKey:@"msg"]isEqualToString:@"User Already Registered"])
    {
        
    }
    
    else if ([[dict valueForKey:@"alert"]isEqualToString:@"User Already Registered"])
    {
        
    }
    
    else if ([[dict valueForKey:@"msg"]isEqualToString:@"Company Edited successfully"])
    {
        
    }
    
    else
    {
        [self showAlertViewOfNotification:dict];
    }
    
    if ([[dict valueForKey:@"msg"]isEqualToString:@"Message added"])
    {
        [[NSUserDefaults standardUserDefaults ]setObject:@"show" forKey:@"redStar"];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"redStarShow"
         object:self];
    }
    
    if ([[dict valueForKey:@"msg"]isEqualToString:@"Message from Society"])
    {
        [USERDEFAULT setObject:@"" forKey:@"FirstCome"];
        
        [[NSUserDefaults standardUserDefaults ]setObject:@"show" forKey:@"redStar"];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"redStarShow"
         object:self];
    }
    
    else if ([[dict valueForKey:@"msg"]isEqualToString:@"Message from Perfect Graduate"])
    {
        [[NSUserDefaults standardUserDefaults ]setObject:@"show" forKey:@"redStar"];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"redStarShow"
         object:self];
    }
}

-(void)showAlertViewOfNotification:(NSDictionary *)dict
{
    
    [[[UIAlertView alloc]initWithTitle:@"Notification" message:[NSString stringWithFormat:@"%@",[dict valueForKey:@"alert"]] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([stringNotifiction isEqualToString:@"frommessage"])
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"messageNotification"
         object:self];
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callPortWebService"];
        
        fromNotification = YES;
    }
    
    else if ([stringNotifiction isEqualToString:@"fromCompany"])
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"companyAddedNotification"
         object:self];
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callPortWebService"];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callMsgWebService"];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callallCompanyWebService"];
    }
    
    else if ([stringNotifiction isEqualToString:@"portfoilio"])
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"callPorfolio"
         object:self];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callMsgWebService"];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callallCompanyWebService"];
    }
    
    else if ([stringNotifiction isEqualToString:@""])
    {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callPortWebService"];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callMsgWebService"];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callallCompanyWebService"];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}
// Handles session state changes in the app

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen)
    {
        //            NSLog(@"Session opened");
        // Show the user the logged-in UI
        [self userLoggedIn];
        return;
    }
    
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed)
    {
        // If the session is closed
        //            NSLog(@"Session closed");
        // Show the user the logged-out UI
        //[self userLoggedOut];
    }
    // Handle errors
    if (error)
    {
        //            NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            // [self showMessage:alertText withTitle:alertTitle];
        }
        
        else
        {
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled)
            {
                //                    NSLog(@"User cancelled login");
                
                // Handle session closures that happen outside of the app
            }
            
            else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession)
            {
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                //  [self showMessage:alertText withTitle:alertTitle];
                
                // Here we will handle all other errors with a generic error message.
                // We recommend you check our Handling Errors guide for more information
                // https://developers.facebook.com/docs/ios/errors/
            }
            
            else
            {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                // [self showMessage:alertText withTitle:alertTitle];
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        // Show the user the logged-out UI
        // [self userLoggedOut];
    }
}

-(void)userLoggedIn
{
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[MessageSubscriptionManager sharedManager] refreshData];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBAppCall handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end





