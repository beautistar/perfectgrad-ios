//
//  ViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 1/30/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "ViewController.h"
#import "StudentDetailsViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "AFHTTPRequestOperationManager.h"
#import "PortfolioViewController.h"

@interface ViewController ()
{
    UIActivityIndicatorView *spinner;
    NSDictionary *dictInfoFB;
}

@end

@implementation ViewController
@synthesize data;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    [_buttonTempLogin setTitleShadowColor:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    _buttonTempLogin.titleLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = self.view.center;
    spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    
    _buttonTempLogin.layer.borderWidth = 2.0f;
    _buttonTempLogin.layer.borderColor = [[UIColor whiteColor]CGColor];
    _buttonTempLogin.layer.cornerRadius = _buttonTempLogin.frame.size.height/2;
    _facebookLoginButton.layer.cornerRadius = _facebookLoginButton.frame.size.height/2;
    _buttonInfo.layer.cornerRadius = 2.0;;
    _buttonInfo.clipsToBounds = YES;
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    stringNotifiction = @"";
    
    stringForNotifyEditCompany = @"";
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    spinner = nil;
}

- (UIStatusBarStyle) preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark:-Session open method
- (void)sessionStateChanged:(NSNotification*)notification
{
    if ([[FBSession activeSession] isOpen])
    {
        [FBRequestConnection
         startForMeWithCompletionHandler:^(FBRequestConnection *connection,
                                           id<FBGraphUser> user,
                                           NSError *error)
         {
             if (! error)
             {
                 //details of user
                 NSLog(@"User details =%@",user);
             }
         }];
    }
    else
    {
        [[FBSession activeSession] closeAndClearTokenInformation];
    }
}

#pragma mark- FacebookLogin Button Action

- (IBAction)loginWithFacebookAction:(id)sender
{
    [spinner startAnimating];
    
    self.view.userInteractionEnabled = NO;
    
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        [self getfacebookuserInfo] ;
    }
    else
    {
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile,email"]
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error)
         {
             if (error)
             {
                 self.view.userInteractionEnabled = YES;
                 [spinner stopAnimating];
             }
             else
                 [self getfacebookuserInfo] ;
         }];
    }
}
#pragma mark-----CallFacebookRegister------

-(void)callWebserviceregister_fb
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    //NSMutableDictionary *parametersFb = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"fb_id",[dictInfoFB objectForKey:@"id"],@"device type",@"I",@"device",strDeviceToken, nil];
    if (strDeviceToken==nil) {
        strDeviceToken=@"2585e86d09a85106c9a579849a35ab95ffc1391d193b5d63e10f87d0d5cd8417";
    }
    
    if (strDeviceToken.length==0) {
        strDeviceToken=@"2585e86d09a85106c9a579849a35ab95ffc1391d193b5d63e10f87d0d5cd8417";
    }
    
    
    NSMutableDictionary *parametersFb = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[dictInfoFB objectForKey:@"id"],@"fb_id",@"I",@"device type",strDeviceToken,@"device", nil];
    
    if ([dictInfoFB objectForKey:@"email"]) {
        [parametersFb setObject:[dictInfoFB objectForKey:@"email"] forKey:@"email"];
    }else{
        [parametersFb setObject:[NSString stringWithFormat:@"%@@facebook.com",[dictInfoFB objectForKey:@"id"]] forKey:@"email"];
    }
    if([dictInfoFB objectForKey:@"first_name"] && [dictInfoFB objectForKey:@"last_name"]){
        [parametersFb setObject:[NSString stringWithFormat:@"%@ %@",[dictInfoFB objectForKey:@"first_name"],[dictInfoFB objectForKey:@"last_name"]] forKey:@"name"];
    }else if([dictInfoFB objectForKey:@"name"]){
        [parametersFb setObject:[NSString stringWithFormat:@"%@",[dictInfoFB objectForKey:@"name"]] forKey:@"name"];
    }
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@register_fb",baseUrl];
    
    [manager POST:urlStr parameters:parametersFb success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [spinner stopAnimating];
         self.view.userInteractionEnabled = YES;
         
         if ([[responseObject objectForKey:@"message"] isEqualToString:@"User Already Registered"])
         {
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callPortWebService"];
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callMsgWebService"];
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callallCompanyWebService"];
             
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callDesciplineWebService"];
             
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"salaryGuide"];
             
             
             [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"FbLogIn"];
             
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"user_id"] forKey:@"userId"];
             
             [[NSUserDefaults standardUserDefaults]synchronize];
             
             if ([[responseObject objectForKey:@"check"] isEqualToString:@"no"])
             {
                 UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                 StudentDetailsViewController *detailsView = [storyBoard instantiateViewControllerWithIdentifier:@"detailsView"];
                 self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:42.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
                 [self.navigationController pushViewController:detailsView animated:YES];
             }
             else
             {
                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"studentviewKey"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
                 
                 [[NSNotificationCenter defaultCenter]
                  postNotificationName:@"facebookCompnies"
                  object:self];
                 
                 PortfolioViewController *portView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
                 self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
                 UINavigationController * navPort = [[UINavigationController alloc]initWithRootViewController:portView];
                 
                 [self.revealViewController setFrontViewController:navPort animated:YES];
             }
         }
         else
         {
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"user_id"] forKey:@"userId"];
             [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"FbLogIn"];
             [[NSUserDefaults standardUserDefaults]synchronize];
             
             UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
             StudentDetailsViewController *detailsView = [storyBoard instantiateViewControllerWithIdentifier:@"detailsView"];
             self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:42.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
             [self.navigationController pushViewController:detailsView animated:YES];
         }
         
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [spinner stopAnimating];
         self.view.userInteractionEnabled = YES;
         
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok",nil]show];
         }
     }];
}

-(void)JsonResponseregister_fb:(id)response
{
    NSDictionary *dictResponse = response;
    if ([[dictResponse valueForKey:@"status"]isEqualToString:@"true"])
    {
        
    }
}

#pragma mark-GetFacebookUserInfo
-(void)getfacebookuserInfo
{
    [FBRequestConnection startWithGraphPath:@"/me"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error)
     {
         if (error)
         {
             self.view.userInteractionEnabled = YES;
             [spinner stopAnimating];
         }
         else
         {
             dictInfoFB = result;
             [[NSUserDefaults standardUserDefaults]  setObject:[dictInfoFB objectForKey:@"id"]  forKey:@"facebookid"];
             [self callWebserviceregister_fb];
         }
     }];
}

- (IBAction)buttonTempAction:(id)sender
{
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    StudentDetailsViewController *detailsView = [storyBoard instantiateViewControllerWithIdentifier:@"detailsView"];
    strFromButton = @"tempLogin";
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:42.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    [self.navigationController pushViewController:detailsView animated:YES];
}

- (IBAction)buttonInfoAction:(id)sender
{
//    [self addLoader];
//    return;
//
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MenuViewController * LeftRevalView = [storyBoard instantiateViewControllerWithIdentifier:@"menuView"];
    UINavigationController * nav1 = [[UINavigationController alloc]initWithRootViewController:LeftRevalView];
    
    InformationVC *infoView = [storyBoard instantiateViewControllerWithIdentifier:@"information"];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:infoView];
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:nav1 frontViewController:nav];
    [self.navigationController presentViewController:revealController animated:YES completion:nil];
}
-(void)addLoader
{
    UIView *viewBG=[[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)];
    viewBG.backgroundColor=[[UIColor whiteColor] colorWithAlphaComponent:0.4];
    viewBG.tag=1992;
    [self.view addSubview:viewBG];
    
    float lblheight=50.0;
    UIImageView *imgView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"progressbar"]];
    imgView.frame=CGRectMake(0.0, 0.0, 90.0, 90.0);
    imgView.backgroundColor=[UIColor clearColor];
    imgView.tag=1991;
    [self.view addSubview:imgView];
    imgView.center=CGPointMake(self.view.center.x, self.view.center.y-(lblheight/2.0));
    
    UILabel *lblLoading=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width*0.15,self.view.center.y-(lblheight/2.0)+(imgView.frame.size.height/2.0)+lblheight*0.15,self.view.frame.size.width*0.7,lblheight*0.7)];
    lblLoading.textColor=[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    lblLoading.font=[UIFont fontWithName:@"OpenSans-Bold" size:25.0];
    lblLoading.adjustsFontSizeToFitWidth=YES;
    lblLoading.text=@"Loading...";
    lblLoading.textAlignment=NSTextAlignmentCenter;
    lblLoading.tag=1993;
    [self.view addSubview:lblLoading];
    
    
    CABasicAnimation *fullRotation;
    fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    fullRotation.duration = 1.2f;
    fullRotation.repeatCount = MAXFLOAT;
    [imgView.layer addAnimation:fullRotation forKey:@"360"];
    
    [self performSelector:@selector(removeLoader) withObject:nil afterDelay:8.0];
}
-(void)removeLoader
{
    if ([self.view viewWithTag:1991])
    {
        UIImageView *imgView=[self.view viewWithTag:1991];
        [imgView.layer removeAnimationForKey:@"360"];
        
        [imgView removeFromSuperview];
    }
    
    if ([self.view viewWithTag:1993])
    {
        UILabel *lblLoading=[self.view viewWithTag:1993];
        [lblLoading removeFromSuperview];
    }
    
    if ([self.view viewWithTag:1992])
    {
        UIView *bgView=[self.view viewWithTag:1992];
        [bgView removeFromSuperview];
    }
}


@end

