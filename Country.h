//
//  Country.h
//  PerfectGraduate
//
//  Created by NSPL on 2/3/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Country : NSObject{
NSString *category;
NSString *name;
}
@property (nonatomic, copy) NSString *category;
@property (nonatomic, copy) NSString *name;

+ (id)countryOfCategory:(NSString*)category name:(NSString*)name;

@end
