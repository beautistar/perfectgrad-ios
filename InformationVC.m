//
//  InformationVC.m
//  PerfectGraduate
//
//  Created by netset on 3/25/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "InformationVC.h"

@interface InformationVC ()
{
    NSDictionary *dictInfoFB;
    UIActivityIndicatorView *spinner;
    
    
}
@end

@implementation InformationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = self.view.center;
    spinner.color = [UIColor whiteColor];
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    
    self.navigationController.navigationBarHidden = YES;
    
    _buttonTempSignin.layer.borderWidth = 2.0f;
    _buttonTempSignin.layer.borderColor = [[UIColor whiteColor]CGColor];
    _buttonTempSignin.layer.cornerRadius = _buttonTempSignin.frame.size.height/2;
    _buttonFbSignIn.layer.cornerRadius = _buttonFbSignIn.frame.size.height/2;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *yearString = [formatter stringFromDate:[NSDate date]];
    
    NSLog(@"year is %@", yearString);
    
    _labelYear.text = [NSString stringWithFormat:@"Perfect Graduate © %@", yearString];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    stringForNotifyEditCompany = @"";
    
    self.navigationController.navigationBarHidden = YES;
    
    stringNotifiction = @"";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)buttonBackAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btnFacebookAction:(id)sender
{
    [spinner startAnimating];
    
    
    self.view.userInteractionEnabled = NO;
    
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        
        [self getfacebookuserInfo] ;
    }
    else
    {
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile,email"]
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error)
         {
             
             if (error)
             {
                 [spinner stopAnimating];
             }
             else
                 [self getfacebookuserInfo] ;
             
         }];
        
    }
    
}

-(void)getfacebookuserInfo
{
    [FBRequestConnection startWithGraphPath:@"/me"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error)
     {
         
         [spinner stopAnimating];
         
         if (error)
         {
             self.view.userInteractionEnabled = YES;
             
         }
         else
         {
             dictInfoFB = result;
             self.view.userInteractionEnabled = YES;
             [[NSUserDefaults standardUserDefaults]  setObject:[dictInfoFB objectForKey:@"id"]  forKey:@"facebookid"];
             [self callWebserviceregister_fb];
         }
         
     }];
}

-(void)callWebserviceregister_fb
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parametersFb = @{@"fb_id":[dictInfoFB objectForKey:@"id"],@"email":[dictInfoFB objectForKey:@"email"],@"name":[NSString stringWithFormat:@"%@ %@",[dictInfoFB objectForKey:@"first_name"],[dictInfoFB objectForKey:@"last_name"]],@"device type":@"I",@"device":strDeviceToken};
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@register_fb",baseUrl];
    [manager POST:urlStr parameters:parametersFb success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [spinner stopAnimating];
         
         if ([[responseObject objectForKey:@"message"] isEqualToString:@"User Already Registered"])
         {
             
             
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"user_id"] forKey:@"userId"];
             
             [[NSUserDefaults standardUserDefaults]synchronize];
             
             if ([[responseObject objectForKey:@"check"] isEqualToString:@"no"])
             {
                 UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                 StudentDetailsViewController *detailsView = [storyBoard instantiateViewControllerWithIdentifier:@"detailsView"];
                 self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:42.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
                 [self.navigationController pushViewController:detailsView animated:YES];
                 
             }
             else
             {
                 
                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"studentviewKey"];
                 [[NSUserDefaults standardUserDefaults]synchronize];
                 
                 
                 PortfolioViewController *portView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
                 self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
                 UINavigationController * navPort = [[UINavigationController alloc]initWithRootViewController:portView];
                 
                 [self.revealViewController setFrontViewController:navPort animated:YES];
             }
         }
         else
         {
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"user_id"] forKey:@"userId"];
             [[NSUserDefaults standardUserDefaults]synchronize];
             
             UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
             StudentDetailsViewController *detailsView = [storyBoard instantiateViewControllerWithIdentifier:@"detailsView"];
             self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:42.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
             [self.navigationController pushViewController:detailsView animated:YES];
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [spinner stopAnimating];
         
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             //[[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}

- (IBAction)btnTempSignin:(id)sender
{
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    StudentDetailsViewController *detailsView = [storyBoard instantiateViewControllerWithIdentifier:@"detailsView"];
    strFromButton = @"tempLogin";
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:42.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    [self.navigationController pushViewController:detailsView animated:YES];
    
}
@end
