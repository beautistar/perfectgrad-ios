//
//  SelectCountryViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 2/18/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "SelectCountryViewController.h"
#import "SelectCountryTableViewCell.h"
#import "LocationFilterSearchViewController.h"
#import "PortfolioViewController.h"
#import "DisciplineViewController.h"
#import "PortfolioViewController.h"
#import "MessagesViewController.h"
#import "SalaryGuideViewController.h"
#import "AFNetworking.h"
#import "LocationFilterViewController.h"
#import "ApiClass.h"

@interface SelectCountryViewController ()
{
    NSArray *arrayCountryName;
    NSDictionary *dictionaryCountryName;
    NSArray *arrayIndexList;
    NSArray *arraySectionName;
    BOOL searchEnableBool;
    NSMutableArray *mutableArrayFilterCountry;
    
    NSDictionary *dictCountry;
    NSString *idStr,*iduniversity;
    NSMutableArray *keyArray;
    NSMutableDictionary *dic;
    
    Database *dummy;
}

@end

@implementation SelectCountryViewController

@synthesize arrTempCountryData;

- (void)viewDidLoad
{
    // Do any additional setup after loading the view.
    
    [super viewDidLoad];
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, _imageRedStar.frame.origin.y, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    self.imageRedStar.hidden = YES;
    
    [_buttonSearch setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    _buttonSearch.backgroundColor = [UIColor clearColor];
    _labelSearchVw.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    dummy=[[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    self.viewBottomBar.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:18.0f],NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    self.title = @"Select Country";
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]
                                  initWithTitle:@"Select"
                                  style:UIBarButtonItemStylePlain
                                  target:self
                                  action:@selector(rightItemAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
    [rightItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                       [UIFont fontWithName:@"OpenSans-Bold" size:14], NSFontAttributeName,
                                       [UIColor whiteColor], NSForegroundColorAttributeName,
                                       nil]
                             forState:UIControlStateNormal];
    searchEnableBool = NO;
    
    arrayIndexList = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", @"", @"", @"", @""];
    
    mutableArrayFilterCountry=[[NSMutableArray alloc] init];
    CGRect rect = _searchBarCountry.frame;
    _searchBarCountry.delegate = self;
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, rect.size.height-2,rect.size.width, 2)];
    lineView.backgroundColor = [UIColor whiteColor];
    [_searchBarCountry addSubview:lineView];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor lightGrayColor]];
    
    NewCountryIdArr = arrCountryIds;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    stringNotifiction = @"";
    
    stringForNotifyEditCompany = @"";
    
    [super viewWillAppear:YES];
    
    if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"] )
    {
        self.imageRedStar.hidden = YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    for (UIView *subView in _searchBarCountry.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews)
        {
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                searchBarTextField.textColor = [UIColor colorWithRed:0.0f/255.0f green:122.0f/255.0f blue:255.0f/255.0f alpha:1];
                break;
            }
        }
    }
    _tableViewSelectCountry.sectionIndexColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    [self reachabilityCheck];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(redStarShow)
                                                 name:@"redStarShow"
                                               object:nil];
}


-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

#pragma mark- reachabilityCheck
-(void)reachabilityCheck
{
    if ([[[dummy loaddataCountries:@"Select * From COUNTRIES" strForscreen:@"countiesList"] copy] count]>0)
    {
        [self loadCountriesOffline];
    }
    
    else if ([Utilities CheckInternetConnection])
    {
        [self getAllCountries];
    }
}


#pragma mark- loadCountriesOffline

-(void)loadCountriesOffline
{
    isInternetOff = YES;
    arrayCountryName = (NSArray*)[dummy loaddataCountries:@"Select * From COUNTRIES" strForscreen:@"countiesList"];
    [[NSUserDefaults standardUserDefaults]setObject:arrayCountryName forKey:@"OriginalCountryData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if (arrayCountryName.count ==0)
    {
        
    }
    else
    {
        
        [self sortingMethod];
    }
}


#pragma mark- getAllCountries

-(void)getAllCountries
{
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@getAllsearchCountries",baseUrl];
    NSDictionary *dicParam = @{@"user_id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"getAllCountries");
         isInternetOff = NO;
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
             NSArray* sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
             arrayCountryName = [[responseObject objectForKey:@"data"] sortedArrayUsingDescriptors:sortDescriptors];
             [[NSUserDefaults standardUserDefaults]setObject:arrayCountryName forKey:@"OriginalCountryData"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 
                 if ([dummy deleteTable:@"delete from COUNTRIES"])
                 {
                     for (int i=0; i<[arrayCountryName count]; i++)
                     {
                         NSString *query = [NSString stringWithFormat: @"INSERT INTO COUNTRIES (id,name,abr) VALUES (\"%@\",\"%@\",\"%@\")"
                                            ,[[arrayCountryName valueForKey:@"id"] objectAtIndex:i]
                                            ,[[arrayCountryName valueForKey:@"name"] objectAtIndex:i]
                                            ,[[arrayCountryName valueForKey:@"abr"] objectAtIndex:i]];
                         
                         const char *query_stmt = [query UTF8String];
                         [dummy  runQueries:query_stmt strForscreen:@"countiesList" ];
                     }
                 }
             });
             
             [self sortingMethod];
         }
         else
         {
             NSLog(@"wrong result");
         }
     }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self loadCountriesOffline];
         isInternetOff = YES;
     }];
    
}


-(void)sortingMethod
{
    NSMutableArray *array = [NSMutableArray arrayWithArray:arrayCountryName];
    
    NSSortDescriptor * sortByRank = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    
    
    [array sortUsingDescriptors:[NSArray arrayWithObject:sortByRank]];
    
    NSLog(@"sorted array is %@", array);
    
    arrayCountryName = array;
    
    NSLog(@"arrayCountryName array is %@", arrayCountryName);
    
    dictionaryCountryName = [self fillingDictionary:[arrayCountryName valueForKey:@"name"]];
    
    arraySectionName = [[dictionaryCountryName allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    [_tableViewSelectCountry reloadData];
}


#pragma mark:- Filling dictionary show data according to section name


-(NSMutableDictionary *)fillingDictionary:(NSMutableArray *)ary
{
    keyArray=[[NSMutableArray alloc]init];
    [keyArray removeAllObjects];
    dic=[[NSMutableDictionary alloc]init];
    for(NSString *str in ary)
    {
        char charval=[str characterAtIndex:0];   // Get the first character of your string which will be your key
        NSString *new=[NSString stringWithFormat:@"%c",charval];
        NSString *charStr=[NSString stringWithFormat:@"%@",new];
        BOOL isUppercase = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:[charStr characterAtIndex:0]];
        if (isUppercase!=YES) {
            NSString *uppercase = [charStr uppercaseString];
            charStr=uppercase;
        }
        if(![keyArray containsObject:charStr])
        {
            NSMutableArray *charArray=[[NSMutableArray alloc]init];
            [charArray addObject:str];
            [keyArray addObject:charStr];
            [dic setValue:charArray forKey:charStr];
        }
        else
        {
            NSMutableArray *prevArray=(NSMutableArray *)[dic valueForKey:charStr];
            [prevArray addObject:str];
            [dic setValue:prevArray forKey:charStr];
        }
    }
    return dic;
}

#pragma mark- UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (searchEnableBool)
    {
        return 1;
    }
    else
    {
        return [arraySectionName count];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (searchEnableBool)
    {
        return [mutableArrayFilterCountry count];
    }
    else
    {
        NSString *sectionTitle = [arraySectionName objectAtIndex:section];
        NSArray *sectionName = [dictionaryCountryName valueForKey:sectionTitle];
        return [sectionName count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (searchEnableBool)
    {
        return 0;
    }
    return 20;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (searchEnableBool)
    {
        return nil;
    }
    
    NSString *string =[arraySectionName objectAtIndex:section];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 16)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, 40, 16)];
    label.font = [UIFont fontWithName:@"OpenSans-Bold" size:16.0f];
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    
    label.textAlignment = NSTextAlignmentCenter;
    
    [view setBackgroundColor:[UIColor colorWithRed:232.0f/255.0f green:232.0f/255.0f blue:232.0f/255.0f alpha:0.4]]; //your background color...
    
    return view;
}

#pragma mark- cellForRowAtIndexPath method

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Draw top border only on first cell
    static NSString *CellIdentifier = @"SelectCountryCell";
    
    SelectCountryTableViewCell *cell = (SelectCountryTableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[SelectCountryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (searchEnableBool)
    {
        cell.labelCountryName.text = [[mutableArrayFilterCountry valueForKey:@"name"] objectAtIndex:indexPath.row];
        
        if ([[arrCountryIds valueForKey:@"id"]  containsObject:[[mutableArrayFilterCountry valueForKey:@"id"] objectAtIndex:indexPath.row]])
        {
            [cell.buttonOutletCheckBox setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.buttonOutletCheckBox setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
        }
        NSString *strId = [[NSString stringWithFormat:@"%@",[[mutableArrayFilterCountry objectAtIndex:indexPath.row] objectForKey:@"id"]] removeNull];
        NSArray *arrIds = [arrTempCountryData valueForKey:@"id"];
        if ([arrIds containsObject:strId]) {
            [cell.buttonOutletCheckBox setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
        }else{
            [cell.buttonOutletCheckBox setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
        }
    }
    else
    {
        NSString *sectionTitle = [arraySectionName objectAtIndex:indexPath.section];
        
        NSArray *sectionName = [dictionaryCountryName objectForKey:sectionTitle];
        
        NSString *names;
        
        if (sectionName.count>indexPath.row)
        {
            names = [sectionName objectAtIndex:indexPath.row];
        }
        cell.labelCountryName.text = names;
        
        cell.labelCountryName.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
        
        NSInteger currentRow;
        
        NSInteger sumSections = 0;
        
        for (int i = 0; i < indexPath.section; i++)
        {
            NSInteger rowsInSection = [tableView numberOfRowsInSection:i];
            sumSections += rowsInSection;
        }
        currentRow = sumSections + indexPath.row;
        if ([[arrCountryIds valueForKey:@"id"] containsObject:[[arrayCountryName valueForKey:@"id"] objectAtIndex:currentRow]])
        {
            [cell.buttonOutletCheckBox setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.buttonOutletCheckBox setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
        }
        NSString *strId = [[NSString stringWithFormat:@"%@",[[arrayCountryName objectAtIndex:currentRow] objectForKey:@"id"]] removeNull];
        NSArray *arrIds = [arrTempCountryData valueForKey:@"id"];
        if ([arrIds containsObject:strId]) {
            [cell.buttonOutletCheckBox setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateNormal];
        }else{
            [cell.buttonOutletCheckBox setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
        }
    }
    cell.labelCountryName.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    cell.labelCountryName.font = [UIFont fontWithName:@"OpenSans" size:15.0f];
    
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return arrayIndexList;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [arraySectionName indexOfObject:title];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger currentRow;
    if (searchEnableBool)
    {
        currentRow = indexPath.row;
        if ([arrCountryIds containsObject:[mutableArrayFilterCountry  objectAtIndex:indexPath.row]])
        {
            stringAddRemove = @"remove";
            
            [arrCountryIds removeObject:[mutableArrayFilterCountry objectAtIndex:indexPath.row]];
        }
        else
        {
            
            [arrCountryIds addObject:[mutableArrayFilterCountry  objectAtIndex:indexPath.row]];
        }
    }
    else
    {
        
        
        NSInteger sumSections = 0;
        
        for (int i = 0; i < indexPath.section; i++)
        {
            NSInteger rowsInSection = [tableView numberOfRowsInSection:i];
            sumSections += rowsInSection;
        }
        currentRow = sumSections + indexPath.row;
        
        if ([[arrCountryIds valueForKey:@"id"]  containsObject:[[arrayCountryName valueForKey:@"id"] objectAtIndex:currentRow]])
        {
            stringAddRemove = @"remove";
            [arrCountryIds removeObjectAtIndex:[[arrCountryIds valueForKey:@"id"] indexOfObject:[[arrayCountryName valueForKeyPath:@"id"] objectAtIndex:currentRow]]];
            if (arrCountryIds.count==0)
            {
                stringSelectedUnselected = @"AllDataGet";
            }
        }
        else
        {
            [arrCountryIds addObject:[arrayCountryName objectAtIndex:currentRow]];
        }
    }
    
    NSArray *arrStates = arrayCountryName;
    if(searchEnableBool){
        arrStates = mutableArrayFilterCountry;
    }
    if ([arrTempCountryData containsObject:[arrStates objectAtIndex:currentRow]]) {
        [arrTempCountryData removeObject:[arrStates objectAtIndex:currentRow]];
    }else{
        [arrTempCountryData addObject:[arrStates objectAtIndex:currentRow]];
    }
    [tableView reloadData];
    
}


#pragma mark- UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [mutableArrayFilterCountry removeAllObjects];
    
    if(searchBar.text.length<1)
    {
        searchEnableBool=NO;
    }
    else{
        searchEnableBool=YES;
        
        for (NSString *sc in [arrayCountryName valueForKey:@"name"])
        {
            NSRange  scrange = [sc rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (scrange.location != NSNotFound)
            {
                for ( int i=0; i<arrayCountryName.count; i++)
                {
                    if([[[arrayCountryName valueForKey:@"name"]objectAtIndex:i] isEqual:sc])
                    {
                        [mutableArrayFilterCountry addObject:[arrayCountryName objectAtIndex:i]];
                    }
                }
            }
        }
    }
    
    [_tableViewSelectCountry reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    _searchBarCountry.showsCancelButton = YES;
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    _searchBarCountry.showsCancelButton = NO;
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar endEditing:YES];
    [searchBar resignFirstResponder];
}

#pragma mark- selfCreatedButtonActions



-(void)backBtnAction
{
    
    arrCountryIds = [NewCountryIdArr mutableCopy];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)sendDataBack:(NSArray *)array
{
    
}

- (void)rightItemAction
{
    if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
    {
    }
    else if ([stringCheckDisciplines isEqualToString:@"fromSubDisciplines"])
    {
        [self localllyFilterMethodDiscilpineMethod ];
    }
    else
    {
        [self  localllyFilterMethod];
    }
    [[NSUserDefaults standardUserDefaults] setBool:isAllCountrySelected forKey:@"AllCountrySelected"];
    [[NSUserDefaults standardUserDefaults] setBool:isAllCountrySelectedSearch forKey:@"AllCountrySelectedSearch"];
    [self.delegate sendDataToStateLoc:arrTempCountryData];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)localllyFilterMethod
{
    NSMutableArray *arrayComp = [[NSMutableArray alloc]init];
    
    arrayComp = [[[NSUserDefaults standardUserDefaults]objectForKey:@"allCompanies"]mutableCopy];
    {
        NSMutableArray *arrayRegState =[NSMutableArray new];
        
        for (int i=0; i<arrayComp.count; i++)
        {
            NSDictionary *dicCompanyDetail = arrayComp [i];
            
            if ([[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@"<null>"]||[[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@""]||[[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@"(null)"])
            {
                
            }
            
            else
            {
                arrayRegState = [[[dicCompanyDetail valueForKey:@"internationl"] componentsSeparatedByString:@","] mutableCopy];
                
                NSMutableArray *arrayTemp = [NSMutableArray new];
                
                for (int d=0; d<arrayRegState.count; d++)
                {
                    NSString *string = [arrayRegState objectAtIndex:d];
                    
                    NSArray *array = [string componentsSeparatedByString:@" ("];
                    
                    [arrayTemp addObject:array[0]];
                }
                
                [arrayRegState removeAllObjects];
                
                arrayRegState = [NSMutableArray new];
                
                arrayRegState = [arrayTemp mutableCopy];
                
                for (int c=0; c<arrCountryIds.count; c++)
                {
                    if ([arrayTemp containsObject:[arrCountryIds[c] objectForKey:@"name"]])
                    {
                        
                        if ([[arrayFilterContry valueForKey:@"id"] containsObject:[arrayComp valueForKey:@"id"][i]])
                            
                        {
                            
                        }
                        
                        else
                            
                        {
                            [arrayFilterContry addObject:arrayComp[i]];
                        }
                    }
                }
            }
        }
        
        NSLog(@"arrayFilterContry is %@", arrayFilterContry);
        
        [[NSUserDefaults standardUserDefaults]setObject:arrayFilterContry forKey:@"filterConties"];
    }
    
    if (arrStateIds.count==0 && arrCountryIds.count==0)
    {
        stringFilterScreen = @"nothing";
    }
    else
    {
        stringFilterScreen = @"filterScreen";
    }
}


-(void)localllyFilterMethodDiscilpineMethod
{
    NSMutableArray *arrayComp = [[NSMutableArray alloc]init];
    
    arrayComp = [[[NSUserDefaults standardUserDefaults]objectForKey:@"allCompanies"]mutableCopy];
    {
        NSMutableArray *arrayRegState =[NSMutableArray new];
        
        
        for (int i=0; i<arrayComp.count; i++)
        {
            NSDictionary *dicCompanyDetail = arrayComp [i];
            
            if ([[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@"<null>"]||[[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@""]||[[dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@"(null)"])
            {
                
            }
            
            else
            {
                arrayRegState = [[[dicCompanyDetail valueForKey:@"internationl"] componentsSeparatedByString:@","] mutableCopy];
                
                NSMutableArray *arrayTemp = [NSMutableArray new];
                
                for (int d=0; d<arrayRegState.count; d++)
                {
                    NSString *string = [arrayRegState objectAtIndex:d];
                    
                    NSArray *array = [string componentsSeparatedByString:@" ("];
                    
                    [arrayTemp addObject:array[0]];
                }
                
                [arrayRegState removeAllObjects];
                
                arrayRegState = [NSMutableArray new];
                
                arrayRegState = [arrayTemp mutableCopy];
                
                for (int c=0; c<arrCountryIds.count; c++)
                {
                    if ([arrayTemp containsObject:[arrCountryIds[c] objectForKey:@"name"]])
                    {
                        
                        if ([[arrayFilterContry valueForKey:@"id"] containsObject:[arrayComp valueForKey:@"id"][i]])
                            
                        {
                            
                        }
                        
                        else
                            
                        {
                            [arrayFilterContry addObject:arrayComp[i]];
                        }
                    }
                }
            }
        }
        
        NSLog(@"arrayFilterContry is %@", arrayFilterContry);
        
        [[NSUserDefaults standardUserDefaults]setObject:arrayFilterContry forKey:@"filterContiesDiscipline"];
    }
    
    if (arrStateIds.count==0 && arrCountryIds.count==0)
    {
        stringFilterScreen = @"nothing";
    }
    else
    {
        stringFilterScreen = @"filterScreen";
    }
    
}


#pragma mark- tabBarButtonActions
- (IBAction)buttonActionPortfolio:(id)sender;
{
    arrCountryIds = [NewCountryIdArr mutableCopy];
    isCancelClicked = YES;
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[PortfolioViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    PortfolioViewController *portfolioView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
    [self.navigationController pushViewController:portfolioView animated:NO];
    
}

- (IBAction)buttonActionDiscip:(id)sender;
{
    arrCountryIds = [NewCountryIdArr mutableCopy];
    isCancelClicked = YES;
    if ([stringCheckDisciplines isEqualToString:@"fromDisciplines"])
    {
        
    }
    else if ([stringCheckDisciplines isEqualToString: @"fromSubDisciplines"])
    {
        
    }
    
    else
    {
        fromLoadDisc = YES;
    }
    
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[DisciplineViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    DisciplineViewController *disciplineVw = [self.storyboard instantiateViewControllerWithIdentifier:@"disciplineView"];
    
    [self.navigationController pushViewController:disciplineVw animated:NO];
    
}
- (IBAction)buttonSearch:(id)sender;
{
    arrCountryIds = [NewCountryIdArr mutableCopy];
    isCancelClicked = YES;
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[LocationFilterSearchViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    LocationFilterSearchViewController  *searchVw = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVw"];
    [self.navigationController pushViewController:searchVw animated:NO];
    
    
}
- (IBAction)buttonMessages:(id)sender;
{
    arrCountryIds = [NewCountryIdArr mutableCopy];
    isCancelClicked = YES;
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[MessagesViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    MessagesViewController  *messageVw = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    
    [self.navigationController pushViewController:messageVw animated:NO];
    
    
}
- (IBAction)buttonActionSalary:(id)sender;
{
    arrCountryIds = [NewCountryIdArr mutableCopy];
    isCancelClicked = YES;
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[SalaryGuideViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    SalaryGuideViewController *salaryVw = [self.storyboard instantiateViewControllerWithIdentifier:@"salaryVw"];
    [self.navigationController pushViewController:salaryVw animated:NO];
}


@end
