//
//  Database.m
//  Database
//
//  Created by IPS Brar on 24/03/15.
//  Copyright (c) 2015 NetSet Software Solutions. All rights reserved.
//

#import "Database.h"
#import <sqlite3.h>

@implementation Database
{
    NSString *stringFromBanner;
    
    NSArray *arrayIds;
    
    NSArray *arraySubDisc;
}

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename
{
    self = [super init];
    if (self)
    {
        // Set the documents directory path to the documentsDirectory property.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.documentsDirectory = [paths objectAtIndex:0];
        
        // Keep the database filename.
        self.databaseFilename = dbFilename;
        
        // Copy the database file into the documents directory if necessary.
        [self copyDatabaseIntoDocumentsDirectory];
    }
    
    return self;
}

-(void)copyDatabaseIntoDocumentsDirectory
{
    // Check if the database file exists in the documents directory.
    NSString *destinationPath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    NSLog(@"the db path:%@",destinationPath);
    if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
        // The database file does not exist in the documents directory, so copy it from the main bundle now.
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        
        // Check if any error occurred during copying and display it.
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
}

#pragma mark-----queryToCreateTables---------

-(void)runQueries:(const char *)query  strForscreen:(NSString *)strForscreen
{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {  sqlite3_stmt    *statement;
        char *errMsg;
        const char *sql_stmt;
        
        if ([strForscreen isEqualToString:@"port"])
        {
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS PORTFOLIO (id Text, application_due_date TEXT,awards Text,city Text,company_address Text,company_name Text,description Text,logo Text,state_id Text,unread_message Text,regstates TEXT,company_intro TEXT,country TEXT,message_status TEXT,website TEXT,international TEXT,perfect_logo TEXT,society_logo TEXT)";
        }
        
        else if([strForscreen isEqualToString:@"search"])
        {
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS COMPANIES (id Text, application_due_date TEXT,awards Text,city Text,company_address Text,company_name Text,description Text,logo Text,state_id Text,regstates TEXT,country_id TEXT,company_intro TEXT,country TEXT, portfolio_status TEXT, message_state TEXT,message_status TEXT,website TEXT,international TEXT,unread_message TEXT)";
        }
        
        else if([strForscreen isEqualToString:@"searchFilter"])
            
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS COMPANIESFILTER (id Text, application_due_date TEXT,awards Text,city Text,company_address Text,company_name Text,description Text,logo Text,state_id Text,regstates TEXT,country_id TEXT,company_intro TEXT,country TEXT, portfolio_status TEXT, message_state TEXT,message_status TEXT,website TEXT,international TEXT)";
        }
        
        else if ([strForscreen isEqualToString:@"countiesList"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS COUNTRIES (id Text, name TEXT, abr TEXT)";
        }
        else if ([strForscreen isEqualToString:@"StatesList"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS STATES (id Text, name TEXT ,country_id TEXT,abr TEXT)";
        }
        else if ([strForscreen isEqualToString:@"Salaries"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS SALARIES (id Text, occupation TEXT, occupation_id TEXT ,salary0to3 TEXT ,title TEXT,sector TEXT ,sector_id TEXT,salary3to5 TEXT,salary5to7 TEXT,year TEXT, large TEXT ,smallMedium TEXT,type TEXT)";
        }
        
        else if ([strForscreen isEqualToString:@"SalariesBanners"])
            
        {
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS SALARIESBANNERS (image_name TEXT, rotation_time TEXT, id TEXT, name TEXT , bannerUrl TEXT)";
        }
        
        else if ([strForscreen isEqualToString:@"Disciplines"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS DISCIPLINES (id Text, name TEXT)";
        }
        
        else if ([strForscreen isEqualToString:@"DisciplinesFilter"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS DISCIPLINESFilter (id Text, name TEXT)";
        }
        
        else if ([strForscreen isEqualToString:@"messsages"])
            
        {
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS MessagesTable (adder_id Text, date TEXT, event_date Text, event_title TEXT,event_venue Text, eventtimefrom TEXT,eventtimeto Text, gender TEXT,id Text, message TEXT,message_type Text, status TEXT , type TEXT ,logo TEXT,name TEXT,eventconfirmation TEXT,default_timezone TEXT,website TEXT,report TEXT,like_status TEXT,addedStatus TEXT,eventstatus TEXT,perfect_logo TEXT)";
        }
        
        if (sqlite3_exec(sqlite3Database, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
        {
            sqlite3_prepare_v2(sqlite3Database, query, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_finalize(statement);
            }
        }
        sqlite3_close(sqlite3Database);
    }
    else
    {
        NSLog(@"Failed");
    }
}


#pragma mark---------ApplicationDueDate------------

-(void)runSubQueryToSaveDueDate:(const char *)query  strForscreen:(NSString *)strForscreen
{
    // Create a sqlite object.
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {  sqlite3_stmt    *statement;
        char *errMsg;
        const char *sql_stmt;
        
        if ([strForscreen isEqualToString:@"port"])
        {
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS PortCompanyDueDate (close_date TEXT,company_id TEXT,id TEXT,program TEXT,program_id TEXT,start_date TEXT,state TEXT,country TEXT)";
        }
        else if ([strForscreen isEqualToString:@"Search"])
        {
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS SearchCompanyDueDate (close_date TEXT,company_id TEXT,id TEXT,program TEXT,program_id TEXT,start_date TEXT,state TEXT,country TEXT)";
        }
        
        else if ([strForscreen isEqualToString:@"SearchFilter"])
        {
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS SearchCompanyDueDateFILTER (close_date TEXT,company_id TEXT,id TEXT,program TEXT,program_id TEXT,start_date TEXT,state TEXT,country TEXT)";
        }
        
        if (sqlite3_exec(sqlite3Database, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
        {
            sqlite3_prepare_v2(sqlite3Database, query, -1, &statement, NULL);
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_finalize(statement);
            }
        }
        
        sqlite3_close(sqlite3Database);
    }
    else
    {
        NSLog(@"Failed");
    }
}

#pragma mark---------SaveAwards------------

-(void)runSubQueryToSaveAwards:(const char *)query  strForscreen:(NSString *)strForscreen
{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {  sqlite3_stmt    *statement;
        char *errMsg;
        const char *sql_stmt;
        
        if ([strForscreen isEqualToString:@"port"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS PortCompanyAwards (awards Text, company_id TEXT,description TEXT,id Text)";
        }
        else if ([strForscreen isEqualToString:@"SearchVw"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS SearchCompanyAwards (awards Text, company_id TEXT,description TEXT,id Text)";
        }
        else if ([strForscreen isEqualToString:@"SearchVwFilter"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS SearchCompanyAwardsFILTER (awards Text, company_id TEXT,description TEXT,id Text)";
        }
        
        if (sqlite3_exec(sqlite3Database, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
        {
            sqlite3_prepare_v2(sqlite3Database, query, -1, &statement, NULL);
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_finalize(statement);
            }
        }
        sqlite3_close(sqlite3Database);
    }
    else
    {
        NSLog(@"Failed");
    }
}


#pragma mark---------CompanyLocations------------





-(void)runSubQueryLocations:(const char *)query  strForscreen:(NSString *)strForscreen
{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {  sqlite3_stmt    *statement;
        char *errMsg;
        const char *sql_stmt;
        
        if ([strForscreen isEqualToString:@"port"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS PortLocationsTable (company_id Text, id TEXT,branch_name Text,lat Text,long Text,location Text)";
        }
        
        else if ([strForscreen isEqualToString:@"COMPANYLOCATIONS"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS COMPANYLOCATIONS (company_id Text, id TEXT,branch_name Text,lat Text,long Text,location Text)";
        }
        
        else if ([strForscreen isEqualToString:@"COMPANYLOCATIONSFilter"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS COMPANYLOCATIONSFILTER (company_id Text, id TEXT,branch_name Text,lat Text,long Text,location Text)";
        }
        
        if (sqlite3_exec(sqlite3Database, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
        {
            sqlite3_prepare_v2(sqlite3Database, query, -1, &statement, NULL);
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_finalize(statement);
            }
        }
        
        sqlite3_close(sqlite3Database);
    }
    else {
        NSLog(@"Failed");
    }
}

#pragma mark------SocialLinksForCompanies----------

-(void)runQuerySocialLinksForCompanies:(const char *)query strForscreen:(NSString *)strForscreen
{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {  sqlite3_stmt    *statement;
        char *errMsg;
        const char *sql_stmt;
        
        if ([strForscreen isEqualToString:@"search"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS SearchCompSocialLinksTable (company_id TEXT, id TEXT,media_link TEXT,media_logo TEXT,media_type TEXT)";
        }
        if ([strForscreen isEqualToString:@"searchFilter"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS SearchCompSocialLinksTableFILTER (company_id TEXT, id TEXT,media_link TEXT,media_logo TEXT,media_type TEXT)";
        }
        else if ([strForscreen isEqualToString:@"Port"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS PortCompSocialLinksTable (company_id TEXT, id TEXT,media_link TEXT,media_logo TEXT,media_type TEXT)";
        }
        
        if (sqlite3_exec(sqlite3Database, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
        {
            sqlite3_prepare_v2(sqlite3Database, query, -1, &statement, NULL);
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_finalize(statement);
            }
        }
        
        sqlite3_close(sqlite3Database);
    }
    else
    {
        NSLog(@"Failed");
    }
}


#pragma mark------QuesAnsForCompanies----------

-(void)runQueryQuesAnsForCompanies:(const char *)query strForscreen:(NSString *)strForscreen
{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {  sqlite3_stmt    *statement;
        char *errMsg;
        const char *sql_stmt;
        
        if ([strForscreen isEqualToString:@"search"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS SearchQuesAnsTable (answer TEXT, company_id TEXT,id TEXT,question TEXT)";
        }
        if ([strForscreen isEqualToString:@"searchFilter"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS SearchQuesAnsTableFILTER (answer TEXT, company_id TEXT,id TEXT,question TEXT)";
        }
        else if ([strForscreen isEqualToString:@"Port"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS PortQuesAnsTable (answer TEXT, company_id TEXT,id TEXT,question TEXT)";
        }
        
        if (sqlite3_exec(sqlite3Database, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
        {
            sqlite3_prepare_v2(sqlite3Database, query, -1, &statement, NULL);
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_finalize(statement);
            }
        }
        sqlite3_close(sqlite3Database);
    }
    else
    {
        NSLog(@"Failed");
    }
}

#pragma mark------DiscSubDiscForCompanies----------

-(void)runQueryDispSubDispFordisp:(const char *)query strForscreen:(NSString *)strForscreen
{
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
        
    {
        sqlite3_stmt *statement;
        
        char *errMsg;
        
        const char *sql_stmt;
        
        if ([strForscreen isEqualToString:@"searchCompanyDisciplines"])
        {
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS DispSubDispDataTable (company_id Text, discipline_id TEXT,name Text,sub_discipline_id TEXT)";
        }
        
        if (sqlite3_exec(sqlite3Database, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
        {
            sqlite3_prepare_v2(sqlite3Database, query, -1, &statement, NULL);
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_finalize(statement);
            }
        }
        sqlite3_close(sqlite3Database);
        
    }
    
}



-(void)runQueryDispSubDispForCompanies:(const char *)query strForscreen:(NSString *)strForscreen
{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
        
    {
        sqlite3_stmt *statement;
        
        char *errMsg;
        
        const char *sql_stmt;
        
        if ([strForscreen isEqualToString:@"searchCompanyDisciplines"])
        {
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS DispSubDispCompaniesTable (company_id Text, discipline_id TEXT,name Text,type TEXT)";
        }
        
        else if ([strForscreen isEqualToString:@"DisciplinesPort"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS PortDisciplinesTable (company_id Text, discipline_id TEXT,name Text)";
        }
        
        else if ([strForscreen isEqualToString:@"searchCompanyDisciplinesFilter"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS DispSubDispCompaniesTableFILTER (company_id Text, discipline_id TEXT,name Text)";
        }
        
        if (sqlite3_exec(sqlite3Database, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
        {
            sqlite3_prepare_v2(sqlite3Database, query, -1, &statement, NULL);
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_finalize(statement);
            }
        }
        sqlite3_close(sqlite3Database);
    }
    else
    {
        NSLog(@"Failed");
    }
}

#pragma mark------QuerySubDisciplines--------

-(void)runQuerySubDisciplines:(const char *)query strForscreen:(NSString *)strForscreen
{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {  sqlite3_stmt    *statement;
        char *errMsg;
        const char *sql_stmt;
        
        
        if ([strForscreen isEqualToString:@"DisciplinesVw"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS SubDisciplines (date Text, id TEXT,name Text,discipline_id TEXT ,companys TEXT)";
        }
        if ([strForscreen isEqualToString:@"DisciplinesVwFilter"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS SubDisciplinesFilter (date Text, id TEXT,name Text,discipline_id TEXT ,companys TEXT)";
        }
        else if ([strForscreen isEqualToString:@"SubDiscipPort"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS PortSubDisciplinesTable (discipline_id Text, name TEXT,company_id TEXT,sub_discipline_id TEXT)";
        }
        else if ([strForscreen isEqualToString:@"SubDiscipSearchCompanies"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS SearchCompSubDisciplineTable (discipline_id Text, name TEXT,company_id TEXT,sub_discipline_id TEXT)";
        }
        else if ([strForscreen isEqualToString:@"SubDiscipSearchCompaniesFilter"])
        {
            sql_stmt = "CREATE TABLE IF NOT EXISTS SearchCompSubDisciplineTableFILTER (discipline_id Text, name TEXT,company_id TEXT,sub_discipline_id TEXT)";
        }
        
        if (sqlite3_exec(sqlite3Database, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
        {
            sqlite3_prepare_v2(sqlite3Database, query, -1, &statement, NULL);
            
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_finalize(statement);
            }
        }
        
        sqlite3_close(sqlite3Database);
    }
    else
    {
        NSLog(@"Failed");
    }
}

#pragma mark--------LoadData---------

-(NSMutableArray*) loadDisciplinedata:(NSString *)query  strForscreen:(NSString *)strForscreen array:(NSArray *)arraySubDicspline
{
    arraySubDisc = arraySubDicspline;
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt  *statement;
    
    if (_arrResults==nil)
    {
        _arrResults=[[NSMutableArray alloc] init];
    }
    
    [_arrResults  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *idStr = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 0)];
                
                NSString *application_due_dateStr = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                
                NSString *awardsStr=[[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 2)];
                
                NSString *cityStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 3)];
                
                NSString *company_addressStr = [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 4)];
                
                NSString *company_nameStr=[[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 5)];
                
                NSString *descriptionStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 6)];
                
                NSString *logoStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 7)];
                
                NSString *state_idStr=[[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 8)];
                
                NSDictionary *dic = nil;
                
                if ([strForscreen isEqualToString:@"port"])
                    
                {
                    NSString *unread_messageStr=[[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 9)];
                    NSString *regstatesStr=[[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 10)];
                    NSString *company_introStr=[[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 11)];
                    
                    const char* date = (const char*)sqlite3_column_text(statement, 12);
                    
                    NSString *countryStr = date == NULL ? nil : [[NSString alloc] initWithUTF8String:date];
                    
                    NSString *message_statusStr=[[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 13)];
                    NSString *websiteLinkStr=[[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 14)];
                    
                    const char* date1 = (const char*)sqlite3_column_text(statement, 15);
                    
                    NSString *internationalStr = date1 == NULL ? nil : [[NSString alloc] initWithUTF8String:date1];
                    
                    NSString *perfect_logo=[[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 16)];
                    
                    NSString *society_logo=[[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 17)];
                    
                    if (internationalStr == nil|| countryStr== nil)
                        
                    {
                        
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"unread_message":unread_messageStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM PortLocationsTable where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM PortCompanyDueDate where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM PortCompanyAwards where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM PortDisciplinesTable where company_id =\"%@\"",idStr] strForscreen:@"DisciplinesPort"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM PortQuesAnsTable where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM PortCompSocialLinksTable where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"company_intro":company_introStr,@"country":@"",@"message_status":message_statusStr,@"website_link":websiteLinkStr,@"internationl":@"",@"perfect_logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,perfect_logo]],@"society_logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,society_logo]]};
                    }
                    
                    else
                    {
                        //  Change logo URl NEERAJ
                        
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"unread_message":unread_messageStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM PortLocationsTable where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM PortCompanyDueDate where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM PortCompanyAwards where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM PortDisciplinesTable where company_id =\"%@\"",idStr] strForscreen:@"DisciplinesPort"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM PortQuesAnsTable where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM PortCompSocialLinksTable where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"company_intro":company_introStr,@"country":countryStr,@"message_status":message_statusStr,@"website_link":websiteLinkStr,@"internationl":internationalStr,@"perfect_logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,perfect_logo]],@"society_logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,society_logo]]};
                    }
                    
                    
                }
                
                else if ([strForscreen isEqualToString:@"discipline"])
                    
                {
                    NSString *regstatesStr = [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 9)];
                    
                    NSString *country_idStr = [[NSString alloc] initWithUTF8String:
                                               (const char *) sqlite3_column_text(statement, 10)];
                    
                    NSString *company_introStr = [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 11)];
                    
                    NSString *countryStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 12)];
                    
                    NSString *portfolio_statusStr = [[NSString alloc] initWithUTF8String:
                                                     (const char *) sqlite3_column_text(statement, 13)];
                    
                    NSString *message_stateStr = [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 14)];
                    
                    NSString *message_statusStr = [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 15)];
                    
                    NSString *websiteLinkString = [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 16)];
                    
                    const char* date = (const char*)sqlite3_column_text(statement, 17);
                    
                    NSString *internationalStr = date == NULL ? nil : [[NSString alloc] initWithUTF8String:date];
                    
                    NSString *unread_messageStr = [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 18)];
                    if (internationalStr == nil)
                        
                    {
                        //                        DispSubDispCompaniesTable
                        
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM COMPANYLOCATIONS where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyDueDate where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyAwards where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM DispSubDispCompaniesTable where company_id =\"%@\"",idStr] strForscreen:@"searchCompanyDisciplines"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM SearchQuesAnsTable where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM SearchCompSocialLinksTable where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"country_id":country_idStr,@"company_intro":company_introStr,@"country":countryStr,@"portfolio_status":portfolio_statusStr,@"message_state":message_stateStr,@"message_status":message_statusStr,@"website_link":websiteLinkString,@"internationl":@"",@"unread_message":unread_messageStr};
                    }
                    else
                    {
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM COMPANYLOCATIONS where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyDueDate where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyAwards where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM DispSubDispCompaniesTable where company_id =\"%@\"",idStr] strForscreen:@"searchCompanyDisciplines"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM SearchQuesAnsTable where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM SearchCompSocialLinksTable where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"country_id":country_idStr,@"company_intro":company_introStr,@"country":countryStr,@"portfolio_status":portfolio_statusStr,@"message_state":message_stateStr,@"message_status":message_statusStr,@"website_link":websiteLinkString,@"internationl":internationalStr,@"unread_message":unread_messageStr};
                    }
                }
                
                else if ([strForscreen isEqualToString:@"searchFilter"])
                {
                    NSString *regstatesStr = [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 9)];
                    
                    NSString *country_idStr = [[NSString alloc] initWithUTF8String:
                                               (const char *) sqlite3_column_text(statement, 10)];
                    
                    NSString *company_introStr = [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 11)];
                    
                    const char* date = (const char*)sqlite3_column_text(statement, 12);
                    
                    NSString *countryStr = date == NULL ? nil : [[NSString alloc] initWithUTF8String:date];
                    
                    NSString *portfolio_statusStr = [[NSString alloc] initWithUTF8String:
                                                     (const char *) sqlite3_column_text(statement, 13)];
                    
                    NSString *message_stateStr = [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 14)];
                    
                    NSString *message_statusStr = [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 15)];
                    
                    NSString *websiteLinkString = [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 16)];
                    
                    const char* date1 = (const char*)sqlite3_column_text(statement, 17);
                    
                    NSLog(@"international  %@", [[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 17)]);
                    
                    NSString *internationalStr = date1 == NULL ? nil : [[NSString alloc] initWithUTF8String:date1];
                    
                    if (internationalStr == nil||countryStr== nil)
                        
                    {
                        
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM COMPANYLOCATIONSFILTER where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyDueDateFILTER where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyAwardsFILTER where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM DispSubDispCompaniesTableFILTER where company_id =\"%@\"",idStr] strForscreen:@"searchCompanyDisciplinesFilter"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM SearchQuesAnsTableFILTER where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM SearchCompSocialLinksTableFILTER where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"country_id":country_idStr,@"company_intro":company_introStr,@"country":@"",@"portfolio_status":portfolio_statusStr,@"message_state":message_stateStr,@"message_status":message_statusStr,@"website_link":websiteLinkString,@"internationl":@""};
                    }
                    else
                    {
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM COMPANYLOCATIONSFILTER where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyDueDateFILTER where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyAwardsFILTER where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM DispSubDispCompaniesTableFILTER where company_id =\"%@\"",idStr] strForscreen:@"searchCompanyDisciplinesFilter"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM SearchQuesAnsTableFILTER where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM SearchCompSocialLinksTableFILTER where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"country_id":country_idStr,@"company_intro":company_introStr,@"country":countryStr,@"portfolio_status":portfolio_statusStr,@"message_state":message_stateStr,@"message_status":message_statusStr,@"website_link":websiteLinkString,@"internationl":internationalStr};
                    }
                }
                
                [_arrResults addObject:dic];
            }
            
            
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
        
    }
    
    return _arrResults;
}

-(void)updateTable:(NSString *)query
{
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    
    sqlite3_stmt  *statement;
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        const char *update_stmt = [query UTF8String];
        sqlite3_prepare_v2(sqlite3Database, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
        }
        else
        {
        }
        sqlite3_reset(statement);
    }
}


-(NSMutableArray*) loaddata:(NSString *)query  strForscreen:(NSString *)strForscreen
{
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    
    sqlite3_stmt  *statement;
    
    if (_arrResults==nil)
    {
        _arrResults=[[NSMutableArray alloc] init];
    }
    
    [_arrResults  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *idStr = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 0)];
                
                NSString *application_due_dateStr = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                
                NSString *awardsStr=[[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 2)];
                
                NSString *cityStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 3)];
                
                NSString *company_addressStr = [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 4)];
                
                NSString *company_nameStr=[[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 5)];
                
                NSString *descriptionStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 6)];
                
                NSString *logoStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 7)];
                
                NSString *state_idStr=[[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 8)];
                
                NSDictionary *dic = nil;
                
                if ([strForscreen isEqualToString:@"port"])
                {
                    NSString *unread_messageStr=[[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 9)];
                    NSString *regstatesStr=[[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 10)];
                    NSString *company_introStr=[[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 11)];
                    
                    const char* date = (const char*)sqlite3_column_text(statement, 12);
                    
                    NSString *countryStr = date == NULL ? nil : [[NSString alloc] initWithUTF8String:date];
                    
                    NSString *message_statusStr=[[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 13)];
                    NSString *websiteLinkStr=[[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 14)];
                    
                    const char* date1 = (const char*)sqlite3_column_text(statement, 15);
                    
                    NSString *internationalStr = date1 == NULL ? nil : [[NSString alloc] initWithUTF8String:date1];
                    
                    NSString *perfect_logo=[[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 16)];
                    
                    NSString *society_logo=[[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 17)];
                    
                    if (internationalStr == nil|| countryStr== nil)
                    {
                        
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"unread_message":unread_messageStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM PortLocationsTable where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM PortCompanyDueDate where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM PortCompanyAwards where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM PortDisciplinesTable where company_id =\"%@\"",idStr] strForscreen:@"DisciplinesPort"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM PortQuesAnsTable where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM PortCompSocialLinksTable where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"company_intro":company_introStr,@"country":@"",@"message_status":message_statusStr,@"website_link":websiteLinkStr,@"internationl":@"",@"perfect_logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,perfect_logo]],@"society_logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,society_logo]]};
                    }
                    else
                    {
                        //  Change logo URl NEERAJ
                        
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"unread_message":unread_messageStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM PortLocationsTable where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM PortCompanyDueDate where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM PortCompanyAwards where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM PortDisciplinesTable where company_id =\"%@\"",idStr] strForscreen:@"DisciplinesPort"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM PortQuesAnsTable where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM PortCompSocialLinksTable where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"company_intro":company_introStr,@"country":countryStr,@"message_status":message_statusStr,@"website_link":websiteLinkStr,@"internationl":internationalStr,@"perfect_logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,perfect_logo]],@"society_logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,society_logo]]};
                    }
                    
                }
                
                else if ([strForscreen isEqualToString:@"search"])
                    
                {
                    NSString *regstatesStr = [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 9)];
                    
                    NSString *country_idStr = [[NSString alloc] initWithUTF8String:
                                               (const char *) sqlite3_column_text(statement, 10)];
                    
                    NSString *company_introStr = [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 11)];
                    
                    NSString *countryStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 12)];
                    
                    NSString *portfolio_statusStr = [[NSString alloc] initWithUTF8String:
                                                     (const char *) sqlite3_column_text(statement, 13)];
                    
                    NSString *message_stateStr = [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 14)];
                    
                    NSString *message_statusStr = [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 15)];
                    
                    NSString *websiteLinkString = [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 16)];
                    
                    const char* date = (const char*)sqlite3_column_text(statement, 17);
                    
                    NSString *internationalStr = date == NULL ? nil : [[NSString alloc] initWithUTF8String:date];
                    
                    NSString *unread_messageStr = [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 18)];
                    if (internationalStr == nil)
                        
                    {
                        //                        DispSubDispCompaniesTable
                        
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM COMPANYLOCATIONS where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyDueDate where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyAwards where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM DispSubDispCompaniesTable where company_id =\"%@\"",idStr] strForscreen:@"searchCompanyDisciplines"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM SearchQuesAnsTable where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM SearchCompSocialLinksTable where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"country_id":country_idStr,@"company_intro":company_introStr,@"country":countryStr,@"portfolio_status":portfolio_statusStr,@"message_state":message_stateStr,@"message_status":message_statusStr,@"website_link":websiteLinkString,@"internationl":@"",@"unread_message":unread_messageStr};
                    }
                    else
                    {
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM COMPANYLOCATIONS where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyDueDate where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyAwards where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM DispSubDispCompaniesTable where company_id =\"%@\"",idStr] strForscreen:@"searchCompanyDisciplines"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM SearchQuesAnsTable where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM SearchCompSocialLinksTable where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"country_id":country_idStr,@"company_intro":company_introStr,@"country":countryStr,@"portfolio_status":portfolio_statusStr,@"message_state":message_stateStr,@"message_status":message_statusStr,@"website_link":websiteLinkString,@"internationl":internationalStr,@"unread_message":unread_messageStr};
                    }
                }
                
                else if ([strForscreen isEqualToString:@"searchFilter"])
                {
                    NSString *regstatesStr = [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 9)];
                    
                    NSString *country_idStr = [[NSString alloc] initWithUTF8String:
                                               (const char *) sqlite3_column_text(statement, 10)];
                    
                    NSString *company_introStr = [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 11)];
                    //
                    
                    const char* date = (const char*)sqlite3_column_text(statement, 12);
                    
                    NSString *countryStr = date == NULL ? nil : [[NSString alloc] initWithUTF8String:date];
                    
                    NSString *portfolio_statusStr = [[NSString alloc] initWithUTF8String:
                                                     (const char *) sqlite3_column_text(statement, 13)];
                    
                    NSString *message_stateStr = [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 14)];
                    
                    NSString *message_statusStr = [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 15)];
                    
                    NSString *websiteLinkString = [[NSString alloc] initWithUTF8String:
                                                   (const char *) sqlite3_column_text(statement, 16)];
                    
                    const char* date1 = (const char*)sqlite3_column_text(statement, 17);
                    
                    NSLog(@"international  %@", [[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 17)]);
                    
                    NSString *internationalStr = date1 == NULL ? nil : [[NSString alloc] initWithUTF8String:date1];
                    
                    if (internationalStr == nil||countryStr== nil)
                        
                    {
                        
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM COMPANYLOCATIONSFILTER where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyDueDateFILTER where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyAwardsFILTER where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM DispSubDispCompaniesTableFILTER where company_id =\"%@\"",idStr] strForscreen:@"searchCompanyDisciplinesFilter"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM SearchQuesAnsTableFILTER where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM SearchCompSocialLinksTableFILTER where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"country_id":country_idStr,@"company_intro":company_introStr,@"country":@"",@"portfolio_status":portfolio_statusStr,@"message_state":message_stateStr,@"message_status":message_statusStr,@"website_link":websiteLinkString,@"internationl":@""};
                    }
                    else
                    {
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM COMPANYLOCATIONSFILTER where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyDueDateFILTER where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM SearchCompanyAwardsFILTER where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM DispSubDispCompaniesTableFILTER where company_id =\"%@\"",idStr] strForscreen:@"searchCompanyDisciplinesFilter"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM SearchQuesAnsTableFILTER where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM SearchCompSocialLinksTableFILTER where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"country_id":country_idStr,@"company_intro":company_introStr,@"country":countryStr,@"portfolio_status":portfolio_statusStr,@"message_state":message_stateStr,@"message_status":message_statusStr,@"website_link":websiteLinkString,@"internationl":internationalStr};
                    }
                }
                
                [_arrResults addObject:dic];
            }
            
            
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
        else
        {
            NSLog(@"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(sqlite3Database));
        }
        
    }
    
    return _arrResults;
}


#pragma mark-----LoadFromSubPortDueDate-----------

-(NSMutableArray*) loaddataSubPortDueDate:(NSString *)query  strForscreen:(NSString *)strForscreen
{
    NSMutableArray *arrDueDate = nil;
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    arrDueDate=[[NSMutableArray alloc] init];
    
    [arrDueDate  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *closeDateStr = [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 0)];
                NSString *CompanyIdStr = [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 1)];
                NSString *idStr = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 2)];
                NSString *programStr=[[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 3)];
                NSString *ProgramIdStr = [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 4)];
                NSString *startDateStr=[[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 5)];
                NSString *stateStr=[[NSString alloc] initWithUTF8String:
                                    (const char *) sqlite3_column_text(statement, 6)];
                
                
                NSDictionary *dic= @{@"close_date":closeDateStr,@"company_id":CompanyIdStr,@"id":idStr,@"program":programStr,@"program_id":ProgramIdStr,@"start_date":startDateStr,@"state":stateStr};
                
                [arrDueDate addObject:dic];
            }
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    
    return arrDueDate;
}

#pragma mark-----loaddataSubAwards-----------

-(NSMutableArray*) loaddataSubAwards:(NSString *)query  strForscreen:(NSString *)strForscreen
{
    NSMutableArray *arrAwards = nil;
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    arrAwards=[[NSMutableArray alloc] init];
    
    [arrAwards  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSString *awardsStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 0)];
                NSString *CompanyIdStr = [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 1)];
                NSString *descriptionStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 2)];
                NSString *idStr=[[NSString alloc] initWithUTF8String:
                                 (const char *) sqlite3_column_text(statement, 3)];
                
                NSDictionary *dic= @{@"awards":awardsStr,@"company_id":CompanyIdStr,@"description":descriptionStr,@"id":idStr};
                
                [arrAwards addObject:dic];
            }
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    return arrAwards;
}
-(NSMutableArray*)getStates:(NSString *)query
{
    NSMutableArray *arrAwards = nil;
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    arrAwards=[[NSMutableArray alloc] init];
    
    [arrAwards  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSString *awardsStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 1)];
//                NSString *CompanyIdStr = [[NSString alloc] initWithUTF8String:
//                                          (const char *) sqlite3_column_text(statement, 1)];
//                NSString *descriptionStr = [[NSString alloc] initWithUTF8String:
                                            //(const char *) sqlite3_column_text(statement, 2)];
                NSString *idStr=[[NSString alloc] initWithUTF8String:
                                 (const char *) sqlite3_column_text(statement, 3)];
                
                NSDictionary *dic= @{@"name":awardsStr,@"abr":idStr};
                
                [arrAwards addObject:dic];
            }
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    return arrAwards;
}

#pragma mark-----loaddataSocialLinks-----------

-(NSMutableArray*) loaddataSocialLinks:(NSString *)query  strForscreen:(NSString *)strForscreen
{
    NSMutableArray *arrSocialLinks = nil;
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    arrSocialLinks=[[NSMutableArray alloc] init];
    
    [arrSocialLinks  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSString *company_idStr = [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 0)];
                NSString *idStr = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 1)];
                NSString *media_linkStr = [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 2)];
                NSString *media_logoStr=[[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 3)];
                NSString *media_typeStr=[[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 4)];
                NSDictionary *dic= @{@"company_id":company_idStr,@"id":idStr,@"media_link":media_linkStr,@"media_logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,media_logoStr]],@"media_type":media_typeStr};
                [arrSocialLinks addObject:dic];
            }
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    return arrSocialLinks;
}

#pragma mark- LoadFromSubPortLocation

-(NSMutableArray*) loaddataFromSubPortLocation:(NSString *)query  strForscreen:(NSString *)strForscreen
{
    NSMutableArray *arrLocations = nil;
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    arrLocations=[[NSMutableArray alloc] init];
    
    //    }
    [arrLocations  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSString *companyIdStr = [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 0)];
                NSString *idStr = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 1)];
                NSString *branch_nameStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 2)];
                NSString *latStr=[[NSString alloc] initWithUTF8String:
                                  (const char *) sqlite3_column_text(statement, 3)];
                NSString *longStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 4)];
                NSString *locationStr=[[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 5)];
                
                
                NSDictionary *dic= @{@"company_id":companyIdStr,@"id":idStr,@"branch_name":branch_nameStr,@"lat":latStr,@"long":longStr,@"location":locationStr};
                
                [arrLocations addObject:dic];
                
            }
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    return arrLocations;
}


#pragma mark------loadDiscSubDiscForCompanies--------

-(NSMutableArray*) loaddataForDispSubDisp:(NSString *)query strForscreen:(NSString *)strForscreen
{
    NSMutableArray *arrDispSubDisp = nil;
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    arrDispSubDisp=[[NSMutableArray alloc] init];
    
    [arrDispSubDisp  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *companyIdStr = [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 0)];
                NSString *discipline_idStr = [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 1)];
                NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 2)];
                NSDictionary *dic;
                
                if ([strForscreen isEqualToString:@"searchCompanyDisciplines"])
                {
                    NSString *typeStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 3)];
                    
                    
                    dic= @{@"company_id":companyIdStr,@"discipline_id":discipline_idStr,@"name":nameStr,@"sub_discipline":[self loaddataPortSubDisciplines:[NSString stringWithFormat:@"SELECT * FROM SearchCompSubDisciplineTable where discipline_id =\"%@\" AND company_id=\"%@\" ",discipline_idStr,companyIdStr] strForscreen:nil],@"type":typeStr};
                    
                    [arrDispSubDisp addObject:dic];
                }
                else if ([strForscreen isEqualToString:@"searchCompanyDisciplinesFilter"])
                {
                    dic= @{@"company_id":companyIdStr,@"discipline_id":discipline_idStr,@"name":nameStr,@"sub_discipline":[self loaddataPortSubDisciplines:[NSString stringWithFormat:@"SELECT * FROM SearchCompSubDisciplineTable where discipline_id =\"%@\" AND company_id=\"%@\" ",discipline_idStr,companyIdStr] strForscreen:nil]};
                    
                    [arrDispSubDisp addObject:dic];
                }
                
                else if ([strForscreen isEqualToString:@"DisciplinesPort"])
                {
                    dic= @{@"company_id":companyIdStr,@"discipline_id":discipline_idStr,@"name":nameStr,@"sub_discipline":[self loaddataPortSubDisciplines:[NSString stringWithFormat:@"SELECT * FROM PortSubDisciplinesTable where discipline_id =\"%@\" AND company_id=\"%@\"",discipline_idStr,companyIdStr] strForscreen:nil]};
                    
                    [arrDispSubDisp addObject:dic];
                }
                
                else if([strForscreen isEqualToString:@"disci"])
                {
                    [arrDispSubDisp addObject:companyIdStr];
                }
            }
            
            sqlite3_finalize(statement);
            
            sqlite3_close(sqlite3Database);
        }
    }
    
    return arrDispSubDisp;
}


-(NSMutableArray*) loaddataForDispSubDispCompanies:(NSString *)query strForscreen:(NSString *)strForscreen
{
    NSMutableArray *arrDispSubDisp = nil;
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    arrDispSubDisp=[[NSMutableArray alloc] init];
    
    [arrDispSubDisp  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *companyIdStr = [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 0)];
                NSString *discipline_idStr = [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 1)];
                NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 2)];
                NSDictionary *dic;
                
                if ([strForscreen isEqualToString:@"searchCompanyDisciplines"])
                {
                    NSString *typeStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 3)];
                    
                    
                    dic= @{@"company_id":companyIdStr,@"discipline_id":discipline_idStr,@"name":nameStr,@"sub_discipline":[self loaddataPortSubDisciplines:[NSString stringWithFormat:@"SELECT * FROM SearchCompSubDisciplineTable where discipline_id =\"%@\" AND company_id=\"%@\" ",discipline_idStr,companyIdStr] strForscreen:nil],@"type":typeStr};
                    
                    [arrDispSubDisp addObject:dic];
                }
                else if ([strForscreen isEqualToString:@"searchCompanyDisciplinesFilter"])
                {
                    dic= @{@"company_id":companyIdStr,@"discipline_id":discipline_idStr,@"name":nameStr,@"sub_discipline":[self loaddataPortSubDisciplines:[NSString stringWithFormat:@"SELECT * FROM SearchCompSubDisciplineTable where discipline_id =\"%@\" AND company_id=\"%@\" ",discipline_idStr,companyIdStr] strForscreen:nil]};
                    
                    [arrDispSubDisp addObject:dic];
                }
                
                else if ([strForscreen isEqualToString:@"DisciplinesPort"])
                {
                    dic= @{@"company_id":companyIdStr,@"discipline_id":discipline_idStr,@"name":nameStr,@"sub_discipline":[self loaddataPortSubDisciplines:[NSString stringWithFormat:@"SELECT * FROM PortSubDisciplinesTable where discipline_id =\"%@\" AND company_id=\"%@\"",discipline_idStr,companyIdStr] strForscreen:nil]};
                    
                    [arrDispSubDisp addObject:dic];
                }
                
                else if([strForscreen isEqualToString:@"disci"])
                {
                    [arrDispSubDisp addObject:companyIdStr];
                }
            }
            
            sqlite3_finalize(statement);
            
            sqlite3_close(sqlite3Database);
        }
    }
    
    return arrDispSubDisp;
}


#pragma mark------loadSubDisciplines------

-(NSMutableArray*) loaddataSubDisciplines:(NSString *)query  strForscreen:(NSString *)strForscreen
{
    NSMutableArray *arraySubDiscp =nil;
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    arraySubDiscp=[[NSMutableArray alloc] init];
    
    [arraySubDiscp  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *dateStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 0)];
                NSString *idStr = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 1)];
                NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 2)];
                NSString *discipline_idStr = [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 3)];
                NSString *companysStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 4)];
                
                NSDictionary *dic;
                
                dic= @{@"date":dateStr,@"id":idStr,@"name":nameStr,@"discipline_id":discipline_idStr,@"companys":companysStr};
                
                [arraySubDiscp addObject:dic];
            }
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    
    return arraySubDiscp;
    
}
#pragma mark------loaddataQuesAns------

-(NSMutableArray*) loaddataQuesAns:(NSString *)query  strForscreen:(NSString *)strForscreen
{
    NSMutableArray *arrayQuesAns =nil;
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    arrayQuesAns=[[NSMutableArray alloc] init];
    
    [arrayQuesAns  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSString *answerStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 0)];
                NSString *company_id_Str = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 1)];
                NSString *idStr = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 2)];
                NSString *questionStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 3)];
                
                
                NSDictionary *dic = @{@"answer":answerStr,@"company_id":company_id_Str,@"id":idStr,@"question":questionStr};
                
                [arrayQuesAns addObject:dic];
                
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    
    return arrayQuesAns;
    
}
#pragma mark------loaddataPortSubDisciplines------

-(NSMutableArray*) loaddataPortSubDisciplines:(NSString *)query  strForscreen:(NSString *)strForscreen
{
    NSMutableArray *arraySubDiscp =nil;
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    arraySubDiscp=[[NSMutableArray alloc] init];
    
    [arraySubDiscp  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *disciplineIdStr = [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 0)];
                NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 1)];
                NSString *company_id_Str = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 2)];
                NSString *sub_discipline_idStr = [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 3)];
                
                NSDictionary *dic = @{@"discipline_id":disciplineIdStr,@"name":nameStr,@"company_id":company_id_Str,@"sub_discipline_id":sub_discipline_idStr};
                
                [arraySubDiscp addObject:dic];
            }
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    
    return arraySubDiscp;
}

#pragma mark------loadDataMessages-----------

-(NSMutableArray*) loadDataMessages:(NSString *)query
{
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    
    sqlite3_stmt  *statement;
    
    if (_arrResults==nil)
    {
        _arrResults=[[NSMutableArray alloc] init];
    }
    
    [_arrResults  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
        
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSString *adder_idStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 0)];
                NSString *dateStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 1)];
                NSString *event_dateStr = [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 2)];
                NSString *event_titleStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 3)];
                NSString *event_venueStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 4)];
                NSString *eventtimefromStr = [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 5)];
                NSString *eventtimetoStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 6)];
                NSString *genderStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 7)];
                NSString *idStr = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 8)];
                NSString *messageStr = [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 9)];
                NSString *message_typeStr = [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 10)];
                NSString *statusStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 11)];
                NSString *typeStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 12)];
                NSString *logoStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 13)];
                NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 14)];
                
                NSString *eventconfirmationStr = [[NSString alloc] initWithUTF8String:
                                                  (const char *) sqlite3_column_text(statement, 15)];
                NSString *default_timezoneStr = [[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 16)];
                NSString *websiteStr = [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 17)];
                NSString *reportStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 18)];
                NSString *like_statusStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 19)];
                NSString *addedStatusStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 20)];
                
                NSString *eventstatusStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 21)];
                
                NSString *logo_perfectStr = [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 22)];
                
                
                NSDictionary *dic= @{@"adder_id":adder_idStr
                                     ,@"date":dateStr
                                     ,@"event_date":event_dateStr
                                     ,@"event_title":event_titleStr
                                     ,@"event_venue":event_venueStr
                                     ,@"eventtimefrom":eventtimefromStr
                                     ,@"eventtimeto":eventtimetoStr
                                     ,@"gender":genderStr
                                     ,@"id":idStr
                                     ,@"message":messageStr
                                     ,@"message_type":message_typeStr
                                     ,@"status":statusStr
                                     ,@"type":typeStr
                                     ,@"adder_detail":@{@"id":adder_idStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",adder_idStr,logoStr]],@"name":nameStr}
                                     ,@"eventconfirmation":eventconfirmationStr
                                     ,@"default_timezone":default_timezoneStr
                                     ,@"website":websiteStr
                                     ,@"report":reportStr
                                     ,@"like_status":like_statusStr
                                     ,@"addedStatus":addedStatusStr,
                                     @"eventStatus":eventstatusStr,@"perfect_logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",adder_idStr,logo_perfectStr]]
                                     };
                
                [_arrResults addObject:dic];
            }
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    return _arrResults;
}

#pragma mark------------loadCountries-----------




-(NSMutableArray*) loadFilterdata:(NSString *)query  strForscreen:(NSString *)strForscreen arryCompanyId:(NSArray *)companyId
{
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt  *statement;
    
    if (_arrResults==nil)
    {
        _arrResults=[[NSMutableArray alloc] init];
    }
    
    [_arrResults  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            for (int i =0; i<companyId.count; i++)
            {
                
                NSString *idStr = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 0)];
                
                NSString *application_due_dateStr = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                
                NSString *awardsStr=[[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 2)];
                
                NSString *cityStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 3)];
                
                NSString *company_addressStr = [[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 4)];
                
                NSString *company_nameStr=[[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 5)];
                
                NSString *descriptionStr = [[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 6)];
                
                NSString *logoStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 7)];
                
                NSString *state_idStr=[[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 8)];
                
                NSDictionary *dic = nil;
                
                if ([strForscreen isEqualToString:@"port"])
                    
                {
                    NSString *unread_messageStr=[[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 9)];
                    NSString *regstatesStr=[[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 10)];
                    NSString *company_introStr=[[NSString alloc] initWithUTF8String:
                                                (const char *) sqlite3_column_text(statement, 11)];
                    
                    const char* date = (const char*)sqlite3_column_text(statement, 12);
                    
                    NSString *countryStr = date == NULL ? nil : [[NSString alloc] initWithUTF8String:date];
                    
                    NSString *message_statusStr=[[NSString alloc] initWithUTF8String:
                                                 (const char *) sqlite3_column_text(statement, 13)];
                    NSString *websiteLinkStr=[[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 14)];
                    
                    const char* date1 = (const char*)sqlite3_column_text(statement, 15);
                    
                    NSString *internationalStr = date1 == NULL ? nil : [[NSString alloc] initWithUTF8String:date1];
                    
                    NSString *perfect_logo=[[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 16)];
                    
                    NSString *society_logo=[[NSString alloc] initWithUTF8String:
                                            (const char *) sqlite3_column_text(statement, 17)];
                    
                    if (internationalStr == nil|| countryStr== nil)
                        
                    {
                        
                        dic= @{@"id":idStr,@"application_due_date":application_due_dateStr,@"awards":awardsStr,@"city":cityStr,@"company_address":company_addressStr,@"company_name":company_nameStr,@"description":descriptionStr,@"logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,logoStr]],@"state_id":state_idStr,@"unread_message":unread_messageStr,@"location":[self loaddataFromSubPortLocation:[NSString stringWithFormat:@"SELECT * FROM PortLocationsTable where company_id =\"%@\"",idStr]strForscreen:nil],@"application_due_date_all":[self loaddataSubPortDueDate:[NSString stringWithFormat:@"SELECT * FROM PortCompanyDueDate where company_id =\"%@\"",idStr]strForscreen:nil],@"allAwards":[self loaddataSubAwards:[NSString stringWithFormat:@"SELECT * FROM PortCompanyAwards where company_id =\"%@\"",idStr] strForscreen:nil],@"disciplines":[self loaddataForDispSubDispCompanies:[NSString stringWithFormat:@"SELECT * FROM PortDisciplinesTable where company_id =\"%@\"",idStr] strForscreen:@"DisciplinesPort"],@"quesAns":[self loaddataQuesAns:[NSString stringWithFormat:@"SELECT * FROM PortQuesAnsTable where company_id =\"%@\"",idStr] strForscreen:nil],@"links":[self loaddataSocialLinks:[NSString stringWithFormat:@"SELECT * FROM PortCompSocialLinksTable where company_id =\"%@\"",idStr] strForscreen:nil],@"regstates":regstatesStr,@"company_intro":company_introStr,@"country":@"",@"message_status":message_statusStr,@"website_link":websiteLinkStr,@"internationl":@"",@"perfect_logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,perfect_logo]],@"society_logo":[self imageUrl:[NSString stringWithFormat:@"%@%@",idStr,society_logo]]};
                    }
                }}}
    }
    
    return _arrResults;
}


-(NSMutableArray*) loadLocaldataSubDisciplines:(NSString *)query  strForscreen:(NSString *)strForscreen
{
    NSMutableArray *arraySubDiscp =nil;
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    arraySubDiscp=[[NSMutableArray alloc] init];
    
    [arraySubDiscp  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *dateStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 0)];
                NSString *idStr = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 1)];
                NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 2)];
                NSString *discipline_idStr = [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 3)];
                NSString *companysStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 4)];
                NSDictionary *dic = @{@"date":dateStr,@"id":idStr,@"name":nameStr,@"discipline_id":discipline_idStr,@"companys":companysStr,@"compniesId":[self loaddataSubDisciplines:[NSString stringWithFormat:@"SELECT * FROM SubDisciplines where discipline_id =\"%@\"",idStr] strForscreen:@"DISCIPLINES"]};
                
                [arraySubDiscp addObject:dic];
            }
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    
    return arraySubDiscp;
    
}

-(NSMutableArray*) loadlocaldataCountries:(NSString *)query  strForscreen:(NSString *)strForscreen arrayId:(NSArray *)ids companyId:(NSArray *)arrayCompId
{
    
    arrayIds = ids;
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    if (_arrResults==nil)
    {
        _arrResults=[[NSMutableArray alloc] init];
    }
    [_arrResults  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSDictionary *dic = nil;
                
                if ([strForscreen isEqualToString:@"DISCIPLINES"])
                {
                    NSString *idStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 0)];
                    NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 1)];
                    
                    dic = @{@"id":idStr,@"name":nameStr,@"subdiscipline":[self loaddataSubDisciplines:[NSString stringWithFormat:@"SELECT * FROM SubDisciplines where discipline_id =\"%@\"",idStr] strForscreen:@"DISCIPLINES"]};
                }
                else   if ([strForscreen isEqualToString:@"DISCIPLINESFilter"])
                {
                    NSString *idStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 0)];
                    NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 1)];
                    
                    dic = @{@"id":idStr,@"name":nameStr,@"subdiscipline":[self loaddataSubDisciplines:[NSString stringWithFormat:@"SELECT * FROM SubDisciplinesFilter where discipline_id =\"%@\"",idStr] strForscreen:@"DISCIPLINESFilter"]};
                }
                else
                {
                    NSString *idStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 0)];
                    NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 1)];
                    NSString *abrStr = [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 2)];
                    dic = @{@"id":idStr,@"name":nameStr,@"abr":abrStr};
                }
                [_arrResults addObject:dic];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    return _arrResults;
}

-(NSMutableArray*) loaddataCountries:(NSString *)query  strForscreen:(NSString *)strForscreen
{
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    if (_arrResults==nil)
    {
        _arrResults=[[NSMutableArray alloc] init];
    }
    [_arrResults  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSDictionary *dic = nil;
                
                if ([strForscreen isEqualToString:@"DISCIPLINES"])
                {
                    NSString *idStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 0)];
                    NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 1)];
                    
                    dic = @{@"id":idStr,@"name":nameStr,@"subdiscipline":[self loaddataSubDisciplines:[NSString stringWithFormat:@"SELECT * FROM SubDisciplines where discipline_id =\"%@\"",idStr] strForscreen:@"DISCIPLINES"]};
                }
                else   if ([strForscreen isEqualToString:@"DISCIPLINESFilter"])
                {
                    NSString *idStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 0)];
                    NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 1)];
                    
                    dic = @{@"id":idStr,@"name":nameStr,@"subdiscipline":[self loaddataSubDisciplines:[NSString stringWithFormat:@"SELECT * FROM SubDisciplinesFilter where discipline_id =\"%@\"",idStr] strForscreen:@"DISCIPLINESFilter"]};
                }
                else
                {
                    NSString *idStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 0)];
                    NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 1)];
                    NSString *abrStr = [[NSString alloc] initWithUTF8String:
                                        (const char *) sqlite3_column_text(statement, 2)];
                    
                    dic = @{@"id":idStr,@"name":nameStr,@"abr":abrStr};
                }
                [_arrResults addObject:dic];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    return _arrResults;
}

#pragma mark------------loadStates-----------

-(NSMutableArray*) loaddataStates:(NSString *)query
{
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    
    sqlite3_stmt    *statement;
    
    if (_arrResults == nil)
    {
        _arrResults=[[NSMutableArray alloc] init];
    }
    [_arrResults  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *idStr = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 0)];
                NSString *nameStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 1)];
                NSString *country_idStr = [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 2)];
                NSString *abrStr = [[NSString alloc] initWithUTF8String:
                                    (const char *) sqlite3_column_text(statement, 3)];
                
                NSDictionary *dic= @{@"id":idStr,@"name":nameStr ,@"country_id":country_idStr,@"abr":abrStr};
                
                [self.arrResults addObject:dic];
            }
            
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    return _arrResults;
}

#pragma mark-------LoadSalaries---------

-(NSMutableArray*) loaddataSalaries:(NSString *)query
{
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *statement;
    
    if (_arrResults==nil)
    {
        _arrResults=[[NSMutableArray alloc] init];
    }
    [_arrResults  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *idStr = [[NSString alloc] initWithUTF8String:
                                   (const char *) sqlite3_column_text(statement, 0)];
                NSString *occupationStr = [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 1)];
                NSString *occupationIdStr = [[NSString alloc] initWithUTF8String:
                                             (const char *) sqlite3_column_text(statement, 2)];
                
                NSString *salary03Str = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 3)];
                NSString *titleStr = [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 4)];
                
                NSString *sectorStr = [[NSString alloc] initWithUTF8String:
                                       (const char *) sqlite3_column_text(statement, 5)];
                NSString *sector_idStr = [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 6)];
                
                NSString *salary3to5Str = [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 7)];
                NSString *salary5to7Str = [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 8)];
                
                NSString *yearStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 9)];
                
                NSString *largeStr = [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 10)];
                NSString *smallMedStr = [[NSString alloc] initWithUTF8String:
                                         (const char *) sqlite3_column_text(statement, 11)];
                NSString *typeStr = [[NSString alloc] initWithUTF8String:
                                     (const char *) sqlite3_column_text(statement, 12)];
                
                
                NSDictionary *dic= @{@"id":idStr,@"occupation":occupationStr,@"occupation_id":occupationIdStr,@"salary0to3":salary03Str,@"title":titleStr,@"sector":sectorStr,@"sector_id":sector_idStr,@"salary3to5":salary3to5Str,@"salary5to7":salary5to7Str,@"year":yearStr,@"large":largeStr,@"smallMedium":smallMedStr,@"type":typeStr};
                
                [self.arrResults addObject:dic];
            }
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    return _arrResults;
}


-(NSMutableArray*) loaddataBanners:(NSString *)query
{
    
    stringFromBanner = @"banner";
    
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    if (_arrResults==nil) {
        _arrResults=[[NSMutableArray alloc] init];
        
    }
    [_arrResults  removeAllObjects];
    
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare_v2(sqlite3Database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                
                
                NSString *image_nameStr = [[NSString alloc] initWithUTF8String:
                                           (const char *) sqlite3_column_text(statement, 0)];
                
                NSString *rotation_timeStr = [[NSString alloc] initWithUTF8String:
                                              (const char *) sqlite3_column_text(statement, 1)];
                
                NSString *id_str = [[NSString alloc] initWithUTF8String:
                                    (const char *) sqlite3_column_text(statement, 2)];
                
                NSString *name_str = [[NSString alloc] initWithUTF8String:
                                      (const char *) sqlite3_column_text(statement, 3)];
                
                NSString *stringBanner = [[NSString alloc] initWithUTF8String:
                                          (const char *) sqlite3_column_text(statement, 4)];
                
                NSDictionary *dic= @{@"image_name":[self imageUrl:[NSString stringWithFormat:@"%@%@",id_str,image_nameStr]],@"rotation_time":rotation_timeStr,@"id":id_str,@"name":name_str,@"bannerUrl":stringBanner};
                
                [self.arrResults addObject:dic];
            }
            
            sqlite3_finalize(statement);
            
            sqlite3_close(sqlite3Database);
            
        }
    }
    
    return _arrResults;
    
}

#pragma mark ---- StudentDetail

-(void)runStudentDetail:(const char *)query
{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {  sqlite3_stmt    *statement;
        char *errMsg;
        const char *sql_stmt = "CREATE TABLE IF NOT EXISTS StudentDetail (name TEXT)";
        if (sqlite3_exec(sqlite3Database, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
        {
            sqlite3_prepare_v2(sqlite3Database, query, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
                
                sqlite3_finalize(statement);
            
        }
        sqlite3_close(sqlite3Database);
    }
    
    
    
    else {
        NSLog(@"Failed");
        
    }
    
}



#pragma mark--------forDisciplines---------

-(void)runQueryDisciplines:(const char *)query
{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {  sqlite3_stmt    *statement;
        char *errMsg;
        const char *sql_stmt = "CREATE TABLE IF NOT EXISTS Disciplines (id Text, name TEXT,date Text,status Text)";
        if (sqlite3_exec(sqlite3Database, sql_stmt, NULL, NULL, &errMsg) == SQLITE_OK)
        {
            
            sqlite3_prepare_v2(sqlite3Database, query, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
                
                sqlite3_finalize(statement);
            
        }
        sqlite3_close(sqlite3Database);
    }
    
    
    
    else {
        NSLog(@"Failed");
        
    }
    
}

-(BOOL)deleteTable:(NSString*)strQuery
{
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
    {
        sqlite3_stmt    *statement;
        
        const char *delete_stmt = [strQuery UTF8String];
        sqlite3_prepare_v2(sqlite3Database, delete_stmt, -1, &statement, NULL );
        
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        sqlite3_finalize(statement);
        sqlite3_close(sqlite3Database);
    }
    else
    {
        NSLog(@"Failed");
        return NO;
    }
    return YES;
}


#pragma mark-ImagesFromAppDirectory

-(NSString*)imageUrl:(NSString*)imageName
{
    {
        imageName = [imageName stringByReplacingOccurrencesOfString:@"/" withString:@""];
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    if ([contents containsObject:imageName])
    {
        return [documentsDirectory stringByAppendingPathComponent:imageName];
    }
    
    return @"N";
}





@end
