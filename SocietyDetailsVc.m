//
//  SocietyDetailsVc.m
//  PerfectGraduate
//
//  Created by netset on 3/26/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "SocietyDetailsVc.h"

@interface SocietyDetailsVc ()
{
    NSDictionary *dictData;
    NSMutableArray *arraySociety;
    NSString *stringTag;
    NSMutableArray *arraySocietyIds;
    UIActivityIndicatorView *spinner;
}

@end

@implementation SocietyDetailsVc

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backToStudentPageBtnAction)];
    
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIImageView *imageBg = [[UIImageView alloc] initWithFrame:self.view.frame];
    [imageBg setImage:[UIImage imageNamed:@"appBack"]];
    imageBg.contentMode = UIViewContentModeScaleAspectFill;
    imageBg.clipsToBounds = YES;
    [self.view addSubview:imageBg];
    [self.view sendSubviewToBack:imageBg];
    
    arraySocietyIds = [[NSMutableArray alloc] init];
    
    _pickerVwSociety.backgroundColor =[UIColor colorWithRed:233.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.0f];
    
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    _viewButtons.backgroundColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    UIView *viewTitles = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    UILabel *lblNavTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(viewTitles.frame), 22)];
    
    lblNavTitle.textAlignment = NSTextAlignmentCenter;
    lblNavTitle.text = @"Student Society";
    lblNavTitle.font = [UIFont fontWithName:@"OpenSans" size:18.0f];
    lblNavTitle.textColor = [UIColor whiteColor];
    
    UILabel *lblNavSubTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 22, CGRectGetWidth(viewTitles.frame), 18)];
    lblNavSubTitle.textAlignment = NSTextAlignmentCenter;
    
    lblNavSubTitle.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"userUniversity"];
    lblNavSubTitle.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
    
    lblNavSubTitle.textColor = [UIColor whiteColor];
    
    [viewTitles addSubview:lblNavTitle];
    [viewTitles addSubview:lblNavSubTitle];
    self.navigationItem.titleView = viewTitles;
    
    _buttonSubmit.layer.cornerRadius = 5.0f;
    _buttonSubmit.layer.borderWidth = 2.0f;
    
    _buttonSubmit.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]  CGColor];
    [_buttonSubmit setTitleColor:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    
    _pickerVwSociety.hidden = YES;
    _viewButtons.hidden = YES;
    
    _buttonSubmit.layer.borderColor = [[UIColor lightGrayColor]  CGColor];
    [_buttonSubmit setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_buttonSubmit setEnabled:NO];
    
    
    if ([_isFromScreen isEqualToString:@"menu"])
    {
        [self.navigationItem setHidesBackButton:NO];
        
        UIView *viewAddBtnsLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 40)];
        
        UIButton * buttonMenu = [[UIButton alloc]initWithFrame:CGRectMake(-3, 4, 30, 30) ];
        [buttonMenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
        
        buttonMenu.backgroundColor = [UIColor clearColor];
        
        [buttonMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        
        [viewAddBtnsLeft addSubview:buttonMenu];
        
        if(![Utilities CheckInternetConnection])
        {
            [self placeSocietiesOffline];
        }
        else
        {
            [self getStudentSocieties];
        }
    }
    else if ([_isFromScreen isEqualToString:@"uniChange"])
    {
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationController.navigationItem.hidesBackButton = YES;
        [self getAllStudentSocieties];
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"Almost there..." message:@"\nShow your support for student societies by selecting them.\n\nSocieties will be able to inform you of upcoming company and social events." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
    }
    
    [self getAllStudentSocieties];
    
}


-(void)placeSocietiesOffline
{
    for (int j=0; j<10; j++)
    {
        UITextField *fld = (UITextField*)[self.view viewWithTag:j+10];
        fld.userInteractionEnabled = NO;
        fld.backgroundColor = [UIColor whiteColor];
    }
    
    NSDictionary *dicSocieties = [[NSUserDefaults standardUserDefaults] objectForKey:@"userSocieties"];
    
    int count= 0;
    
    for (int i=0; i<[[dicSocieties valueForKeyPath:@"data.society_name"] count]; i++)
    {
        count++;
        
        UITextField *tf = (UITextField*)[self.view viewWithTag:10+i];
        tf.text = [[dicSocieties valueForKeyPath:@"data.society_name"] objectAtIndex:i];
        tf.backgroundColor = [UIColor whiteColor];
    }
    
    [arraySocietyIds addObjectsFromArray:[dicSocieties valueForKeyPath:@"data.id"]];
}


- (void)setRightViewFortextField:(UITextField *)textField
{
    UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetHeight(textField.frame), CGRectGetHeight(textField.frame))];
    imgV.contentMode = UIViewContentModeCenter;
    imgV.image = [UIImage imageNamed:@"FIRST"];
    textField.rightView = imgV;
    textField.rightViewMode = UITextFieldViewModeAlways;
    UIImageView *imgVLeft = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 10, 5)];
    textField.leftView = imgVLeft;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    CALayer *layer1 = [[CALayer alloc]init];
    layer1.frame = CGRectMake(0, 0, CGRectGetWidth(textField.frame)-CGRectGetHeight(textField.frame)+1, CGRectGetHeight(textField.frame));
    layer1.borderColor = [[UIColor lightGrayColor] CGColor];
    layer1.borderWidth = 1.0;
    
    [textField.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc]init];
    layer2.frame = CGRectMake(CGRectGetWidth(textField.frame)-CGRectGetHeight(textField.frame), 0,CGRectGetHeight(textField.frame), CGRectGetHeight(textField.frame));
    layer2.borderColor = [[UIColor lightGrayColor] CGColor];
    layer2.borderWidth = 1.0;
    [textField.layer addSublayer:layer2];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    stringNotifiction = @"";
    [super viewWillAppear:animated];
    stringForNotifyEditCompany = @"";
    
    for (int i=0; i<6; i++)
    {
        [arraySocietyIds addObject:@"0"];
    }
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    for (int i=10; i<16; i++)
    {
        UITextField *textField = (UITextField *)[self.view viewWithTag:i];
        [self setRightViewFortextField:textField];
    }
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self ShowDataLoader:NO];
}


-(void)enableButton
{
    _buttonSubmit.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]  CGColor];
    [_buttonSubmit setTitleColor:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [_buttonSubmit setEnabled:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- WebServices

-(void)getStudentSocieties
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"societyDetail" parameters:@{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]} success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [[NSUserDefaults standardUserDefaults] setValue:responseObject forKey:@"userSocieties"];
             
             [[NSUserDefaults standardUserDefaults]synchronize];
             
             int count= 0;
             
             for (int i=0; i<[[responseObject valueForKeyPath:@"data.society_name"] count]; i++)
                 
             {
                 count++;
                 
                 UITextField *tf = (UITextField*)[self.view viewWithTag:10+i];
                 tf.text = [[responseObject valueForKeyPath:@"data.society_name"] objectAtIndex:i];
                 
                 tf.backgroundColor = [UIColor whiteColor];
                 
                 [arraySocietyIds replaceObjectAtIndex:i withObject:[[responseObject valueForKeyPath:@"data.id"] objectAtIndex:i]];
                 
                 [self enableButton];
             }
         }
         else
         {
             NSLog(@"wrong result");
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"%@",error.localizedDescription);
         [self placeSocietiesOffline];
         
     }];
}


-(void)getAllStudentSocieties
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"getAllStudentSocieties" parameters:@{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userUniversityId"]} success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             dictData = [[NSDictionary alloc] initWithDictionary:responseObject];
             
             if (!arraySociety) {
                 arraySociety = [[NSMutableArray alloc] init];
             }
             arraySociety = [[dictData valueForKey:@"data"] mutableCopy];
             
             [_pickerVwSociety reloadAllComponents];
         }
         else
         {
             NSLog(@"wrong result");
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         NSLog(@"%@",error.localizedDescription);
         
     }];
}


-(void)updateSocieties
{
    if (arraySocietyIds.count>0)
    {
        [arraySocietyIds removeObject:@"0"];
    }
    else
    {
        return;
    }
    
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *dicParam =@{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"sid":[arraySocietyIds componentsJoinedByString:@","]};
    
    [manager POST:@"addAllSociety" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             if ([_isFromScreen isEqualToString:@"menu"]||[_isFromScreen isEqualToString:@"uniChange"])
             {
                 PortfolioViewController *portView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
                 UINavigationController * navPort = [[UINavigationController alloc]initWithRootViewController:portView];
                 [self.revealViewController setFrontViewController:navPort animated:NO];
             }
             
             [[[UIAlertView alloc]initWithTitle:@"Student Society Selections" message:@"\nYour student society selections have been successfully updated" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             NSLog(@"wrong result");
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}


#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if ([stringTag isEqualToString:@"10"])
    {
        if (arraySociety.count>0)
        {
            if ([[[arraySociety valueForKey:@"society_name"] objectAtIndex:0] isEqualToString:@"None"])
            {
                [arraySociety removeObjectAtIndex:0];
            }
        }
    }
    else
    {
        if (arraySociety)
        {
            if (![[[arraySociety valueForKey:@"society_name"] objectAtIndex:0] isEqualToString:@"None"])
            {
                [arraySociety insertObject:@{@"society_name":@"None",@"id":@"0"} atIndex:0];
            }
        }
    }
    
    return [arraySociety count];
}

#pragma mark- Picker View Delegate

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSString *stringTitle = [[arraySociety valueForKey:@"society_name"] objectAtIndex:row];
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:stringTitle attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]}];
    return attString;
}

-(void)viewChange:(int)frameY
{
    CGRect frame = _viewCompContainer.frame;
    frame.origin.y = frameY;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:0.5];
    _viewCompContainer.frame = frame;
    [UIView commitAnimations];
}

#pragma mark- ButtonActions

-(void)backToStudentPageBtnAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark----UITextFieldDelegate----

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self buttonOpenPickerAction:textField];
    [self.view endEditing:YES];
}

#pragma mark- buttonActions
-(void)buttonOpenPickerAction:(id)sender
{
    
    if ([sender tag] == 13 || [sender tag] == 14 || [sender tag] == 15)
    {
        [self viewChange:-120];
    }
    else
    {
        [self viewChange:60];
    }
    
    _pickerVwSociety.hidden = NO;
    _viewButtons.hidden = NO;
    
    if ([sender tag]==10)
    {
        stringTag = @"10";
    }
    else if ([sender tag]==11)
    {
        stringTag = @"11";
    }
    else if ([sender tag]==12)
    {
        stringTag = @"12";
    }
    else if ([sender tag]==13)
    {
        stringTag = @"13";
    }
    else if ([sender tag]==14)
    {
        stringTag = @"14";
    }
    else
    {
        stringTag = @"15";
    }
    
    [_pickerVwSociety reloadAllComponents];
}

- (IBAction)buttonSubmitAction:(id)sender
{
    [self viewChange:60];
    
    _pickerVwSociety.hidden = YES;
    _viewButtons.hidden = YES;
    [arraySocietyIds removeObject:@"0"];
    
    if ([_isFromScreen isEqualToString:@"menu"]||[_isFromScreen isEqualToString:@"uniChange"])
    {
        [self updateSocieties];
    }
    else if (arraySocietyIds.count>0)
    {
        TermsConditionViewController *termsConditionView = [self.storyboard instantiateViewControllerWithIdentifier:@"termsConditionView"];
        termsConditionView.arrSocietiesFromScPg = arraySocietyIds;
        termsConditionView.dicUserInfoFromScPg = _dictUserInfoFromUsrPg;
        [self.navigationController pushViewController:termsConditionView animated:YES];
    }
}

- (IBAction)buttonCancelAction:(id)sender
{
    [self viewChange:60];
    
    _pickerVwSociety.hidden = YES;
    _viewButtons.hidden = YES;
    
}

- (IBAction)buttonDoneAction:(id)sender
{
    [self enableButton];
    
    NSInteger row;
    row = [_pickerVwSociety selectedRowInComponent:0];
    [self viewChange:60];
    
    _pickerVwSociety.hidden = YES;
    _viewButtons.hidden = YES;
    
    int count= 0;
    
    for (int i=0; i<7; i++)
    {
        count++;
        UITextField *tf = (UITextField*)[self.view viewWithTag:10+i];
        
        tf.backgroundColor = [UIColor whiteColor];
        
        if ([tf.text isEqualToString:[[arraySociety valueForKey:@"society_name"]objectAtIndex:row]])
        {
            return;
        }
    }
    
    if (dictData)
    {
        if ([stringTag isEqualToString:@"10"])
        {
            _textFldSocietyOne.text =[[arraySociety valueForKey:@"society_name"]objectAtIndex:row];
            [arraySocietyIds replaceObjectAtIndex:0 withObject:[[arraySociety valueForKey:@"id"]objectAtIndex:row]];
        }
        else if ([stringTag isEqualToString:@"11"])
        {
            if (![[[arraySociety valueForKey:@"society_name"]objectAtIndex:row] isEqualToString:@"None"])
            {
                _textFldSocietyTwo.text =[[arraySociety valueForKey:@"society_name"]objectAtIndex:row];
                [arraySocietyIds replaceObjectAtIndex:1 withObject:[[arraySociety valueForKey:@"id"]objectAtIndex:row]];
            }
            else
            {
                _textFldSocietyTwo.text = @"";
                [arraySocietyIds replaceObjectAtIndex:1 withObject:@"0"];
                
            }
        }
        else if ([stringTag isEqualToString:@"12"])
        {
            if (![[[arraySociety valueForKey:@"society_name"]objectAtIndex:row] isEqualToString:@"None"])
            {
                _textFldSocietyThree.text = [[arraySociety valueForKey:@"society_name"]objectAtIndex:row];
                
                [arraySocietyIds replaceObjectAtIndex:2 withObject:[[arraySociety valueForKey:@"id"]objectAtIndex:row]];
            }
            else
            {
                _textFldSocietyThree.text = @"";
                [arraySocietyIds replaceObjectAtIndex:2 withObject:@"0"];
                
            }
        }
        else if ([stringTag isEqualToString:@"13"])
        {
            if (![[[arraySociety valueForKey:@"society_name"]objectAtIndex:row] isEqualToString:@"None"])
            {
                _textFldSocietyFour.text = [[arraySociety valueForKey:@"society_name"]objectAtIndex:row];
                
                [arraySocietyIds replaceObjectAtIndex:3 withObject:[[arraySociety valueForKey:@"id"]objectAtIndex:row]];
            }
            else
            {
                _textFldSocietyFour.text = @"";
                [arraySocietyIds replaceObjectAtIndex:3 withObject:@"0"];
            }
        }
        else if ([stringTag isEqualToString:@"14"])
        {
            if (![[[arraySociety valueForKey:@"society_name"]objectAtIndex:row] isEqualToString:@"None"])
            {
                _textFldSocietyFive.text = [[arraySociety valueForKey:@"society_name"]objectAtIndex:row];
                
                [arraySocietyIds replaceObjectAtIndex:4 withObject:[[arraySociety valueForKey:@"id"]objectAtIndex:row]];
            }
            else
            {
                _textFldSocietyFive.text = @"";
                [arraySocietyIds replaceObjectAtIndex:4 withObject:@"0"];
            }
        }
        else
        {
            if (![[[arraySociety valueForKey:@"society_name"]objectAtIndex:row] isEqualToString:@"None"])
            {
                _textFldSocietySix.text = [[arraySociety valueForKey:@"society_name"]objectAtIndex:row];
                [arraySocietyIds replaceObjectAtIndex:5 withObject:[[arraySociety valueForKey:@"id"]objectAtIndex:row]];
            }
            else
            {
                _textFldSocietySix.text = @"";
                [arraySocietyIds replaceObjectAtIndex:5 withObject:@"0"];
            }
        }
    }
}


#pragma mark- UIActivityIndicatorView

-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        [self.view.window setUserInteractionEnabled:NO];
        spinner.center = self.view.center;
        spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [spinner startAnimating];
        [self.view addSubview:spinner];
        
    }
    else
    {
        [spinner stopAnimating];
        [self.view.window setUserInteractionEnabled:YES];
        [spinner removeFromSuperview];
        spinner = nil;
    }
}

#pragma mark- UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([_isFromScreen isEqualToString:@"menu"])
    {
        PortfolioViewController *portView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
        UINavigationController * navPort = [[UINavigationController alloc]initWithRootViewController:portView];
        [self.revealViewController setFrontViewController:navPort animated:NO];
    }
}

@end
