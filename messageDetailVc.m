

//
//  messageDetailVc.m
//  PerfectGraduate
//
//  Created by NSPL on 2/21/15.
//  Copyright (c) 2015 netset. All rights reserved.
//


#import "messageDetailVc.h"
#import "PortfolioViewController.h"
#import "DisciplineViewController.h"
#import "SalaryGuideViewController.h"
#import "MessagesViewController.h"
#import "LocationFilterSearchViewController.h"
#import "OpenMapViewController.h"
#import "Globals.h"
#import "MessageSubscriptionManager.h"

@interface messageDetailVc ()

{
    Database *dummy;
    
    UIActivityIndicatorView *spinner;
    
    UIButton * buttonSocialMsg;
    
    UIButton* buttonPort;
    
    OAuthLoginView *linkedInLoginView;
    
    NSString *stringCompanyId;
    
    NSString *stringRemoveSoc;
    
    NSString *stringFromRightButton;
    
    NSMutableArray *arrayRemovedSociety;
    
    NSMutableArray *arraySocities;
    
    NSString *stringAlrdyAdded;
}

@end

@implementation messageDetailVc

-(void)locationLatitudeLongitude
{
    //http://maps.google.com/maps/api/geocode/json?sensor=false&address=australia
    
    
    NSString *string = [NSString stringWithFormat:@"%@%@", @"http://maps.google.com/maps/api/geocode/json?sensor=false&address=",[_dicMessageDetail valueForKey:@"event_venue"]];
    
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *encoded = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    [manager POST:encoded parameters: nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         NSLog(@"response google is %@", responseObject);
         
         if ([[responseObject valueForKey:@"results"]count]==0)
             
         {
             latitudeEvent = 0.0;
             
             longitudeEvent = 0.0;
         }
         
         else
             
         {
             NSDictionary *dictgeom = [[responseObject valueForKeyPath:@"results.geometry"]objectAtIndex:0];
             
             NSDictionary *dicLoc = [dictgeom valueForKey:@"location"];
             
             NSLog(@"dic loc is %@", dicLoc);
             
             latitudeEvent = [[dicLoc valueForKey:@"lat"] floatValue];
             
             longitudeEvent = [[dicLoc valueForKey:@"lng"] floatValue];
             
             NSLog(@"View Controller get Location Logitute : %f",latitudeEvent);
             
             NSLog(@"View Controller get Location Latitute : %f",longitudeEvent);
             
             if ([[responseObject objectForKey:@"status"] isEqualToString:@"OK"])
             {
                 
             }
             
             else if ([[responseObject objectForKey:@"status"] isEqualToString:@"false"])
             {
                 
             }
             
             else if ([[responseObject objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
             {
                 latitudeEvent = 0.0;
                 
                 longitudeEvent = 0.0;
             }
             else
             {
                 NSLog(@"wrong result");
             }
         }
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if(![Utilities CheckInternetConnection])
         {
         }
         else
         {
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }
     }];
}


-(void)chnageStatusOfPortPage
{
    stringPortCompanyScreen = @"fromComapny";
    
    NSArray *arrayId= [arrayPortDataGlobal valueForKey:@"id"];
    
    if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
    {
        int index = (int)[arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]] ;
        
        NSMutableDictionary *dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayPortDataGlobal[index] copyItems:YES];
        
        int messageCount = [[dictChnage objectForKey:@"unread_message"] intValue];
        
        if (messageCount==0)
        {
            
        }
        else
        {
            messageCount = messageCount - 1;
        }
        
        [dictChnage removeObjectForKey:@"unread_message"];
        
        [dictChnage setObject:[NSString stringWithFormat:@"%d", messageCount] forKey:@"unread_message"];
        
        [arrayPortDataGlobal replaceObjectAtIndex:index withObject:dictChnage];
    }
}

-(void)changeDataInPortTable:(NSString *)stringValue
{
    stringPortCompanyScreen = @"fromComapny";
    
    if ([stringValue isEqualToString:@"0"])
    {
        NSArray *arrayId= [arrayPortDataGlobal valueForKey:@"id"];
        
        if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
        {
            int index = (int)[arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
            
            NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayPortDataGlobal[index] copyItems:YES];
            
            [dictChnage removeObjectForKey:@"portfolio_status"];
            
            [dictChnage setObject:stringValue forKey:@"portfolio_status"];
            
            [arrayPortDataGlobal removeObjectAtIndex:index];
        }
    }
    else
    {
        NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
        
        if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
        {
            int index = (int)[arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
            
            NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
            
            [dictChnage removeObjectForKey:@"portfolio_status"];
            
            [dictChnage setObject:@"1" forKey:@"portfolio_status"];
            
            
            [arrayPortDataGlobal addObject:dictChnage];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    dummy=[[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    if ([stringMesagFirstCome isEqualToString:@"stringFirst"])
    {
        if (arrayCompaniesList.count==0)
        {
            NSArray *arrayTemp = [dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"];
            
            NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
            
            NSArray* sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
            
            NSArray *arraySorted = [[NSArray alloc] initWithArray:[arrayTemp sortedArrayUsingDescriptors:sortDescriptors]];
            
            [arrayCompaniesList removeAllObjects];
            
            arrayCompaniesList = [arraySorted mutableCopy];
        }
    }
    else
    {
        stringMesagFirstCome = @"stringFirst";
        if (arrayCompaniesList.count==0)
        {
            NSArray *arrayTemp = [dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"];
            
            NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
            
            NSArray* sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
            
            NSArray *arraySorted = [[NSArray alloc] initWithArray:[arrayTemp sortedArrayUsingDescriptors:sortDescriptors]];
            
            [arrayCompaniesList removeAllObjects];
            
            arrayCompaniesList = [arraySorted mutableCopy];
            
        }
    }
    arraySocities = [NSMutableArray  arrayWithArray:PREF(USER_SELECTED_SOCITIES)];
    
    NSLog(@"array is %@", arraySocities);
    
    if ([[_dicMessageDetail valueForKey:@"status"] isEqualToString:@"unread"])
    {
        stringPortCompanyScreen = @"fromComapny";
        
        
        if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"admin"])
        {
            int societyCount = [[[NSUserDefaults standardUserDefaults]objectForKey:@"adminCount"] intValue];
            if (societyCount==0)
            {
                
            }
            else
            {
                societyCount = societyCount -1;
            }
            
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",societyCount] forKey:@"adminCount"];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(),
                           ^{
                               [self performSelectorInBackground:@selector(readMessage) withObject:nil];
                           });
        }
        
        else if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"studentsociety"])
        {
            if ([arraySocities containsObject:[_dicMessageDetail valueForKeyPath:@"adder_detail.id"]])
            {
                int societyCount = [[[NSUserDefaults standardUserDefaults]objectForKey:@"societyCount"] intValue];
                if (societyCount==0)
                {
                    
                }
                else
                {
                    societyCount = societyCount -1;
                }
                [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",societyCount] forKey:@"societyCount"];
                
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(),
                           ^{
                               [self performSelectorInBackground:@selector(readMessage) withObject:nil];
                               
                           });
        }
        
        else
        {
            NSMutableArray *arrId = [[NSMutableArray alloc] init];
            for (NSDictionary *dictid in arrayPortDataGlobal){
                if([dictid objectForKey:@"id"]){
                    NSString *strid = [[NSString stringWithFormat:@"%@",[dictid objectForKey:@"id"]] removeNull];
                    [arrId addObject:strid];
                }
            }
            
            
            NSString *strid2 = [[NSString stringWithFormat:@"%@",[[_dicMessageDetail objectForKey:@"adder_detail"] objectForKey:@"id"]] removeNull];
            if([ arrId containsObject:strid2]){
                int companyCount = [[[NSUserDefaults standardUserDefaults]objectForKey:@"companyCount"] intValue];
                if (companyCount==0)
                {
                    
                }
                else
                {
                    companyCount = companyCount -1;
                }
                [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",companyCount] forKey:@"companyCount"];
                
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            
            
            stringCompanyId = [_dicMessageDetail valueForKeyPath:@"adder_detail.id"];
            
            [self performSelectorInBackground:@selector(readMessage) withObject:nil];
            
            NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
            
            if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
            {
                int index = (int) [arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
                
                NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
                
                int messageCount = [[dictChnage objectForKey:@"unread_message"] intValue];
                
                if (messageCount==0)
                {
                    
                }
                else
                {
                    messageCount = messageCount - 1;
                }
                
                [dictChnage removeObjectForKey:@"unread_message"];
                
                [dictChnage setObject:[NSString stringWithFormat:@"%d", messageCount] forKey:@"unread_message"];
                
                [arrayCompaniesList replaceObjectAtIndex:index withObject:dictChnage];
                
                [self chnageStatusOfPortPage];
            }
        }
    }
    
    arrayRemovedSociety = [[NSMutableArray alloc]init];
    
    arrayRemovedSociety =[NSMutableArray arrayWithArray: [[NSUserDefaults standardUserDefaults]objectForKey:@"arrayRemovedSociety"]];
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, 1, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    else
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x, 1, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    
    self.imageRedStar.hidden = YES;
    
    if ([[_dicMessageDetail valueForKey:@"message_type"]isEqualToString:@"message"])
    {
        
    }
    else
    {
        [self locationLatitudeLongitude];
    }
    
    _labelResponseConfirmation.hidden = YES;
    
    _labelResponseConfirmation.text = @"Please kindly wait for a \n confirmation response.";
    
    _buttonMessages.backgroundColor = [UIColor whiteColor];
    
    [_buttonMessages setImage:[UIImage imageNamed:@"messagesSelect"] forState:UIControlStateNormal];
    
    _labelmessag.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    self.viewBottomBar.backgroundColor = [UIColor whiteColor];
    
    // Do any additional setup after loading the view.
    _buttonRequest.layer.borderWidth = 2.0;
    
    _buttonRequest.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] CGColor];
    
    _buttonRequest.layer.cornerRadius = 5.0f;
    
    
    _viewImageContainer.layer.cornerRadius = 5.0f;
    _viewImageContainer.layer.borderColor = [UIColor whiteColor].CGColor;
    _viewImageContainer.layer.borderWidth = 1.0f;
    _viewImageContainer.clipsToBounds = YES;
    
    UIImageView *imageBg = [[UIImageView alloc] initWithFrame:self.view.frame];
    [imageBg setImage:[UIImage imageNamed:@"appBack"]];
    imageBg.contentMode = UIViewContentModeScaleAspectFill;
    imageBg.clipsToBounds = YES;
    
    
    if (IS_IPHONE_4_OR_LESS)
    {
        if ([[_dicMessageDetail valueForKey:@"eventconfirmation"] isEqualToString:@"Confirmed"])
        {
            CGRect rectTxtVw = _textViewDetail.frame;
            rectTxtVw.size.height = 50;
            _textViewDetail.frame = rectTxtVw;
        }
    }
    
    [self checkSocialLinks];
    
    if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
    {
        _btnLikeMsg.hidden = NO;
        
        [_btnLikeMsg setImage:[UIImage imageNamed:@"NotLikedWhite"] forState:UIControlStateNormal];
    }
    
    else if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"studentsociety"])
    {
        _btnLikeMsg.hidden = NO;
        
        [_btnLikeMsg setImage:[UIImage imageNamed:@"NotLikedWhite"] forState:UIControlStateNormal];
    }
    
    else if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"admin"])
    {
        _btnLikeMsg.hidden = NO;
        
        [_btnLikeMsg setImage:[UIImage imageNamed:@"notLiked"] forState:UIControlStateNormal];
    }
    
    else
    {
        _btnLikeMsg.hidden = YES;
        //notLiked
    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];
}


-(void)viewWillAppear:(BOOL)animated
{
    // NotLikedWhite
    
    stringNotifiction = @"";
    
    stringForNotifyEditCompany = @"";
    
    [super viewWillAppear:animated];
    
    if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"] )
    {
        self.imageRedStar.hidden = YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:18], NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    self.navigationItem.hidesBackButton = YES;
    
    UIBarButtonItem  *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
    
    self.navigationItem.leftBarButtonItem = leftItem;
    
    _textViewDetail.text = [_dicMessageDetail valueForKey:@"message"];
    
    _labelName.text = [_dicMessageDetail valueForKeyPath:@"adder_detail.name"];
    
    _labelName.adjustsFontSizeToFitWidth = YES;
    
    [_labelName setMinimumScaleFactor:10.0/[UIFont labelFontSize]];
    
    if (_strDetailtype)
        
    {
        
        if ([[_dicMessageDetail valueForKey:@"message_type"] isEqualToString:@"event"])
        {
            self.title = @"Event Details";
            
            if (![[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"admin"])
            {
                _imageViewBackGd.image = [UIImage imageNamed:@"green"];
            }
            
            _labelEventDate.text =[NSString stringWithFormat:@"%@", [_dicMessageDetail valueForKey:@"event_date"]];
            
            NSDateFormatter *dtFormatter1=[[NSDateFormatter alloc]init];
            [dtFormatter1 setDateFormat:@"dd/mm/yyyy"];
            NSDateFormatter *dtFormatter2=[[NSDateFormatter alloc]init];
            [dtFormatter2 setDateFormat:@"dd-MMM-yyyy"];
            if ([dtFormatter1 dateFromString:[NSString stringWithFormat:@"%@", [_dicMessageDetail valueForKey:@"event_date"]]]) {
                _labelEventDate.text =[dtFormatter2 stringFromDate:[dtFormatter1 dateFromString:[NSString stringWithFormat:@"%@", [_dicMessageDetail valueForKey:@"event_date"]]]];
            }
            
            //vijay007
            
            if ([[_dicMessageDetail valueForKey:@"eventconfirmation"] isEqualToString:@"Request Sent"])
            {
                [_buttonRequest setTitle:@"Invitation requested" forState:UIControlStateNormal];
                
                _buttonRequest.enabled = NO;
                
                CGRect rectTextVw = _textViewDetail.frame;
                
                rectTextVw.size.height = rectTextVw.size.height-45;
                
                _textViewDetail.frame = rectTextVw;
                
                _labelResponseConfirmation.hidden= NO;
            }
            
            else if ([[_dicMessageDetail valueForKey:@"eventconfirmation"] isEqualToString:@"Confirmed"])
            {
                
                [_buttonRequest setTitle:@"Congratulations" forState:UIControlStateNormal];
                
                _buttonRequest.enabled = NO;
                
                _imageViewBackGd.image = [UIImage imageNamed:@"pinkBack"];
                
                UILabel *lbl = (UILabel*)[_viewMsgDetails viewWithTag:5854];
                
                lbl.hidden = NO;
                
                NSInteger height = CGRectGetMinY(lbl.frame)-CGRectGetMinY(_textViewDetail.frame);
                CGRect rectTextVw = _textViewDetail.frame;
                
                rectTextVw.size.height = height-5;
                
                _textViewDetail.frame = rectTextVw;
            }
            
            if ([[_dicMessageDetail valueForKey:@"eventStatus"] isEqualToString:@"1"])
                
            {
                
            }
            
            else if ([[_dicMessageDetail valueForKey:@"eventStatus"] isEqualToString:@"0"])
                
            {
                
                [_buttonRequest setTitle:@"Event cancelled" forState:UIControlStateNormal];
                
                _buttonRequest.enabled = NO;
                
                _btnLikeMsg.hidden = NO;
            }
            
            else if ([[_dicMessageDetail valueForKey:@"eventStatus"] isEqualToString:@"2"])
                
            {
                [_buttonRequest setTitle:@"Event closed" forState:UIControlStateNormal];
                
                _buttonRequest.enabled = NO;
                
                _btnLikeMsg.hidden = NO;
            }
        }
        
        else if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"studentsociety"])
            
        {
            if ([[_dicMessageDetail valueForKey:@"message_type"] isEqualToString:@"message"])
                
            {
                _staticLabelEventDate.hidden=YES;
                _staticLabelEventLocation.hidden=YES;
                _staticLabelEventName.hidden=YES;
                _staticLabelEventTime.hidden=YES;
                
                _labelEventLocation.hidden = YES;
                _btnOpenLink.hidden =YES;
                _labelEventDate.hidden=YES;
                _labelEventName.hidden=YES;
                _LabelEventTime.hidden=YES;
                _buttonRequest.hidden = YES;
                
                CGRect txtVwFrame = _textViewDetail.frame;
                txtVwFrame.size.height = _imageViewBackGd.frame.size.height/1.5;
                txtVwFrame.origin.y = _labelDate.frame.origin.y+_labelDate.frame.size.height+15;
                _textViewDetail.frame = txtVwFrame;
            }
        }
        
        else
        {
            if (![[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"admin"])
            {
                _imageViewBackGd.image = [UIImage imageNamed:@"blueBack"];
            }
            
            self.title = @"Message Details";
            _staticLabelEventDate.hidden=YES;
            _staticLabelEventLocation.hidden=YES;
            _staticLabelEventName.hidden=YES;
            _staticLabelEventTime.hidden=YES;
            
            _labelEventLocation.hidden = YES;
            _btnOpenLink.hidden =YES;
            _labelEventDate.hidden=YES;
            _labelEventName.hidden=YES;
            _LabelEventTime.hidden=YES;
            _buttonRequest.hidden = YES;
            
            CGRect txtVwFrame = _textViewDetail.frame;
            txtVwFrame.size.height = _imageViewBackGd.frame.size.height/1.5;
            txtVwFrame.origin.y = _labelDate.frame.origin.y+_labelDate.frame.size.height+15;
            _textViewDetail.frame = txtVwFrame;
        }
        
        
        if (isInternetOff)
        {
            [_imageViewComp setImage:[UIImage imageWithContentsOfFile:stringAdminLogo]] ;
        }
        else
        {
            if ([stringAdminLogo hasPrefix:@"/Users/"])
            {
                [_imageViewComp setImage:[UIImage imageWithContentsOfFile:stringAdminLogo]];
            }
            
            else if ([stringAdminLogo hasPrefix:@"/var/mobile/Containers/"])
                
            {
                [_imageViewComp setImage:[UIImage imageWithContentsOfFile:stringAdminLogo]];
            }
            
            else
            {
                UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,stringAdminLogo]]]];
                
                [_imageViewComp setImage:imge];
            }
        }
    }
    
    else
    {
        
        _buttonMessages.backgroundColor = [UIColor clearColor];
        
        [_buttonMessages setImage:[UIImage imageNamed:@"messagesSelect"] forState:UIControlStateNormal];
        
        _labelmessag.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        
        if ([[_dicMessageDetail valueForKey:@"message_type"] isEqualToString:@"event"])
        {
            self.title = @"Event Details";
            
            if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"admin"])
            {
                _imageViewBackGd.backgroundColor = [UIColor whiteColor];
                _imageViewBackGd.image = nil;
                _imageViewBackGd.layer.borderWidth = 1.0f;
                _imageViewBackGd.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]  CGColor];
                
                [_btnLikeMsg setImage:[UIImage imageNamed:@"notLiked"] forState:UIControlStateNormal];
            }
            else
            {
                _imageViewBackGd.image = [UIImage imageNamed:@"green"];
            }
            
            if ([[_dicMessageDetail valueForKey:@"eventconfirmation"] isEqualToString:@"Request Sent"])
            {
                [_buttonRequest setTitle:@"Invitation requested" forState:UIControlStateNormal];
                
                _buttonRequest.enabled = NO;
                
                CGRect rectTextVw = _textViewDetail.frame;
                
                rectTextVw.size.height = rectTextVw.size.height-45;
                
                _textViewDetail.frame = rectTextVw;
                
                _labelResponseConfirmation.hidden= NO;
            }
            
            else if ([[_dicMessageDetail valueForKey:@"eventconfirmation"] isEqualToString:@"Confirmed"])
            {
                [_buttonRequest setTitle:@"Congratulations" forState:UIControlStateNormal];
                
                _buttonRequest.enabled = NO;
                
                _imageViewBackGd.image = [UIImage imageNamed:@"pinkBack"];
                
                UILabel *lbl = (UILabel*)[_viewMsgDetails viewWithTag:5854];
                
                lbl.hidden = NO;
                
                NSInteger height = CGRectGetMinY(lbl.frame)-CGRectGetMinY(_textViewDetail.frame);
                
                CGRect rectTextVw = _textViewDetail.frame;
                
                rectTextVw.size.height = height-5;
                
                _textViewDetail.frame = rectTextVw;
            }
            
            if ([[_dicMessageDetail valueForKey:@"eventStatus"] isEqualToString:@"1"])
            {
                
            }
            
            else if ([[_dicMessageDetail valueForKey:@"eventStatus"] isEqualToString:@"0"])
            {
                
                [_buttonRequest setTitle:@"Event cancelled" forState:UIControlStateNormal];
                
                _buttonRequest.enabled = NO;
                
                _btnLikeMsg.hidden = NO;
            }
            
            else if ([[_dicMessageDetail valueForKey:@"eventStatus"] isEqualToString:@"2"])
                
            {
                [_buttonRequest setTitle:@"Event closed" forState:UIControlStateNormal];
                
                _buttonRequest.enabled = NO;
                
                _btnLikeMsg.hidden = NO;
            }
            
            _labelEventDate.text = [NSString stringWithFormat:@"%@",[_dicMessageDetail valueForKey:@"event_date"]];
            NSDateFormatter *dtFormatter1=[[NSDateFormatter alloc]init];
            [dtFormatter1 setDateFormat:@"dd/mm/yyyy"];
            NSDateFormatter *dtFormatter2=[[NSDateFormatter alloc]init];
            [dtFormatter2 setDateFormat:@"dd-MMM-yyyy"];
            if ([dtFormatter1 dateFromString:[NSString stringWithFormat:@"%@", [_dicMessageDetail valueForKey:@"event_date"]]]) {
                _labelEventDate.text =[dtFormatter2 stringFromDate:[dtFormatter1 dateFromString:[NSString stringWithFormat:@"%@", [_dicMessageDetail valueForKey:@"event_date"]]]];
            }
            
            //vijay007
        }
        
        else
        {
            if (![[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"admin"])
            {
                _imageViewBackGd.image = [UIImage imageNamed:@"blueBack"];
            }
            
            self.title = @"Message Details";
            _staticLabelEventDate.hidden=YES;
            _staticLabelEventLocation.hidden=YES;
            _staticLabelEventName.hidden=YES;
            _staticLabelEventTime.hidden=YES;
            
            _labelEventLocation.hidden = YES;
            _btnOpenLink.hidden =YES;
            _labelEventDate.hidden=YES;
            _labelEventName.hidden=YES;
            _LabelEventTime.hidden=YES;
            _buttonRequest.hidden = YES;
            
            CGRect txtVwFrame = _textViewDetail.frame;
            
            txtVwFrame.size.height = _imageViewBackGd.frame.size.height/1.5;
            
            txtVwFrame.origin.y = _labelDate.frame.origin.y+_labelDate.frame.size.height+10;
            
            _textViewDetail.frame = txtVwFrame;
        }
    }
    
    _labelDate.text = [self calculateDateAccordingToTimeZone2:[_dicMessageDetail valueForKey:@"date"] isDateType:nil timeZoneName:[_dicMessageDetail valueForKey:@"default_timezone"]];
    
    _labelEventName.text = [_dicMessageDetail valueForKey:@"event_title"];
    
    _LabelEventTime.text = [NSString stringWithFormat:@"%@ to %@",[_dicMessageDetail valueForKey:@"eventtimefrom"],[_dicMessageDetail valueForKey:@"eventtimeto"]];
    
    _labelEventLocation.text = [_dicMessageDetail valueForKey:@"event_venue"];
    
    self.navigationController.navigationBar.hidden = NO;
    
    [_btnLikeMsg setImage:[UIImage imageNamed:@"liked"] forState:UIControlStateSelected];
    
    if ([[_dicMessageDetail valueForKey:@"like_status"] boolValue])
    {
        [_btnLikeMsg setSelected:YES];
    }
    
    [self setCompanyImage];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(redStarShow)
                                                 name:@"redStarShow"
                                               object:nil];
}

-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

-(void)setBtnBorderColor :(UIColor *)color btn:(UIButton*)btn  image:(UIImage*)image
{
    [btn setImage:image forState:UIControlStateNormal];
    btn.layer.cornerRadius = 5.0;
    btn.layer.borderColor = color.CGColor;
    btn.layer.borderWidth = 1.0f;
}

-(void)addRightBarButton
{
    if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"studentsociety"])
    {
        buttonPort = [[UIButton alloc]initWithFrame:CGRectMake(0,5, 30, 30)];
        
        if ([arraySocities containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
        {
            buttonPort =[[UIButton alloc]initWithFrame:CGRectMake(0,5, 32, 30)];
            
            [buttonPort setImage:[UIImage imageNamed:@"RemoveSoc"] forState:UIControlStateNormal];
            
            [buttonPort setBackgroundColor:[UIColor whiteColor]];
            
            buttonPort.layer.cornerRadius = 5.0;
            
            buttonPort.layer.borderColor = [UIColor colorWithRed:11.0f/255.0f green:47.0f/255.0f blue:91.0f/255.0f alpha:1.0f].CGColor;
            
            buttonPort.layer.borderWidth = 1.0f;
        }
        else
        {
            buttonPort =[[UIButton alloc]initWithFrame:CGRectMake(0,5, 30, 30)];
            
            [buttonPort setImage:[UIImage imageNamed:@"societies"] forState:UIControlStateNormal];
            
            buttonPort.layer.cornerRadius = 5.0;
            buttonPort.layer.borderColor = [UIColor whiteColor].CGColor;
            buttonPort.layer.borderWidth = 1.0f;
            
            
        }
    }
    
    else if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
        
    {
        buttonPort =[[UIButton alloc]initWithFrame:CGRectMake(0,5, 31, 30)];
        
        NSArray *arrayId= ([arrayCompaniesList valueForKey:@"id"])?[arrayCompaniesList valueForKey:@"id"]:[NSArray array];
        //crash007
        
        if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
        {
            int index = (int)[arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]] ;
            
            NSMutableDictionary *dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
            
            [_dicMessageDetail removeObjectForKey:@"report"];
            
            @try {
                [_dicMessageDetail setObject:[dictChnage valueForKeyPath:@"portfolio_status"] forKey:@"report"];
                
                if ([[dictChnage valueForKeyPath:@"portfolio_status"] isEqualToString:@"1"])
                {
                    stringAlrdyAdded = @"added";
                    
                    buttonPort =[[UIButton alloc]initWithFrame:CGRectMake(0,5, 33, 30)];
                    
                    
                    [self setBtnBorderColor:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] btn:buttonPort image:[UIImage imageNamed:@"addP"]];
                    [buttonPort setBackgroundColor:[UIColor whiteColor]];
                }
                
                else
                {
                    [self setBtnBorderColor:[UIColor whiteColor] btn:buttonPort image:[UIImage imageNamed:@"portRemoved"]];
                }
            } @catch (NSException *exception) {
                
            } @finally {
                
            }
        }
        else
        {
            if ([[_dicMessageDetail valueForKey:@"addedStatus"] boolValue])
            {
                buttonPort =[[UIButton alloc]initWithFrame:CGRectMake(0,5, 33, 30)];
                
                
                [self setBtnBorderColor:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] btn:buttonPort image:[UIImage imageNamed:@"addP"]];
                [buttonPort setBackgroundColor:[UIColor whiteColor]];
            }
            else
            {
                [self setBtnBorderColor:[UIColor whiteColor] btn:buttonPort image:[UIImage imageNamed:@"portRemoved"]];
            }
        }
    }
    
    [buttonPort addTarget:self action:@selector(portBarButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnPort =  [[UIBarButtonItem alloc] initWithCustomView:buttonPort];
    
    buttonSocialMsg = [[UIButton alloc]initWithFrame:CGRectMake(0,5, 30, 30)];
    
    if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"studentsociety"])
    {
        if (arraySocietyGlobal.count>0)
        {
            NSArray *arrayAdder = [arraySocietyGlobal valueForKey:@"adder_id"];
            
            if ([arrayAdder containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
                
            {
                int indes = (int)[arrayAdder indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
                NSDictionary *dict = arraySocietyGlobal[indes];
                
                if ([[dict valueForKey:@"value"]isEqualToString:@"1"])
                {
                    stringFromRightButton = @"rightButton";
                    
                    [buttonSocialMsg setImage:[UIImage imageNamed:@"msgStopDetail"] forState:UIControlStateNormal];
                }
                else
                {
                    [buttonSocialMsg setImage:[UIImage imageNamed:@"msgDetail"] forState:UIControlStateNormal];
                }
            }
            else
            {
                if ([[_dicMessageDetail valueForKey:@"report"]isEqualToString:@"1"])
                {
                    stringFromRightButton = @"rightButton";
                    
                    [buttonSocialMsg setImage:[UIImage imageNamed:@"msgStopDetail"] forState:UIControlStateNormal];
                }
                else
                {
                    [buttonSocialMsg setImage:[UIImage imageNamed:@"msgDetail"] forState:UIControlStateNormal];
                }
            }
        }
        else
        {
            if ([[_dicMessageDetail valueForKey:@"report"]isEqualToString:@"1"])
            {
                stringFromRightButton = @"rightButton";
                
                [buttonSocialMsg setImage:[UIImage imageNamed:@"msgStopDetail"] forState:UIControlStateNormal];
            }
            else
            {
                [buttonSocialMsg setImage:[UIImage imageNamed:@"msgDetail"] forState:UIControlStateNormal];
            }
        }
        
        //msg007
        BOOL isMessagesStopped=NO;
        isMessagesStopped=![[MessageSubscriptionManager sharedManager] isMessageSubscribedForId:[NSString stringWithFormat:@"%@",[_dicMessageDetail valueForKey:@"adder_id"]] isCompany:NO];
        NSLog(@"Messages StoppedNN:%@ For SocietyId:%@",isMessagesStopped?@"YES":@"NO",[NSString stringWithFormat:@"%@",[_dicMessageDetail valueForKey:@"adder_id"]]);
        
        //if ([[_dicMessageDetail valueForKey:@"report"] boolValue])
        if (isMessagesStopped)
        {
            stringFromRightButton = @"rightButton";
            
            [buttonSocialMsg setImage:[UIImage imageNamed:@"msgStopDetail"] forState:UIControlStateNormal];
        }
        else
        {
            [buttonSocialMsg setImage:[UIImage imageNamed:@"msgDetail"] forState:UIControlStateNormal];
        }
    }
    else
    {
        NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
        
        if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
        {
            int index = (int)[arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]] ;
            
            NSMutableDictionary *dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
            //msg007
            BOOL isMessagesStopped=NO;
            isMessagesStopped=[[dictChnage valueForKey:@"message_status"] boolValue];
            NSLog(@"Messages Stopped:%@",isMessagesStopped?@"YES":@"NO");
            isMessagesStopped=![[MessageSubscriptionManager sharedManager] isMessageSubscribedForId:[NSString stringWithFormat:@"%@",[dictChnage valueForKey:@"id"]] isCompany:YES];
            NSLog(@"Messages StoppedNN:%@ For CompanyId:%@",isMessagesStopped?@"YES":@"NO",[NSString stringWithFormat:@"%@",[dictChnage valueForKey:@"id"]]);
            if (isMessagesStopped)
            {
                [buttonSocialMsg setImage:[UIImage imageNamed:@"msgStopDetail"] forState:UIControlStateNormal];
            }
            else
            {
                [buttonSocialMsg setImage:[UIImage imageNamed:@"msgDetail"] forState:UIControlStateNormal];
            }
        }
        
    }
    
    UIBarButtonItem *reportBarBtn = [[UIBarButtonItem alloc] initWithCustomView:buttonSocialMsg];
    
    [buttonSocialMsg addTarget:self action:@selector(openPopUpMsgDetail:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[reportBarBtn,btnPort];
}

#pragma mark---CustomMethod---

-(void)setCompanyImage
{
    if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"admin"])
    {
        if ([[_dicMessageDetail valueForKey:@"eventStatus"] isEqualToString:@"0"])
        {
            _btnLikeMsg.hidden = NO;
        }
        
        else
        {
            _btnLikeMsg.hidden = NO;
        }
        
        
        if (isInternetOff)
        {
            [_imageViewComp setImage:[UIImage imageWithContentsOfFile:stringAdminLogo]] ;
        }
        else
        {
            if ([stringAdminLogo hasPrefix:@"/Users/"])
            {
                [_imageViewComp setImage:[UIImage imageWithContentsOfFile:stringAdminLogo]];
            }
            else if ([stringAdminLogo hasPrefix:@"/var/mobile/Containers/"])
            {
                [_imageViewComp setImage:[UIImage imageWithContentsOfFile:stringAdminLogo]];
            }
            else
            {
                UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,stringAdminLogo]]]];
                
                [_imageViewComp setImage:imge];
            }
        }
        
        _viewImageContainer.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
        
        if ([[_dicMessageDetail valueForKey:@"eventconfirmation"] isEqualToString:@"Confirmed"])
        {
            
        }
        
        else
        {
            
            self.textViewDetail.textColor = [UIColor blueColor];
            
            _imageViewBackGd.backgroundColor = [UIColor whiteColor];
            _imageViewBackGd.image = nil;
            _imageViewBackGd.layer.borderWidth = 1.0f;
            _imageViewBackGd.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]  CGColor];
        }
    }
    
    else if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
        
    {
        [self addRightBarButton];
        
        _viewImageContainer.backgroundColor = [UIColor whiteColor];
        
        if (isInternetOff)
        {
            if (![[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"] isEqualToString:@"N"])
            {
                if ([[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"] isEqualToString:@"event"])
                {
                    _imageViewComp.image = [UIImage imageWithContentsOfFile:[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]];
                }
                
                else
                {
                    _imageViewComp.image = [UIImage imageWithContentsOfFile:[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]];
                }
            }
            
            else
                
            {
                _imageViewComp.image = [UIImage imageNamed:@"defaultBuilding"];
            }
        }
        
        else
        {
            if (![[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"] isEqual:[NSNull null]])
            {
                _imageViewComp.image = [UIImage imageWithContentsOfFile:[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]];
                
            }
            else
            {
                _imageViewComp.image = [UIImage imageNamed:@"defaultBuilding"];
            }
        }
    }
    
    else if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"studentsociety"])
        
    {
        [self addRightBarButton];
        
        if ([[_dicMessageDetail valueForKey:@"message_type"] isEqualToString:@"message"])
        {
            _imageViewBackGd.image = [UIImage imageNamed:@"societyMsgDetail"];
        }
        
        else
        {
            if ([[_dicMessageDetail valueForKey:@"eventconfirmation"] isEqualToString:@"Confirmed"])
            {
                [_buttonRequest setTitle:@"Congratulations" forState:UIControlStateNormal];
                
                _buttonRequest.enabled = NO;
                
                _imageViewBackGd.image = [UIImage imageNamed:@"pinkBack"];
                
                UILabel *lbl = (UILabel*)[_viewMsgDetails viewWithTag:5854];
                
                lbl.hidden = NO;
                
                NSInteger height = CGRectGetMinY(lbl.frame)-CGRectGetMinY(_textViewDetail.frame);
                CGRect rectTextVw = _textViewDetail.frame;
                
                rectTextVw.size.height = height-5;
                
                _textViewDetail.frame = rectTextVw;
            }
            
            else
            {
                
                _imageViewBackGd.image = [UIImage imageNamed:@"societyEventDetail"];
                
            }
        }
        
        if ([[_dicMessageDetail valueForKey:@"message_type"] isEqualToString:@"message"])
            
        {
            
            if ([[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"] hasPrefix:@"/Users/"])
            {
                [_imageViewComp setImage:[UIImage imageWithContentsOfFile:[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]]];
            }
            
            else if ([[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"] hasPrefix:@"/var/mobile/Containers/"])
            {
                [_imageViewComp setImage:[UIImage imageWithContentsOfFile:[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]]];
            }
            
            else
            {
                UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]]]]];
                
                [_imageViewComp setImage:imge];
            }
        }
        
        else
        {
            if ([[_dicMessageDetail valueForKey:@"eventconfirmation"] isEqualToString:@"Confirmed"])
            {
                [_buttonRequest setTitle:@"Congratulations" forState:UIControlStateNormal];
                
                _buttonRequest.enabled = NO;
                
                
                if ([[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"] hasPrefix:@"/Users/"])
                {
                    [_imageViewComp setImage:[UIImage imageWithContentsOfFile:[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]]];
                }
                
                else if ([[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"] hasPrefix:@"/var/mobile/Containers/"])
                {
                    [_imageViewComp setImage:[UIImage imageWithContentsOfFile:[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]]];
                }
                else
                {
                    UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]]]]];
                    
                    [_imageViewComp setImage:imge];
                }
                
            }
            
            else
            {
                if ([[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"] hasPrefix:@"/Users/"])
                {
                    [_imageViewComp setImage:[UIImage imageWithContentsOfFile:[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]]];
                }
                
                else if ([[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"] hasPrefix:@"/var/mobile/Containers/"])
                {
                    [_imageViewComp setImage:[UIImage imageWithContentsOfFile:[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]]];
                }
                else
                {
                    UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]]]]];
                    
                    [_imageViewComp setImage:imge];
                }
                
            }
        }
        
        _imageViewComp.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
        
        self.textViewDetail.textColor = [UIColor blackColor];
        
    }
    
    else
        
    {
        
        _viewImageContainer.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
        _imageViewComp.backgroundColor = [UIColor clearColor];
        
        if (isInternetOff)
            
        {
            if (![[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"] isEqualToString:@"N"])
            {
                _viewImageContainer.backgroundColor = [UIColor whiteColor] ;
                
                _imageViewComp.image = [UIImage imageWithContentsOfFile:[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]];
            }
            else
            {
                _imageViewComp.image = [UIImage imageNamed:@"eventLogo"];
            }
            
            if ([[_dicMessageDetail valueForKey:@"message_type"] isEqualToString:@"event"])
            {
                if ([[_dicMessageDetail valueForKey:@"eventconfirmation"] isEqualToString:@"Confirmed"])
                {
                    _imageViewBackGd.image = [UIImage imageNamed:@"pinkBack"];
                }
                else
                {
                    _imageViewBackGd.image = [UIImage imageNamed:@"green"];
                }
            }
            else
            {
                _imageViewBackGd.image = [UIImage imageNamed:@"blueBack"];
            }
        }
        
        else
            
        {
            if (![[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"] isEqual:[NSNull null]])
            {
                _viewImageContainer.backgroundColor = [UIColor whiteColor] ;
                
                _imageViewComp.imageURL=[NSURL URLWithString: [NSString stringWithFormat:@"%@",[_dicMessageDetail valueForKeyPath:@"adder_detail.logo"]]];
                
            }
            else
            {
                _viewImageContainer.backgroundColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
                _imageViewComp.image = [UIImage imageNamed:@"eventLogo"];
            }
        }
    }
    _viewImageContainer.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:70.0f/255.0f blue:170.0f/255.0f alpha:1.0f] CGColor];
    _viewImageContainer.layer.borderWidth = 2.0f;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark- CustomButtonActions

-(void)portBarButtonClick
{
    if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"studentsociety"])
    {
        if ([buttonPort.imageView.image isEqual:[UIImage imageNamed:@"societies"]])
        {
            [self webServiceAddRemoveSociety:@"0"];
        }
        else
        {
            [self webServiceAddRemoveSociety:@"1"];
            
            NSLog(@"vvghadfhvdfbvgvf %@",[_dicMessageDetail valueForKeyPath:@"adder_detail.id"]);
            
            if ([arrayRemovedSociety containsObject:[_dicMessageDetail valueForKeyPath:@"adder_detail.id"]])
            {
                
            }
            else
            {
                [arrayRemovedSociety addObject:[_dicMessageDetail valueForKeyPath:@"adder_detail.id"]];
            }
            
            NSMutableArray *arrayTemp = [[NSMutableArray alloc]init];
            
            arrayTemp = [[NSUserDefaults standardUserDefaults]objectForKey:@"arrayRemovedSociety"];
            
            if (arrayTemp.count==0)
            {
                [[NSUserDefaults standardUserDefaults]setObject:arrayRemovedSociety forKey:@"arrayRemovedSociety"];
            }
            
            else
            {
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arrayRemovedSociety"];
                
                [[NSUserDefaults standardUserDefaults]setObject:arrayRemovedSociety forKey:@"arrayRemovedSociety"];
            }
            
            NSLog(@"user default is %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"arrayRemovedSociety"]);
            
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
    }
    else
    {
        if ([buttonPort.imageView.image isEqual:[UIImage imageNamed:@"addP"]])
        {
            [self webServiceRemovePort];
        }
        else
        {
            [self webServiceAddPort];
        }
    }
}

-(void)backBtnAction

{    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"callMsgWebService"])
{
    stringMessageDetail = @"";
}
else
{
    stringMessageDetail = @"messagedetail";
}
    
    [self.navigationController popViewControllerAnimated:NO];
}


-(void)checkReloadMessageMethod
{
    
    if ([[_dicMessageDetail valueForKey:@"status"] isEqualToString:@"unread"])
        
    {
        
        if (messageAlrdyLoad == NO)
        {
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"messageReloadBackground"
             object:self];
            
            messageAlrdyLoad = YES;
        }
        
    }
}

#pragma mark- tabBarButtonActions

-(void)updateValueInArray
{
    
    NSString *stringSelectedMsg = [_dicMessageDetail valueForKey:@"id"];
    
    {
        
        int index = (int) [[arrayMessageGlobal valueForKey:@"id"]indexOfObject:stringSelectedMsg];
        
        NSLog(@"index is %d",index);
        
        NSMutableDictionary *dictnryTemp = [[NSMutableDictionary alloc]initWithDictionary:arrayMessageGlobal[index] copyItems:YES];
        
        if ([stringLike isEqualToString:@"like"])
        {
            stringLike = @"";
            
            if ([[dictnryTemp objectForKey:@"status"]isEqualToString:@"unread"])
            {
                [dictnryTemp removeObjectForKey:@"status"];
                
                [dictnryTemp setObject:@"read" forKey:@"status"];
            }
            [dictnryTemp removeObjectForKey:@"like_status"];
            
            [dictnryTemp setObject:@"1" forKey:@"like_status"];
        }
        
        else
        {
            if ([[dictnryTemp objectForKey:@"status"]isEqualToString:@"unread"])
            {
                [dictnryTemp removeObjectForKey:@"status"];
                
                [dictnryTemp setObject:@"read" forKey:@"status"];
            }
            
            else
            {
                
            }
        }
        
        if ([stringReport isEqualToString: @"report"])
        {
            if ([stringBlock isEqualToString:@"1"])
            {
                [dictnryTemp removeObjectForKey:@"report"];
                
                [dictnryTemp setObject:@"1" forKey:@"report"];
            }
            else
            {
                [dictnryTemp removeObjectForKey:@"report"];
                
                [dictnryTemp setObject:@"0" forKey:@"report"];
            }
            stringReport = @"";
            stringBlock = @"";
        }
        
        if ([stringFromSocietyMessage isEqualToString:@"added"])
        {
            [dictnryTemp removeObjectForKey:@"addedStatus"];
            
            [dictnryTemp setObject:@"1" forKey:@"addedStatus"];
            
            stringFromSocietyMessage= @"";
        }
        
        else if ([stringFromSocietyMessage isEqualToString:@"removed"])
        {
            [dictnryTemp removeObjectForKey:@"addedStatus"];
            
            [dictnryTemp setObject:@"0" forKey:@"addedStatus"];
            
            stringFromSocietyMessage = @"";
        }
        
        if ([stringRequestInvite isEqualToString: @"requested"])
        {
            [dictnryTemp removeObjectForKey:@"eventconfirmation"];
            
            [dictnryTemp setObject:@"Request Sent" forKey:@"eventconfirmation"];
            
            stringRequestInvite = @"";
        }
        
        int index1 = (int) [[arrayMessageGlobal valueForKey:@"id"]indexOfObject:stringSelectedMsg];
        
        NSLog(@"index is %d",index1);
        
        [arrayMessageGlobal replaceObjectAtIndex:index1 withObject:dictnryTemp];
    }
    
}

- (IBAction)buttonActionPortfolio:(id)sender;
{
    [self updateValueInArray];
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[PortfolioViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    PortfolioViewController *portfolioView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
    [self.navigationController pushViewController:portfolioView animated:NO];
}

- (IBAction)buttonActionDiscip:(id)sender;
{
    [self updateValueInArray];
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[DisciplineViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    DisciplineViewController *disciplineVw = [self.storyboard instantiateViewControllerWithIdentifier:@"disciplineView"];
    [self.navigationController pushViewController:disciplineVw animated:NO];
}

- (IBAction)buttonSearch:(id)sender;
{
    [self updateValueInArray];
    
    self.navigationController.navigationBar.hidden = NO;
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[LocationFilterSearchViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    LocationFilterSearchViewController  *searchVw = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVw"];
    [self.navigationController pushViewController:searchVw animated:NO];
}

- (IBAction)buttonMessages:(id)sender;
{
    [self updateValueInArray];
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[MessagesViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    MessagesViewController  *messageVw = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    [self.navigationController pushViewController:messageVw animated:YES];
    _labelmessag.textColor = [UIColor blueColor];
}


- (IBAction)buttonActionSalary:(id)sender;
{
    [self updateValueInArray];
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[SalaryGuideViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    SalaryGuideViewController *salaryVw = [self.storyboard instantiateViewControllerWithIdentifier:@"salaryVw"];
    [self.navigationController pushViewController:salaryVw animated:NO];
}


-(IBAction)btnlinkSite:(id)sender
{
    if ([sender isSelected])
    {
        [sender setSelected:NO];
        
        NSLog(@"sender state is no ");
    }
    
    else
    {
        [sender setSelected:YES];
        
        NSLog(@"sender state is yes ");
    }
    
    [self performSelector:@selector(chnageStatus:) withObject:sender afterDelay:1.0];
    
    [self performSelector:@selector(openLink) withObject:nil afterDelay:0.2];
}

-(void)openLink
{
    [self updateValueInArray];
    
    if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
    {
        CompaniesDetailViewController *companiesView = [self.storyboard instantiateViewControllerWithIdentifier:@"CompaniesDetailView"];
        companiesView.strVwChane = @"fromMessage";
        companiesView.dicCompanyDetail = _dicMessageDetail;
        
        [self.navigationController pushViewController:companiesView animated:NO];
    }
    
    else if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"studentsociety"])
    {
        NSString *stringUrl = [_dicMessageDetail valueForKey:@"website"] ;
        
        if (![stringUrl containsString:@"http://"])
        {
            stringUrl = [NSString stringWithFormat:@"http://%@",stringUrl];
        }
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringUrl]];
    }
}

-(void)chnageStatus:(id)sender
{
    [sender setSelected:NO];
}

#pragma mark- IBButtonActions

- (IBAction)buttonRequestInvitationAction:(id)sender
{
    
    if([Utilities CheckInternetConnection])
    {
        if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
        {
            
            if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"FbLogIn"] boolValue])
            {
                UIAlertView *alertFbLogin = [[UIAlertView alloc] initWithTitle:nil message:@"Please save your portfolio before continuing." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                alertFbLogin.tag = 5800;
                [alertFbLogin show];
            }
            
            else if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"LinkedInLogIn"] boolValue])
            {
                [self signInToLinkedIn];
            }
            
            else
            {
                [self confirmLinkedInInvitaion];
            }
        }
        
        else
        {
            if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"FbLogIn"] boolValue])
            {
                UIAlertView *alertFbLogin = [[UIAlertView alloc] initWithTitle:nil message:@"Please save your portfolio before continuing." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                
                alertFbLogin.tag = 5800;
                
                [alertFbLogin show];
            }
            else
            {
                [self confirmSocietyEventInInvitaion];
            }
        }
    }
    else
    {
        isInternetOff = YES;
        if(![Utilities CheckInternetConnection])
        {
        }
        else
        {
            [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
        }
    }
}

-(void)signInToLinkedIn
{
    [self addAlert:@"LinkedIn Sign In" msgStr:@"\nPlease sign in to LinkedIn to request this event" btnArr:@[@"Sign In",@"No"] alertTag:74587];
}

-(void)confirmLinkedInInvitaion
{
    [self addAlert:@"Confirmation" msgStr:@"\nPlease confirm if you would like to request an invitation with your LinkedIn profile?" btnArr:@[@"Yes",@"No"] alertTag:8598];
}

-(void)confirmSocietyEventInInvitaion
{
    [self addAlert:@"Confirmation" msgStr:@"\nPlease confirm if you would like to request an invitation?" btnArr:@[@"Yes",@"No"] alertTag:8598];
}

-(void)savePortfolioAlert
{
    
    [self addAlert:@"Save My Portfolio" msgStr:@"\nPlease confirm if you would like to save your portfolio using your Facebook login?" btnArr:@[@"Sign In",@"No"] alertTag:5858];
}

-(NSString *)calculateDateAccordingToTimeZone1:(NSString *)stringfromDate isDateType:(NSString*)isDateType timeZoneName:(NSString*)timeZoneName
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    
    NSArray *array = [stringfromDate componentsSeparatedByString:@" "];
    
    if ([array containsObject:@"AM"])
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    }
    
    else if ([array containsObject:@"PM"])
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    }
    
    else
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    }
    
    NSDate *date = [dateFormatter1 dateFromString:stringfromDate];
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"dd-MMM-yyyy hh:mm a"];
    [dateFormatters setDateStyle:NSDateFormatterShortStyle];
    [dateFormatters setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatters setDoesRelativeDateFormatting:YES];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    stringfromDate = [dateFormatters stringFromDate: date];
    NSLog(@"DateString : %@", stringfromDate);
    return stringfromDate;
}
-(NSString *)calculateDateAccordingToTimeZone2:(NSString *)stringfromDate isDateType:(NSString*)isDateType timeZoneName:(NSString*)timeZoneName
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    
    NSArray *array = [stringfromDate componentsSeparatedByString:@" "];
    
    if ([array containsObject:@"AM"])
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    }
    else if ([array containsObject:@"PM"])
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    }
    else
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    }
    
    NSDate *date = [dateFormatter1 dateFromString:stringfromDate];
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"dd-MMM-yyyy hh:mm a"];
    //[dateFormatters setDateStyle:NSDateFormatterShortStyle];
    //[dateFormatters setTimeStyle:NSDateFormatterShortStyle];
    //[dateFormatters setDoesRelativeDateFormatting:YES];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    stringfromDate = [dateFormatters stringFromDate: date];
    NSLog(@"DateString : %@", stringfromDate);
    return stringfromDate;
}
-(NSString *)calculateDateAccordingToTimeZone:(NSString *)stringfromDate isDateType:(NSString*)isDateType timeZoneName:(NSString*)timeZoneName
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    if ([isDateType isEqualToString:@"event"])
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
    }
    else
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    NSDate *date = [dateFormatter1 dateFromString:stringfromDate];
    
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithName:timeZoneName];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:date];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:date];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    if ([isDateType isEqualToString:@"event"]) {
        [dateFormatters setDateFormat:@"dd/MM/yyyy"];
    }
    else{
        dateFormatters.dateFormat = @"dd/MM/yyyy hh:mm a";
    }
    [dateFormatters setDateStyle:NSDateFormatterShortStyle];
    
    if (!isDateType ) {
        [dateFormatters setTimeStyle:NSDateFormatterShortStyle];
    }
    [dateFormatters setDoesRelativeDateFormatting:YES];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    NSLog(@"DateString : %@", [dateFormatters stringFromDate: destinationDate]);
    
    return [dateFormatters stringFromDate: destinationDate];
}

#pragma mark- CustomAlertMethod----
-(void)addAlert :(NSString *)titleStr msgStr:(NSString*)msgStr btnArr:(NSArray*)btnArr alertTag:(int)alertTag
{
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:titleStr
                                        message:msgStr
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *leftBtnAction =
    [UIAlertAction actionWithTitle:[btnArr objectAtIndex:0]
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action)
     {
         if (alertTag==74587)
         {
             
             [self getProfileDetailsFromLinkedIn];
         }
         else if (alertTag==8598)
         {
             [self requestWebservice];
         }
         else if (alertTag==5858)
         {
             [self loginWithFacebook];
         }
     }];
    UIAlertAction *rightBtnAction =
    [UIAlertAction actionWithTitle:[btnArr objectAtIndex:1]
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action)
     {
         
     }];
    
    [alert addAction:leftBtnAction];
    [alert addAction:rightBtnAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark- methodToLoginLinkedIn

-(void)getProfileDetailsFromLinkedIn
{
    linkedInLoginView=[[OAuthLoginView alloc] init];
    linkedInLoginView.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showDetailsOfProfile:) name:@"loginViewDidFinish" object:nil];
    
    NSString *string = [ NSString stringWithFormat:@"%@",linkedInLoginView.accessToken];
    
    NSLog(@"access token is %@", string);
    
    [self presentViewController:linkedInLoginView animated:YES completion:nil];
}


-(void)showDetailsOfProfile:(NSNotification *)notification
{
    NSDictionary *jsonResponse = notification.userInfo;
    
    NSLog(@"notification  : %@", notification.userInfo);
    
    NSString *position;
    
    NSString *company;
    
    if([jsonResponse objectForKey:@"positions"])
    {
        if ([[jsonResponse objectForKey:@"positions"] objectForKey:@"values"])
        {
            if([[[jsonResponse objectForKey:@"positions"] objectForKey:@"values"] objectAtIndex:0])
            {
                if ([[[[jsonResponse objectForKey:@"positions"] objectForKey:@"values"] objectAtIndex:0] objectForKey:@"title"])
                {
                    position=[[[[jsonResponse objectForKey:@"positions"] objectForKey:@"values"] objectAtIndex:0] objectForKey:@"title"];
                }
                
                if ([[[[jsonResponse objectForKey:@"positions"] objectForKey:@"values"] objectAtIndex:0] objectForKey:@"company"])
                {
                    if ([[[[[jsonResponse objectForKey:@"positions"] objectForKey:@"values"] objectAtIndex:0] objectForKey:@"company"]objectForKey:@"name"])
                    {
                        company=[[[[[jsonResponse objectForKey:@"positions"] objectForKey:@"values"] objectAtIndex:0] objectForKey:@"company"]objectForKey:@"name"];
                    }
                }
            }
        }
    }
    
    NSString *_id;
    
    if ([jsonResponse objectForKey:@"id"])
    {
        _id=[jsonResponse objectForKey:@"id"];
    }
    
    NSString *profileUrl;
    
    if ([jsonResponse objectForKey:@"publicProfileUrl"])
    {
        profileUrl=[jsonResponse objectForKey:@"publicProfileUrl"];
    }
    
    NSString *email;
    if ([jsonResponse objectForKey:@"emailAddress"])
    {
        email=[jsonResponse objectForKey:@"emailAddress"];
    }
    
    NSString *industry;
    if ([jsonResponse objectForKey:@"industry"])
    {
        industry=[jsonResponse objectForKey:@"industry"];
    }
    
    NSString *pictureUrl;
    if ([jsonResponse objectForKey:@"pictureUrl"])
    {
        pictureUrl=[jsonResponse objectForKey:@"pictureUrl"];
    }
    
    NSString *biography;
    if ([jsonResponse objectForKey:@"summary"])
    {
        biography=[jsonResponse objectForKey:@"summary"];
    }
    
    NSString *location;
    if ([jsonResponse objectForKey:@"location"])
    {
        NSDictionary *locationDictionary = [jsonResponse objectForKey:@"location"];
        location = [locationDictionary objectForKey:@"name"];
    }
    
    NSString *twitterAccountName;
    if ([jsonResponse objectForKey:@"primaryTwitterAccount"])
    {
        if ([[jsonResponse objectForKey:@"primaryTwitterAccount"] objectForKey:@"providerAccountName"])
        {
            twitterAccountName=[[jsonResponse objectForKey:@"primaryTwitterAccount"] objectForKey:@"providerAccountName"];
        }
    }
    
    NSString *str = [jsonResponse objectForKey:@"publicProfileUrl"];
    NSLog(@"publicProfileUrl is %@", str);
    
    NSString *day;
    NSString *month;
    NSString *year;
    NSDate *dob ;
    
    if ([jsonResponse objectForKey:@"dateOfBirth"] )
    {
        day = [[jsonResponse objectForKey:@"dateOfBirth"] objectForKey:@"day"];
        month = [[jsonResponse objectForKey:@"dateOfBirth"] objectForKey:@"month"];
        year = [[jsonResponse objectForKey:@"dateOfBirth"] objectForKey:@"year"];
        
        NSString *str ;
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
       // [formatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        //vijay007
        [formatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [formatter setTimeZone:gmt];
        str = [NSString stringWithFormat:@"%@/%@/%@ 00:00 AM",month,day,year];
        dob = [formatter dateFromString:str];
    }
    
    NSString *contactNumber;
    if ([jsonResponse objectForKey:@"phoneNumbers"])
    {
        if ([[jsonResponse objectForKey:@"phoneNumbers"] objectForKey:@"values"]) {
            NSArray *arr = [[jsonResponse objectForKey:@"phoneNumbers"] objectForKey:@"values"];
            if ([arr count]>0) {
                NSDictionary *con = [arr objectAtIndex:0];
                contactNumber = [con objectForKey:@"phoneNumber"];
            }
        }
    }
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"pictureUrl":pictureUrl,@"publicProfileUrl":profileUrl};
    
    [self callWebserviceLinkedIn:dicParam];
}

- (void)loginLinkedIn
{
    [self.client getAuthorizationCode:^(NSString *code)
     {
         [self.client getAccessToken:code success:^(NSDictionary *accessTokenData)
          {
              NSString *accessToken = [accessTokenData objectForKey:@"access_token"];
              
              [self requestMeWithToken:accessToken];
              
          }                   failure:^(NSError *error) {
              NSLog(@"Quering accessToken failed %@", error);
          }];
     }                      cancel:^{
         NSLog(@"Authorization was cancelled by user");
     }                     failure:^(NSError *error) {
         NSLog(@"Authorization failed %@", error);
     }];
    
}

- (LIALinkedInHttpClient *)client
{
    LIALinkedInApplication *application = [LIALinkedInApplication applicationWithRedirectURL:@"http://netsetsoftware.com/"
                                                                                    clientId:@"75yp653ipmtqm3"
                                                                                clientSecret:@"mGsFOpD2Ssn1Ey05"
                                                                                       state:@"DCEEFWF45453sdffef424"
                                                                               grantedAccess:@[@"r_basicprofile", @"r_emailaddress"]];
    
    NSLog(@"%@",[LIALinkedInHttpClient clientForApplication:application presentingViewController:nil]);
    
    return [LIALinkedInHttpClient clientForApplication:application presentingViewController:nil];
}

- (void)requestMeWithToken:(NSString *)accessToken
{
    // https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,formatted-name,email-address,location:(name),industry,public-profile-url,picture-url,primary-twitter-account,network,skills,summary,phone-numbers,date-of-birth,main-address,positions:(title,company:(name)),educations:(school-name,field-of-study,start-date,end-date,degree,activities))"
    
    
    [self.client GET:[NSString stringWithFormat:@"https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,formatted-name,email-address,public-profile-url,picture-url,picture-urls::(original))?oauth2_access_token=%@&format=json", accessToken] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *result)
     {
         [self callWebserviceLinkedIn:result];
         
     }        failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"failed to fetch current user %@", error);
     }];
}


#pragma mark---WebServices----

-(void)webServiceAddPort
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"cid":[_dicMessageDetail valueForKeyPath:@"adder_detail.id"]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"addportfolio" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         [self ShowDataLoader:NO];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             buttonPort.frame = CGRectMake(0,5, 33, 30);
             dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
             [dummy updateTable:[NSString stringWithFormat:@"UPDATE COMPANIES SET portfolio_status='1' WHERE id = '%@'",[_dicMessageDetail valueForKeyPath:@"adder_detail.id"]]];

             [[[UIAlertView alloc] initWithTitle:@"Nice choice!" message:@"\nThe company has been successfully added to your portfolio." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
             
             [self setBtnBorderColor:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] btn:buttonPort image:[UIImage imageNamed:@"addP"]];
             [buttonPort setBackgroundColor:[UIColor whiteColor]];
             
             [self changeDataInPortTable:@"1"];
             
             NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
             
             if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
                 
             {
                 int index = (int) [arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
                 
                 NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
                 
                 [dictChnage removeObjectForKey:@"portfolio_status"];
                 
                 [dictChnage setObject:@"1" forKey:@"portfolio_status"];
                 
                 [arrayCompaniesList replaceObjectAtIndex:index withObject:dictChnage];
                 
                 NSInteger points = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"] integerValue];
                 
                 points = points + 50;
                 
                 [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld", (long)points] forKey:@"userPoints"];
             }
             
         }
         
         else if ([[responseObject objectForKey:@"message"] isEqualToString:@"Already added"])
         {
             buttonPort.frame = CGRectMake(buttonPort.frame.origin.x,5, 33, 30);
             dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
             [dummy updateTable:[NSString stringWithFormat:@"UPDATE COMPANIES SET portfolio_status='1' WHERE id = '%@'",[_dicMessageDetail valueForKeyPath:@"adder_detail.id"]]];

             [self setBtnBorderColor:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] btn:buttonPort image:[UIImage imageNamed:@"addP"]];
             //
             [buttonPort setBackgroundColor:[UIColor whiteColor]];
             
             [self changeDataInPortTable:@"1"];
             
             NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
             
             if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
                 
             {
                 int index = (int) [arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
                 
                 NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
                 
                 [dictChnage removeObjectForKey:@"portfolio_status"];
                 
                 [dictChnage setObject:@"1" forKey:@"portfolio_status"];
                 
                 [arrayCompaniesList replaceObjectAtIndex:index withObject:dictChnage];
                 
                 NSInteger points = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"] integerValue];
                 
                 points = points + 50;
                 
                 [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld", (long)points] forKey:@"userPoints"];
             }
             
         }
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         if(![Utilities CheckInternetConnection])
         {
         }
         else
         {
             
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }
     }];
}

-(void)webServiceRemovePort
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"cid":[_dicMessageDetail valueForKeyPath:@"adder_detail.id"]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"removeCompany" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         [self ShowDataLoader:NO];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             buttonPort.frame = CGRectMake(0,5, 31, 30);
             dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
             [dummy updateTable:[NSString stringWithFormat:@"UPDATE COMPANIES SET portfolio_status='0' WHERE id = '%@'",[_dicMessageDetail valueForKeyPath:@"adder_detail.id"]]];

             [self setBtnBorderColor:[UIColor whiteColor] btn:buttonPort image:[UIImage imageNamed:@"portRemoved"]];
             
             
             [buttonPort setBackgroundColor:[UIColor clearColor]];
             
             
             [[[UIAlertView alloc] initWithTitle:@"Let’s remove it…" message:@"\nThe company has now been removed from your portfolio." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
             
             [self changeDataInPortTable:@"0"];
             
             NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
             
             if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
             {
                 int index = (int) [arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
                 
                 NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
                 
                 [dictChnage removeObjectForKey:@"portfolio_status"];
                 
                 [dictChnage setObject:@"0" forKey:@"portfolio_status"];
                 
                 [arrayCompaniesList replaceObjectAtIndex:index withObject:dictChnage];
                 
                 NSInteger points = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"] integerValue];
                 
                 points = points - 50;
                 
                 [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld", (long)points] forKey:@"userPoints"];
             }
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         if(![Utilities CheckInternetConnection])
         {
         }
         else
         {
             
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             
         }
     }];
}


-(IBAction)btnOpenMap:(id)sender
{
    
    [self updateValueInArray];
    
    if ([[_dicMessageDetail valueForKey:@"event_venue"]isEqualToString:@""])
    {
        
    }
    else
    {
        OpenMapViewController *companiesView = [self.storyboard instantiateViewControllerWithIdentifier:@"OpenMapViewController"];
        companiesView.dictDataMap = _dicMessageDetail;
        
        [self.navigationController pushViewController:companiesView animated:YES];
        
    }
}

-(void)webServiceAddRemoveSociety :(NSString *)strStatus
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"society_id":[_dicMessageDetail valueForKeyPath:@"adder_detail.id"],@"add_status":strStatus};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"addstudentsociety" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         
         [self getStudentSocieties:[responseObject objectForKey:@"message"]];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             if ([[responseObject objectForKey:@"message"] isEqualToString:@"Succesfully Added"])
             {
                 
                 [buttonPort setImage:[UIImage imageNamed:@"RemoveSoc"] forState:UIControlStateNormal];
                 
                 [buttonPort setBackgroundColor:[UIColor whiteColor]];
                 
                 buttonPort.layer.cornerRadius = 5.0;
                 
                 buttonPort.layer.borderColor = [UIColor colorWithRed:11.0f/255.0f green:47.0f/255.0f blue:91.0f/255.0f alpha:1.0f].CGColor;
                 
                 buttonPort.layer.borderWidth = 1.0f;
                 
                 
                 [[[UIAlertView alloc] initWithTitle:@"Nice choice!" message:@"\nThe student society has been successfully added to your favourites." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
                 
                 [arraySocities addObject:[_dicMessageDetail valueForKeyPath:@"adder_detail.id"]];
                 
                 [[NSUserDefaults standardUserDefaults]setObject:arraySocities forKey:USER_SELECTED_SOCITIES];
                 
                 [USERDEFAULT synchronize];
                 
                 NSInteger points = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"] integerValue];
                 
                 points = points + 75;
                 
                 [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld", (long)points] forKey:@"userPoints"];
                 
                 stringFromSocietyMessage = @"added";
             }
             
             else
             {
                 
                 [buttonPort setImage:[UIImage imageNamed:@"societies"] forState:UIControlStateNormal];
                 
                 buttonPort.layer.cornerRadius = 5.0;
                 buttonPort.layer.borderColor = [UIColor whiteColor].CGColor;
                 buttonPort.layer.borderWidth = 1.0f;
                 
                 [buttonPort setBackgroundColor:[UIColor clearColor]];
                 
                 
                 stringFromRightButton = @"";
                 
                 stringRemoveSoc = @"remove";
                 
                 stringFromSocietyMessage = @"removed";
                 
                 [[[UIAlertView alloc] initWithTitle:@"Let’s remove it…" message:@"\nThe student society has now been removed from your favourites." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
                 
                 if ([arraySocities containsObject:[_dicMessageDetail valueForKeyPath:@"adder_detail.id"]])
                 {
                     int indexSoc = (int) [arraySocities indexOfObject:[_dicMessageDetail valueForKeyPath:@"adder_detail.id"]] ;
                     
                     [arraySocities removeObjectAtIndex:indexSoc];
                     
                     [[NSUserDefaults standardUserDefaults]setObject:arraySocities forKey:USER_SELECTED_SOCITIES];
                     
                     [USERDEFAULT synchronize];
                     
                     NSInteger points = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"] integerValue];
                     
                     points = points - 75;
                     
                     [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld", (long)points] forKey:@"userPoints"];
                 }
             }
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         if(![Utilities CheckInternetConnection])
         {
         }
         else
         {
             
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }
     }];
}

-(void)getStudentSocieties:(NSString *)statusSociety
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"societyDetail" parameters:@{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]} success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [[NSUserDefaults standardUserDefaults] setValue:responseObject forKey:@"userSocieties"];
             
             [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"society"]] forKey:@"societyCount"];
         }
         else
         {
             NSLog(@"wrong result");
         }
         
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];
}


-(void)loginWithFacebook
{
    
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        [self getfacebookuserInfo];
    }
    
    else
    {
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile,email"]
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error)
         {
             if (!error)
             {
                 [self getfacebookuserInfo];
             }
         }];
    }
}

-(void)getfacebookuserInfo
{
    [FBRequestConnection startWithGraphPath:@"/me"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error)
     {
         if (!error)
         {
             [self callRegisterFbAPI:result];
         }
         else
         {
             if(![Utilities CheckInternetConnection])
             {
             }
             else
             {
                 if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
                 {
                     [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                 }
                 else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
                 {
                     [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                 }
                 else
                 {
                     [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                 }
             }
         }
     }];
}

-(void)callRegisterFbAPI:(NSDictionary*)dictInfoFB
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parametersFb = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"fb_id":[dictInfoFB objectForKey:@"id"],@"email":[dictInfoFB objectForKey:@"email"],@"name":[NSString stringWithFormat:@"%@ %@",[dictInfoFB objectForKey:@"first_name"],[dictInfoFB objectForKey:@"last_name"]],@"device type":@"I",@"device":strDeviceToken};
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@register_fb",baseUrl];
    
    [manager POST:urlStr parameters:parametersFb success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] boolValue])
         {
             [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"FbLogIn"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             [[[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"\nPortfolio Saved" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             
             if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
             {
                 if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"LinkedInLogIn"] boolValue])
                 {
                     [self signInToLinkedIn];
                 }
             }
             else{
                 [self confirmSocietyEventInInvitaion];
             }
         }
         else
         {
             if ([[responseObject valueForKey:@"message"] isEqualToString:@"User Already Registered"])
             {
                 NSString *stringUserId =[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"user_id"]];
                 NSString *stringSelfUserId =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
                 
                 if ([stringSelfUserId isEqualToString:stringUserId])
                 {
                     [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"FbLogIn"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     
                     if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
                     {
                         if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"LinkedInLogIn"] boolValue])
                         {
                             [self signInToLinkedIn];
                         }
                     }
                     else{
                         [self confirmSocietyEventInInvitaion];
                     }
                 }
                 else
                 {
                     ZAlertWithParam(@"Please try again",@"This Facebook account is already registered with other user. Please try with different Facebook account.");
                 }
             }
             else{
                 ZAlertWithParam(@"Please try again",[responseObject objectForKey:@"message"]);
             }
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if(![Utilities CheckInternetConnection])
         {
         }
         else
         {
             
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }
     }];
}

-(void)requestWebservice
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"evid":[NSString stringWithFormat:@"%@",[_dicMessageDetail valueForKey:@"id"]]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"requestEvent" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         
         if ([[responseObject valueForKey:@"status"] isEqualToString:@"true"])
         {
             stringRequestInvite = @"requested";
             
             [_buttonRequest setTitle:@"Invitation requested" forState:UIControlStateNormal];
             
             [_buttonRequest setEnabled:NO];
             
             if ([[_dicMessageDetail valueForKey:@"type"]isEqualToString:@"studentsociety"])
             {
                 
             }
             else  if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"admin"])
             {
                 
             }
             else
             {
                 if ( [stringAlrdyAdded isEqualToString: @"added"])
                 {
                     NSInteger points = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"] integerValue];
                     
                     points = points + 150;
                     
                     [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld", (long)points] forKey:@"userPoints"];
                 }
                 else
                 {
                     [self webServiceAddPort];
                     
                     NSInteger points = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"] integerValue];
                     
                     points = points + 150;
                     
                     [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld", (long)points] forKey:@"userPoints"];
                 }
             }
             
             CGRect rectTextVw = _textViewDetail.frame;
             
             rectTextVw.size.height = rectTextVw.size.height-45;
             
             _textViewDetail.frame = rectTextVw;
             
             _labelResponseConfirmation.hidden= NO;
             
             [[[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"\nInvitation request has been sent" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         
         if ([[responseObject valueForKey:@"message"] isEqualToString:@"Already  added"])
             
         {
             [_buttonRequest setTitle:@"Invitation requested" forState:UIControlStateNormal];
             
             [_buttonRequest setEnabled:NO];
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         if(![Utilities CheckInternetConnection])
         {
         }
         else
         {
             
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }
     }];
}


-(void)callWebserviceLinkedIn:(NSDictionary*)dic
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    NSString *imageUrl = nil;
    
    if ([dic valueForKey:@"pictureUrl"])
    {
        imageUrl = [dic valueForKey:@"pictureUrl"];
    }
    
    else
    {
        imageUrl = @"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQFWv5GGlGD36t-cjXx3EMOH0qf9XAVmYbltGCGMaOH1UIdxlfW";
    }
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"image":imageUrl,@"url":[dic valueForKey:@"publicProfileUrl"]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"studentLinkedIn" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         
         if ([[responseObject valueForKey:@"status"] isEqualToString:@"true"])
         {
             [self addAlert:@"Confirmation" msgStr:@"Please confirm if you would like to request an invitation with your LinkedIn profile?" btnArr:@[@"Yes",@"No"] alertTag:8598];
             
             [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"LinkedInLogIn"];
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         if(![Utilities CheckInternetConnection])
         {
         }
         else
         {
             
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
                 
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
                 
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }
     }];
}

-(void)checkSocialLinks
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"CheckSocial" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] boolValue])
         {
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"fb"] forKey:@"FbLogIn"];
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"linkedIn"] forKey:@"LinkedInLogIn"];
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"%@",error.localizedDescription);
     }];
}

-(void)readMessage
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    NSDictionary *param =  @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"msgid":[NSString stringWithFormat:@"%@",[_dicMessageDetail valueForKey:@"id"]]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"read_message" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         NSLog(@" read status %@",responseObject);
         
         stringLike = @"";
         
         [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callPortWebService"];
         badge = (int) [[NSUserDefaults standardUserDefaults]integerForKey:@"badge"];
         
         if (badge ==0)
         {
             [UIApplication sharedApplication].applicationIconBadgeNumber = badge;
         }
         else
         {
             NSLog(@"badge is userdefault %d", (int) [[NSUserDefaults standardUserDefaults]integerForKey:@"badge"]);
             
             NSLog(@"badge is unread %d", badge);
             
             if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"admin"])
             {
                 badge --;
                 
                 [[NSUserDefaults standardUserDefaults]setInteger:badge forKey:@"badge"];
                 
                 [UIApplication sharedApplication].applicationIconBadgeNumber = badge;
             }
             else if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
             {
                 if ([arrayCompanyIdsContain containsObject:stringCompanyId])
                 {
                     badge --;
                     
                     [[NSUserDefaults standardUserDefaults]setInteger:badge forKey:@"badge"];
                     
                     [UIApplication sharedApplication].applicationIconBadgeNumber = badge;
                 }
                 else
                 {
                     
                 }
             }
             
             else if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"studentsociety"])
             {
                 NSArray *array = [NSArray arrayWithArray:PREF(USER_SELECTED_SOCITIES)];
                 
                 NSLog(@"array is %@", array);
                 
                 if ([array containsObject:stringCompanyId])
                 {
                     badge --;
                     
                     [[NSUserDefaults standardUserDefaults]setInteger:badge forKey:@"badge"];
                     
                     [UIApplication sharedApplication].applicationIconBadgeNumber = badge;
                 }
                 else
                 {
                     
                 }
             }
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"%@",error.localizedDescription);
         
         [self performSelectorInBackground:@selector(readMessage) withObject:nil];
     }];
}


-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        spinner.center = self.view.center;
        spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [spinner startAnimating];
        [self.navigationController.view setUserInteractionEnabled:NO];
        [self.view addSubview:spinner];
    }
    else
    {
        [spinner stopAnimating];
        [self.navigationController.view setUserInteractionEnabled:YES];
        [spinner removeFromSuperview];
        spinner = nil;
    }
}

- (void)openPopUpMsgDetail:(id)sender
{
    

    if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
    {
        //msg007
        BOOL isMessagesStopped=NO;
        isMessagesStopped=![[MessageSubscriptionManager sharedManager] isMessageSubscribedForId:[NSString stringWithFormat:@"%@",[_dicMessageDetail valueForKey:@"adder_id"]] isCompany:YES];
        NSLog(@"Messages StoppedNN:%@ For CompanyId:%@",isMessagesStopped?@"YES":@"NO",[NSString stringWithFormat:@"%@",[_dicMessageDetail valueForKey:@"adder_id"]]);
        
        //if ([[_dicMessageDetail valueForKey:@"report"] boolValue])
        if (isMessagesStopped)
        {
            [self alertView:@"Continue receiving messages" strMessage:@"\nPlease confirm if you would like to continue receiving messages from this company?" alertTag:100];
        }
        else
        {
            
            [self alertView:@"Stop receiving messages" strMessage:@"\nPlease confirm if you would like to stop receiving messages from this company?" alertTag:99];
        }
    }
    
    else
    {
        //msg007
        BOOL isMessagesStopped=NO;
        isMessagesStopped=![[MessageSubscriptionManager sharedManager] isMessageSubscribedForId:[NSString stringWithFormat:@"%@",[_dicMessageDetail valueForKey:@"adder_id"]] isCompany:NO];
        NSLog(@"Messages StoppedNN:%@ For SocietyId:%@",isMessagesStopped?@"YES":@"NO",[NSString stringWithFormat:@"%@",[_dicMessageDetail valueForKey:@"adder_id"]]);
        
        //if ([[_dicMessageDetail valueForKey:@"report"] boolValue])
        if (isMessagesStopped)
        {
            [self alertView:@"Continue receiving messages" strMessage:@"\nPlease confirm if you would like to continue receiving messages/events from this society?" alertTag:100];
        }
        
        else
        {
            [self alertView:@"Stop receiving messages" strMessage:@"\nPlease confirm if you would like to stop receiving messages/events from this society?" alertTag:99];
        }
    }
}

-(void)alertView:(NSString *)strHeader strMessage:(NSString*)strMessage  alertTag:(NSInteger)alertTag
{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:strHeader message:strMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *leftBtnAction =
    [UIAlertAction actionWithTitle:@"Yes"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action)
     {
         if (alertTag==99)
         {
             stringFromRightButton = @"";
             
             if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
             {
                 [self webServiceMessageBlockUnblock:@"1"];
             }
             else{
                 [self webServiceSocietyMessageBlockUnblock:@"1"];
             }
         }
         else if (alertTag==100)
         {
             if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
             {
                 [self webServiceMessageBlockUnblock:@"2"];
             }
             else{
                 [self webServiceSocietyMessageBlockUnblock:@"2"];
             }
         }
         
         else if (alertTag == 5800)
         {
             [self savePortfolioAlert];
         }
         else if (alertTag==5858)
         {
             [self loginWithFacebook];
         }
     }];
    
    UIAlertAction *rightBtnAction =
    [UIAlertAction actionWithTitle:@"No"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action)
     {
         
     }];
    
    [alertVC addAction:leftBtnAction];
    [alertVC addAction:rightBtnAction];
    [self presentViewController:alertVC animated:YES completion:nil];
    
    
}

#pragma mark----UIAlertViewDelegate----

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    stringFromRightButton = @"";
    
    if (alertView.tag==99)
    {
        
        
        if (buttonIndex==0)
        {
            if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
            {
                [self webServiceMessageBlockUnblock:@"1"];
            }
            else{
                [self webServiceSocietyMessageBlockUnblock:@"1"];
            }
        }
    }
    else if (alertView.tag==100)
    {
        if (buttonIndex==0)
        {
            if ([[_dicMessageDetail valueForKey:@"type"] isEqualToString:@"companybehalf"])
            {
                [self webServiceMessageBlockUnblock:@"2"];
            }
            else{
                [self webServiceSocietyMessageBlockUnblock:@"2"];
            }
        }
    }
    
    else if (alertView.tag == 5800){
        if (buttonIndex==0){
            [self savePortfolioAlert];
        }
    }
    else if (alertView.tag==5858)
    {
        if (buttonIndex==0)
        {
            [self loginWithFacebook];
        }
    }
}

-(void)changeDataInPortStatus:(NSString *)stringValue
{
    stringPortCompanyScreen = @"fromComapny";
    
    if ([stringValue isEqualToString:@"1"])
    {
        NSArray *arrayId = [arrayPortDataGlobal valueForKey:@"id"];
        
        if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
        {
            int index = (int)[arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
            
            NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayPortDataGlobal[index] copyItems:YES];
            
            [dictChnage removeObjectForKey:@"message_status"];
            
            [dictChnage setValue:@"1" forKey:@"message_status"];
            
            //msg007
            [[MessageSubscriptionManager sharedManager] updateSubscriptionMessagesForId:[NSString stringWithFormat:@"%@",[dictChnage valueForKey:@"id"]] isCompany:YES shouldSubscribe:NO];
            
            [arrayPortDataGlobal replaceObjectAtIndex:index withObject:dictChnage];
        }
    }
    
    else
    {
        NSArray *arrayId= [arrayPortDataGlobal valueForKey:@"id"];
        
        if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
        {
            int index = (int)[arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
            
            NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayPortDataGlobal[index] copyItems:YES];
            
            [dictChnage removeObjectForKey:@"message_status"];
            
            [dictChnage setValue:@"0" forKey:@"message_status"];
            //msg007
            [[MessageSubscriptionManager sharedManager] updateSubscriptionMessagesForId:[NSString stringWithFormat:@"%@",[dictChnage valueForKey:@"id"]] isCompany:YES shouldSubscribe:YES];
            
            [arrayPortDataGlobal replaceObjectAtIndex:index withObject:dictChnage];
        }
    }
}

-(void)webServiceMessageBlockUnblock:(NSString *)strStatus
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *dicParam =@{@"company_id":[_dicMessageDetail valueForKey:@"adder_id"],@"user_id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"status":strStatus};
    
    [manager POST:@"MessageStatus" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         
         stringReport = @"report";
         
         if ([[responseObject valueForKey:@"status"] boolValue])
         {
             if ([strStatus isEqualToString:@"1"])
             {
                 stringBlock = @"1";
                 
                 [buttonSocialMsg setImage:[UIImage imageNamed:@"msgStopDetail"] forState:UIControlStateNormal];
                 
                 [_dicMessageDetail setValue:@"1" forKey:@"report"];
                 
                 [self changeDataInPortStatus:@"1"];
                 
                 NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
                 
                 if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
                     
                 {
                     int index = (int) [arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
                     
                     NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
                     
                     [dictChnage removeObjectForKey:@"message_status"];
                     
                     [dictChnage setObject:@"1" forKey:@"message_status"];
                     //msg007
                     [[MessageSubscriptionManager sharedManager] updateSubscriptionMessagesForId:[NSString stringWithFormat:@"%@",[dictChnage valueForKey:@"id"]] isCompany:YES shouldSubscribe:NO];
                     [arrayCompaniesList replaceObjectAtIndex:index withObject:dictChnage];
                 }
             }
             else
             {
                 stringBlock = @"0";
                 
                 [buttonSocialMsg setImage:[UIImage imageNamed:@"msgDetail"] forState:UIControlStateNormal];
                 
                 [_dicMessageDetail setValue:@"0" forKey:@"report"];
                 
                 [self changeDataInPortStatus:@"0"];
                 
                 NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
                 
                 if ([arrayId containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
                 {
                     int index = (int) [arrayId indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
                     
                     NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
                     
                     [dictChnage removeObjectForKey:@"message_status"];
                     
                     [dictChnage setObject:@"0" forKey:@"message_status"];
                     //msg007
                     [[MessageSubscriptionManager sharedManager] updateSubscriptionMessagesForId:[NSString stringWithFormat:@"%@",[dictChnage valueForKey:@"id"]] isCompany:YES shouldSubscribe:YES];
                     [arrayCompaniesList replaceObjectAtIndex:index withObject:dictChnage];
                 }
             }
         }
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
     }];
}


-(void)webServiceSocietyMessageBlockUnblock:(NSString *)strStatus
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *dicParam =@{@"society_id":[_dicMessageDetail valueForKey:@"adder_id"],@"user_id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"status":strStatus};
    
    [manager POST:@"MessageStatussociety" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         
         stringReport = @"report";
         
         if ( [stringFromRightButton isEqualToString:@"rightButton"])
             
         {
             stringFromRightButton = @"";
         }
         
         if ([[responseObject valueForKey:@"status"] boolValue])
         {
             if ([strStatus isEqualToString:@"1"])
             {
                 if ([stringRemoveSoc isEqualToString:@"remove"])
                 {
                     stringRemoveSoc = @"";
                     [_dicMessageDetail setValue:@"1" forKey:@"report"];
                 }
                 else
                 {
                     [buttonSocialMsg setImage:[UIImage imageNamed:@"msgStopDetail"] forState:UIControlStateNormal];
                     [_dicMessageDetail setValue:@"1" forKey:@"report"];
                 }
                 
                 NSMutableDictionary *dict = [NSMutableDictionary new];
                 
                 [dict setObject:[_dicMessageDetail valueForKey:@"adder_id"] forKey:@"adder_id"];
                 
                 [dict setObject:@"1" forKey:@"value"];
                 
                 if ([[arraySocietyGlobal valueForKey:@"adder_id"]containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
                 {
                     int ind = (int)[[arraySocietyGlobal valueForKey:@"adder_id"] indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
                     
                     [arraySocietyGlobal  removeObjectAtIndex:ind];
                     
                     [arraySocietyGlobal addObject:dict];
                 }
                 else
                 {
                     [arraySocietyGlobal addObject:dict];
                 }
                 
                 stringBlock =@"1";
                 
                 //msg007
                 [[MessageSubscriptionManager sharedManager] updateSubscriptionMessagesForId:[NSString stringWithFormat:@"%@",[_dicMessageDetail valueForKey:@"adder_id"]] isCompany:YES shouldSubscribe:NO];
             }
             else
             {
                 [buttonSocialMsg setImage:[UIImage imageNamed:@"msgDetail"] forState:UIControlStateNormal];
                 
                 [_dicMessageDetail setValue:@"0" forKey:@"report"];
                 
                 NSMutableDictionary *dict= [NSMutableDictionary new];
                 [dict setObject:[_dicMessageDetail valueForKey:@"adder_id"] forKey:@"adder_id"];
                 
                 [dict setObject:@"0" forKey:@"value"];
                 
                 if ([[arraySocietyGlobal valueForKey:@"adder_id"]containsObject:[_dicMessageDetail valueForKey:@"adder_id"]])
                 {
                     int ind = (int)[[arraySocietyGlobal valueForKey:@"adder_id"] indexOfObject:[_dicMessageDetail valueForKey:@"adder_id"]];
                     
                     [arraySocietyGlobal  removeObjectAtIndex:ind];
                     
                     [arraySocietyGlobal addObject:dict];
                 }
                 else
                 {
                     [arraySocietyGlobal addObject:dict];
                 }
                 stringBlock =@"0";
                 
                 //msg007
                 [[MessageSubscriptionManager sharedManager] updateSubscriptionMessagesForId:[NSString stringWithFormat:@"%@",[_dicMessageDetail valueForKey:@"adder_id"]] isCompany:YES shouldSubscribe:YES];

             }
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
     }];
}

-(void)webServiceLikeMsg:(NSString *)stringStatus
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *dicParam =@{@"message_id":[_dicMessageDetail valueForKey:@"id"],@"user_id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"status":stringStatus};
    
    [manager POST:@"MessageLikeDislike" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         
         if ([responseObject valueForKey:@"status"])
         {
             stringLike = @"like";
             
             [_btnLikeMsg setSelected:YES];
             
         }
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
     }];
}

- (IBAction)btnLikeMsgAction:(id)sender
{
    if (![[_dicMessageDetail valueForKey:@"like_status"] boolValue])
    {
        [self webServiceLikeMsg:@"1"];
    }
}

@end


