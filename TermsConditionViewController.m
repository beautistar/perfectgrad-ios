
//
//  TermsConditionViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 2/2/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "TermsConditionViewController.h"
#import "PortfolioViewController.h"
#import "ApiClass.h"
#import "Globals.h"
#import "NSString+Utilities.h"

@class CircleProgressBar;

typedef enum : NSUInteger {
    CustomizationStateDefault = 0,
    CustomizationStateCustom,
    CustomizationStateCustomAttributed,
} CustomizationState;

@interface TermsConditionViewController ()
{
    UIActivityIndicatorView *spinner;
    
    ApiClass *cls;
    Database *dummy;
    
    NSArray *arrayData;
    
    NSString *stringSocietyIcon;
    
    PortfolioViewController *portfolioView;
    
    NSDictionary *dictData;
    
    NSArray *arrayData1;
    
    NSArray *arrayBanner;
    
    NSArray *arraySalaries;
    
    NSArray *arrayAllCompaniesPort;
}

@end

@implementation TermsConditionViewController

-(void)viewWillAppear:(BOOL)animated
{
    stringNotifiction = @"";
    stringForNotifyEditCompany = @"";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    dummy=[[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    _circleProgressBar.hidden = YES;
    
    [_checkBoxButton setImage:[UIImage imageNamed:@"agree"] forState:UIControlStateSelected];
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
    self.navigationItem.leftBarButtonItem = leftItem;
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:20.0f],     NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
    
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    _viewTerms.layer.borderWidth = 1.0;
    _viewTerms.layer.borderColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f].CGColor;
    
    self.navigationItem.title = @"Terms and Conditions";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _buttonSubmit.layer.cornerRadius = 5.0f;
    _buttonSubmit.layer.borderWidth = 2.0f;
    
    UIColor *btnColor =[UIColor lightGrayColor];
    _buttonSubmit.layer.borderColor = [btnColor  CGColor];
    [_buttonSubmit setTitleColor:btnColor forState:UIControlStateNormal];
    
    NSLog(@"value in cntryLatitude detail %@", PREF(LATITUDE_COUNTRY));
    
    NSLog(@"value in cntryLongitude detail %@", PREF(LONGITUDE_COUNTRY));
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark-----ButtonActions-------

- (IBAction)checkBoxButtonAction:(id)sender
{
    /* here is checking if the box is ticked or not. If it is not ticked, we change the BOOL state and change the UIButton image to a checked box, and vice versa*/
    if(![sender isSelected])
    {
        _buttonSubmit.enabled=YES;
        _buttonSubmit.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]  CGColor];
        [_buttonSubmit setTitleColor:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        
        [sender setSelected:YES];
    }
    else
    {
        [sender setSelected:NO];
        _buttonSubmit.layer.borderColor = [[UIColor lightGrayColor]  CGColor];
        [_buttonSubmit setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        _buttonSubmit.enabled=NO;
    }
}


- (IBAction)submitButtonAction:(id)sender
{
    //[self ShowDataLoader:YES];
    [self addLoader];
    //[[MyAppManager sharedManager] showLoaderInMainThread];
    if([_dicUserInfoFromScPg valueForKey:@"id"])
    {
        [self registerUserWithFacebook];
    }
    else{
        [self registerTempUser];
    }
}

-(void)addLoader
{
    UIView *viewBG=[[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.navigationController.view.frame.size.width, self.navigationController.view.frame.size.height)];
    viewBG.backgroundColor=[[UIColor whiteColor] colorWithAlphaComponent:0.4];
    viewBG.tag=1992;
    [self.navigationController.view addSubview:viewBG];
    
    float lblheight=50.0;
    UIImageView *imgView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"progressbar"]];
    imgView.frame=CGRectMake(0.0, 0.0, 90.0, 90.0);
    imgView.backgroundColor=[UIColor clearColor];
    imgView.tag=1991;
    [self.navigationController.view addSubview:imgView];
    imgView.center=CGPointMake(self.view.center.x, self.view.center.y-(lblheight/2.0));
    
    UILabel *lblLoading=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width*0.15,self.view.center.y-(lblheight/2.0)+(imgView.frame.size.height/2.0)+lblheight*0.15,self.view.frame.size.width*0.7,lblheight*0.7)];
    //lblLoading.textColor=[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    lblLoading.textColor=[UIColor colorWithRed:51.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
    lblLoading.font=[UIFont fontWithName:@"OpenSans-Bold" size:25.0];
    lblLoading.adjustsFontSizeToFitWidth=YES;
    lblLoading.text=@"Loading";
    lblLoading.textAlignment=NSTextAlignmentCenter;
    lblLoading.tag=1993;
    [self.navigationController.view addSubview:lblLoading];
    
    CABasicAnimation *fullRotation;
    fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    fullRotation.duration = 1.2f;
    fullRotation.repeatCount = MAXFLOAT;
    [imgView.layer addAnimation:fullRotation forKey:@"360"];
    
    //[self performSelector:@selector(removeLoader) withObject:nil afterDelay:8.0];
}
-(void)removeLoader
{
    if ([self.navigationController.view viewWithTag:1991])
    {
        UIImageView *imgView=[self.navigationController.view viewWithTag:1991];
        [imgView.layer removeAnimationForKey:@"360"];
        
        [imgView removeFromSuperview];
    }
    
    if ([self.navigationController.view viewWithTag:1993])
    {
        UILabel *lblLoading=[self.navigationController.view viewWithTag:1993];
        [lblLoading removeFromSuperview];
    }

    if ([self.navigationController.view viewWithTag:1992])
    {
        UIView *bgView=[self.navigationController.view viewWithTag:1992];
        [bgView removeFromSuperview];
    }
}


-(void)registerUserWithFacebook
{
    NSString* urlStr =  [NSString stringWithFormat:@"%@UpStudent",baseUrl];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:urlStr parameters: _dicUserInfoFromScPg success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             
             if ([responseObject objectForKey:@"user_id"])
             {
                 [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"user_id"] forKey:@"userId"];
             }
             
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"studentviewKey"];
             [[NSUserDefaults standardUserDefaults]synchronize];
             
             [self updateSocieties];
         }
         else
         {
             [self removeLoader];
             NSLog(@"wrong result");
         }
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         //[self ShowDataLoader:NO];
         [self removeLoader];
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}

-(void)registerTempUser
{
    NSString* urlStr =  [NSString stringWithFormat:@"%@UpdateStudent",baseUrl];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    AFHTTPRequestOperation *operation= [manager POST:urlStr parameters: _dicUserInfoFromScPg success:^(AFHTTPRequestOperation *operation, id responseObject)
                                        {
                                            NSLog(@"response object");
                                            
                                            if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
                                            {
                                                if ([responseObject objectForKey:@"user_id"])
                                                {
                                                    [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"user_id"] forKey:@"userId"];
                                                }
                                                
                                                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"studentviewKey"];
                                                [[NSUserDefaults standardUserDefaults]synchronize];
                                                
                                                [self updateSocieties];
                                            }
                                            else
                                            {
                                                [self removeLoader];
                                                NSLog(@"wrong result");
                                            }
                                        }
                                        
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                        {
                                            // [self ShowDataLoader:NO];
                                            [self removeLoader];
                                            NSLog(@"wrong result errrrorrrrrr %@", error);
                                        }];
    
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
     {
         NSLog(@"float value is %lld", (int)totalBytesRead / totalBytesExpectedToRead);
         
         
     }];
}

-(void)backBtnAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)termsConditionCloseButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        [self.view.window setUserInteractionEnabled:NO];
        spinner.center = self.view.center;
        spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [spinner startAnimating];
        [self.view addSubview:spinner];
    }
    else
    {
        [spinner stopAnimating];
        [self.view.window setUserInteractionEnabled:YES];
        [spinner removeFromSuperview];
        spinner = nil;
    }
}

-(void)updateSocieties
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *dict =@{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"sid":[_arrSocietiesFromScPg componentsJoinedByString:@","]};
    
    [manager POST:@"addAllSociety" parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //[self ShowDataLoader:NO];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [self getStudentSocieties];
         }
         else
         {
             [self removeLoader];
             NSLog(@"wrong result");
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         //[self ShowDataLoader:NO];
         [self removeLoader];
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getStudentSocieties
{
    NSString* urlStr = [NSString stringWithFormat:@"%@societyDetail",baseUrl];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [[NSUserDefaults standardUserDefaults] setValue:responseObject forKey:@"userSocieties"];
             
             NSArray *array = [responseObject valueForKeyPath:@"data"];
             
             if (array.count ==0)
             {
                 
             }
             else
             {
                 stringSocietyIcon=[NSString stringWithFormat:@"%@",[[array valueForKey:@"logo"] objectAtIndex:0]];
                 
                 NSArray *arrayId=[array valueForKey:@"id"] ;
                 
                 [USERDEFAULT setObject:arrayId forKey:USER_SELECTED_SOCITIES];
             }
             [self getUserCompanies];
         }
         else
         {
             [self removeLoader];
         }
     }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         //[self ShowDataLoader:NO];
         [self removeLoader];
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
         }
     }];
}

-(void)getUserCompanies
{
    NSString* urlStr =  [NSString stringWithFormat:@"%@user_company",baseUrl];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"callPortWebService"];
         
         NSLog(@"badge count is %@",[responseObject valueForKey:@"badge_count"] );
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:@"badge_count"] forKey:@"badgeCount"];
         
         [UIApplication sharedApplication].applicationIconBadgeNumber = [[responseObject valueForKey:@"badge_count"] intValue];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.country_id"] forKey:@"countryId"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[responseObject valueForKeyPath:@"admin"]] forKey:@"adminCount"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[responseObject valueForKeyPath:@"society"]] forKey:@"societyCount"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.university"] forKey:@"userUniversity"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.uni_id"] forKey:@"userUniversityId"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.score"] forKey:@"userPoints"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.country"] forKey:@"countryName"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.abr"] forKey:@"stateAbr"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.program_name"] forKey:@"programName"];
         
         arrayData = [[NSArray alloc] initWithArray:[responseObject objectForKey:@"data"]];
         
         NSArray *arrayWithoutDuplicates = [[NSOrderedSet orderedSetWithArray:[responseObject valueForKeyPath:@"user_discipline.discipline"]] array];
         
         [[NSUserDefaults standardUserDefaults]setObject:[arrayWithoutDuplicates componentsJoinedByString:@", "] forKey:@"userDiscipline"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.state"] forKey:@"StateName"];
         
         [self savePortfolioData];
     }
     
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         // [self ShowDataLoader:NO];
         {
             // [self ShowDataLoader:NO];
             [self removeLoader];
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
             }
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
             }
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
             }
         }
     }];
}

-(void)savePortfolioData
{
    if ([dummy deleteTable:@"delete from PORTFOLIO"])
    {
        if ([dummy deleteTable:@"delete from PortLocationsTable"])
        {
            if ([dummy deleteTable:@"delete from PortAppDueDate"])
            {
                if ([dummy deleteTable:@"delete from PortCompanyAwards"])
                {
                    if ([dummy deleteTable:@"delete from PortDisciplinesTable"])
                    {
                        if ([dummy deleteTable:@"delete from PortSubDisciplinesTable"])
                        {
                            if ([dummy deleteTable:@"delete from PortQuesAnsTable"])
                            {
                                if ([dummy deleteTable:@"delete from PortCompanyDueDate"])
                                {
                                    if ([dummy deleteTable:@"delete from PortCompSocialLinksTable"])
                                    {
                                        for (int i=0; i<[arrayData count]; i++)
                                        {
                                            NSString *stringINt;
                                            
                                            if ([[[arrayData valueForKey:@"international"] objectAtIndex:i]isKindOfClass:[NSNull class]])
                                            {
                                                stringINt = @"";
                                            }
                                            
                                            else
                                            {
                                                stringINt =[[arrayData valueForKey:@"international"] objectAtIndex:i];
                                            }
                                            
                                            NSString *query = [NSString stringWithFormat: @"INSERT INTO PORTFOLIO (id,application_due_date, awards,city,company_address,company_name,description,logo,state_id,unread_message,regstates,company_intro,country,message_status,website,international,perfect_logo,society_logo) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\",\"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")"
                                                               ,[[arrayData valueForKey:@"id"] objectAtIndex:i]
                                                               ,[[arrayData valueForKey:@"application_due_date"] objectAtIndex:i]
                                                               ,[[arrayData valueForKey:@"awards"] objectAtIndex:i],[[arrayData valueForKey:@"city"] objectAtIndex:i]
                                                               ,[[arrayData valueForKey:@"company_address"] objectAtIndex:i]
                                                               ,[[arrayData valueForKey:@"company_name"] objectAtIndex:i]
                                                               ,[[arrayData valueForKey:@"description"] objectAtIndex:i]
                                                               ,[[arrayData valueForKey:@"logo"] objectAtIndex:i]
                                                               ,[[arrayData valueForKey:@"state_id"] objectAtIndex:i]
                                                               ,[NSString stringWithFormat:@"%@",[[arrayData valueForKey:@"unread_message"] objectAtIndex:i]]
                                                               ,[[arrayData valueForKey:@"regstates"] objectAtIndex:i]
                                                               ,[[arrayData valueForKey:@"company_intro"] objectAtIndex:i]
                                                               ,[[arrayData valueForKey:@"country"] objectAtIndex:i]
                                                               ,[[arrayData valueForKey:@"message_status"] objectAtIndex:i]
                                                               ,[[arrayData valueForKey:@"website"] objectAtIndex:i],stringINt,[[arrayData valueForKey:@"perfect_logo"] objectAtIndex:i],stringSocietyIcon];
                                            
                                            [self saveImages:[[NSString stringWithFormat:@"%@%@",[[arrayData valueForKey:@"id"] objectAtIndex:i],[[arrayData valueForKey:@"logo"] objectAtIndex:i]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] imageUrl:[[arrayData valueForKey:@"logo"] objectAtIndex:i]];
                                            
                                            [self saveImages:[[NSString stringWithFormat:@"%@%@",[[arrayData valueForKey:@"id"] objectAtIndex:i],[[arrayData valueForKey:@"perfect_logo"] objectAtIndex:i]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] imageUrl:[[arrayData valueForKey:@"perfect_logo"] objectAtIndex:i]];
                                            
                                            [self saveImages:[[NSString stringWithFormat:@"%@%@",[[arrayData valueForKey:@"id"] objectAtIndex:i],stringSocietyIcon] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] imageUrl:stringSocietyIcon];
                                            
                                            const char *query_stmt = [query UTF8String];
                                            [dummy  runQueries:query_stmt strForscreen:@"port"];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    [self getDisciplineAndSubDiscp];
}


-(void)saveImages:(NSString *)ImageName imageUrl :(NSString *)imageUrl
{
    ImageName = [ImageName stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    if (imageUrl) {
        if ([[imageUrl removeNull] length]>0)
        {
            if (![self imageCheck:ImageName])
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:ImageName];
                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,imageUrl]]];
                [imageData writeToFile:savedImagePath atomically:NO];
            }
        }
    }
}

-(BOOL)imageCheck :(NSString *)strName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    if ([contents containsObject:strName])
    {
        return YES;
    }
    return NO;
}

-(void)getDisciplineAndSubDiscp
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"getAlldisciplines" parameters:@{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"cid":@""} success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSMutableDictionary *dictDiscA = [[NSMutableDictionary alloc]init];
         
         NSMutableArray *arrayCompyFinal= [[NSMutableArray alloc]init];
         
         NSMutableArray *arrayComp = [[NSMutableArray alloc]init];
         
         NSMutableArray *arrayAllDiscipline=[NSMutableArray new];
         
         arrayAllDiscipline = [[responseObject valueForKey:@"data"] mutableCopy];
         
         int indexDisci = 0;
         
         BOOL boolFirst = false;
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"callDesciplineWebService"];
             
             dictData = [[NSDictionary alloc]initWithDictionary:responseObject];
             
             for (int c =0; c < [[dictData valueForKey:@"data"] count]; c++)
             {
                 for (int k = 0; k<[[[dictData valueForKeyPath:@"data.subdiscipline"]objectAtIndex:c] count]; k++)
                 {
                     NSLog(@"k is %d",k);
                     
                     if (indexDisci <= [[dictData valueForKeyPath:@"data.subdiscipline"] count]-1)
                     {
                         if (boolFirst == YES)
                         {
                             if (k==0)
                             {
                                 indexDisci ++;
                                 arrayComp = [[NSMutableArray alloc]init];
                             }
                         }
                         else
                         {
                             boolFirst = YES;
                         }
                     }
                     
                     NSString *stringName = [[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:@"name"]objectAtIndex:c]objectAtIndex:k];
                     
                     NSMutableArray *arrayDes = [[NSMutableArray alloc]init];
                     
                     NSMutableDictionary *dictE = [[NSMutableDictionary alloc]init];
                     
                     if ([[[[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:stringName]objectAtIndex:c]objectAtIndex:k]valueForKey:@"allcompanys"] count] == 0)
                     {
                         
                         [arrayDes addObject:@"000"];
                         
                         [dictE setObject:arrayDes forKey:[NSString stringWithFormat:@"%d", k]];
                         
                         [arrayComp addObject:dictE];
                     }
                     else
                     {
                         for (int j=0; j<[[[[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:stringName]objectAtIndex:c]objectAtIndex:k]valueForKey:@"allcompanys"] count]; j++)
                         {
                             NSString *stringCompanyName = [[[[[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:stringName]objectAtIndex:c]objectAtIndex:k]valueForKey:@"allcompanys"]valueForKey:@"company_id"]objectAtIndex:j];
                             
                             
                             [arrayDes addObject:stringCompanyName];
                         }
                         
                         [dictE setObject:arrayDes forKey:[NSString stringWithFormat:@"%d", k]];
                         
                         [arrayComp addObject:dictE];
                     }
                     
                     [dictDiscA setObject:arrayComp forKey:[NSString stringWithFormat:@"%d", indexDisci]];
                 }
             }
             
             [arrayCompyFinal addObject:dictDiscA];
             
             NSLog(@"array is %@",dictDiscA);
             
             [[NSUserDefaults standardUserDefaults]setObject:arrayCompyFinal forKey:@"companyDisciplinearray"];
             
             [[NSUserDefaults standardUserDefaults]setObject:arrayAllDiscipline forKey:@"allDiscipline"];
             
             [[NSUserDefaults standardUserDefaults]synchronize];
             
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 
                 if ([dummy deleteTable:@"delete from DISCIPLINES"])
                 {
                     if ([dummy deleteTable:@"delete from SubDisciplines"])
                     {
                         for (int i=0; i<[[dictData valueForKey:@"data"] count]; i++)
                         {
                             NSString *query;
                             query = [NSString stringWithFormat: @"INSERT INTO DISCIPLINES (id,name) VALUES (\"%@\",\"%@\")",[[dictData valueForKeyPath:@"data.id"] objectAtIndex:i],[[dictData valueForKeyPath:@"data.name"] objectAtIndex:i]];
                             
                             const char *query_stmt = [query UTF8String];
                             [dummy  runQueries:query_stmt strForscreen:@"Disciplines" ];
                             
                             for (int k=0; k<[[[dictData valueForKeyPath:@"data.subdiscipline"]objectAtIndex:i] count]; k++)
                             {
                                 NSString *stringName = [[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:@"name"]objectAtIndex:i]objectAtIndex:k];
                                 
                                 NSString *stringCom = [NSString stringWithFormat:@"%@",[[[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:stringName]valueForKey:@"companys"]objectAtIndex:i]objectAtIndex:k]];
                                 
                                 NSString *subQuery;
                                 subQuery = [NSString stringWithFormat: @"INSERT INTO SubDisciplines (date,id,name,discipline_id,companys) VALUES (\"%@\",\"%@\", \"%@\",\"%@\",\"%@\")",[[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:@"date"]objectAtIndex:i]objectAtIndex:k],[[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:@"id"]objectAtIndex:i]objectAtIndex:k],[[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:@"name"]objectAtIndex:i]objectAtIndex:k],[[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:@"discipline_id"]objectAtIndex:i]objectAtIndex:k],stringCom];
                                 
                                 const char *query_stmtSubDiscp = [subQuery UTF8String];
                                 
                                 [dummy runQuerySubDisciplines:query_stmtSubDiscp strForscreen:@"DisciplinesVw"];
                             }
                         }
                     }
                 }
                 
                 NSLog(@"discipline %@", [dummy loaddataCountries:@"Select * From DISCIPLINES" strForscreen:@"DISCIPLINES"]);
                 
             });
             
             
             [self getMessages];
         }
         else
         {
             [self removeLoader];
             [[[UIAlertView alloc]initWithTitle:@"Error" message:@"\nNo result to display." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         {
             [self removeLoader];
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                 
             }
             
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                 
             }
             
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }
         
     }];
}

-(void)getMessages
{
    messageAlrdyLoad = NO;
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"getmessages" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSDictionary *dictResponse = [[NSDictionary alloc]initWithDictionary:responseObject];
         
         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"callMsgWebService"];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [self.navigationController.view setUserInteractionEnabled:NO];
             
             [self saveMessageData:dictResponse];
             
             [self performSelectorInBackground:@selector(getSalariesList) withObject:nil];
             
         }
         else
         {
         }
         self.navigationController.view.userInteractionEnabled = YES;
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         {
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"No Internet " message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                 
                 [self getMessages];
             }
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"No Internet " message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                 [self getMessages];
             }
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                 [self removeLoader];
             }
         }
     }];
}

-(void)saveMessageData:(NSDictionary *)dictResponse
{
    if ([dummy deleteTable:@"delete from MessagesTable"])
    {
        if ([dummy deleteTable:@"delete from messagerAdderDetailTable"])
        {
            for (int i=0; i<[[dictResponse valueForKey:@"data"] count]; i++)
            {
                NSString *strLogo = [[[dictResponse valueForKeyPath:@"data.adder_detail"] valueForKey:@"logo"]objectAtIndex:i];
                if ([strLogo isKindOfClass:[NSNull class]])
                {
                    strLogo = @"";
                }
                NSString *perfectLogo = [[dictResponse valueForKeyPath:@"data.perfect_logo"]objectAtIndex:i];
                
                if ([perfectLogo isKindOfClass:[NSNull class]])
                {
                    perfectLogo = @"";
                }
                NSString *queryMessages = [NSString stringWithFormat: @"INSERT INTO MessagesTable (adder_id,date, event_date,event_title,event_venue,eventtimefrom,eventtimeto,gender,id,message,message_type,status,type,logo,name,eventconfirmation,default_timezone,website,report,like_status,addedStatus,eventstatus,perfect_logo) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\",\"%@\", \"%@\",\"%@\",\"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")"
                                           ,[[dictResponse valueForKeyPath:@"data.adder_id"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.date"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.event_date"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.event_title"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.event_venue"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.eventtimefrom"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.eventtimeto"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.gender"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.id"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.message"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.message_type"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.status"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.type"]objectAtIndex:i]
                                           ,strLogo
                                           ,[[[dictResponse valueForKeyPath:@"data.adder_detail"] valueForKey:@"name"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.eventconfirmation"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.default_timezone"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.website"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.report"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.like_status"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.addedStatus"]objectAtIndex:i],[[dictResponse valueForKeyPath:@"data.eventstatus"]objectAtIndex:i],perfectLogo];
                [self saveImages:[NSString stringWithFormat:@"%@%@",[[dictResponse valueForKeyPath:@"data.adder_id"]objectAtIndex:i],strLogo] imageUrl:strLogo];
                
                [self saveImages:[NSString stringWithFormat:@"%@%@",[[dictResponse valueForKeyPath:@"data.adder_id"]objectAtIndex:i],perfectLogo] imageUrl:perfectLogo];
                
                const char *queryMessagesStat = [queryMessages UTF8String];
                
                [dummy runQueries:queryMessagesStat strForscreen:@"messsages"];
            }
        }
    }
    
    [arrayMessageGlobal removeAllObjects];
    
    arrayMessageGlobal = [[dummy loadDataMessages:@"Select * From MessagesTable"] mutableCopy];
}

#pragma mark getSalary

-(void)getSalariesList
{
    //[self ShowDataLoader:YES];
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@getAllSalaries",baseUrl];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"salaryGuide"];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             arrayData1 = nil;
             
             arrayData1 = [[NSArray alloc]initWithArray:[responseObject valueForKeyPath:@"data.salary"]];
             
             arrayBanner = [responseObject valueForKeyPath:@"data.banners"];
             
             arraySalaries = [arrayData1 mutableCopy];
             
             if (arrayBanner.count == 0)
             {
                 
             }
             else
             {
                 
             }
             
             NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryName"]];
             
             if ([str isEqualToString:@"(null)"])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"\nPlease update your profile again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else
             {
             }
             for (int i=0; i<[arrayBanner count]; i++)
             {
                 NSString *query;
                 NSString   *stringId;
                 
                 if (![arrayBanner valueForKey:@"id"])
                 {
                     stringId = @"0189654";
                 }
                 else
                 {
                     stringId = [[arrayBanner valueForKey:@"id"] objectAtIndex:i];
                 }
                 query = [NSString stringWithFormat: @"INSERT INTO SALARIESBANNERS (image_name,rotation_time,id,name,bannerUrl) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",[[arrayBanner valueForKey:@"image_name"] objectAtIndex:i],[[arrayBanner valueForKey:@"rotation_time"] objectAtIndex:i],stringId,[[arrayBanner valueForKey:@"name"]objectAtIndex:i],[[arrayBanner valueForKey:@"banner_url"]objectAtIndex:i]];
                 
                 [self saveImages:[[NSString stringWithFormat:@"%@%@",[[arrayBanner valueForKey:@"id"] objectAtIndex:i],[[arrayBanner valueForKey:@"image_name"] objectAtIndex:i]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] imageUrl:[[arrayBanner valueForKey:@"image_name"] objectAtIndex:i]];
                 
                 const char *query_stmt = [query UTF8String];
                 [dummy  runQueries:query_stmt strForscreen:@"SalariesBanners"];
             }
             
             [self performSelectorInBackground:@selector(saveSalaryGuide) withObject:nil];
             
             [self getAllCompaniesSearch];
         }
     }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         //[self ShowDataLoader:NO];
         
         if(![Utilities CheckInternetConnection])
         {
             [self removeLoader];
         }
         else
         {
             {
                 if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
                 {
                     [[[UIAlertView alloc]initWithTitle:@"No Internet " message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                     
                     [self getSalariesList];
                 }
                 else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
                 {
                     [[[UIAlertView alloc]initWithTitle:@"No Internet " message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                     [self getSalariesList];
                 }
                 else
                 {
                     [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                     [self removeLoader];
                 }
             }
         }
     }];
}

-(void)saveSalaryGuide
{
    {
        if ([dummy deleteTable:@"delete from SALARIES"])
        {
            for (int i=0; i<[arrayData1 count]; i++)
            {
                NSString *query;
                
                query = [NSString stringWithFormat: @"INSERT INTO SALARIES (id,occupation,occupation_id,salary0to3,title,sector,sector_id,salary3to5,salary5to7,year,large,smallMedium,type) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")"
                         ,[[arrayData1 valueForKey:@"id"] objectAtIndex:i]
                         ,[[arrayData1 valueForKey:@"occupation"] objectAtIndex:i]
                         ,[[arrayData1 valueForKey:@"occupation_id"] objectAtIndex:i]
                         ,[[arrayData1 valueForKey:@"salary0to3"] objectAtIndex:i]
                         ,[[arrayData1 valueForKey:@"title"] objectAtIndex:i]
                         ,[[arrayData1 valueForKey:@"sector"] objectAtIndex:i]
                         ,[[arrayData1 valueForKey:@"sector_id"] objectAtIndex:i]
                         ,[[arrayData1 valueForKey:@"salary3to5"] objectAtIndex:i]
                         ,[[arrayData1 valueForKey:@"salary5to7"] objectAtIndex:i]
                         ,[[arrayData1 valueForKey:@"year"] objectAtIndex:i]
                         ,[[arrayData1 valueForKey:@"large"] objectAtIndex:i]
                         ,[[arrayData1 valueForKey:@"smallMedium"] objectAtIndex:i]
                         ,[[arrayData1 valueForKey:@"type"] objectAtIndex:i]];
                
                const char *query_stmt = [query UTF8String];
                
                [dummy  runQueries:query_stmt strForscreen:@"Salaries" ];
            }
        }
        
        if ([dummy deleteTable:@"delete from SALARIESBANNERS"])
        {
            NSString *string = [NSString stringWithFormat:@"%@",[[arrayBanner valueForKey:@"msg"] objectAtIndex:0]];
            
            if ([string isEqualToString:@"Nothing found"])
            {
                
            }
            else
            {
                NSString *stringId;
                
                for (int i=0; i<[arrayBanner count]; i++)
                {
                    NSString *query;
                    
                    if (![arrayBanner valueForKey:@"id"])
                    {
                        stringId = @"0189654";
                    }
                    else
                    {
                        stringId = [[arrayBanner valueForKey:@"id"] objectAtIndex:i];
                    }
                    query = [NSString stringWithFormat: @"INSERT INTO SALARIESBANNERS (image_name,rotation_time,id,name,bannerUrl) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",[[arrayBanner valueForKey:@"image_name"] objectAtIndex:i],[[arrayBanner valueForKey:@"rotation_time"] objectAtIndex:i],stringId,[[arrayBanner valueForKey:@"name"]objectAtIndex:i],[[arrayBanner valueForKey:@"banner_url"]objectAtIndex:i]];
                    
                    [self saveImages:[[NSString stringWithFormat:@"%@%@",[[arrayBanner valueForKey:@"id"] objectAtIndex:i],[[arrayBanner valueForKey:@"image_name"] objectAtIndex:i]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] imageUrl:[[arrayBanner valueForKey:@"image_name"] objectAtIndex:i]];
                    
                    const char *query_stmt = [query UTF8String];
                    [dummy  runQueries:query_stmt strForscreen:@"SalariesBanners"];
                }
            }
        }
        // [self ShowDataLoader:NO];
    }
}


#pragma mark - GetAllCompanies

-(void)getAllCompaniesSearch
{
    NSString* urlStr =  [NSString stringWithFormat:@"%@getAllCompanies",baseUrl];
    
    NSDictionary *dicParam = @{@"id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             arrayAllCompaniesPort = [responseObject valueForKeyPath:@"data"];
             
             [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"callallCompanyWebService"];
             
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             [self saveSearchCompanies];
             
             NSArray *arrCompanies = [[NSArray alloc]initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]];
             
             [[NSUserDefaults standardUserDefaults]setObject:arrCompanies forKey:@"allCompanies"];
             
             NSArray *arrayTemp = [dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"];
             
             NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
             
             NSArray* sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
             
             NSArray *arraySorted = [[NSArray alloc] initWithArray:[arrayTemp sortedArrayUsingDescriptors:sortDescriptors]];
             
             [arrayCompaniesList removeAllObjects];
             
             arrayCompaniesList = [arraySorted mutableCopy];
             
             // [self ShowDataLoader:NO];
             [self removeLoader];
             fromTermsCondition = YES;
             
             portfolioView =[self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
             
             portfolioView.arrayData = arrayData;
             
             [self.navigationController pushViewController:portfolioView animated:YES];
         }
         else
         {
             [self removeLoader];
             if ([[responseObject objectForKey:@"data"]isEqualToString:@"no result"])
             {
                 
             }
         }
     }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         // [self ShowDataLoader:NO];
         
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"No Internet " message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             
             [self getAllCompaniesSearch];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"No Internet " message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             [self getAllCompaniesSearch];
         }
         else
         {
             [self removeLoader];
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}


-(void)saveSearchCompanies
{
    if ([dummy deleteTable:@"delete from COMPANIES"])
    {
        if ([dummy deleteTable:@"delete from COMPANYLOCATIONS"])
        {
            if ([dummy deleteTable:@"delete from DispSubDispCompaniesTable"])
            {
                if ([dummy deleteTable:@"delete from SearchCompSubDisciplineTable"])
                {
                    if ([dummy deleteTable:@"delete from SearchCompanyAwards"])
                    {
                        if ([dummy deleteTable:@"delete from SearchCompanyDueDate"])
                        {
                            if ([dummy deleteTable:@"delete from SearchQuesAnsTable"])
                            {
                                if ([dummy deleteTable:@"delete from SearchCompSocialLinksTable"])
                                {
                                    for (int i=0; i<[arrayAllCompaniesPort count]; i++)
                                    {
                                        NSString *query = [NSString stringWithFormat: @"INSERT INTO COMPANIES (id,application_due_date, awards,city,company_address,company_name,description,logo,state_id,regstates,country_id,company_intro,country,portfolio_status,message_state,message_status, website,international,unread_message) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")"
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"id"]
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"application_due_date"]
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"awards"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"city"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"company_address"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"company_name"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"description"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"logo"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"state_id"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"regstates"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"country_id"]
                                                           ,[[arrayAllCompaniesPort  objectAtIndex:i] valueForKey:@"company_intro"]
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"country"]
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"portfolio_status"]
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"message_state"]
                                                           ,[[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"message_status"],
                                                           [[arrayAllCompaniesPort objectAtIndex:i] valueForKey:@"website"],[[arrayAllCompaniesPort valueForKey:@"international"] objectAtIndex:i],[[arrayAllCompaniesPort valueForKey:@"unread_message"] objectAtIndex:i]];
                                        
                                        [self saveImages:[NSString stringWithFormat:@"%@%@",[[arrayAllCompaniesPort  objectAtIndex:i] valueForKeyPath:@"id"],[[arrayAllCompaniesPort  objectAtIndex:i] valueForKeyPath:@"logo"]] imageUrl:[[arrayAllCompaniesPort  objectAtIndex:i] valueForKeyPath:@"logo"]];
                                        
                                        const char *query_stmt = [query UTF8String];
                                        [dummy  runQueries:query_stmt strForscreen:@"search"];
                                        
                                        [self saveDueDateSearchCompanies:i];
                                        [self saveCompanyLocations:i];
                                        [self saveCompanyDisciplines:i];
                                        [self saveSearchCompanyAwards:i];
                                        [self saveQuestionsSearchComp:i];
                                        [self saveSearchCompanyLinks:i];
                                        [self saveCompanySubDisciplines:i];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

-(void)saveSearchCompanyLinks:(int)index
{
    if ([[[arrayAllCompaniesPort valueForKey:@"links"] objectAtIndex:index]isKindOfClass:[NSNull class]])
    {
        
    }
    else
    {
        for (int k=0; k<[[[arrayAllCompaniesPort valueForKey:@"links"] objectAtIndex:index] count]; k++)
        {
            NSString *subQueryLinks;
            
            subQueryLinks = [NSString stringWithFormat: @"INSERT INTO SearchCompSocialLinksTable (company_id,id,media_link,media_logo,media_type) VALUES (\"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                             ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"links.company_id"] objectAtIndex:k]
                             ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"links.id"] objectAtIndex:k]
                             ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"links.media_link"] objectAtIndex:k]
                             ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"links.media_logo"] objectAtIndex:k]
                             ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"links.media_type"] objectAtIndex:k]];
            
            [self saveImages:[[NSString stringWithFormat:@"%@%@",[[[arrayAllCompaniesPort valueForKeyPath:@"links.id"] objectAtIndex:index] objectAtIndex:k],[[[arrayAllCompaniesPort valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                    imageUrl:[[[arrayAllCompaniesPort valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]];
            
            const char *query_stmt_SocialLinks = [subQueryLinks UTF8String];
            [dummy runQuerySocialLinksForCompanies:query_stmt_SocialLinks strForscreen:@"search"];
        }
    }
}

-(void)saveSearchCompanyAwards:(int)index
{
    for (int k=0; k<[[[arrayAllCompaniesPort objectAtIndex:index] valueForKey:@"allAwards"] count]; k++)
    {
        NSString *subQueryAwards;
        
        subQueryAwards = [NSString stringWithFormat: @"INSERT INTO SearchCompanyAwards (awards,company_id,description, id) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                          ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"allAwards.awards"] objectAtIndex:k]
                          ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"allAwards.company_id"] objectAtIndex:k]
                          ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"allAwards.description"] objectAtIndex:k]
                          ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"allAwards.id"] objectAtIndex:k]];
        
        const char *query_stmt_Awards= [subQueryAwards UTF8String];
        
        [dummy runSubQueryToSaveAwards:query_stmt_Awards strForscreen:@"SearchVw"];
    }
}

-(void)saveDueDateSearchCompanies:(int)index
{
    for (int j=0; j<[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all"] count]; j++)
    {
        NSString *subQueryDueDate;
        
        subQueryDueDate = [NSString stringWithFormat:@"INSERT INTO SearchCompanyDueDate(close_date,company_id,id,program,program_id,start_date,state,country) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.close_date"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.company_id"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.id"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.program"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.program_id"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.start_date"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.state"] objectAtIndex:j]
                           ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"application_due_date_all.country"] objectAtIndex:j]];
        
        const char *query_stmt_dueDate = [subQueryDueDate UTF8String];
        
        [dummy runSubQueryToSaveDueDate:query_stmt_dueDate strForscreen:@"Search"];
    }
}


-(void)saveCompanySubDisciplines:(int)index
{
    NSLog(@"index is %d", index);
    
    for (int k=0; k<[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines"] count]; k++)
    {
        NSString *subQuery;
        
        NSArray *arraySubDisciplineId = [[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline.sub_discipline_id"] objectAtIndex:k];
        
        for (int i=0; i<arraySubDisciplineId.count; i++)
        {
            subQuery = [NSString stringWithFormat: @"INSERT INTO DispSubDispDataTable (company_id,discipline_id,name,sub_discipline_id) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                        ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:k]
                        ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.discipline_id"] objectAtIndex:k]
                        ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.name"] objectAtIndex:k],[arraySubDisciplineId objectAtIndex:i]];
            
            const char *query_stmt_disciplines = [subQuery UTF8String];
            [dummy runQueryDispSubDispFordisp:query_stmt_disciplines strForscreen:@"searchCompanyDisciplines"];
        }
    }
}

-(void)saveCompanyDisciplines:(int)index
{
    for (int k=0; k<[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines"] count]; k++)
    {
        NSString *subQuery;
        
        subQuery = [NSString stringWithFormat: @"INSERT INTO DispSubDispCompaniesTable (company_id,discipline_id,name,type) VALUES (\"%@\",\"%@\", \"%@\",\"%@\")"
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:k]
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.discipline_id"] objectAtIndex:k]
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.name"] objectAtIndex:k],[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.type"] objectAtIndex:k]];
        
        const char *query_stmt_disciplines = [subQuery UTF8String];
        [dummy runQueryDispSubDispForCompanies:query_stmt_disciplines strForscreen:@"searchCompanyDisciplines"];
        [self saveSubDisciplinesPort1:index disciplineTag:k];
    }
}

-(void)saveSubDisciplinesPort1:(int)index disciplineTag:(int)disciplineTag
{
    for (int j=0; j<[[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag] count]; j++)
    {
        NSString *subQuery;
        
        subQuery = [NSString stringWithFormat: @"INSERT INTO SearchCompSubDisciplineTable (discipline_id,name,company_id,sub_discipline_id) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                    ,[[[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"discipline_id"] objectAtIndex:j]
                    ,[[[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"name"] objectAtIndex:j]
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:disciplineTag]
                    ,[[[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"sub_discipline_id"] objectAtIndex:j]
                    ];
        
        const char *query_stmt_PortSubDiscip = [subQuery UTF8String];
        
        [dummy runQuerySubDisciplines:query_stmt_PortSubDiscip strForscreen:@"SubDiscipSearchCompanies"];
    }
}

-(void)saveCompanyLocations:(int)index
{
    for (int k=0; k<[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location"] count]; k++)
    {
        NSString *subQuery = [NSString stringWithFormat: @"INSERT INTO COMPANYLOCATIONS (company_id,id,branch_name, lat,long,location) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\")"
                              ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location.company_id"] objectAtIndex:k]
                              ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location.id"] objectAtIndex:k]
                              ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location.branch_name"] objectAtIndex:k]
                              ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location.lat"] objectAtIndex:k]
                              ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location.long"] objectAtIndex:k]
                              ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"location.location"] objectAtIndex:k]];
        
        const char *query_stmtLocation = [subQuery UTF8String];
        [dummy runSubQueryLocations:query_stmtLocation strForscreen:@"COMPANYLOCATIONS"];
    }
}
-(void)saveQuestionsSearchComp:(int)index
{
    for (int k=0; k<[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"quesAns"] count]; k++)
    {
        NSString *subQuery;
        subQuery = [NSString stringWithFormat: @"INSERT INTO SearchQuesAnsTable (answer,company_id,id,question) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"quesAns.answer"] objectAtIndex:k]
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"quesAns.company_id"] objectAtIndex:k]
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"quesAns.id"] objectAtIndex:k]
                    ,[[[arrayAllCompaniesPort objectAtIndex:index] valueForKeyPath:@"quesAns.question"] objectAtIndex:k]];
        
        const char *query_stmt_QuesAns = [subQuery UTF8String];
        [dummy runQueryQuesAnsForCompanies:query_stmt_QuesAns strForscreen:@"search"];
    }
}

@end



