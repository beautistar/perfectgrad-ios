//
//  StudentDetailsViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 1/30/15.
//  Copyright (c) 2015 netset. All rights reserved.
//


#define kOFFSET_FOR_KEYBOARD 70.0
#import "Globals.h"
#import <UIKit/UIKit.h>

@interface StudentDetailsViewController : UIViewController<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIAlertViewDelegate>

@property (strong,nonatomic)NSString *isFromStr;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UITextField *stateNameTextField, *testingF;
@property (weak, nonatomic) IBOutlet UITextField *universityNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *positionNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *subDisciplineTextField;
@property (weak, nonatomic) IBOutlet UIButton *buttonGenderSelect;
@property (weak, nonatomic) IBOutlet UIButton *buttonStateSelect;
@property (weak, nonatomic) IBOutlet UIButton *buttonUniversitySelect;
@property (weak, nonatomic) IBOutlet UIButton *buttonPositionNameSelect;
@property (weak, nonatomic) IBOutlet UIButton *buttonDisciplineSelect;
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UITextField *genderTextField;
@property (strong, nonatomic) IBOutlet UITextField *textFieldDisciplinesName;
@property (strong, nonatomic) IBOutlet UIButton *buttonDisciplinesNameSelect;
@property (strong, nonatomic) IBOutlet UIButton *buttonSubmit;
@property (strong, nonatomic) IBOutlet UIImageView *imageVw;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewContainer;

@property (strong, nonatomic) IBOutlet UIView *viewTabBar;


@property (strong, nonatomic) IBOutlet UITextField *textFieldCountry;

@property (strong, nonatomic) IBOutlet UIButton *buttonCountry;
@property (strong, nonatomic) IBOutlet UIView *view_Picker;
@property (strong, nonatomic) IBOutlet UIButton *buttonCancel;

@property (strong, nonatomic) IBOutlet UIButton *btnDone;

@property (strong, nonatomic) IBOutlet UITextField *textFldDiscipline2;
@property (strong, nonatomic) IBOutlet UITextField *textFldSubdiscipline2;
@property (weak, nonatomic) IBOutlet UIButton *buttonDiscipline2Select;

@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;


- (IBAction)genderButtonAction:(id)sender;
- (IBAction)btnActionDone:(id)sender;
- (IBAction)btnActionCancel:(id)sender;

@end
