//
//  MessageSubscriptionManager.m
//  PerfectGraduate
//
//  Created by Mubin Lakadia on 3/3/17.
//  Copyright © 2017 netset. All rights reserved.
//

#import "MessageSubscriptionManager.h"
#import "AFHTTPRequestOperationManager.h"

@implementation MessageSubscriptionManager
+(id)sharedManager
{
    static MessageSubscriptionManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(id)init
{
    if (self = [super init])
    {
        [self refreshData];
    }
    return self;
}

-(void)refreshData
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userId"])
    {
        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
        NSString *strUserId=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
        
        NSDictionary *dicParam = @{@"uid":strUserId};
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.requestSerializer setTimeoutInterval:20];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [manager POST:@"block_user_message" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSLog(@"The Response:%@",responseObject);
             
             if ([[responseObject objectForKey:@"status"] boolValue])
             {
                 NSString *strCompanies=[NSString stringWithFormat:@"companies-%@",strUserId];
                 NSString *strSocieties=[NSString stringWithFormat:@"societies-%@",strUserId];

                 [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"data"] objectForKey:@"company"] forKey:strCompanies];
                 [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"data"] objectForKey:@"society"] forKey:strSocieties];
                 [[NSUserDefaults standardUserDefaults] synchronize];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             
         }];
    }
}
-(BOOL)isMessageSubscribedForId:(NSString *)companyId isCompany:(BOOL)isCompany
{
    BOOL isSubscribed=YES;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userId"])
    {
        NSString *strKey=[NSString stringWithFormat:@"%@-%@",(isCompany)?@"companies":@"societies",[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:strKey])
        {
            NSArray *arrCompanies=[NSArray arrayWithArray:[[defaults objectForKey:strKey] componentsSeparatedByString:@","]];
            for (int i=0; i<arrCompanies.count; i++)
            {
                if ([[arrCompanies objectAtIndex:i] integerValue]==[companyId integerValue]) {
                    isSubscribed=NO;
                }
            }
        }
        else
            
        {
            NSLog(@"no date found...");
        }
    }
    
    return isSubscribed;
}
-(void)updateSubscriptionMessagesForId:(NSString *)companyId isCompany:(BOOL)isCompany shouldSubscribe:(BOOL)shouldSubscribe
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userId"])
    {
        NSString *strKey=[NSString stringWithFormat:@"%@-%@",(isCompany)?@"companies":@"societies",[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:strKey])
        {
            NSMutableArray *arrCompanies=[[NSMutableArray alloc]initWithArray:[[defaults objectForKey:strKey] componentsSeparatedByString:@","]];
            NSInteger foundAtIndex=0;
            BOOL isFound=NO;

            for (int i=0; i<arrCompanies.count; i++)
            {
                if ([[arrCompanies objectAtIndex:i] integerValue]==[companyId integerValue])
                {
                    isFound=YES;
                    foundAtIndex=i;
                }
            }
            
            if (shouldSubscribe && isFound) {
                [arrCompanies removeObjectAtIndex:foundAtIndex];
                [defaults setObject:[arrCompanies componentsJoinedByString:@","] forKey:strKey];
                [defaults synchronize];
            }
            else if(!shouldSubscribe && !isFound)
            {
                [arrCompanies addObject:companyId];
                [defaults setObject:[arrCompanies componentsJoinedByString:@","] forKey:strKey];
                [defaults synchronize];
            }
        }
        else if(!shouldSubscribe)
        {
            NSMutableArray *arrCompanies=[[NSMutableArray alloc]init];
            [arrCompanies addObject:companyId];
            [defaults setObject:[arrCompanies componentsJoinedByString:@","] forKey:strKey];
            [defaults synchronize];
        }
    }
}

@end
