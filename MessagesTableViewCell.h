//
//  MessagesTableViewCell.h
//  PerfectGraduate
//
//  Created by netset on 4/2/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIKit+AFNetworking.h"

@interface MessagesTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelMsgDate;

@property (weak, nonatomic) IBOutlet UIImageView *imageVwLogoMsgs;
@property (weak, nonatomic) IBOutlet UIImageView *imageVwLogoEvents;
@property (strong, nonatomic) IBOutlet UIView *viewLogoEvents;
@property (weak, nonatomic) IBOutlet UIImageView *imageVwEventBack;
@property (strong, nonatomic) IBOutlet UIView *viewLogoMsgs;

@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelEventName;
@property (weak, nonatomic) IBOutlet UILabel *labelEventCompanyName;
@property (weak, nonatomic) IBOutlet UILabel *labelEventDate;
@property (weak, nonatomic) IBOutlet UILabel *labelNameMsgs;
@property (strong, nonatomic) IBOutlet UIView *viewEvents;
@property (weak, nonatomic) IBOutlet UIView *viewMessages;
@property (strong, nonatomic) IBOutlet UILabel *labelMsgDetail;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureEvents;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureMsgs;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewMsgBg;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewEventReadStatus;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewMessageReadStatus;
@property(strong,nonatomic)IBOutlet UIButton *btnMsg;
@property(strong,nonatomic)IBOutlet UIButton *btnEvent;

@end
