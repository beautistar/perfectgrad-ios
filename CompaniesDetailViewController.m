//
//  CompaniesDetailViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 2/19/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "CompaniesDetailViewController.h"
#import "MessagesViewController.h"
#import "SalaryGuideViewController.h"
#import "LocationFilterSearchViewController.h"
#import "DisciplineViewController.h"
#import "PortfolioViewController.h"
#import "Database.h"
#import "Globals.h"
#import "MessageSubscriptionManager.h"

@interface CompaniesDetailViewController ()
{
    NSMutableDictionary *sectionContentDict;
    UIButton * buttonSelect;
    NSMutableArray *arrayTitleName;
    NSDictionary *dicAppDates;
    UIActivityIndicatorView*spinner;
    NSArray *arrayRegState;
    NSMutableArray *arrAreasOfOp;
    Database *dummy;
    UITableView *tableAreaOfOperation1;
    UIView* backView;
    int xValue;
    NSArray * arrayAllCompanies;
    BOOL isRefreshed, isFromDetailApi;
    UIView *viewBackTabel;
    NSMutableString *strStateIds;
}

@end

@implementation CompaniesDetailViewController

-(void)loadMapView1
{
    CLLocationCoordinate2D coord;
    
    
    [USERDEFAULT synchronize];
    
    coord.latitude = [PREF(LATITUDE_COUNTRY)floatValue];
    
    coord.longitude = [PREF(LONGITUDE_COUNTRY)floatValue];
    
    GMSCameraPosition *position = [GMSCameraPosition cameraWithLatitude:coord.latitude
                                                              longitude:coord.longitude
                                                                   zoom:2.5];
    _mapViewGoogle.camera = position;
    
    _mapViewGoogle.mapType = kGMSTypeNormal;
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(coord.latitude, coord.longitude);
    
    if ([[_dicCompanyDetail valueForKeyPath:@"location"] count]==0)
    {
        
    }
    else
    {
        marker.title = [NSString stringWithFormat:@"%@",[[_dicCompanyDetail valueForKeyPath:@"location.location"]objectAtIndex:0]];
        for (int i=0; i<[[_dicCompanyDetail valueForKeyPath:@"location.lat"] count]; i++)
        {
            CLLocationCoordinate2D coord;
            
            coord.latitude = [[NSString stringWithFormat:@"%@",[[_dicCompanyDetail valueForKeyPath:@"location.lat"] objectAtIndex:i]] floatValue];
            
            coord.longitude = [[NSString stringWithFormat:@"%@",
                                [[_dicCompanyDetail valueForKeyPath:@"location.long"] objectAtIndex:i]] floatValue];
            
            _mapViewGoogle.mapType = kGMSTypeNormal;
            
            // Creates a marker in the center of the map.
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(coord.latitude, coord.longitude);
            marker.title = [NSString stringWithFormat:@"%@",[[_dicCompanyDetail valueForKeyPath:@"location.location"]objectAtIndex:i]];
            marker.appearAnimation = kGMSMarkerAnimationPop;
            
            marker.map = _mapViewGoogle;
        }
        
    }
}

- (void)loadMapView
{
    
    CLLocationCoordinate2D coord;
    
    
    [USERDEFAULT synchronize];
    
    coord.latitude = [PREF(LATITUDE_COUNTRY)floatValue];
    
    coord.longitude = [PREF(LONGITUDE_COUNTRY)floatValue];
    
    GMSCameraPosition *position = [GMSCameraPosition cameraWithLatitude:coord.latitude
                                                              longitude:coord.longitude
                                                                   zoom:2.5];
    _mapViewGoogle.camera = position;
    
    _mapViewGoogle.mapType = kGMSTypeNormal;
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    
    marker.position = CLLocationCoordinate2DMake(coord.latitude, coord.longitude);
    
    if ([[_dicCompanyDetail valueForKeyPath:@"location"] count]==0)
    {
        
    }
    else
    {
        marker.title = [NSString stringWithFormat:@"%@",[[_dicCompanyDetail valueForKeyPath:@"location.location"]objectAtIndex:0]];
        
        for (int i=0; i<[[_dicCompanyDetail valueForKeyPath:@"location.lat"] count]; i++)
        {
            CLLocationCoordinate2D coord;
            
            coord.latitude = [[NSString stringWithFormat:@"%@",[[_dicCompanyDetail valueForKeyPath:@"location.lat"] objectAtIndex:i]] floatValue];
            
            coord.longitude = [[NSString stringWithFormat:@"%@",
                                [[_dicCompanyDetail valueForKeyPath:@"location.long"] objectAtIndex:i]] floatValue];
            
            _mapViewGoogle.mapType = kGMSTypeNormal;
            
            // Creates a marker in the center of the map.
            GMSMarker *marker = [[GMSMarker alloc] init];
            
            marker.position = CLLocationCoordinate2DMake(coord.latitude, coord.longitude);
            
            marker.title = [NSString stringWithFormat:@"%@",[[_dicCompanyDetail valueForKeyPath:@"location.location"]objectAtIndex:i]];
        }
        marker.appearAnimation = kGMSMarkerAnimationPop;
        
        marker.map = _mapViewGoogle;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    strStateIds=[[NSMutableString alloc]init];
    
    if (_dicCompanyDetail) {
        if ([_dicCompanyDetail valueForKey:@"opration_state"])
        {
            [strStateIds setString:[NSString stringWithFormat:@"%@",[_dicCompanyDetail valueForKey:@"opration_state"]]];
        }
    }
    
    
    stringMesagFirstCome = @"stringFirst";
    
    // Do any additional setup after loading the view.
    
    dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
    
    int index;
    
    if ([_strVwChane isEqualToString:@"fromMessage"])
    {
        if ([arrayId containsObject:[_dicCompanyDetail valueForKey:@"adder_id"]])
        {
            index = (int)[arrayId indexOfObject:[_dicCompanyDetail valueForKey:@"adder_id"]] ;
            
            _dicCompanyDetail = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
        }
    }
    else if([_strVwChane isEqualToString:@"portfolioVw"]){
        
    }
    else
    {
        if ([arrayId containsObject:[_dicCompanyDetail valueForKey:@"id"]])
        {
            index = (int)[arrayId indexOfObject:[_dicCompanyDetail valueForKey:@"id"]];
            
            _dicCompanyDetail = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
        }
    }
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, _imageRedStar.frame.origin.y, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    
    self.imageRedStar.hidden = YES;
    
    
    _mapViewGoogle.layer.borderColor = [UIColor colorWithRed:171.0f/255.0f green:171.0f/255.0f blue:171.0f/255.0f alpha:0.5f].CGColor;
    
    _mapViewGoogle.layer.borderWidth =1.0;
    
    _labelOurLocation.layer.borderColor = [UIColor colorWithRed:171.0f/255.0f green:171.0f/255.0f blue:171.0f/255.0f alpha:0.5f].CGColor;
    
    _labelOurLocation.layer.borderWidth =1.0;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    refreshControl = [[UIRefreshControl alloc] init];
    
    
    [refreshControl addTarget:self action:@selector(comanyRefresh) forControlEvents:UIControlEventValueChanged];
    
    [_scrollViewOutlet addSubview:refreshControl];
    
    
    arrayTitleName = [[NSMutableArray alloc] init];
    
    self.viewBottomBar.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationItem.hidesBackButton = YES;
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
    
    self.navigationItem.leftBarButtonItem = leftItem;
    
    buttonSelect = [[UIButton alloc]init ];
    buttonSelect = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSelect.frame =CGRectMake(30 , 5, 34, 30);
    buttonSelect.backgroundColor = [UIColor clearColor];
    
    [buttonSelect addTarget:self action:@selector(rightItemAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:buttonSelect];
    
    self.scrollViewOutlet.backgroundColor = [UIColor colorWithRed:234.0f/255.0f green:234.0f/255.0f blue:234.0f/255.0f alpha:1.0];
    
    self.navigationController.navigationBar.hidden = NO;
    
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _labelOurLocation.layer.borderColor = [UIColor colorWithRed:171.0f/255.0f green:171.0f/255.0f blue:171.0f/255.0f alpha:0.5f].CGColor;
    _labelOurLocation.layer.borderWidth =1.0;
    
    _mapView.layer.borderColor = [UIColor colorWithRed:171.0f/255.0f green:171.0f/255.0f blue:171.0f/255.0f alpha:0.5f].CGColor;
    _mapView.layer.borderWidth =1.0;
    
    _buttonAreaOfOprtn.layer.borderColor = [UIColor colorWithRed:171.0f/255.0f green:171.0f/255.0f blue:171.0f/255.0f alpha:0.5f].CGColor;
    _buttonAreaOfOprtn.layer.borderWidth =1.0;
    
    _labelMoreinformation.layer.borderColor = [[UIColor colorWithRed:233.0f/255.0f green:233.0f/255.0f blue:233.0f/255.0f alpha:1.0f]CGColor];
    _labelMoreinformation.layer.borderWidth =1.0;
    
    /*----Facebook login button & Linkdin Login Border Layout---*/
    _labelfblink.layer.borderColor = [[UIColor colorWithRed:233.0f/255.0f green:233.0f/255.0f blue:233.0f/255.0f alpha:1.0f]CGColor];
    _labelfblink.layer.borderWidth =1.0;
    
    self.navigationController.navigationBar.hidden = NO;
    
    _textViewCompanyDescrip.editable = NO;
    
    if([_strVwChane isEqualToString:@"seachViewString"] || [_strVwChane isEqualToString:@"CompaniesVw"] || [_strVwChane isEqualToString:@"fromMessage"]){
        NSArray *arrCompany = [dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"];
        NSString *strID2 = [NSString stringWithFormat:@"%@",[self.dicCompanyDetail objectForKey:@"id"]];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@",strID2];
        NSArray *arrData = [arrCompany filteredArrayUsingPredicate:predicate];
        if (arrData.count>0) {
            self.dicCompanyDetail = [[NSMutableDictionary alloc]initWithDictionary:arrData[0]];
        }
    }
    
    if(![Utilities CheckInternetConnection])
    {
        isInternetOff = YES;
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"callallCompanyWebService"];
        
        if ([_strVwChane isEqualToString:@"seachViewString"])
        {
            [_buttonSearch setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
            
            _buttonSearch.backgroundColor = [UIColor clearColor];
            
            _labelSearchTitle.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
            
            if(![Utilities CheckInternetConnection])
            {
                isInternetOff = YES;
                [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"id"] integerValue]];
            }
            else
            {
                [self createDictionary];
                [self PlaceData:_dicCompanyDetail];
            }
            
            [self.tableViewCompany  reloadData];
        }
        
        else if ([_strVwChane isEqualToString:@"portfolioVw"])
        {
            _buttonPortfolio.backgroundColor = [UIColor clearColor];
            [_buttonPortfolio setImage:[UIImage imageNamed:@"portfolio1"] forState:UIControlStateNormal];
            _labelPortfolio.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
            
            if(![Utilities CheckInternetConnection])
            {
                [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"id"] integerValue]];
            }
            else
            {
                [self createDictionary];
                [self PlaceData:_dicCompanyDetail];
            }
            
            [self.tableViewCompany  reloadData];
        }
        
        else if ([_strVwChane isEqualToString:@"CompaniesVw"])
        {
            _buttonDiscipline.backgroundColor = [UIColor clearColor];
            
            [_buttonDiscipline setImage:[UIImage imageNamed:@"sisciplines"] forState:UIControlStateNormal];
            
            _labelDiscipline.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
            
            if(![Utilities CheckInternetConnection])
            {
                [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"id"] integerValue]];
            }
            else
            {
                [self createDictionary];
                [self PlaceData:_dicCompanyDetail];
            }
            [self.tableViewCompany  reloadData];
        }
        
        else if ([_strVwChane isEqualToString:@"fromMessage"])
        {
            _buttonMessages.backgroundColor = [UIColor clearColor];
            
            [_buttonMessages setImage:[UIImage imageNamed:@"messagesSelect"] forState:UIControlStateNormal];
            
            _labelmessag.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
            
            [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"adder_id"] integerValue]];
        }
        
        else
        {
            [self createDictionary];
            [self PlaceData:_dicCompanyDetail];
        }
        
        [self createDictionary];
    }
    
    else
    {
        isInternetOff = NO;
        
        [self createDictionary];
        
        showInternetAlert = NO;
        
        if ([_strVwChane isEqualToString:@"seachViewString"])
        {
            [_buttonSearch setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
            
            _buttonSearch.backgroundColor = [UIColor clearColor];
            
            _labelSearchTitle.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
            
            if(![Utilities CheckInternetConnection])
            {
                isInternetOff = YES;
                [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"id"] integerValue]];
            }
            else
            {
                [self createDictionary];
                [self PlaceData:_dicCompanyDetail];
            }
            
            [self.tableViewCompany  reloadData];
        }
        
        else if ([_strVwChane isEqualToString:@"portfolioVw"])
        {
            _buttonPortfolio.backgroundColor = [UIColor clearColor];
            [_buttonPortfolio setImage:[UIImage imageNamed:@"portfolio1"] forState:UIControlStateNormal];
            _labelPortfolio.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
            
            if(![Utilities CheckInternetConnection])
            {
                [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"id"] integerValue]];
            }
            else
            {
                [self createDictionary];
                [self PlaceData:_dicCompanyDetail];
            }
            
            [self.tableViewCompany  reloadData];
        }
        
        else if ([_strVwChane isEqualToString:@"CompaniesVw"])
        {
            _buttonDiscipline.backgroundColor = [UIColor clearColor];
            
            [_buttonDiscipline setImage:[UIImage imageNamed:@"sisciplines"] forState:UIControlStateNormal];
            
            _labelDiscipline.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
            
            if(![Utilities CheckInternetConnection])
            {
                [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"id"] integerValue]];
            }
            else
            {
                [self createDictionary];
                [self PlaceData:_dicCompanyDetail];
            }
            [self.tableViewCompany  reloadData];
        }
        
        else if ([_strVwChane isEqualToString:@"fromMessage"])
        {
            _buttonMessages.backgroundColor = [UIColor clearColor];
            
            [_buttonMessages setImage:[UIImage imageNamed:@"messagesSelect"] forState:UIControlStateNormal];
            
            _labelmessag.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
            if (arrayCompaniesList.count ==0)
            {
                [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"adder_id"] integerValue]];
            }
            else
            {
                [self PlaceData:_dicCompanyDetail];
                
                [self.tableViewCompany  reloadData];
            }
        }
        else
        {
            [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"id"] integerValue]];
        }
    }
    
    
    [self loadMapView1];
}


-(void)comanyRefresh
{
    xValue = 90;
    
    if(![Utilities CheckInternetConnection])
    {
        isInternetOff = YES;
        [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"id"] integerValue]];
    }
    else
    {
        if ([[_dicCompanyDetail valueForKeyPath:@"location"] count]==0)
        {
            if ([refreshControl isRefreshing])
                
            {
                [refreshControl endRefreshing];
            }
        }
        else
        {
            NSString *str =[NSString stringWithFormat:@"%@", [_dicCompanyDetail valueForKeyPath:@"id"]];
            
            NSInteger intId = [str integerValue];
            
            [self ShowDataLoader:YES];
            
            [self companyDetailService:intId];}
    }
    
    
}


-(void)refresh
{
    isRefreshed = YES;
    
    NSInteger integerId = [[_dicCompanyDetail valueForKey:@"adder_id"] integerValue];
    
    [self companyDetailService:integerId];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    stringNotifiction = @"";
    
    stringForNotifyEditCompany = @"companyDetail";
    
    if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"] )
    {
        self.imageRedStar.hidden = YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    
    [super viewWillAppear:YES];
    
    _lblTop.backgroundColor = [UIColor clearColor];
    
    _imageViewLogo.layer.borderColor = [[UIColor clearColor] CGColor];
    
    xValue = 90;
    _viewTitle.layer.borderColor = [UIColor colorWithRed:171.0f/255.0f green:171.0f/255.0f blue:171.0f/255.0f alpha:0.5f].CGColor;
    _viewTitle.layer.borderWidth =1.0;
    _textViewCompanyDescrip.layer.borderColor = [UIColor colorWithRed:171.0f/255.0f green:171.0f/255.0f blue:171.0f/255.0f alpha:0.5f].CGColor;
    _textViewCompanyDescrip.clipsToBounds=YES;
    _textViewCompanyDescrip.layer.borderWidth = 1.0;
    _textViewCompanyDescrip.layer.shadowOpacity = 1.0;
    _textViewCompanyDescrip.layer.shadowRadius = 0.5;
    _textViewCompanyDescrip.layer.shadowColor = [UIColor redColor].CGColor;
    _textViewCompanyDescrip.layer.shadowOffset = CGSizeMake(4, -4);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(companyRefresDetailData)
                                                 name:@"redStarShow"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(redStarShow)
                                                 name:@"companyRefresDetail"
                                               object:nil];
    
    _imageViewLogo.layer.cornerRadius = 5.0f;
    _imageViewLogo.clipsToBounds = YES;
    _imageViewLogo.layer.borderWidth = 2.0f;
    _imageViewLogo.layer.borderColor = [[UIColor whiteColor] CGColor];
    
}

-(void)companyRefresDetailData
{
    [self getAllCompanies];
}

-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

-(void)setBorderColorToImage:(UIColor *)color
{
    buttonSelect.layer.borderColor = [color CGColor];
    buttonSelect.layer.borderWidth = 1.0f;
    buttonSelect.layer.cornerRadius = 5.0f;
    buttonSelect.clipsToBounds = YES;
    
}

-(void)createDictionary
{
    arrAreasOfOp = [[NSMutableArray alloc]init];
    
    if ([[_dicCompanyDetail valueForKeyPath:@"portfolio_status"] isEqualToString:@"1"]||![_dicCompanyDetail valueForKeyPath:@"portfolio_status"])
    {
        [buttonSelect setBackgroundColor:[UIColor whiteColor]];
        
        [buttonSelect setImage:[UIImage imageNamed:@"addP"] forState:UIControlStateNormal];
        
        [self setBorderColorToImage:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]];
    }
    
    else
    {
        [buttonSelect setImage:[UIImage imageNamed:@"portRemoved"] forState:UIControlStateNormal];
        
        
        [self setBorderColorToImage:[UIColor whiteColor]];
        
        [buttonSelect setBackgroundColor:[UIColor clearColor]];
        
    }
    
    /*------------Initialize Company Name in  Name Array---------------- */
    
    [arrayTitleName removeAllObjects];
    
    NSMutableArray *arrayAwardSort = [[NSMutableArray alloc]init];
    
    
    
    
    NSArray *array = [self.dicCompanyDetail valueForKey:@"allAwards"];
    
    for (int i=0; i<array.count; i++)
    {
        if ([[[array valueForKey:@"awards"]objectAtIndex:i]isEqualToString:@"2"])
        {
            if(![arrayAwardSort containsObject:[array objectAtIndex:i]]){
                [arrayAwardSort addObject:[array objectAtIndex:i]];
            }
        }
    }
    
    for (int i=0; i<array.count; i++)
    {
        if ([[[array valueForKey:@"awards"]objectAtIndex:i]isEqualToString:@"3"])
        {
            if(![arrayAwardSort containsObject:[array objectAtIndex:i]]){
                [arrayAwardSort addObject:[array objectAtIndex:i]];
            }
        }
    }
    
    for (int i=0; i<array.count; i++)
    {
        if ([[[array valueForKey:@"awards"]objectAtIndex:i]isEqualToString:@"1"])
        {
            if(![arrayAwardSort containsObject:[array objectAtIndex:i]]){
                [arrayAwardSort addObject:[array objectAtIndex:i]];
            }
        }
    }
    [self.dicCompanyDetail removeObjectForKey:@"allAwards"];
    
    [self.dicCompanyDetail setObject:arrayAwardSort forKey:@"allAwards"];
    
    NSArray * arrDis = [_dicCompanyDetail valueForKey:@"disciplines"];
    NSMutableArray * arrDispline = [[NSMutableArray alloc]init];
    
    for(NSDictionary * dict in arrDis){
        if(![arrDispline containsObject:dict]){
            [arrDispline addObject:dict];
        }
    }
    [_dicCompanyDetail setObject:arrDispline forKey:@"disciplines"];
    
    NSArray * arrDue = [_dicCompanyDetail valueForKey:@"application_due_date_all"];
    NSMutableArray * arrDueDate = [[NSMutableArray alloc]init];
    for(NSDictionary * dict in arrDue){
        if(![arrDueDate containsObject:dict]){
            [arrDueDate addObject:dict];
        }
    }
    [_dicCompanyDetail setObject:arrDueDate forKey:@"application_due_date_all"];
    
    NSArray * arrque = [_dicCompanyDetail valueForKey:@"quesAns"];
    NSMutableArray * arrAns = [[NSMutableArray alloc]init];
    for(NSDictionary * dict in arrque){
        if(![arrAns containsObject:dict]){
            [arrAns addObject:dict];
        }
    }
    [_dicCompanyDetail setObject:arrAns forKey:@"quesAns"];
    
    NSArray * arrlink = [_dicCompanyDetail valueForKey:@"links"];
    NSMutableArray * arrlinks = [[NSMutableArray alloc]init];
    for(NSDictionary * dict in arrlink){
        if(![arrlinks containsObject:dict]){
            [arrlinks addObject:dict];
        }
    }
    [_dicCompanyDetail setObject:arrlinks forKey:@"links"];
    
    dicAppDates = [[NSDictionary alloc] initWithObjectsAndKeys:
                   [_dicCompanyDetail valueForKey:@"application_due_date_all"],@"Application Due Dates"
                   ,[_dicCompanyDetail valueForKey:@"disciplines"],@"Target Disciplines"
                   ,[_dicCompanyDetail valueForKey:@"allAwards"],@"Awards"
                   ,[_dicCompanyDetail valueForKey:@"quesAns"],@"Frequently Asked Questions"
                   ,[_dicCompanyDetail valueForKey:@"links"],@"More Information", nil];
    
    if ([[_dicCompanyDetail valueForKey:@"application_due_date_all"] count]>0)
    {
        [arrayTitleName addObject:@"Application Due Dates"];
    }
    
    if ([[_dicCompanyDetail valueForKey:@"disciplines"] count]>0)
    {
        [arrayTitleName addObject:@"Target Disciplines"];
    }
    
    if ([[_dicCompanyDetail valueForKey:@"allAwards"] count]>0)
    {
        [arrayTitleName addObject:@"Awards"];
    }
    
    if ([[_dicCompanyDetail valueForKey:@"quesAns"] count]>0)
    {
        [arrayTitleName addObject:@"Frequently Asked Questions"];
    }
    
    if ([[_dicCompanyDetail valueForKey:@"links"] count]>0)
    {
        [arrayTitleName addObject:@"More Information"];
    }
    
    if ([[_dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"<null>"]||[[_dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@""]||[[_dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"(null)"])
    {
        
    }
    
    else
    {
        arrayRegState = [[_dicCompanyDetail valueForKey:@"regstates"] componentsSeparatedByString:@","];
        
        arrayRegState = [[NSOrderedSet orderedSetWithArray:arrayRegState] array];
    }
    
    BOOL shouldLoadFromDB=YES;
    if (arrayRegState)
    {
        if (arrayRegState.count>0) {
            shouldLoadFromDB=NO;
        }
    }
    
    dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    NSArray *arrst = [dummy loaddataStates:[NSString stringWithFormat:@"SELECT * FROM STATES WHERE id IN (%@)",strStateIds]];
    NSMutableArray *arrC=[[NSMutableArray alloc]init];
    for (int i=0; i<arrst.count; i++) {
        [arrC addObject:[NSString stringWithFormat:@"%@",arrst[i][@"name"]]];
    }
    
    if (shouldLoadFromDB && arrC.count>0) {
       
        
        arrayRegState = [[arrC componentsJoinedByString:@","] componentsSeparatedByString:@","];
        
        arrayRegState = [[NSOrderedSet orderedSetWithArray:arrayRegState] array];

    }
    else
    {
        NSLog(@"restricted fill");
    }
    
    
    NSLog(@"the stdata:%@ and %@",arrC,arrayRegState);

    
//
//    arrayRegState = [arrC copy];
//    
    
    for (int i = 0; i<arrayRegState.count; i++)
    {
//        if ([[_dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"<null>"]||[[_dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@""]||[[_dicCompanyDetail valueForKey:@"regstates"]isEqualToString:@"(null)"])
//        {
//            
//        }
//        
//        else
        
        {
            
            NSDictionary *dictR = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",arrayRegState[i]],@"RegState", nil];
            
            [arrAreasOfOp addObject:dictR];
            
        }
    }
    
    if (isFromDetailApi== YES)
    {
        //if ([[_dicCompanyDetail valueForKey:@"international"]isEqualToString:@"<null>"]||[[_dicCompanyDetail valueForKey:@"international"]isEqualToString:@""])
        BOOL shouldIgnore=YES;
        if ([_dicCompanyDetail objectForKey:@"international"]) {
            if ([[[NSString stringWithFormat:@"%@",[_dicCompanyDetail objectForKey:@"international"]] removeNull] length]>0)
            {
                shouldIgnore=NO;
            }
        }
        
        if (shouldIgnore)
        {
            NSLog(@"ignored...");
        }
        else
        {
            NSArray *arrayInt = [[_dicCompanyDetail valueForKey:@"international"] componentsSeparatedByString:@","];
            
            arrayInt = [[NSOrderedSet orderedSetWithArray:arrayInt] array];
            
            for (int i = 0; i<arrayInt.count; i++)
            {
                NSDictionary *dictR = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",arrayInt[i]],@"International", nil];
                [arrAreasOfOp addObject:dictR];
            }
        }
    }
    
    if ([[_dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@"<null>"]||[[_dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@""]||[[_dicCompanyDetail valueForKey:@"internationl"]isEqualToString:@"(null)"])
    {
        
    }
    
    else
    {
        
        NSArray *arrayInt = [[_dicCompanyDetail valueForKey:@"internationl"] componentsSeparatedByString:@","];
        
        arrayInt = [[NSOrderedSet orderedSetWithArray:arrayInt] array];
        
        for (int i = 0; i<arrayInt.count; i++)
        {
            
            NSDictionary *dictR = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",arrayInt[i]],@"International", nil];
            
            [arrAreasOfOp addObject:dictR];
        }
        
    }
    
    NSLog(@"the dataarp:%@",arrAreasOfOp);
    
    [self.tableViewCompany  reloadData];
}

-(void)backBtnAction
{
    stringBackScreen = @"companyProfile";
    
    [self.navigationController popViewControllerAnimated:NO];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
    
    else  // use whatever annotation class you used when creating the annotation
    {
        MKAnnotationView* annotationView = [[MKAnnotationView alloc] init];
        annotationView.annotation = annotation;
        annotationView.canShowCallout = YES;
        annotationView.image = [UIImage imageNamed:@"annotation"];
        return annotationView;
    }
    return nil;
}

-(void)webServiceAddPort
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"cid":[_dicCompanyDetail valueForKey:@"id"]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"addportfolio" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         checkCopmanyApiCall = NO;
         
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
             [dummy updateTable:[NSString stringWithFormat:@"UPDATE COMPANIES SET portfolio_status='1' WHERE id = '%@'",[_dicCompanyDetail objectForKey:@"id"]]];
             
             
             [[[UIAlertView alloc] initWithTitle:@"Nice choice!" message:@"\nThe company has been successfully added to your portfolio." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
             [buttonSelect setBackgroundColor:[UIColor whiteColor]];
             
             [buttonSelect setImage:[UIImage imageNamed:@"addP"] forState:UIControlStateNormal];
             
             [self setBorderColorToImage:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]];
             
             [self  changeDataInPortTable: @"1"];
             
             NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
             
             int index = (int) [arrayId indexOfObject:[_dicCompanyDetail valueForKey:@"id"]];
             
             NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
             
             [dictChnage removeObjectForKey:@"portfolio_status"];
             
             [dictChnage setObject:@"1" forKey:@"portfolio_status"];
             
             [arrayCompaniesList replaceObjectAtIndex:index withObject:dictChnage];
             
             NSInteger points = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"] integerValue];
             
             points = points + 50;
             
             [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld", (long)points] forKey:@"userPoints"];
             
             int compCount = [[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"companyCount"]] intValue];
             int unread = [[[NSString stringWithFormat:@"%@",[_dicCompanyDetail objectForKey:@"unread_message"]] removeNull] intValue];
             int TotalCount = compCount+unread;
             
             [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",TotalCount] forKey:@"companyCount"];
             [[NSUserDefaults standardUserDefaults] synchronize];
         }
         
         else if ([[responseObject objectForKey:@"message"] isEqualToString:@"Already added"])
             
         {
             dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
             [dummy updateTable:[NSString stringWithFormat:@"UPDATE COMPANIES SET portfolio_status='1' WHERE id = '%@'",[_dicCompanyDetail objectForKey:@"id"]]];

             [[[UIAlertView alloc] initWithTitle:@"Nice choice!" message:@"\nThe company has been successfully added to your portfolio." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
             
             
             [buttonSelect setBackgroundColor:[UIColor whiteColor]];
             
             [buttonSelect setImage:[UIImage imageNamed:@"addP"] forState:UIControlStateNormal];
             
             [self setBorderColorToImage:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]];
             
             [self  changeDataInPortTable: @"1"];
             
             NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
             
             int index = (int) [arrayId indexOfObject:[_dicCompanyDetail valueForKey:@"id"]];
             
             NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
             
             [dictChnage removeObjectForKey:@"portfolio_status"];
             
             [dictChnage setObject:@"1" forKey:@"portfolio_status"];
             
             [arrayCompaniesList replaceObjectAtIndex:index withObject:dictChnage];
             
             NSInteger points = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"] integerValue];
             
             points = points + 50;
             
             [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld", (long)points] forKey:@"userPoints"];
             
         }
         
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             //[[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"No Internet" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             NSString *str = error.localizedDescription;
             
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:[NSString stringWithFormat:@"\n%@",str] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}


-(void)changeDataInPortTable:(NSString *)stringValue
{
    stringPortCompanyScreen = @"fromComapny";
    
    if ([stringValue isEqualToString:@"0"])
    {
        NSArray *arrayId= [arrayPortDataGlobal valueForKey:@"id"];
        
        if ([arrayId containsObject:[_dicCompanyDetail valueForKey:@"id"]])
        {
            int index = (int)[arrayId indexOfObject:[_dicCompanyDetail valueForKey:@"id"]];
            
            NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayPortDataGlobal[index] copyItems:YES];
            
            [dictChnage removeObjectForKey:@"portfolio_status"];
            
            [dictChnage setObject:stringValue forKey:@"portfolio_status"];
            
            [arrayPortDataGlobal removeObjectAtIndex:index];
        }
    }
    else
    {
        NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
        
        int index = (int)[arrayId indexOfObject:[_dicCompanyDetail valueForKey:@"id"]];
        
        if ((arrayCompaniesList.count-1)>=index) {
            NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
            
            [dictChnage removeObjectForKey:@"portfolio_status"];
            
            [dictChnage setObject:@"1" forKey:@"portfolio_status"];
            
            [arrayPortDataGlobal addObject:dictChnage];
        }
    }
}


-(void)changeDataInPortStatus:(NSString *)stringValue
{
    stringPortCompanyScreen = @"fromComapny";
    if ([stringValue isEqualToString:@"1"])
    {
        NSArray *arrayId = [arrayPortDataGlobal valueForKey:@"id"];
        
        if ([arrayId containsObject:[_dicCompanyDetail valueForKey:@"id"]])
        {
            
            int index = (int)[arrayId indexOfObject:[_dicCompanyDetail valueForKey:@"id"]];
            
            NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayPortDataGlobal[index] copyItems:YES];
            
            [dictChnage removeObjectForKey:@"message_status"];
            
            [dictChnage setValue:@"1" forKey:@"message_status"];
            //msg007
            [[MessageSubscriptionManager sharedManager] updateSubscriptionMessagesForId:[NSString stringWithFormat:@"%@",[_dicCompanyDetail valueForKey:@"id"]] isCompany:YES shouldSubscribe:NO];
            
            [arrayPortDataGlobal replaceObjectAtIndex:index withObject:dictChnage];
        }
    }
    
    else
    {
        NSArray *arrayId= [arrayPortDataGlobal valueForKey:@"id"];
        
        if ([arrayId containsObject:[_dicCompanyDetail valueForKey:@"id"]])
        {
            
            int index = (int)[arrayId indexOfObject:[_dicCompanyDetail valueForKey:@"id"]];
            
            NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayPortDataGlobal[index] copyItems:YES];
            
            [dictChnage removeObjectForKey:@"message_status"];
            
            [dictChnage setValue:@"0" forKey:@"message_status"];
            //msg007
            [[MessageSubscriptionManager sharedManager] updateSubscriptionMessagesForId:[NSString stringWithFormat:@"%@",[_dicCompanyDetail valueForKey:@"id"]] isCompany:YES shouldSubscribe:YES];

            [arrayPortDataGlobal replaceObjectAtIndex:index withObject:dictChnage];
            
        }
    }
}

-(void)webServiceRemovePort
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"cid":[_dicCompanyDetail valueForKey:@"id"]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"removeCompany" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         checkCopmanyApiCall = NO;
         [self ShowDataLoader:NO];

         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
             [dummy updateTable:[NSString stringWithFormat:@"UPDATE COMPANIES SET portfolio_status='0' WHERE id = '%@'",[_dicCompanyDetail objectForKey:@"id"]]];

             [buttonSelect setImage:[UIImage imageNamed:@"portRemoved"] forState:UIControlStateNormal];
             
             [self setBorderColorToImage:[UIColor whiteColor]];
             
             [buttonSelect setBackgroundColor:[UIColor clearColor]];
             
             [[[UIAlertView alloc] initWithTitle:@"Let’s remove it…" message:@"\nThe company has now been removed from your portfolio." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil, nil] show];
             
             [self  changeDataInPortTable: @"0"];
             
             NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
             
             int index = (int)[arrayId indexOfObject:[_dicCompanyDetail valueForKey:@"id"]];
             
             NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
             
             [dictChnage removeObjectForKey:@"portfolio_status"];
             
             [dictChnage setObject:@"0" forKey:@"portfolio_status"];
             
             [arrayCompaniesList replaceObjectAtIndex:index withObject:dictChnage];
             
             
             
             NSInteger points = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"] integerValue];
             
             points = points - 50;
             
             [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld", (long)points] forKey:@"userPoints"];
             
             int compCount = [[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"companyCount"]] intValue];
             int unread = [[[NSString stringWithFormat:@"%@",[_dicCompanyDetail objectForKey:@"unread_message"]] removeNull] intValue];
             int TotalCount = compCount-unread;
             
             [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",TotalCount] forKey:@"companyCount"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
         [dummy updateTable:[NSString stringWithFormat:@"UPDATE COMPANIES SET portfolio_status='0' WHERE id = '%@'",[_dicCompanyDetail objectForKey:@"id"]]];

         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             //[[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"No Internet" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             NSString *str = error.localizedDescription;
             
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:[NSString stringWithFormat:@"\n%@",str] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}

#pragma mark---UIActivityIndicatorView-----

-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        spinner.center = self.view.center;
        spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [spinner startAnimating];
        [self.view.window setUserInteractionEnabled:NO];
        
        [self.view addSubview:spinner];
    }
    else
    {
        if (spinner) {
            [spinner stopAnimating];
            [self.view.window setUserInteractionEnabled:YES];
            
            [spinner removeFromSuperview];
            spinner = nil;
        }
       
    }
}

- (void)rightItemAction
{
    if(![Utilities CheckInternetConnection])
    {
        [[[UIAlertView alloc] initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    }
    else
    {
        if ([buttonSelect.imageView.image isEqual:[UIImage imageNamed:@"portRemoved"]])
        {
            [self webServiceAddPort];
        }
        else
        {
            [self webServiceRemovePort];
        }
    }
}

-(void)loadCompanyOffline:(NSInteger)companyId
{
    isInternetOff = YES;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"callallCompanyWebService"])
    {
        if (boolNotifyEditCompanyDetail==YES)
        {
            [self performSelectorInBackground:@selector(getAllCompanies) withObject:nil];
            
        }
        
        else
        {
            
            [self getAllCompanies];
        }
    }
    else
    {
        
        NSArray *arrCompanies = (NSArray*)[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"];
        
        if ([_strVwChane isEqualToString:@"fromMessage"])
        {
            if ([[arrCompanies valueForKey:@"id"] containsObject:[NSString stringWithFormat:@"%ld",(long)companyId]])
            {
                for (int i=0; i<arrCompanies.count; i++)
                {
                    if (companyId == [[[arrCompanies valueForKey:@"id"]objectAtIndex:i ] integerValue])
                    {
                        _dicCompanyDetail = [[NSMutableDictionary alloc] initWithDictionary: [[arrCompanies objectAtIndex:i] copy]];
                    }
                }
                [self createDictionary];
                [self PlaceData:_dicCompanyDetail];
            }
            else
            {
                NSInteger integerId = [[_dicCompanyDetail valueForKey:@"adder_id"] integerValue];
                [self companyDetailService:integerId];
            }
        }
        else
        {
            for (int i=0; i<arrCompanies.count; i++)
            {
                if (companyId == [[[arrCompanies valueForKey:@"id"]objectAtIndex:i] integerValue])
                {
                    _dicCompanyDetail = [[NSMutableDictionary alloc] initWithDictionary:[[arrCompanies objectAtIndex:i] copy]];
                    
                }
            }
            
            [self createDictionary];
            [self PlaceData:_dicCompanyDetail];
        }
    }
    
}

-(void)PlaceData:(NSDictionary*)dicCompanyData
{
    UIView  *textView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 130, 40)];
    
    UILabel *labelTitleName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 0, CGRectGetWidth(textView.frame), 40)];
    labelTitleName.backgroundColor = [UIColor clearColor];
    labelTitleName.font = [UIFont fontWithName:@"OpenSans" size:18.0f];
    labelTitleName.textAlignment = NSTextAlignmentCenter;
    labelTitleName.textColor = [UIColor whiteColor];
    labelTitleName.text = [dicCompanyData valueForKey:@"company_name"];
    labelTitleName.numberOfLines = 0;
    [textView addSubview:labelTitleName];
    self.navigationItem.titleView=textView;
    
    labelTitleName.adjustsFontSizeToFitWidth = YES;
    [labelTitleName setMinimumScaleFactor:8.0/[UIFont labelFontSize]];
    
    _labelCompaniesTitle.text =[NSString stringWithFormat:@"%@", [dicCompanyData valueForKey:@"company_name"]];
    
    _labelCompaniesTitle.adjustsFontSizeToFitWidth = YES;
    [_labelCompaniesTitle setMinimumScaleFactor:8.0/[UIFont labelFontSize]];
    
    _textViewCompanyDescrip.text = [dicCompanyData valueForKey:@"company_intro"];
    
    CGRect txtVwRect = _textViewCompanyDescrip.frame;
    
    CGFloat fixedWidth = _textViewCompanyDescrip.frame.size.width;
    CGSize newSize = [_textViewCompanyDescrip sizeThatFits:CGSizeMake(_textViewCompanyDescrip.frame.size.width, _textViewCompanyDescrip.frame.size.height)];
    
    CGRect newFrame = _textViewCompanyDescrip.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    
    CGRect rectViewTitles = _viewTitle.frame;
    
    if (!(newFrame.size.height>txtVwRect.size.height)){
        rectViewTitles.size.height -=_textViewCompanyDescrip.frame.size.height-newFrame.size.height;
        _viewTitle.frame = rectViewTitles;
        _textViewCompanyDescrip.frame = newFrame;
    }
    
    CGRect rectViewMaps = _viewMapLocations.frame;
    rectViewMaps.origin.y = CGRectGetMaxY(_viewTitle.frame)+5;
    _viewMapLocations.frame = rectViewMaps;
    
    CGRect rectTableVw =_tableViewCompany.frame;
    rectTableVw.origin.y = CGRectGetMaxY(_viewMapLocations.frame)+8;
    _tableViewCompany.frame = rectTableVw;
    
    if ( rectTableVw.size.height > 100 )
    {
        if ([[_dicCompanyDetail valueForKey:@"links"] count] > 5 )
        {
            self.scrollViewOutlet.contentSize = CGSizeMake(self.view.frame.size.width, 600+(arrayTitleName.count+5)*50);
        }
        else
        {
            
            self.scrollViewOutlet.contentSize = CGSizeMake(self.view.frame.size.width, 600+(arrayTitleName.count+[[_dicCompanyDetail valueForKey:@"links"] count])*50);
        }
    }
    else
    {
        if ([[_dicCompanyDetail valueForKey:@"links"] count]>5)
        {
            self.scrollViewOutlet.contentSize =CGSizeMake(self.view.frame.size.width, 550+(arrayTitleName.count+5)*50);
        }
        else
        {
            self.scrollViewOutlet.contentSize =CGSizeMake(self.view.frame.size.width, 550+arrayTitleName.count*50);
        }
    }
    
    if(isInternetOff)
    {
        if ([[dicCompanyData valueForKey:@"logo"] isEqualToString:@"N"])
        {
            [_imageViewLogo setImage:[UIImage imageNamed:@"defaultBuilding"]];
        }
        else
        {
            _imageViewLogo.image = [UIImage imageWithContentsOfFile:[dicCompanyData valueForKey:@"logo"]];
        }
    }
    else
    {
        if ([[dicCompanyData valueForKey:@"logo"] isEqualToString:@""])
        {
            _imageViewLogo.image = [UIImage imageNamed:@"defaultBuilding"];
        }
        else if ([[dicCompanyData valueForKey:@"logo"] hasPrefix:@"http:"])
        {
            [_imageViewLogo setImageWithURL: [NSURL URLWithString:[dicCompanyData valueForKey:@"logo"]]];
        }
        else if ([[dicCompanyData valueForKey:@"logo"] hasPrefix:@"/Users/"])
        {
            _imageViewLogo.image = [UIImage imageWithContentsOfFile:[dicCompanyData valueForKey:@"logo"]];
        }
        else if ([[dicCompanyData valueForKey:@"logo"] hasPrefix:@"/var/mobile/Containers/"])
        {
            _imageViewLogo.image = [UIImage imageWithContentsOfFile:[dicCompanyData valueForKey:@"logo"]];
        }
        
        else
        {
            [_imageViewLogo setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl, [dicCompanyData valueForKey:@"logo"]]]];
        }
    }
    CGRect rectTable = _tableViewCompany.frame;
    rectTable.size.height = _scrollViewOutlet.contentSize.height;
    _tableViewCompany.frame = rectTable;
    
    [self performSelector:@selector(addPinsOnMap) withObject:nil afterDelay:2.0f];
}


-(void)addPinsOnMap
{
    CLLocationCoordinate2D coord;
    
    if (CLLocationCoordinate2DIsValid(coord))
    {
    }
    else
    {
    }
    
    if ([[_dicCompanyDetail valueForKeyPath:@"location"] count]==0)
    {
        
    }
    else
    {
        int indexVal =  [self getDistanceBetweenTwoLocation];
        
        coord.latitude = [[NSString stringWithFormat:@"%@",[[_dicCompanyDetail valueForKeyPath:@"location.lat"] objectAtIndex:indexVal]] floatValue];
        
        coord.longitude = [[NSString stringWithFormat:@"%@",[[_dicCompanyDetail valueForKeyPath:@"location.long"] objectAtIndex:indexVal]] floatValue];
    }
    [_mapViewGoogle.settings setAllGesturesEnabled:NO];
    [_mapViewGoogle.settings setZoomGestures:YES];
    [_mapViewGoogle.settings setScrollGestures:YES];
    
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat: 1.0f] forKey:kCATransactionAnimationDuration];
    // change the camera, set the zoom, whatever.  Just make sure to call the animate* method.
    [_mapViewGoogle animateToCameraPosition: [GMSCameraPosition cameraWithLatitude:coord.latitude
                                                                         longitude:coord.longitude
                                                                              zoom:12]];
    [CATransaction commit];
    
    for (int i=0; i<[[_dicCompanyDetail valueForKeyPath:@"location.lat"] count]; i++)
    {
        CLLocationCoordinate2D coord;
        
        coord.latitude = [[NSString stringWithFormat:@"%@",[[_dicCompanyDetail valueForKeyPath:@"location.lat"] objectAtIndex:i]] floatValue];
        
        coord.longitude = [[NSString stringWithFormat:@"%@",
                            [[_dicCompanyDetail valueForKeyPath:@"location.long"] objectAtIndex:i]] floatValue];
        
        _mapViewGoogle.mapType = kGMSTypeNormal;
        
        // Creates a marker in the center of the map.
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(coord.latitude, coord.longitude);
        marker.title = [NSString stringWithFormat:@"%@",[[_dicCompanyDetail valueForKeyPath:@"location.location"]objectAtIndex:i]];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        
        marker.map = _mapViewGoogle;
    }
}

-(int)getDistanceBetweenTwoLocation
{
    
    CLLocationCoordinate2D coord, coodState;
    
    NSMutableArray *arrayDistance = [NSMutableArray new];
    
    coodState.latitude = [[USERDEFAULT objectForKey:@"LAtitude_State"]floatValue] ;
    
    coodState.longitude = [[USERDEFAULT objectForKey:@"LONGITUDE_State"]floatValue] ;
    
    CLLocation *userloc = [[CLLocation alloc]initWithLatitude: coodState.latitude longitude:coodState.longitude];
    
    if ([[_dicCompanyDetail valueForKeyPath:@"location"] count]==0)
    {
        
    }
    else
    {
        for (int i =0; i< [[_dicCompanyDetail valueForKeyPath:@"location.lat"] count]; i++)
        {
            coord.latitude = [[NSString stringWithFormat:@"%@",[[_dicCompanyDetail valueForKeyPath:@"location.lat"] objectAtIndex:i]] floatValue];
            
            coord.longitude = [[NSString stringWithFormat:@"%@",[[_dicCompanyDetail valueForKeyPath:@"location.long"] objectAtIndex:i]] floatValue];
            
            CLLocation *dest = [[CLLocation alloc]initWithLatitude:coord.latitude longitude:coord.longitude];
            
            CLLocationDistance dist = [userloc distanceFromLocation:dest]/1000;
            
            NSNumber * num1 = [NSNumber numberWithFloat:dist];
            
            
            [arrayDistance addObject:num1];
        }
        
    }
    
    int index = (int)[arrayDistance indexOfObject:[arrayDistance valueForKeyPath:@"@min.floatValue"]];
    
    return  index;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark---GetCompanyDetail---

-(void)companyDetailService :(NSInteger) idOfCompany
{
    if (![refreshControl isRefreshing])
    {
        [self ShowDataLoader:YES];
    }
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"cid":[NSString stringWithFormat:@"%ld",(long)idOfCompany]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"companyDetail" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         
         [self ShowDataLoader:NO];
         
         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             _dicCompanyDetail = nil;
             
             NSDictionary *dict = [responseObject valueForKey:@"data"];
             
             _dicCompanyDetail = [[NSMutableDictionary alloc] initWithDictionary:dict];
             
             NSString *string = [_dicCompanyDetail valueForKey:@"message_status"];
             
             [_dicCompanyDetail setObject:string forKey:@"message_status"];
             
             isInternetOff = NO;
             
             isFromDetailApi = YES;
             
             [self createDictionary];
             
             [self PlaceData:[responseObject valueForKey:@"data"]];
             
         }
         else
         {
             ZAlertWithParam(@"Alert !", @"No messages to show.");
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         //else
         {
             [self ShowDataLoader:NO];
         }
         
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             //[[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             NSString *str = error.localizedDescription;
             
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:[NSString stringWithFormat:@"\n%@",str] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}

-(void)getAllCompanies
{
    if (boolNotifyEditCompanyDetail==YES)
    {
        [self ShowDataLoader:NO];
        
        boolNotifyEditCompanyDetail = NO;
    }
    else
    {
        [self ShowDataLoader:YES];
    }
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"getAllCompanies" parameters:@{@"id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]} success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"callallCompanyWebService"];
         [self ShowDataLoader:NO];

         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             arrayAllCompanies = [responseObject valueForKeyPath:@"data"];
             
             [self saveSearchCompanies:responseObject];
             
             NSArray *arrayTemp = [dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"];
             
             NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
             
             NSArray* sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
             
             NSArray *arraySorted = [[NSArray alloc] initWithArray:[arrayTemp sortedArrayUsingDescriptors:sortDescriptors]];
             
             [arrayCompaniesList removeAllObjects];
             
             arrayCompaniesList = [arraySorted mutableCopy];
         }
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             //[[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             NSString *str = error.localizedDescription;
             
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:[NSString stringWithFormat:@"\n%@",str] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}

-(void)saveSearchCompanies :(NSDictionary *)dicData
{
    if ([dummy deleteTable:@"delete from COMPANIES"])
    {
        if ([dummy deleteTable:@"delete from COMPANYLOCATIONS"])
        {
            if ([dummy deleteTable:@"delete from DispSubDispCompaniesTable"])
            {
                if ([dummy deleteTable:@"delete from SearchCompSubDisciplineTable"])
                {
                    if ([dummy deleteTable:@"delete from SearchCompanyAwards"])
                    {
                        if ([dummy deleteTable:@"delete from SearchCompanyDueDate"])
                        {
                            if ([dummy deleteTable:@"delete from SearchQuesAnsTable"])
                            {
                                if ([dummy deleteTable:@"delete from SearchCompSocialLinksTable"])
                                {
                                    for (int i=0; i<[arrayAllCompanies count]; i++)
                                    {
                                        NSString *query = [NSString stringWithFormat: @"INSERT INTO COMPANIES (id,application_due_date, awards,city,company_address,company_name,description,logo,state_id,regstates,country_id,company_intro,country,portfolio_status,message_state,message_status, website,international,unread_message) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")"
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"id"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"application_due_date"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"awards"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"city"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"company_address"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"company_name"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"description"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"logo"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"state_id"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"regstates"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"country_id"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"company_intro"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"country"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"portfolio_status"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"message_state"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"message_status"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"website"],[[arrayAllCompanies valueForKey:@"international"] objectAtIndex:i],[[arrayAllCompanies valueForKey:@"unread_message"] objectAtIndex:i]];
                                        
                                        [self saveImages:[NSString stringWithFormat:@"%@%@",[[arrayAllCompanies  objectAtIndex:i] valueForKeyPath:@"id"],[[arrayAllCompanies  objectAtIndex:i] valueForKeyPath:@"logo"]] imageUrl:[[arrayAllCompanies  objectAtIndex:i] valueForKeyPath:@"logo"]];
                                        
                                        const char *query_stmt = [query UTF8String];
                                        [dummy  runQueries:query_stmt strForscreen:@"search"];
                                        
                                        [self saveDueDateSearchCompanies:i];
                                        [self saveCompanyLocations:i];
                                        [self saveCompanyDisciplines:i];
                                        [self saveSearchCompanyAwards:i];
                                        [self saveQuestionsSearchComp:i];
                                        [self saveSearchCompanyLinks:i];
                                        [self saveCompanySubDisciplines:i];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    [self ShowDataLoader:NO];
    
    if ([_strVwChane isEqualToString:@"seachViewString"])
    {
        if(![Utilities CheckInternetConnection])
        {
            isInternetOff = YES;
            [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"id"] integerValue]];
        }
        else
        {
            [self createDictionary];
            [self PlaceData:_dicCompanyDetail];
        }
    }
    else if ([_strVwChane isEqualToString:@"portfolioVw"])
    {
        if(![Utilities CheckInternetConnection])
        {
            [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"id"] integerValue]];
        }
        else
        {
            [self createDictionary];
            [self PlaceData:_dicCompanyDetail];
        }
    }
    else if ([_strVwChane isEqualToString:@"CompaniesVw"])
    {
        if(![Utilities CheckInternetConnection])
        {
            [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"id"] integerValue]];
        }
        else
        {
            [self createDictionary];
            [self PlaceData:_dicCompanyDetail];
        }
    }
    else if ([_strVwChane isEqualToString:@"fromMessage"])
    {
        [self loadCompanyOffline:[[_dicCompanyDetail valueForKey:@"adder_id"] integerValue]];
    }
}

-(void)saveImages:(NSString *)ImageName imageUrl :(NSString *)imageUrl
{
    ImageName = [ImageName stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    if (imageUrl.length>0)
    {
        if (![self imageCheck:ImageName])
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:ImageName];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,imageUrl]]];
            
            [imageData writeToFile:savedImagePath atomically:NO];
        }
    }
}

#pragma mark- CheckForImageInDataBase

-(BOOL)imageCheck :(NSString *)strName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    if ([contents containsObject:strName])
    {
        return YES;
    }
    return NO;
}

-(void)saveDueDateSearchCompanies:(int)index
{
    
    for (int j=0; j<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all"] count]; j++)
    {
        NSString *subQueryDueDate;
        
        subQueryDueDate = [NSString stringWithFormat:@"INSERT INTO SearchCompanyDueDate(close_date,company_id,id,program,program_id,start_date,state,country) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.close_date"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.company_id"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.id"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.program"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.program_id"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.start_date"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.state"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.country"] objectAtIndex:j]];
        
        const char *query_stmt_dueDate = [subQueryDueDate UTF8String];
        
        [dummy runSubQueryToSaveDueDate:query_stmt_dueDate strForscreen:@"Search"];
        
    }
}

-(void)saveCompanyLocations:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location"] count]; k++)
    {
        NSString *subQuery = [NSString stringWithFormat: @"INSERT INTO COMPANYLOCATIONS (company_id,id,branch_name, lat,long,location) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\")"
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.company_id"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.id"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.branch_name"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.lat"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.long"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.location"] objectAtIndex:k]];
        
        const char *query_stmtLocation = [subQuery UTF8String];
        [dummy runSubQueryLocations:query_stmtLocation strForscreen:@"COMPANYLOCATIONS"];
        
    }
}

-(void)saveCompanySubDisciplines:(int)index
{
    NSLog(@"index is %d", index);
    
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines"] count]; k++)
    {
        NSString *subQuery;
        
        NSArray *arraySubDisciplineId = [[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline.sub_discipline_id"] objectAtIndex:k];
        
        for (int i=0; i<arraySubDisciplineId.count; i++)
        {
            subQuery = [NSString stringWithFormat: @"INSERT INTO DispSubDispDataTable (company_id,discipline_id,name,sub_discipline_id) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                        ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:k]
                        ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.discipline_id"] objectAtIndex:k]
                        ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.name"] objectAtIndex:k],[arraySubDisciplineId objectAtIndex:i]];
            
            const char *query_stmt_disciplines = [subQuery UTF8String];
            [dummy runQueryDispSubDispFordisp:query_stmt_disciplines strForscreen:@"searchCompanyDisciplines"];
        }
    }
}


-(void)saveCompanyDisciplines:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines"] count]; k++)
    {
        NSString *subQuery;
        
        subQuery = [NSString stringWithFormat: @"INSERT INTO DispSubDispCompaniesTable (company_id,discipline_id,name,type) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.discipline_id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.name"] objectAtIndex:k],[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.type"] objectAtIndex:k]];
        
        const char *query_stmt_disciplines = [subQuery UTF8String];
        [dummy runQueryDispSubDispForCompanies:query_stmt_disciplines strForscreen:@"searchCompanyDisciplines"];
        [self saveSubDisciplinesPort:index disciplineTag:k];
    }
}

-(void)saveSubDisciplinesPort:(int)index disciplineTag:(int)disciplineTag
{
    for (int j=0; j<[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag] count]; j++)
    {
        NSString *subQuery;
        
        subQuery = [NSString stringWithFormat: @"INSERT INTO SearchCompSubDisciplineTable (discipline_id,name,company_id,sub_discipline_id) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                    ,[[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"discipline_id"] objectAtIndex:j]
                    ,[[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"name"] objectAtIndex:j]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:disciplineTag]
                    ,[[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"sub_discipline_id"] objectAtIndex:j]
                    ];
        
        const char *query_stmt_PortSubDiscip = [subQuery UTF8String];
        
        [dummy runQuerySubDisciplines:query_stmt_PortSubDiscip strForscreen:@"SubDiscipSearchCompanies"];
        
    }
}

-(void)saveSearchCompanyAwards:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKey:@"allAwards"] count]; k++)
    {
        NSString *subQueryAwards;
        
        
        subQueryAwards = [NSString stringWithFormat: @"INSERT INTO SearchCompanyAwards (awards,company_id,description, id) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.awards"] objectAtIndex:k]
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.company_id"] objectAtIndex:k]
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.description"] objectAtIndex:k]
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.id"] objectAtIndex:k]];
        
        const char *query_stmt_Awards= [subQueryAwards UTF8String];
        [dummy runSubQueryToSaveAwards:query_stmt_Awards strForscreen:@"SearchVw"];
    }
}

  -(void)saveQuestionsSearchComp:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns"] count]; k++)
    {
        NSString *subQuery;
        subQuery = [NSString stringWithFormat: @"INSERT INTO SearchQuesAnsTable (answer,company_id,id,question) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.answer"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.company_id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.question"] objectAtIndex:k]];
        
        const char *query_stmt_QuesAns = [subQuery UTF8String];
        [dummy runQueryQuesAnsForCompanies:query_stmt_QuesAns strForscreen:@"search"];
    }
}

-(void)saveSearchCompanyLinks:(int)index
{
    
    for (int k=0; k<[[[arrayAllCompanies valueForKey:@"links"] objectAtIndex:index] count]; k++)
    {
        NSString *subQueryLinks;
        
        subQueryLinks = [NSString stringWithFormat: @"INSERT INTO SearchCompSocialLinksTable (company_id,id,media_link,media_logo,media_type) VALUES (\"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.company_id"] objectAtIndex:k]
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.id"] objectAtIndex:k]
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.media_link"] objectAtIndex:k]
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.media_logo"] objectAtIndex:k]
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.media_type"] objectAtIndex:k]];
        
        [self saveImages:[[NSString stringWithFormat:@"%@%@",[[[arrayAllCompanies valueForKeyPath:@"links.id"] objectAtIndex:index] objectAtIndex:k],[[[arrayAllCompanies valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                imageUrl:[[[arrayAllCompanies valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]];
        const char *query_stmt_SocialLinks = [subQueryLinks UTF8String];
        [dummy runQuerySocialLinksForCompanies:query_stmt_SocialLinks strForscreen:@"search"];
    }
}


#pragma mark - UITableViewDataSource------


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView

{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    if (tableView ==_tableViewCompany)
        
    {
        return [arrayTitleName count]+1;
    }
    
    else
        
    {
        return [arrAreasOfOp count] ;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellId"];
    cell=nil;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UILabel *headingLabel = [[UILabel alloc] init];
    
    if (tableView ==_tableViewCompany)
    {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (indexPath.row!=arrayTitleName.count)
        {
            // code for displying application due date, target discipline etc
            
            headingLabel.frame = CGRectMake(20, 0, tableView.frame.size.width-20, 50);
            
            headingLabel.text = [arrayTitleName objectAtIndex:indexPath.row];
            
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.10];
            [cell setSelectedBackgroundView:bgColorView];
            
            if ([[arrayTitleName objectAtIndex:indexPath.row] isEqualToString:@"Awards"])
            {
                for (int k=0; k<[[dicAppDates valueForKey:@"Awards"] count]; k++)
                {
                    UIImageView *imageAwards = [[UIImageView alloc] initWithFrame:CGRectMake(xValue, 17.5f, 15, 15)];
                    xValue = xValue+20;
                    
                    if ([[[dicAppDates valueForKeyPath:@"Awards.awards"] objectAtIndex:k] isEqualToString:@"2"])
                    {
                        imageAwards.image = [UIImage imageNamed:@"yellow-cup"];
                    }
                    
                    else if ([[[dicAppDates valueForKeyPath:@"Awards.awards"] objectAtIndex:k] isEqualToString:@"3"])
                    {
                        imageAwards.image = [UIImage imageNamed:@"silverCup"];
                    }
                    
                    else if ([[[dicAppDates valueForKeyPath:@"Awards.awards"] objectAtIndex:k] isEqualToString:@"1"])
                    {
                        imageAwards.image = [UIImage imageNamed:@"honour"];
                    }
                    
                    [cell.contentView addSubview:imageAwards];
                }
                
                cell.selectionStyle = UITableViewCellSelectionStyleDefault;
                
                UIView *bgColorView = [[UIView alloc] init];
                
                bgColorView.backgroundColor = [UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.10];
                
                [cell setSelectedBackgroundView:bgColorView];
            }
            
            headingLabel.font = [UIFont fontWithName:@"OpenSans" size:16.0f];
            headingLabel.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0];
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            if ([[_dicCompanyDetail valueForKey:@"links"] count]>0&&indexPath.row==[arrayTitleName count]-1)
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                if ([[_dicCompanyDetail valueForKey:@"links"] count]>0)
                {
                    int x=15;
                    
                    float y = 55;
                    
                    for (int k=0; k<[[dicAppDates valueForKey:@"More Information"] count]; k++)
                    {
                        UIButton *btnSocialLinks = [[UIButton alloc] initWithFrame:CGRectMake(x, y, 60, 60)];
                        
                        UIImageView *imgSocialLinks = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, 60, 60)];
                        
                        if(isInternetOff)
                        {
                            NSString *strImage = [[[dicAppDates valueForKey:@"More Information"] valueForKey:@"media_logo"] objectAtIndex:k];
                            
                            imgSocialLinks.image = [UIImage imageNamed:strImage];
                            
                        }
                        else
                        {
                            if ([[[[dicAppDates valueForKey:@"More Information"] valueForKey:@"media_logo"] objectAtIndex:k] hasPrefix:@"/Users/"])
                            {
                                UIImage *img =[UIImage imageWithContentsOfFile:[[[dicAppDates valueForKey:@"More Information"] valueForKey:@"media_logo"] objectAtIndex:k]];
                                
                                imgSocialLinks.image = img;
                                
                            }
                            
                            else if ([[[[dicAppDates valueForKey:@"More Information"] valueForKey:@"media_logo"] objectAtIndex:k] hasPrefix:@"/var/mobile/Containers/"])
                            {
                                UIImage *img =[UIImage imageWithContentsOfFile:[[[dicAppDates valueForKey:@"More Information"] valueForKey:@"media_logo"] objectAtIndex:k]];
                                
                                imgSocialLinks.image = img;
                                
                            }
                            else
                            {
                                
                                //   http://52.5.162.191/perfect/assets/uploads/socialLinks/jkku.png
                                
                                imgSocialLinks.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[[[dicAppDates valueForKey:@"More Information"] valueForKey:@"media_logo"] objectAtIndex:k]]];
                            }
                        }
                        btnSocialLinks.tag = 1001+k;
                        btnSocialLinks.layer.cornerRadius = 10.0f;
                        btnSocialLinks.clipsToBounds = YES;
                        
                        [btnSocialLinks addTarget:self action:@selector(moreInfo:) forControlEvents:UIControlEventTouchUpInside];
                        x = btnSocialLinks.frame.origin.x+btnSocialLinks.frame.size.width+15;
                        
                        if (x+60>=tableView.frame.size.width)
                        {
                            y = y+btnSocialLinks.frame.size.height+10;
                            
                            x=15;
                        }
                        
                        [cell.contentView addSubview:imgSocialLinks];
                        
                        [cell.contentView addSubview:btnSocialLinks];
                        
                        [btnSocialLinks setBackgroundImage:[UIImage imageNamed:@"messageSelecteddddddd"] forState:UIControlStateSelected];
                    }
                }
            }
        }
        
        else
        {
            
            UIButton *btnActiveMsgs =[[UIButton alloc] initWithFrame:CGRectMake(15, 10, tableView.frame.size.width-30, 50)];
            //msg007
            BOOL isMessagesStopped=NO;
            isMessagesStopped=[[_dicCompanyDetail valueForKey:@"message_status"] boolValue];
            NSLog(@"Messages Stopped:%@",isMessagesStopped?@"YES":@"NO");
            isMessagesStopped=![[MessageSubscriptionManager sharedManager] isMessageSubscribedForId:[NSString stringWithFormat:@"%@",[_dicCompanyDetail valueForKey:@"id"]] isCompany:YES];
            NSLog(@"Messages StoppedNN:%@ For CompanyId:%@",isMessagesStopped?@"YES":@"NO",[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[_dicCompanyDetail valueForKey:@"id"]]]);
            if (isMessagesStopped)
            {
                [btnActiveMsgs setTitle:@"Messages are stopped" forState:UIControlStateNormal];
                [btnActiveMsgs setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            }
            else
            {
                [btnActiveMsgs setTitle:@"Stop receiving messages" forState:UIControlStateNormal];
                [btnActiveMsgs setTitleColor:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0] forState:UIControlStateNormal];
            }
            
            btnActiveMsgs.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:16.0f];
            [btnActiveMsgs setBackgroundColor:[UIColor whiteColor]];
            btnActiveMsgs.layer.cornerRadius = 5;
            btnActiveMsgs.layer.borderWidth = 1;
            [btnActiveMsgs addTarget:self action:@selector(openPopUp:) forControlEvents:UIControlEventTouchUpInside];
            
            btnActiveMsgs.layer.borderColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0].CGColor;
            cell.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:btnActiveMsgs];
        }
    }
    else
    {
        tableAreaOfOperation1.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        {
            if (arrayRegState.count==0)
            {
                if (indexPath.row == 0)
                {
                    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"International:"];
                    [attributeString addAttribute:NSUnderlineStyleAttributeName
                                            value:[NSNumber numberWithInt:1]
                                            range:(NSRange){0,[attributeString length]}];
                    
                    UILabel  *headingLabel1 = [[UILabel alloc]init];
                    
                    headingLabel1.frame = CGRectMake(20, 0, tableView.frame.size.width-20, 35);
                    
                    
                    headingLabel1.attributedText = attributeString;
                    
                    [cell.contentView addSubview:headingLabel1];
                    
                    headingLabel.frame = CGRectMake(20, 30, tableView.frame.size.width-20, 35);
                    headingLabel.text = [[arrAreasOfOp objectAtIndex:indexPath.row]valueForKey:@"International"];
                    headingLabel.font = [UIFont fontWithName:@"OpenSans" size:11.0f];
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
                else
                {
                    headingLabel.frame = CGRectMake(20, 0, tableView.frame.size.width-20, 35);
                    headingLabel.text = [[arrAreasOfOp objectAtIndex:indexPath.row]valueForKey:@"International"];
                    headingLabel.font = [UIFont fontWithName:@"OpenSans" size:11.0f];
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
            else
            {
                if (indexPath.row <= arrayRegState.count-1)
                {
                    if (indexPath.row == 0)
                    {
                        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Local:"];
                        
                        [attributeString addAttribute:NSUnderlineStyleAttributeName
                                                value:[NSNumber numberWithInt:1]
                                                range:(NSRange){0,[attributeString length]}];
                        
                        UILabel  * headingLabel1 = [[UILabel alloc]init];
                        
                        headingLabel1.frame = CGRectMake(20, 0, tableView.frame.size.width-20, 35);
                        
                        headingLabel1.attributedText = attributeString;
                        
                        [cell.contentView addSubview:headingLabel1];
                        
                        headingLabel.frame = CGRectMake(20, 30, tableView.frame.size.width-20, 35);
                        headingLabel.text = [[arrAreasOfOp objectAtIndex:indexPath.row]valueForKey:@"RegState"];
                        headingLabel.font = [UIFont fontWithName:@"OpenSans" size:11.0f];
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                    else
                    {
                        headingLabel.frame = CGRectMake(20, 0, tableView.frame.size.width-20, 35);
                        headingLabel.text = [[arrAreasOfOp objectAtIndex:indexPath.row]valueForKey:@"RegState"];
                        headingLabel.font = [UIFont fontWithName:@"OpenSans" size:11.0f];
                        cell.accessoryType = UITableViewCellAccessoryNone;
                    }
                }
                else if (indexPath.row == arrayRegState.count)
                {
                    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"International:"];
                    [attributeString addAttribute:NSUnderlineStyleAttributeName
                                            value:[NSNumber numberWithInt:1]
                                            range:(NSRange){0,[attributeString length]}];
                    
                    UILabel  * headingLabel1 = [[UILabel alloc]init];
                    
                    headingLabel1.frame = CGRectMake(20, 0, tableView.frame.size.width-20, 35);
                    
                    headingLabel1.attributedText = attributeString;
                    
                    [cell.contentView addSubview:headingLabel1];
                    
                    headingLabel.frame = CGRectMake(20, 30, tableView.frame.size.width-20, 35);
                    headingLabel.text = [[arrAreasOfOp objectAtIndex:indexPath.row]valueForKey:@"International"];
                    headingLabel.font = [UIFont fontWithName:@"OpenSans" size:11.0f];
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
                else
                {
                    headingLabel.frame = CGRectMake(20, 0, tableView.frame.size.width-20, 35);
                    headingLabel.text = [[arrAreasOfOp objectAtIndex:indexPath.row]valueForKey:@"International"];
                    headingLabel.font = [UIFont fontWithName:@"OpenSans" size:11.0f];
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
        }
    }
    
    [cell.contentView addSubview:headingLabel];
    
    return cell;
}

#pragma mark--UITableViewDelegate--

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    self.tableViewCompany.contentInset = UIEdgeInsetsMake(-1.0f, -1.0f, 0.0f, 0.0);
    
    /*---Added one UIVIEW programmatically in table view header--*/
    
    UIView *headerView = [[UIView alloc] init];
    
    if (tableView ==_tableViewCompany)
    {
        
    }
    else
    {
        headerView.frame = CGRectMake(0, 0, 0,0);
        //
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (tableView ==_tableViewCompany)
    {
        return 0;
    }
    else
    {
        return 0;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView ==_tableViewCompany)
    {
        if ([[_dicCompanyDetail valueForKey:@"links"] count]>0&&indexPath.row==[arrayTitleName count]-1)
        {
            if ([[_dicCompanyDetail valueForKey:@"links"] count]>0)
            {
                int x=15;
                float y = 55;
                
                for (int k=0; k<[[dicAppDates valueForKey:@"More Information"] count]; k++)
                {
                    if (x+60>=tableView.frame.size.width)
                    {
                        y = y+70;
                        x=15;
                    }
                    x = x+75;
                }
                
                return y+70;
            }
            return 130;
        }
        else if(indexPath.row!=arrayTitleName.count)
        {
            return 50;
        }
        else {
            return 60;
        }
    }
    else
    {
        int height ;
        
        if (indexPath.row == 0)
            
        {
            height = 65;
        }
        
        else if (indexPath.row == arrayRegState.count)
        {
            height = 65;
        }
        else
        {
            height = 35;
        }
        
        return height;
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView ==_tableViewCompany)
        
    {
        if ([[dicAppDates valueForKey:[arrayTitleName objectAtIndex:indexPath.row]] count]==0)
            
        {
            [[[UIAlertView alloc] initWithTitle:@"Alert @@" message:@"\nNo data to show for this screen" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
            
            return;
        }
        
        if ([[_dicCompanyDetail valueForKey:@"links"] count]>0&&indexPath.row!=[arrayTitleName count]-1)
            
        {
            CompanyProgramsDetailVC *companyProgramsVw = [self.storyboard instantiateViewControllerWithIdentifier:@"CompanyPrograms"];
            
            companyProgramsVw.strCompanyName = _labelCompaniesTitle.text;
            
            companyProgramsVw.strCountryame = [_dicCompanyDetail valueForKey:@"country"];
            
            companyProgramsVw.dicData = @{[arrayTitleName objectAtIndex:indexPath.row]:
                                              [dicAppDates valueForKey:[arrayTitleName objectAtIndex:indexPath.row]]};
            
            [self.navigationController pushViewController:companyProgramsVw animated:NO];
            
        }
        
    }
    
}


#pragma mark --HeaderTapped--

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    
    if (indexPath.row == 0)
        
    {
        
    }
}

-(void)removeTable:(id)sender
{
    [tableAreaOfOperation1 removeFromSuperview];
    tableAreaOfOperation1=nil;
    UIView*viewOver = [self.view viewWithTag:211];
    [viewOver removeFromSuperview];
    viewOver = nil;
    [viewBackTabel removeFromSuperview];
}

-(void)moreInfo:(id)sender

{
    if ([sender isSelected])
        
    {
        [sender setSelected:NO];
    }
    
    else
    {
        [sender setSelected:YES];
    }
    
    [self performSelector:@selector(openLinkcompany:) withObject:sender afterDelay:0.5];
    
    
    [self performSelector:@selector(setStateCompany:) withObject:sender afterDelay:1.0];
}


-(void)openLinkcompany:(id)sender
{
    NSInteger BtnTag = [sender tag]-1001;
    NSString *linkStr = [[dicAppDates valueForKeyPath:@"More Information.media_link"] objectAtIndex:BtnTag];
    
    if (![linkStr hasPrefix:@"http:/"])
    {
        linkStr = [NSString stringWithFormat:@"http:/%@",[[dicAppDates valueForKeyPath:@"More Information.media_link"] objectAtIndex:BtnTag]];
    }
    
    NSURL *urlLink = [NSURL URLWithString:linkStr];
    
    [[UIApplication sharedApplication] openURL:urlLink];
    
}


-(void)setStateCompany:(id)sender
{
    [sender setSelected:NO];
}

#pragma mark- tabBarButtonAction

- (IBAction)buttonActionPortfolio:(id)sender
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[PortfolioViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    PortfolioViewController *portfolioView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
    
    [self.navigationController pushViewController:portfolioView animated:NO];
    
    [self.navigationController pushViewController:portfolioView animated:YES];
}

- (IBAction)buttonActionDiscip:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[DisciplineViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    DisciplineViewController *disciplineVw = [self.storyboard instantiateViewControllerWithIdentifier:@"disciplineView"];
    
    [self.navigationController pushViewController:disciplineVw animated:NO];
}

- (IBAction)buttonSearch:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[LocationFilterSearchViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    LocationFilterSearchViewController  *searchVw = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVw"];
    [self.navigationController pushViewController:searchVw animated:NO];
}

- (IBAction)buttonMessages:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[MessagesViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    MessagesViewController  *messageVw = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    [self.navigationController pushViewController:messageVw animated:NO];
}
- (IBAction)buttonActionSalary:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[SalaryGuideViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    SalaryGuideViewController *salaryVw = [self.storyboard instantiateViewControllerWithIdentifier:@"salaryVw"];
    [self.navigationController pushViewController:salaryVw animated:NO];
}


#pragma mark- ibButtonAction


- (IBAction)buttonAreaOfOprtnAction:(id)sender
{
    UIWindow *currentWindow =[UIApplication sharedApplication].keyWindow;
    
    backView = [[UIView alloc] initWithFrame:currentWindow.bounds];
    backView.backgroundColor = [UIColor blackColor];
    backView.tag=211;
    backView.alpha=0.2;
    [self.view addSubview:backView];
    
    viewBackTabel = [[UIView alloc]initWithFrame:CGRectMake(0, _viewMapLocations.frame.origin.y+10, self.view.frame.size.width, 320)];
    viewBackTabel.center =  self.view.center;
    
    [self.view addSubview:viewBackTabel];
    
    UIView *headerView = [[UIView alloc]init];
    
    headerView.frame = CGRectMake(20, 10, self.view.frame.size.width-40, 40);
    
    headerView.backgroundColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0];
    
    UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-130, CGRectGetHeight(headerView.frame))];
    
    labelTitle.text = @"Areas of Operation";
    
    labelTitle.font = [UIFont fontWithName:@"OpenSans" size:18.0f];
    
    labelTitle.textColor = [UIColor whiteColor];
    
    UIButton *btnClose =[[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(labelTitle.frame)+10, 5, CGRectGetMaxX(headerView.frame)-CGRectGetMaxX(labelTitle.frame), 30)];
    btnClose.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:10.0f];
    [btnClose addTarget:self action:@selector(removeTable:) forControlEvents:UIControlEventTouchUpInside];
    [btnClose setTitle:@"Close" forState:UIControlStateNormal];
    [headerView addSubview:btnClose];
    
    [headerView addSubview:labelTitle];
    
    [viewBackTabel addSubview:headerView];
    
    [headerView bringSubviewToFront:self.view];
    
    tableAreaOfOperation1 = [[UITableView alloc] initWithFrame:CGRectMake(20, 50, self.view.frame.size.width-40, 240)];
    
    
    tableAreaOfOperation1.scrollEnabled = YES;
    tableAreaOfOperation1.bounces = YES;
    tableAreaOfOperation1.tag = 112;
    tableAreaOfOperation1.delegate=self;
    tableAreaOfOperation1.dataSource=self;
    
    [viewBackTabel addSubview:tableAreaOfOperation1];
}

- (IBAction)btnOpenWebSite:(id)sender
{
    if ([sender isSelected])
    {
        [sender setSelected:NO];
    }
    
    else
    {
        [sender setSelected:YES];
    }
    
    
    [self performSelector:@selector(openLink) withObject:nil afterDelay:0.1];
    
    [self performSelector:@selector(setStateCompany:) withObject:sender afterDelay:0.30];
}

-(void)openLink
{
    NSString *link = [_dicCompanyDetail valueForKey:@"website_link"];
    
    if (![link hasPrefix:@"http://"])
    {
        link = [NSString stringWithFormat:@"http://%@",[_dicCompanyDetail valueForKey:@"website_link"]];
    }
    
    NSURL *urlLink = [NSURL URLWithString:link];
    
    [[UIApplication sharedApplication] openURL:urlLink];
}

-(void)labelClearColor
{
    
    _imageViewLogo.layer.borderColor = [[UIColor clearColor] CGColor];
}

- (void)openPopUp:(id)sender
{
    BOOL isMessagesStopped=NO;
    isMessagesStopped=[[_dicCompanyDetail valueForKey:@"message_status"] boolValue];
    NSLog(@"Messages Stopped:%@",isMessagesStopped?@"YES":@"NO");
    isMessagesStopped=![[MessageSubscriptionManager sharedManager] isMessageSubscribedForId:[NSString stringWithFormat:@"%@",[_dicCompanyDetail valueForKey:@"id"]] isCompany:YES];
    NSLog(@"Messages StoppedNN:%@ For CompanyId:%@",isMessagesStopped?@"YES":@"NO",[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[_dicCompanyDetail valueForKey:@"id"]]]);

    if (isMessagesStopped)
    {
        [self alertView:@"Continue receiving messages" strMessage:@"\nPlease confirm if you would like to continue receiving messages from this company?" alertTag:100];
    }
    else
    {
        [self alertView:@"Stop receiving messages" strMessage:@"\nPlease confirm if you would like to stop receiving messages from this company?" alertTag:99];
    }
}

-(void)alertView:(NSString *)strHeader strMessage:(NSString*)strMessage alertTag:(NSInteger)alertTag
{
    UIAlertView *alert =[[UIAlertView alloc] initWithTitle:strHeader message:strMessage delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"No", nil];
    
    alert.tag = alertTag;
    
    [alert show];
}


-(void)webServiceMessageBlockUnblock:(NSString *)strStatus
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *dicParam = @{@"company_id":[_dicCompanyDetail valueForKey:@"id"],@"user_id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"status":strStatus};
    
    [manager POST:@"MessageStatus" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         
         if ([responseObject valueForKey:@"status"])
         {
             xValue = 90;
             
             if ([strStatus isEqualToString:@"1"])
             {
                 [_dicCompanyDetail setValue:@"1" forKey:@"message_status"];
                 //msg007
                 [[MessageSubscriptionManager sharedManager] updateSubscriptionMessagesForId:[NSString stringWithFormat:@"%@",[_dicCompanyDetail objectForKey:@"id"]] isCompany:YES shouldSubscribe:NO];
                 NSArray *arrayId = [arrayCompaniesList valueForKey:@"id"];
                 
                 int index = (int)[arrayId indexOfObject:[_dicCompanyDetail valueForKey:@"id"]];
                 
                 NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
                 
                 [dictChnage removeObjectForKey:@"message_status"];
                 
                 [dictChnage setValue:@"1" forKey:@"message_status"];
                 
                 [arrayCompaniesList replaceObjectAtIndex:index withObject:dictChnage];
                 
                 [self changeDataInPortStatus:strStatus];
             }
             
             else
             {
                 [_dicCompanyDetail setValue:@"0" forKey:@"message_status"];
                 [[MessageSubscriptionManager sharedManager] updateSubscriptionMessagesForId:[NSString stringWithFormat:@"%@",[_dicCompanyDetail objectForKey:@"id"]] isCompany:YES shouldSubscribe:YES];

                 NSArray *arrayId= [arrayCompaniesList valueForKey:@"id"];
                 
                 int index = (int)[arrayId indexOfObject:[_dicCompanyDetail valueForKey:@"id"]];
                 
                 NSMutableDictionary* dictChnage = [[NSMutableDictionary alloc]initWithDictionary:arrayCompaniesList[index] copyItems:YES];
                 
                 [dictChnage removeObjectForKey:@"message_status"];
                 
                 [dictChnage setValue:@"0" forKey:@"message_status"];
                 
                 [arrayCompaniesList replaceObjectAtIndex:index withObject:dictChnage];
                 
                 [self changeDataInPortStatus:@"0"];
             }
             
             checkCopmanyApiCall = NO;
             
             [_tableViewCompany reloadData];
             
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         [self ShowDataLoader:NO];
         
     }];
}

#pragma mark -UIAlertViewDelegate----

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==99)
    {
        if (buttonIndex==0)
        {
            [self webServiceMessageBlockUnblock:@"1"];
        }
    }
    else
    {
        if (buttonIndex==0)
        {
            [self webServiceMessageBlockUnblock:@"2"];
        }
    }
}

@end
