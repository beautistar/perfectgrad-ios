//
//  GamificationVC.h
//  PerfectGraduate
//
//  Created by netset on 4/23/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GamificationVC : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableViewGamification;
@property (strong, nonatomic) IBOutlet UIButton *buttonClose;
- (IBAction)buttonCancelAction:(id)sender;

@end
