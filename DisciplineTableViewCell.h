//
//  DisciplineTableViewCell.h
//  PerfectGraduate
//
//  Created by NSPL on 2/2/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisciplineTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *disciplinesNamesLabel;
@property (weak, nonatomic) IBOutlet UIButton *disciplineNameButton;
@property (weak, nonatomic) IBOutlet UIButton *bButton;

@end
