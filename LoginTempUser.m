//
//  LoginTempUser.m
//  PerfectGraduate
//
//  Created by netset on 3/16/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "LoginTempUser.h"

@interface LoginTempUser ()
{
    int viewY;
}

@end

@implementation LoginTempUser

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [_buttonRemember setImage:[UIImage imageNamed:@"withou_tick"] forState:UIControlStateNormal];
    [_buttonRemember setImage:[UIImage imageNamed:@"blue_tick"] forState:UIControlStateSelected];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userEmail"])
    {
        _textFieldEmail.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userEmail"];
        _textFieldPassword.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userPass"];
        [_buttonRemember setSelected:YES];
    }
    
    else{
        _textFieldEmail.text = @"";
        _textFieldPassword.text = @"";
        
    }
    
    
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = NO;
    self.title = @"Login";
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:20], NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    _textFieldEmail.layer.borderWidth = 1.0f;
    _textFieldEmail.layer.borderColor = [[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0/255.0f alpha:1.0]CGColor];
    _textFieldEmail.layer.cornerRadius = 3.0f;
    
    _textFieldPassword.layer.borderWidth = 1.0f;
    _textFieldPassword.layer.borderColor = [[UIColor colorWithRed:146.0f/255.0f green:146.0f/255.0f blue:146.0/255.0f alpha:1.0]CGColor];
    _textFieldPassword.layer.cornerRadius = 3.0f;
    
    _buttonLogin.layer.borderWidth = 2.0f;
    _buttonLogin.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0/255.0f alpha:1.0]CGColor];
    _buttonLogin.layer.cornerRadius = 4.0f;
    
    _buttonSendForgetPass.layer.borderWidth = 2.0f;
    _buttonSendForgetPass.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0/255.0f alpha:1.0]CGColor];
    _buttonSendForgetPass.layer.cornerRadius = 4.0f;
    
    _viewForgetPass.layer.cornerRadius = 5.0f;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    stringForNotifyEditCompany = @"";
    
    stringNotifiction = @"";
    
    _viewForgetPass.frame = CGRectMake (_viewForgetPass.frame.origin.x, _viewForgetPass.frame.origin.y, 0, 0);
    _viewForgetPass.center = self.view.center;
    CGRect viewRect =_viewOverlay.frame;
    viewRect.size.width = 0;
    viewRect.size.height = 0;
    _viewOverlay.frame = viewRect;
    _viewOverlay.center = self.view.center;
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark-----UITextFieldDelegate----------

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField endEditing:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==_textFldEmailForget)
    {
        if (_viewForgetPass.frame.origin.y>=viewY)
        {
            [self setViewMovedUp:YES];
        }
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    
    if (_viewForgetPass.frame.origin.y<viewY)
    {
        [self setViewMovedUp:NO];
    }
    
    [self.view endEditing:YES];
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView animateWithDuration:0.3 animations:^
     {
         CGRect rect = _viewForgetPass.frame;
         
         if (movedUp)
         {
             rect.origin.y -= kOFFSET_FOR_KEYBOARD;
         }
         else
         {
             rect.origin.y += kOFFSET_FOR_KEYBOARD;
         }
         _viewForgetPass.frame = rect;
         
     }];
    
}


#pragma mark------ButtonActions--------

- (IBAction)buttonRememberAction:(id)sender
{
    if ([sender isSelected]) {
        
        [sender setSelected:NO];
    }
    else{
        [sender setSelected:YES];
        
    }
}

- (IBAction)buttonLoginAction:(id)sender
{
    _textFieldEmail.text = [_textFieldEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    _textFieldPassword.text = [_textFieldPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (_textFieldEmail.text.length==0||_textFieldPassword.text.length==0)
    {
        [[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"\nBoth Fields are mandatory!" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
    }
    else if (![self NSStringIsValidEmail:_textFieldEmail.text])
    {
        [[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"\nPlease enter a valid Email address!" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
    }
    else
    {
        if ([_buttonRemember isSelected])
        {
            [[NSUserDefaults standardUserDefaults] setObject:_textFieldEmail.text forKey:@"userEmail"];
            [[NSUserDefaults standardUserDefaults] setObject:_textFieldPassword.text forKey:@"userPass"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }
        else{
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userEmail"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userPass"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        [self LoginUser];
    }
}


-(void)LoginUser
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *dictParam = @{@"email":_textFieldEmail.text,@"pass":_textFieldPassword.text};
    
    [manager POST:@"login" parameters:dictParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"studentviewKey"];
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"user_id"] forKey:@"userId"];
             [[NSUserDefaults standardUserDefaults]synchronize];
             
             PortfolioViewController *portView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
             self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
             UINavigationController * navPort = [[UINavigationController alloc]initWithRootViewController:portView];
             [self.revealViewController setFrontViewController:navPort animated:YES];
         }
         else
         {
             ZAlertWithParam(@"Alert", [responseObject objectForKey:@"message"]);
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             
             //[[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"Please check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"Please check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}

- (IBAction)buttonSignUpAction:(id)sender
{
    
}

- (IBAction)buttonForgetPass:(id)sender
{
    
    
    [UIView animateWithDuration:0.3 animations:^
     {
         _viewForgetPass.alpha = 1;
         _viewOverlay.alpha = 0.6;
         _viewForgetPass.frame = CGRectMake (self.view.frame.origin.x+20,240, self.view.frame.size.width-40, 173);
         _viewOverlay.frame = self.view.bounds;
         _viewForgetPass.center = self.view.center;
         _viewForgetPass.hidden =NO;
         _viewOverlay.hidden =NO;
     }];
    
    viewY = _viewForgetPass.frame.origin.y;
    
}


- (IBAction)buttonSendPassAction:(id)sender
{
    [self.view endEditing:YES];
    
    if (_viewForgetPass.frame.origin.y<viewY)
    {
        [self setViewMovedUp:NO];
        
    }
    
    
    _textFldEmailForget.text = [_textFldEmailForget.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (_textFldEmailForget.text.length==0) {
        
        [[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Please enter email address!" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil, nil]show];
        
    }
    else if(![self NSStringIsValidEmail:_textFldEmailForget.text])
    {
        [[[UIAlertView alloc]initWithTitle:@"Alert !" message:@"Please enter a valid email address!" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil, nil]show];
        
    }
    
    else{
        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
        NSDictionary *dictParam = @{@"email":_textFldEmailForget.text};
        
        [manager POST:@"forgotpass" parameters:dictParam success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
             {
                 [self removeOverlay];
             }
             else
             {
                 ZAlertWithParam(@"Alert", [responseObject objectForKey:@"message"]);
                 
             }
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 //[[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"Please check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"Please check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }];
        
    }
    
    
}

- (IBAction)buttonClosePassViewAction:(id)sender
{
    [self removeOverlay];
}

-(void)removeOverlay
{
    [self.view endEditing:YES];
    
    if (_viewForgetPass.frame.origin.y<viewY)
    {
        [self setViewMovedUp:NO];
        
    }
    
    [UIView animateWithDuration:0.3 animations:^
     {
         _viewForgetPass.frame = CGRectMake (_viewForgetPass.frame.origin.x, _viewForgetPass.frame.origin.y, 0, 0);
         _viewForgetPass.center = self.view.center;
         
         CGRect viewRect =_viewOverlay.frame;
         viewRect.size.width = 0;
         viewRect.size.height = 0;
         _viewOverlay.frame = viewRect;
         _viewOverlay.center = self.view.center;
         
         _viewOverlay.alpha = 0;
         _viewForgetPass.alpha = 0;
     }];
    
}

@end
