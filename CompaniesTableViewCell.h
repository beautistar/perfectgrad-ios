//
//  CompaniesTableViewCell.h
//  PerfectGraduate
//
//  Created by NSPL on 2/19/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompaniesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelInformation;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewBorder;
@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@end
