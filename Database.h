//
//  Database.h
//  Database
//
//  Created by IPS Brar on 24/03/15.
//  Copyright (c) 2015 NetSet Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Database : NSObject

@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;
-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;
-(void)copyDatabaseIntoDocumentsDirectory;

@property(strong,nonatomic)NSMutableArray *arrResults;
@property(strong,nonatomic)NSDictionary *dicTemp;

-(void)runQueries:(const char *)query  strForscreen:(NSString *)strForscreen;
-(void)runSubQueryLocations:(const char *)query  strForscreen:(NSString *)strForscreen;
-(void)runQuerySubDisciplines:(const char *)query strForscreen:(NSString *)strForscreen;
-(void)runQueryDisciplines:(const char *)query;
-(void)runQueryDispSubDispForCompanies:(const char *)query strForscreen:(NSString *)strForscreen;
-(void)runSubQueryToSaveDueDate:(const char *)query  strForscreen:(NSString *)strForscreen;
-(void)runSubQueryToSaveAwards:(const char *)query  strForscreen:(NSString *)strForscreen;
-(void)runQueryQuesAnsForCompanies:(const char *)query strForscreen:(NSString *)strForscreen;
-(void)runQuerySocialLinksForCompanies:(const char *)query strForscreen:(NSString *)strForscreen;
-(NSMutableArray*)getStates:(NSString *)query;


-(NSMutableArray*) loaddataSubDisciplines:(NSString *)query strForscreen:(NSString *)strForscreen;
-(NSMutableArray*) loaddata:(NSString *)query  strForscreen:(NSString *)strForscreen;
-(NSMutableArray*) loaddataCountries:(NSString *)query  strForscreen:(NSString *)strForscreen;
-(NSMutableArray*) loaddataStates:(NSString *)query ;
-(NSMutableArray*) loaddataSalaries:(NSString *)query;
-(NSMutableArray*) loaddataFromSubPortLocation:(NSString *)query  strForscreen:(NSString *)strForscreen;

-(NSMutableArray*) loadDataMessages:(NSString *)query;

-(NSMutableArray*) loaddataPortSubDisciplines:(NSString *)query  strForscreen:(NSString *)strForscreen;

-(NSMutableArray*) loaddataForDispSubDispCompanies:(NSString *)query strForscreen:(NSString *)strForscreen;
-(BOOL)deleteTable:(NSString*)strQuery;
-(NSMutableArray*) loaddataBanners:(NSString *)query;

-(NSMutableArray*) loadlocaldataCountries:(NSString *)query  strForscreen:(NSString *)strForscreen arrayId:(NSArray *)ids companyId:(NSArray *)arrayCompId;

-(NSMutableArray*) loadDisciplinedata:(NSString *)query  strForscreen:(NSString *)strForscreen array:(NSArray *)arraySubDicspline;
-(void)runQueryDispSubDispFordisp:(const char *)query strForscreen:(NSString *)strForscreen;
-(NSMutableArray*) loaddataForDispSubDisp:(NSString *)query strForscreen:(NSString *)strForscreen;
-(void)updateTable:(NSString *)query;

@property NSUInteger loadAttempt;
@end
