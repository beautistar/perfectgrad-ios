//
//  FilterTableViewCell.h
//  PerfectGraduate
//
//  Created by NSPL on 2/6/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *buttonCheckBoxOutlet;
- (IBAction)buttonCheckboxAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *labelRegionName;

@end
