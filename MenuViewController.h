//
//  MenuViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 2/17/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "LIALinkedInApplication.h"
#import "LIALinkedInHttpClient.h"
#import "SocietyDetailsVc.h"
#import "SocietySelectionVC.h"

@interface MenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableViewMenu;

@end
