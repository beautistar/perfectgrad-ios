//
//  SelectCountryTableViewCell.h
//  PerfectGraduate
//
//  Created by NSPL on 2/18/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectCountryTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelCountryName;
@property (strong, nonatomic) IBOutlet UIButton *buttonOutletCheckBox;

@end
