//
//  GamificationVC.m
//  PerfectGraduate
//
//  Created by netset on 4/23/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "GamificationVC.h"
#import "AppDelegate.h"

@interface GamificationVC ()
{
    NSMutableArray   *arrayForBool;
    NSDictionary *dicData;
    NSArray *arrKeys;
}

@end

@implementation GamificationVC

-(void)viewWillAppear:(BOOL)animated
{
    stringNotifiction = @"";
    stringForNotifyEditCompany = @"";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.navigationController.navigationBarHidden = YES;
    
    NSArray *arr1 = [[NSArray alloc]initWithObjects:@{@"name":@"Candidate",@"points":@"0+ Pts"},@{@"name":@"Good Candidate",@"points":@"2000+ Pts"},@{@"name":@"Popular Candidate",@"points":@"5000+ Pts"},@{@"name":@"Perfect Candidate",@"points":@"8000+ Pts"}, nil];
    
    dicData = [[NSDictionary alloc] initWithObjects:@[arr1,@[@"Display an interest in companies by adding them to your Portfolio page (50 points for each company added)",@"Show your support for Student Societies by selecting them from the Student Society page (75 points for each Student Society added)",@"Be proactive in requesting invitations from companies (150 points for each invite requested)",@"Receive confirmation from companies who have accepted your invitation requests. (400 points for each accepted invite received)"]] forKeys:@[@"Perfect Graduate status include:",@"How to earn points to help improve your status:"]];
    
    arrKeys = @[@"Perfect Graduate status include:",@"How to earn points to help improve your status:"];
    
    arrayForBool = [[NSMutableArray alloc]init];
    
    [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    [arrayForBool addObject:[NSNumber numberWithBool:NO]];
    [arrayForBool addObject:[NSNumber numberWithBool:NO]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark------UITableViewDataSource---------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0||section==1||section==2)
    {
        return 0;
    }
    else
    {
        if ([[arrayForBool objectAtIndex:section] boolValue])
        {
            if (section==3)
            {
                return [[dicData valueForKey:[arrKeys objectAtIndex:0]] count];
            }
            else
            {
                return [[dicData valueForKey:[arrKeys objectAtIndex:1]] count];
            }
        }
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.backgroundColor = [UIColor clearColor];
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    BOOL manyCells  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
    
    if (!manyCells)
    {
        cell.textLabel.text = @"click to enlarge";
    }
    
    else
    {
        if (indexPath.section==3)
        {
            UILabel *labelNames;
            
            if (labelNames==nil)
            {
                labelNames = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, tableView.frame.size.width/2, 40)];
                labelNames.textColor = [UIColor whiteColor];
            }
            labelNames.text =[[[dicData valueForKey:@"Perfect Graduate status include:"] valueForKey:@"name"] objectAtIndex:indexPath.row];
            labelNames.font = [UIFont fontWithName:@"OpenSans" size:14.0f];
            [cell.contentView addSubview:labelNames];
            
            UILabel *labelPoints;
            
            if (labelPoints==nil)
            {
                labelPoints = [[UILabel alloc] initWithFrame:CGRectMake(labelNames.frame.size.width+labelNames.frame.origin.x+25,0,tableView.frame.size.width-(labelNames.frame.size.width+labelNames.frame.origin.x+40), 40)];
                
                labelPoints.textColor = [UIColor whiteColor];
                
            }
            
            labelPoints.text =[[[dicData valueForKey:@"Perfect Graduate status include:"] valueForKey:@"points"] objectAtIndex:indexPath.row];
            
            labelPoints.font = [UIFont fontWithName:@"OpenSans-Bold" size:14.0f];
            [cell.contentView addSubview:labelPoints];
        }
        else if(indexPath.section==4)
        {
            UILabel *labelTxt;
            
            NSAttributedString *attributedText =[[NSAttributedString alloc]initWithString:[[dicData valueForKey:@"How to earn points to help improve your status:"]  objectAtIndex:indexPath.row]  attributes:@{NSFontAttributeName:[UIFont fontWithName:@"OpenSans" size:17.0f]}];
            
            CGRect rect = [attributedText boundingRectWithSize:(CGSize){300, CGFLOAT_MAX}options:NSStringDrawingUsesLineFragmentOrigin
                                                       context:nil];
            CGSize size = rect.size;
            CGFloat height=0;
            height=size.height;
            
            height +=10;
            
            if (indexPath.row==0)
            {
                height +=15;
            }
            
            
            labelTxt = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, tableView.frame.size.width-80, height)];
            labelTxt.textColor = [UIColor whiteColor];
            
            
            labelTxt.font = [UIFont fontWithName:@"OpenSans" size:14.0f];
            
            NSMutableAttributedString*  attString =
            [[NSMutableAttributedString alloc]
             initWithString: [[dicData valueForKey:@"How to earn points to help improve your status:"]  objectAtIndex:indexPath.row]];
            
            NSRange range1 = [[[dicData valueForKey:@"How to earn points to help improve your status:"]  objectAtIndex:indexPath.row] rangeOfString:@"("];
            
            if (range1.location == NSNotFound)
            {
                NSLog(@"The string (testString) does not contain 'how are you doing' as a substring");
            }
            else
            {
                NSLog(@"Found the range of the substring at (%lu, %lu)", (unsigned long)range1.location, range1.location + range1.length);
            }
            
            NSRange range2 = [[[dicData valueForKey:@"How to earn points to help improve your status:"]  objectAtIndex:indexPath.row] rangeOfString:@")"];
            
            if (range2.location == NSNotFound)
            {
                NSLog(@"The string (testString) does not contain 'how are you doing' as a substring");
            }
            else
            {
                NSLog(@"Found the range of the substring at (%lu, %lu)", (unsigned long)range2.location, range2.location + range2.length);
            }
            NSInteger diff = range2.location - range1.location;
            
            [attString addAttribute: NSFontAttributeName
                              value:  [UIFont fontWithName:@"OpenSans-Bold" size:14]
                              range: NSMakeRange(range1.location,diff+1)];
            
            labelTxt.numberOfLines = 0;
            labelTxt.attributedText = attString;
            [cell.contentView addSubview:labelTxt];
        }
    }
    return cell;
}

#pragma mark----UITableViewDelegate--------

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView  = [[UIView alloc] init];
    
    headerView.tag = section;
    
    UILabel *headerString = [[UILabel alloc] init];
    
    headerString.font = [UIFont fontWithName:@"OpenSans" size:14.0f];
    
    BOOL manyCells = [[arrayForBool objectAtIndex:section] boolValue];
    
    if (section==0)
    {
        headerString.text = @"► Increase your employment opportunities by improving your status.";
        
        headerView.frame = CGRectMake(0, 0, tableView.frame.size.width, 70);
        headerString.frame = CGRectMake(40, 0, tableView.frame.size.width-80, headerView.frame.size.height);
    }
    else if (section==1)
    {
        headerView.frame = CGRectMake(0, 0, tableView.frame.size.width, 100);
        headerString.frame = CGRectMake(40, 0, tableView.frame.size.width-80, headerView.frame.size.height);
        
        headerString.text = @"► Your Perfect Graduate status will be communicated to employers to help you stand out so they can find you faster. ";
    }
    else if (section==2)
    {
        headerString.text = @"► The higher your status the more your name will standout to employees.";
        headerView.frame = CGRectMake(0, 0, tableView.frame.size.width, 70);
        headerString.frame = CGRectMake(40, 0, tableView.frame.size.width-80, headerView.frame.size.height);
    }
    
    else
    {
        headerView.frame = CGRectMake(0, 0, tableView.frame.size.width, 60);
        headerString.frame = CGRectMake(70, 0, tableView.frame.size.width-140, headerView.frame.size.height);
        UIImageView *upDownArrow        = [[UIImageView alloc] initWithImage:manyCells ? [UIImage imageNamed:@"-veWhite"] : [UIImage imageNamed:@"add_white"]];
        upDownArrow.frame               = CGRectMake(40, self.view.frame.origin.y+20,20,20);
        
        if(section==3)
        {
            headerString.text = @"Perfect Graduate status include:";
        }
        else
        {
            upDownArrow.frame               = CGRectMake(40, self.view.frame.origin.y+10,20,20);
            headerString.text = @"How to earn points to help improve your status:";
        }
        
        upDownArrow.autoresizingMask    = UIViewAutoresizingFlexibleRightMargin;
        [headerView addSubview:upDownArrow];
        UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
        [headerView addGestureRecognizer:headerTapped];
    }
    
    headerString.textAlignment      = NSTextAlignmentLeft;
    headerString.textColor          = [UIColor whiteColor];
    headerString.backgroundColor = [UIColor clearColor];
    headerString.numberOfLines = 0;
    [headerView addSubview:headerString];
    
    return headerView;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footer  = [[UIView alloc] initWithFrame:CGRectZero];
    return footer;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==1)
    {
        return 100;
    }
    else if (section==2||section==0)
    {
        return 70;
    }
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[arrayForBool objectAtIndex:indexPath.section] boolValue])
        
    {
        if (indexPath.section==4)
            
        {
            NSAttributedString *attributedText=[[NSAttributedString alloc]initWithString:[[dicData valueForKey:@"How to earn points to help improve your status:"]  objectAtIndex:indexPath.row] attributes:@{NSFontAttributeName:[UIFont fontWithName:@"OpenSans" size:17.0f]}];
            
            CGRect rect = [attributedText boundingRectWithSize:(CGSize){300, CGFLOAT_MAX}
                                                       options:NSStringDrawingUsesLineFragmentOrigin context:nil];
            
            CGSize size = rect.size;
            
            CGFloat height=0;
            
            height=size.height;
            
            height +=10;
            
            if (indexPath.row==0)
                
            {
                height +=15;
            }
            
            
            return height;
        }
        
        else
            
        {
            return 40;
        }
        
    }
    
    return 0;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer

{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    
    if (indexPath.row == 0)
        
    {
        
        BOOL collapsed = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        
        collapsed = !collapsed;
        
        [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:collapsed]];
        
        //reload specific section animated
        NSRange range   = NSMakeRange(indexPath.section, 1);
        
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        
        [_tableViewGamification reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationFade];
        
        NSIndexPath *indPth = [NSIndexPath indexPathForItem:0 inSection:indexPath.section];
        
        [_tableViewGamification scrollToRowAtIndexPath:indPth atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
}


- (IBAction)buttonCancelAction:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


@end




