//
//  LoginTempUser.h
//  PerfectGraduate
//
//  Created by netset on 3/16/15.
//  Copyright (c) 2015 netset. All rights reserved.
//
#define kOFFSET_FOR_KEYBOARD 70.0

#import <UIKit/UIKit.h>
#import "PortfolioViewController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"

@interface LoginTempUser : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *buttonLogin;
@property (strong, nonatomic) IBOutlet UIButton *buttonRemember;
@property (strong, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (strong, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (strong, nonatomic) IBOutlet UIView *viewForgetPass;
@property (strong, nonatomic) IBOutlet UITextField *textFldEmailForget;
@property (strong, nonatomic) IBOutlet UIView *viewOverlay;
@property (strong, nonatomic) IBOutlet UIButton *buttonSendForgetPass;
@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;


- (IBAction)buttonRememberAction:(id)sender;
- (IBAction)buttonLoginAction:(id)sender;
- (IBAction)buttonSignUpAction:(id)sender;
- (IBAction)buttonForgetPass:(id)sender;
- (IBAction)buttonSendPassAction:(id)sender;
- (IBAction)buttonClosePassViewAction:(id)sender;

@end
