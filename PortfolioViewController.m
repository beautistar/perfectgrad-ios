

//
//  PortfolioViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 2/2/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "PortfolioViewController.h"
#import "AFNetworking.h"
#import "SWRevealViewController.h"
#import "CompaniesDetailViewController.h"
#import "DisciplineViewController.h"
#import "SearchLocationFilterViewController.h"
#import "MessagesViewController.h"
#import "SalaryGuideViewController.h"
#import "LocationFilterSearchViewController.h"
#import "AsyncImageView.h"
#import "ApiClass.h"
#import "Globals.h"
#import "MessageSubscriptionManager.h"

@interface PortfolioViewController ()
{
    
    UILabel *labelSubTitle;
    Database *dummy;
    UILabel *labelPoints;
    UIButton *buttonCandidatePoints;
    int imgCount;
    UIActivityIndicatorView *spinner;
    BOOL isEditingEnabled;
    BOOL isEditingEnabledStudent;
    UILabel *lblSocialMsgCount;
    UILabel *lblPgMsgCount;
    UIRefreshControl* refreshControl;
    UIRefreshControl* refreshControlSociety;
    BOOL isRefreshed, loadFromView;
    UIButton * buttonPgMsg;
    NSString *stringSocietyIcon;
    UIButton * buttonSocialMsg;
    
    UIView *viewAnim;
    
    NSString *stringCntryId ;
    
    NSArray *arrayFinalSaveData;
    
    dispatch_queue_t backgroundQueue;
    
    NSString *stringFirstCome;
    
    UIImageView *imgviewCircle;
    UIImageView *imgviewCircle1;
    
    CAShapeLayer *circle;
    CABasicAnimation *animation;
    
    int countForLabel, noOfCount;
    
    UILabel *progressLabel;
    
    UIView *viewFront;
    
    NSArray *arrayTempValues;
    
    NSInteger currentPage;
    UIButton * buttonEdit;
    
    NSString *strCompanyBadge;
    NSMutableArray *arrStudnetSocirtyData;
}

@end

@implementation PortfolioViewController

@synthesize companyBtn,awardsBtn,duebtn;

-(BOOL)canBecomeFirstResponder
{
    return YES;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
    [self becomeFirstResponder];
}
-(void)viewDidDisappear:(BOOL)animated
{
    [self resignFirstResponder];
    [super viewDidDisappear:NO];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [[MessageSubscriptionManager sharedManager] refreshData];

    backgroundQueue = dispatch_queue_create("com.razeware.imagegrabber.bgqueue", NULL);
    
    loadFromView = YES;
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, _imageRedStar.frame.origin.y, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    self.imageRedStar.hidden = YES;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [_protfolioTableView setEditing:NO animated:YES];
    isInternetOff = NO;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor lightGrayColor];
    
    refreshControlSociety = [[UIRefreshControl alloc] init];
    refreshControlSociety.backgroundColor = [UIColor clearColor];
    refreshControlSociety.tintColor = [UIColor lightGrayColor];
    
    [refreshControl addTarget:self
                       action:@selector(callWebService)
             forControlEvents:UIControlEventValueChanged];
    
    [refreshControlSociety addTarget:self
                       action:@selector(callWebService)
             forControlEvents:UIControlEventValueChanged];
    
    [_protfolioTableView addSubview:refreshControl];
    [self.tblStudentSociety addSubview:refreshControlSociety];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    
    self.viewBottomBar.backgroundColor = [UIColor whiteColor];
    _protfolioTableView.backgroundColor = [UIColor clearColor];
    
    dummy=[[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    arrayTempValues = [_arrayData copy];
    
    if (fromTermsCondition==YES)
    {
        [self saveOtherData];
    }
    
    [self addNavTitles];
    [self addLeftButtons];
    [self addRightButtons];
    [self addGamificationButton];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"stateAbr"])
    {
        [self setDueDateText:[NSString stringWithFormat:@"Application Due Dates \n%@ %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"stateAbr"],[[NSUserDefaults standardUserDefaults]objectForKey:@"programName"]]];
    }
    _labelCompany.layer.borderWidth = 0.5;
    _labelDueDate.layer.borderWidth = 0.5;
    
    _labelCompany.layer.borderColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0].CGColor;
    
    _labelDueDate.layer.borderColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0].CGColor;
    if(![Utilities CheckInternetConnection])
    {
    }
    else
    {
        if (fromTermsCondition==YES)
        {
            fromTermsCondition = NO;
            
            stringFirstCome = @"firstCome";
        }
        else
        {
            [self performSelectorInBackground:@selector(getStudentSocieties) withObject:nil];
            
            [self performSelectorInBackground:@selector(getAllCountries) withObject:nil];
        }
    }
    currentPage = 0;
    [self SetScrollView];
}

- (void)lblClick:(UITapGestureRecognizer *)tapGesture
{
    UILabel *label = (UILabel *)tapGesture.view;
    label.textColor = [UIColor redColor];
}

-(void)checkSocialLinks
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:20];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"CheckSocial" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] boolValue])
         {
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"fb"] forKey:@"FbLogIn"];
             [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"linkedIn"] forKey:@"LinkedInLogIn"];
             [[NSUserDefaults standardUserDefaults] synchronize];
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
     }];
}

-(void)callWebService
{
    if(currentPage == 1){
        [self CallStudentSocitey];
    }else{
        isRefreshed = YES;
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callPortWebService"];
        [self reachabilityCheck];
    }
    
}

-(void)addGamificationButton
{
    buttonCandidatePoints = [[UIButton alloc]init];
    buttonCandidatePoints.frame = CGRectMake(self.view.frame.origin.x+20,self.view.frame.size.height-88, self.view.frame.size.width-40,45);
    [buttonCandidatePoints addTarget:self action:@selector(buttonGami) forControlEvents:UIControlEventTouchUpInside];
    
    labelPoints = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+20,self.view.frame.size.height-95, self.view.frame.size.width-40,45)];
    labelPoints.textAlignment = NSTextAlignmentCenter;
    labelPoints.font = [UIFont fontWithName:@"OpenSans" size:13];
    
    labelPoints.autoresizingMask = UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleTopMargin;
    
    buttonCandidatePoints.autoresizingMask = UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleTopMargin;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"])
    {
        NSInteger points = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"] integerValue];
        [self  gamificationNumbers:points];
    }
    labelPoints.textColor   = [UIColor whiteColor];
    [labelPoints setShadowColor:[UIColor blackColor]];
    labelPoints.shadowOffset = CGSizeMake(0.8, 0.8);
    
    [self.view addSubview:buttonCandidatePoints];
    [self.view addSubview:labelPoints];
}

-(void)addNavTitles
{
    UIView  *textView;
    
    int rect = self.view.frame.size.height;
    
    if (rect == 736)
    {
        textView  =[[UIView alloc]initWithFrame:CGRectMake(-15, 0, 235, 40)];
    }
    else  if (rect> 568)
    {
        textView  =[[UIView alloc]initWithFrame:CGRectMake(-15, 0, 205, 40)];
    }
    else
    {
        textView  =[[UIView alloc]initWithFrame:CGRectMake(-15, 0, 150, 40)];
    }
    
    UILabel *labelTitleName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 0, CGRectGetWidth(textView.frame), 22)];
    labelTitleName.backgroundColor = [UIColor clearColor];
    labelTitleName.font = [UIFont fontWithName:@"OpenSans" size:18.0f];
    labelTitleName.textAlignment = NSTextAlignmentCenter;
    labelTitleName.textColor = [UIColor whiteColor];
    labelTitleName.text = @"Portfolio";
    
    /*----UILabel Sub-title Name-------*/
    labelSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 22, CGRectGetWidth(textView.frame), 18)];
    labelSubTitle.backgroundColor = [UIColor clearColor];
    labelSubTitle.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
    labelSubTitle.textAlignment = NSTextAlignmentCenter;
    labelSubTitle.textColor = [UIColor whiteColor];
    [textView addSubview:labelSubTitle];
    [textView addSubview:labelTitleName];
    
    self.navigationItem.titleView=textView;
}

-(void)addLeftButtons
{
    UIView *viewAddBtnsLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 80, 40)];
    
    UIButton * buttonMenu = [[UIButton alloc]initWithFrame:CGRectMake(-3, 4, 30, 30) ];
    
    [buttonMenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    
    buttonMenu.backgroundColor = [UIColor clearColor];
    
    
    [buttonMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    [viewAddBtnsLeft addSubview:buttonMenu];
    
    buttonEdit = [[UIButton alloc]initWithFrame:CGRectMake(40, 4, 30, 30)];
    buttonEdit.backgroundColor = [UIColor clearColor];
    [buttonEdit addTarget:self action:@selector(editPressed:) forControlEvents:UIControlEventTouchUpInside];
    [buttonEdit setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
    [viewAddBtnsLeft addSubview:buttonEdit];
    
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:viewAddBtnsLeft];
}

-(void)addRightButtons
{
    UIView *viewBtnsRight = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 65, 40)];
    
    buttonSocialMsg = [[UIButton alloc]initWithFrame:CGRectMake(0,5, 30, 30)];
    
    [buttonSocialMsg setImage:[UIImage imageNamed:@"societies"] forState:UIControlStateNormal];
    
    buttonSocialMsg.layer.cornerRadius = 5.0;
    buttonSocialMsg.layer.borderColor = [UIColor whiteColor].CGColor;
    buttonSocialMsg.layer.borderWidth = 1.0f;
    
    [buttonSocialMsg addTarget:self action:@selector(buttonGroupAction) forControlEvents:UIControlEventTouchUpInside];
    
    [viewBtnsRight addSubview:buttonSocialMsg];
    
    buttonPgMsg = [[UIButton alloc]initWithFrame:CGRectMake(38,5, 32, 30)];
    buttonPgMsg.layer.cornerRadius = 5.0;
    buttonPgMsg.layer.borderColor = [UIColor whiteColor].CGColor;
    buttonPgMsg.layer.borderWidth = 1.0f;
    [buttonPgMsg addTarget:self action:@selector(buttonPerfectGraduateAction) forControlEvents:UIControlEventTouchUpInside];
    
    [viewBtnsRight addSubview:buttonPgMsg];
    
    lblPgMsgCount=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(buttonPgMsg.frame)-8, CGRectGetMinY(buttonPgMsg.frame)-8, 16, 16)];
    
    [buttonPgMsg setImage:[UIImage imageNamed:@"PerfectGrd"] forState:UIControlStateNormal];
    
    lblPgMsgCount.layer.cornerRadius= CGRectGetHeight(lblPgMsgCount.frame)/2;
    lblPgMsgCount.clipsToBounds=YES;
    lblPgMsgCount.hidden = YES;
    
    lblPgMsgCount.font = [UIFont fontWithName:@"OpenSans" size:10];
    lblPgMsgCount.backgroundColor=[UIColor colorWithRed:255.0f/255.0f green:37.0f/255.0f blue:41.0f/255.0f alpha:1.0];
    lblPgMsgCount.textColor = [UIColor whiteColor];
    lblPgMsgCount.textAlignment = NSTextAlignmentCenter;
    [viewBtnsRight addSubview:lblPgMsgCount];
    
    lblSocialMsgCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(buttonSocialMsg.frame)-12, CGRectGetMinY(buttonSocialMsg.frame)-8, 16, 16)];
    lblSocialMsgCount.layer.cornerRadius= CGRectGetHeight(lblSocialMsgCount.frame)/2;
    lblSocialMsgCount.clipsToBounds=YES;
    lblSocialMsgCount.font = [UIFont fontWithName:@"OpenSans" size:10];
    lblSocialMsgCount.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:37.0f/255.0f blue:41.0f/255.0f alpha:1.0];
    lblSocialMsgCount.textColor = [UIColor whiteColor];
    lblSocialMsgCount.textAlignment = NSTextAlignmentCenter;
    lblSocialMsgCount.hidden = YES;
    [viewBtnsRight addSubview:lblSocialMsgCount];
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:viewBtnsRight];
}

-(void)SetScrollView{
    self.PortScrollView.delegate = self;
    self.PortScrollView.contentSize = CGSizeMake(([UIScreen mainScreen].bounds.size.width)*2, self.PortScrollView.frame.size.height);
    CGRect theRect = self.tblStudentSociety.frame;
    theRect.origin.x = [UIScreen mainScreen].bounds.size.width-1;
    theRect.size.width = [UIScreen mainScreen].bounds.size.width+1;
    self.tblStudentSociety.frame = theRect;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if([scrollView isEqual:self.PortScrollView]){
        CGFloat width = scrollView.frame.size.width;
        NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
        
        currentPage = page;
        
        if(scrollView.contentOffset.x<=([UIScreen mainScreen].bounds.size.width/2)){
            self.labelCompany.text = @"Company";
            self.labelDueDate.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
            
            [buttonSocialMsg setImage:[UIImage imageNamed:@"societies"] forState:UIControlStateNormal];
        }else{
            self.labelCompany.text = @"Student Society";
            self.labelDueDate.textColor = [UIColor colorWithRed:131.0f/255.0f green:131.0f/255.0f blue:131.0f/255.0f alpha:1.0f];
            [buttonSocialMsg setImage:[UIImage imageNamed:@"portRemoved"] forState:UIControlStateNormal];
        }
        [self setCountLabel];
    }
    
}


-(void)setFrameAfterPaging{
    NSLog(@"page:%ld",(long)currentPage);
    if (currentPage == 0) {
        self.labelCompany.text = @"Company";
        self.labelDueDate.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        
        [buttonSocialMsg setImage:[UIImage imageNamed:@"societies"] forState:UIControlStateNormal];
    }else{
        self.labelCompany.text = @"Student Society";
        self.labelDueDate.textColor = [UIColor colorWithRed:131.0f/255.0f green:131.0f/255.0f blue:131.0f/255.0f alpha:1.0f];
        [buttonSocialMsg setImage:[UIImage imageNamed:@"ICO_Company"] forState:UIControlStateNormal];
    }
    [self setCountLabel];
}

-(void)setCountLabel{
    if(currentPage == 0){
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"societyCount"] integerValue]!=0)
        {
            lblSocialMsgCount.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"societyCount"];
            lblSocialMsgCount.hidden = NO;
        }
        else
        {
            lblSocialMsgCount.hidden = YES;
        }
    }else{
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"companyCount"] integerValue]!=0)
        {
            lblSocialMsgCount.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"companyCount"];
            lblSocialMsgCount.hidden = NO;
        }
        else
        {
            lblSocialMsgCount.hidden = YES;
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self reachabilityCheck];
    [[MessageSubscriptionManager sharedManager] refreshData];

    _protfolioTableView.userInteractionEnabled = YES;
    
    stringForNotifyEditCompany = @"portfoilio";
    
    stringNotifiction = @"portfoilio";
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"])
    {
        self.imageRedStar.hidden =YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationItem.hidesBackButton = YES;
    
    [companyBtn setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+61, self.view.frame.size.width/3,38)];
    [awardsBtn setFrame:CGRectMake(companyBtn.frame.origin.x+companyBtn.frame.size.width, self.view.frame.origin.y+61, self.view.frame.size.width/3, 38)];
    [duebtn setFrame:CGRectMake(awardsBtn.frame.origin.x+awardsBtn.frame.size.width, self.view.frame.origin.y+61, self.view.frame.size.width/3,38)];
    
    _buttonPortfolio.backgroundColor = [UIColor clearColor];
    [_buttonPortfolio setImage:[UIImage imageNamed:@"portfolio1"] forState:UIControlStateNormal];
    _labelPortfolio.textColor=[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    if ([stringPortCompanyScreen isEqualToString:@"fromComapny"])
    {
        if ( fromNotification == YES)
        {
            [self getUserCompanies];
            
            fromNotification = NO;
        }
        else
        {
            
            _arrayData = nil;
            
            _arrayData = [[NSArray alloc]init];
            
            _arrayData = [arrayPortDataGlobal copy];
            
            labelSubTitle.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userDiscipline"];
            
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"adminCount"] integerValue]!=0)
            {
                lblPgMsgCount.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"adminCount"];
                lblPgMsgCount.hidden = NO;
            }
            else
            {
                lblPgMsgCount.hidden = YES;
            }
            if (_arrayData.count==0)
            {
                [_protfolioTableView reloadData];
            }
            else
            {
                stringAdminLogo = [NSString stringWithFormat:@"%@", [[_arrayData valueForKeyPath:@"perfect_logo"] objectAtIndex:0]];
                
                [_protfolioTableView reloadData];
            }
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"])
            {
                NSInteger points = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userPoints"] integerValue];
                [self  gamificationNumbers:points];
            }
            
            int socCount = [lblSocialMsgCount.text intValue];
            
            int adminCount = [lblPgMsgCount.text intValue];
            
            int copmanyCount = 0;
            
            arrayCompanyIdsContain = [[NSMutableArray alloc]init];
            
            for (int i=0; i< _arrayData.count; i++)
            {
                NSString *stringCount = [NSString stringWithFormat:@"%@",[[_arrayData valueForKey:@"unread_message"] objectAtIndex:i]];
                for (int k=0; k<[[[_arrayData valueForKey:@"links"] objectAtIndex:i] count]; k++)
                {
                    [arrayCompanyIdsContain addObject:[[[_arrayData valueForKeyPath:@"links.company_id"] objectAtIndex:i] objectAtIndex:0]];
                }
                int count = [stringCount intValue];
                
                copmanyCount = copmanyCount + count;
            }
            
            int totalCount = socCount + adminCount + copmanyCount;
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",totalCount] forKey:@"badgeCount"];
            
            [UIApplication sharedApplication].applicationIconBadgeNumber = [[[NSUserDefaults standardUserDefaults]objectForKey:@"badgeCount"] intValue];
        }
    }
    
    else
    {
        
        [self reachabilityCheck];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(callPorfolio)
                                                 name:@"callPorfolio"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"messageNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(redStarShow)
                                                 name:@"redStarShow"
                                               object:nil];
    
    NSString *filePath = [self getSocietyArrPath];
    arrStudnetSocirtyData = [NSMutableArray arrayWithContentsOfFile:filePath];
    
    [self CallStudentSocitey];
    [self setCountLabel];
}

-(NSString*)getSocietyArrPath{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [path objectAtIndex:0];
    return [documentFolder stringByAppendingFormat:@"%@_myfile.plist",[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]];
}

-(void)CallStudentSocitey{
    if(![Utilities CheckInternetConnection])
    {
        NSLog(@"notConnected");
        [self ShowDataLoader:NO];
        if ([refreshControlSociety isRefreshing])
        {
            [refreshControlSociety endRefreshing];
        }
    }else{
        if (arrStudnetSocirtyData.count == 0){
            [self ShowDataLoader:YES];
        }
        
        NSString* urlStr =  [NSString stringWithFormat:@"%@user_society",baseUrl];
        
        NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
        
        [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             //if (_arrayData.count!=0){
                 [self ShowDataLoader:NO];
             //}
             
             if ([refreshControlSociety isRefreshing])
             {
                 [refreshControlSociety endRefreshing];
             }
             if([responseObject objectForKey:@"status"]){
                 NSString * strStatus = [[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"status"]] removeNull];
                 if ([strStatus isEqualToString:@"true"])
                 {
                     if([responseObject objectForKey:@"badge_count"]){
                         NSString *badgeCount = [[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"badge_count"]] removeNull];
                         [[NSUserDefaults standardUserDefaults] setObject:badgeCount forKey:@"badgeCount"];
                         
                         [UIApplication sharedApplication].applicationIconBadgeNumber = [badgeCount intValue];
                     }
                     if([responseObject objectForKey:@"data"]){
                         if([[responseObject objectForKey:@"data"] isKindOfClass:[NSArray class]]){
                             arrStudnetSocirtyData = [[NSMutableArray alloc]initWithArray:[responseObject objectForKey:@"data"]];
                             NSString *filePath = [self getSocietyArrPath];
                             [arrStudnetSocirtyData writeToFile:filePath atomically:YES];
                             [self.tblStudentSociety reloadData];
                         }else if([[responseObject objectForKey:@"data"] isKindOfClass:[NSDictionary class]]){
                             arrStudnetSocirtyData = [NSMutableArray arrayWithObject:[responseObject objectForKey:@"data"]];
                             NSString *filePath = [self getSocietyArrPath];
                             [arrStudnetSocirtyData writeToFile:filePath atomically:YES];
                             [self.tblStudentSociety reloadData];
                         }
                     }
                 }
             }
             
         }
                                                 failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             [self ShowDataLoader:NO];
             
             if ([refreshControlSociety isRefreshing])
             {
                 [refreshControlSociety endRefreshing];
             }
             
         }];
    }
}



-(void)callPorfolio
{
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callMsgWebService"];
    
    //[self ShowDataLoader:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
                   {
                       [self getUserCompanies];
                   });
}

-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    fromNotification = NO;
    
    if ([[notification name] isEqualToString:@"messageNotification"])
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callMsgWebService"];
    
    if (boolNotifyEditCompanyPort == YES)
    {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
            
            [self getUserCompanies];
        });
    }
    else
    {
        //[self ShowDataLoader:YES];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
            
            [self getUserCompanies];
        });
    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    arrayPortDataGlobal = [_arrayData mutableCopy];
    [super viewWillDisappear:YES];
    [_protfolioTableView setEditing:NO animated:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"messageNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];
}


-(void)reachabilityCheck
{
    if(![Utilities CheckInternetConnection])
    {
        NSLog(@"notConnected");
        
        isInternetOff = YES;
        
        [self loadDataOffLine];
        if ([refreshControl isRefreshing])
        {
            [refreshControl endRefreshing];
        }
    }
    else
    {
        isInternetOff = NO;
        
        NSLog(@"connected");
        
        showInternetAlert = NO;
        
        if ([[NSUserDefaults standardUserDefaults]boolForKey:@"callPortWebService"])
        {
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"callPortWebService"];
            
            if (boolNotifyEditCompanyPort== YES)
            {
                [self performSelectorInBackground:@selector(getUserCompanies) withObject:nil];
            }
            else
            {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    
                    [self getUserCompanies];
                });
                
                
            }
        }
        
        else if (![refreshControl isRefreshing])
        {
            [self loadDataOffLine];
        }
        
    }
}

#pragma mark-----UIActivityIndicatorView

-(void)ShowDataLoader:(BOOL)set
{
    @try {
        if (set)
        {
            if([Utilities CheckInternetConnection])
            {
                NSLog(@"Connected");
                if (!spinner)
                {
                    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                }
                
                if(spinner)
                {
                    spinner.center = self.view.center;
                    spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
                    [spinner startAnimating];
                    [self.navigationController.view setUserInteractionEnabled:NO];
                    [self.view addSubview:spinner];
                }
            }
            else
            {
                NSLog(@"Connected");
            }
        }
        else
        {
            [self.navigationController.view setUserInteractionEnabled:YES];
            
            if(spinner)
            {
                [spinner stopAnimating];
                [spinner removeFromSuperview];
                spinner = nil;
            }

        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}

-(void)loadDataOffLine
{
    if ([[[dummy loaddata:@"Select * From PORTFOLIO" strForscreen:@"port"] copy] count]>0)
    {
        isInternetOff = YES;
        
        [self showPortfolioCompanies];
    }
    else
    {
        isInternetOff = NO;
        
        [self performSelectorInBackground:@selector(getUserCompanies) withObject:nil];
    }
}



-(void)showPortfolioCompanies
{
    _arrayData = nil;
    
    _arrayData = [NSArray new];
    
    _arrayData = [[NSArray alloc] initWithArray:[dummy loaddata:@"Select * From PORTFOLIO" strForscreen:@"port"]];
    
    labelSubTitle.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userDiscipline"];
    
    [self setCountLabel];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"adminCount"] integerValue]!=0)
    {
        lblPgMsgCount.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"adminCount"];
        lblPgMsgCount.hidden = NO;
    }
    else
    {
        lblPgMsgCount.hidden = YES;
    }
    if (_arrayData.count==0)
    {
        [self ShowDataLoader:NO];
        
        [_protfolioTableView reloadData];
    }
    else
    {
        stringAdminLogo =[NSString stringWithFormat:@"%@",[[_arrayData valueForKeyPath:@"perfect_logo"] objectAtIndex:0]];
        
        if (boolFomAppDele == YES)
        {
            [self reloadDataTable];
            
            boolFomAppDele = NO;
        }
        else
        {
            [self reloadDataTable];
        }
        
        int socCount = [lblSocialMsgCount.text intValue];
        
        int adminCount = [lblPgMsgCount.text intValue];
        
        int copmanyCount = 0;
        
        arrayCompanyIdsContain = [[NSMutableArray alloc]init];
        
        for (int i=0; i< _arrayData.count; i++)
        {
            NSString *stringCount = [NSString stringWithFormat:@"%@",[[_arrayData valueForKey:@"unread_message"] objectAtIndex:i]];
            for (int k=0; k<[[[_arrayData valueForKey:@"links"] objectAtIndex:i] count]; k++)
            {
                [arrayCompanyIdsContain addObject:[[[_arrayData valueForKeyPath:@"links.company_id"] objectAtIndex:i] objectAtIndex:0]];
            }
            int count = [stringCount intValue];
            
            copmanyCount = copmanyCount + count;
        }
    }
    
    int TotalCount = 0;
    for(NSDictionary *dict in self.arrayData){
        if([dict objectForKey:@"unread_message"]){
            int Count = [[[NSString stringWithFormat:@"%@",[dict objectForKey:@"unread_message"]] removeNull] intValue];
            TotalCount = TotalCount+Count;
        }
    }
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",TotalCount] forKey:@"companyCount"];
    [self setCountLabel];
    
    if (_arrayData.count==0)
    {
        //[[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nThe network connection was lost." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
    }
}


-(void)perfectAdminIconMethod
{
    if (isInternetOff)
    {
        [buttonPgMsg setImage:[UIImage imageWithContentsOfFile:[[_arrayData valueForKeyPath:@"perfect_logo"] objectAtIndex:0]] forState:UIControlStateNormal] ;
    }
    else
    {
        if ([[[_arrayData valueForKeyPath:@"perfect_logo"] objectAtIndex:0] hasPrefix:@"/Users/"])
        {
            [buttonPgMsg setImage:[UIImage imageWithContentsOfFile:[[_arrayData valueForKeyPath:@"perfect_logo"] objectAtIndex:0]] forState:UIControlStateNormal];
        }
        
        else if ([[[_arrayData valueForKeyPath:@"perfect_logo"] objectAtIndex:0] hasPrefix:@"/var/mobile/Containers/"])
        {
            [buttonPgMsg setImage:[UIImage imageWithContentsOfFile:[[_arrayData valueForKeyPath:@"perfect_logo"] objectAtIndex:0]] forState:UIControlStateNormal];
        }
        else
        {
            UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[[_arrayData valueForKeyPath:@"perfect_logo"]objectAtIndex:0]]]]];
            
            [buttonPgMsg setImage:imge forState:UIControlStateNormal];
        }
        buttonPgMsg.contentMode = UIViewContentModeScaleAspectFit;
    }
}

-(void)societyIconMethod
{
    if (isInternetOff)
    {
        [buttonSocialMsg setImage:[UIImage imageWithContentsOfFile:[[_arrayData valueForKeyPath:@"society_logo"] objectAtIndex:0]] forState:UIControlStateNormal] ;
    }
    else
    {
        if ([[[_arrayData valueForKeyPath:@"society_logo"] objectAtIndex:0] hasPrefix:@"/Users/"])
        {
            [buttonSocialMsg setImage:[UIImage imageWithContentsOfFile:[[_arrayData valueForKeyPath:@"society_logo"] objectAtIndex:0]] forState:UIControlStateNormal] ;
        }
        
        else if ([[[_arrayData valueForKeyPath:@"society_logo"] objectAtIndex:0] hasPrefix:@"/var/mobile/Containers/"])
        {
            [buttonSocialMsg setImage:[UIImage imageWithContentsOfFile:[[_arrayData valueForKeyPath:@"society_logo"] objectAtIndex:0]] forState:UIControlStateNormal];
        }
        else
        {
            UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[[_arrayData valueForKeyPath:@"society_logo"]objectAtIndex:0]]]]];
            
            [buttonSocialMsg setImage:imge forState:UIControlStateNormal];
        }
        
        buttonSocialMsg.contentMode = UIViewContentModeScaleAspectFit;
    }
}

#pragma mark-------didReceiveMemoryWarning------------

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark-------UITableViewDataSource------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.tblStudentSociety]) {
        return arrStudnetSocirtyData.count;
    }
    return [_arrayData count];
}

-(void)OpenLinkInSafari:(UIButton*)sender{
    NSDictionary *dictSociety = [arrStudnetSocirtyData objectAtIndex:sender.tag];
    NSString *stringUrl = [dictSociety objectForKey:@"website"];
    
    if (![stringUrl containsString:@"http://"])
    {
        stringUrl = [NSString stringWithFormat:@"http://%@",stringUrl];
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringUrl]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cellID";
    
    PortfolioTableCell *cell = (PortfolioTableCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PortfolioTableCell" owner:self options:nil];
        
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.btnBackground.tag = indexPath.row;
    
    cell.buttonMessageOutlet.tag = indexPath.row;
    
    cell.logoImageView.layer.cornerRadius = 5.0f;
    cell.logoImageView.clipsToBounds = YES;
    
    cell.bgImageView.layer.borderColor = [[UIColor colorWithRed:215.0f/255.0f green:215.0f/255.0f blue:215.0f/255.0f alpha:1.0] CGColor];
    cell.bgImageView.layer.cornerRadius=3;
    cell.bgImageView.clipsToBounds=YES;
    cell.bgImageView.layer.borderWidth = 0.3;
    
    cell.companyNameLabel.textColor=[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    CGFloat height = 38;
    cell.companyNameLabel.font = [UIFont fontWithName:@"OpenSans" size:14];
    cell.labelDate.hidden = NO;
    
    [cell.buttonMessageOutlet addTarget:nil action:@selector(buttonMessageAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([tableView isEqual:self.tblStudentSociety]) {
        cell.labelDate.hidden = YES;
        
        height = 54;
        cell.btnProfile.hidden = NO;
        cell.btnProfile.tag = indexPath.row;
        [cell.btnProfile addTarget:self action:@selector(OpenLinkInSafari:) forControlEvents:UIControlEventTouchUpInside];
        NSDictionary *dictSociety = [arrStudnetSocirtyData objectAtIndex:indexPath.row];
        if ([dictSociety objectForKey:@"society_name"]){
            cell.companyNameLabel.text = [[NSString stringWithFormat:@"%@",[dictSociety objectForKey:@"society_name"]] removeNull];
        }else{
            cell.companyNameLabel.text = @"";
        }
        
        if([dictSociety objectForKey:@"societylogo"]){
            NSString *strImgName = [[NSString stringWithFormat:@"%@",[dictSociety valueForKey:@"societylogo"]] removeNull];
            if(![strImgName isEqualToString:@""]){
                NSString *stringIm = [NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,strImgName]]];
                cell.logoImageView.imageURL = [NSURL URLWithString:stringIm];
            }
        }
        cell.labelMessageCount.hidden = YES;
        if ([dictSociety objectForKey:@"unread_message"]){
            NSString *strCount = [[NSString stringWithFormat:@"%@",[dictSociety objectForKey:@"unread_message"]] removeNull];
            if(![strCount isEqualToString:@"0"] || [strCount isEqualToString:@""]){
                cell.labelMessageCount.hidden = NO;
                [cell.labelMessageCount setTitle:strCount forState:UIControlStateNormal];
            }
        }
        
        //msg007
        BOOL isMessagesStopped=NO;
        //isMessagesStopped=[[[_arrayData valueForKey:@"message_status"] objectAtIndex:indexPath.row] boolValue];
        NSLog(@"Messages Stopped:%@",isMessagesStopped?@"YES":@"NO");
        isMessagesStopped=![[MessageSubscriptionManager sharedManager] isMessageSubscribedForId:[NSString stringWithFormat:@"%@",[dictSociety valueForKey:@"id"]] isCompany:NO];
        NSLog(@"Messages StoppedNN:%@ For dictSociety:%@",isMessagesStopped?@"YES":@"NO",[NSString stringWithFormat:@"%@",dictSociety]);
        
        if ([[cell.labelMessageCount.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@"0"])
        {
            cell.labelMessageCount.hidden =YES;
            if (isMessagesStopped)
            {
                [cell.buttonMessageOutlet setImage:[UIImage imageNamed:@"msgStopPort"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.buttonMessageOutlet setImage:[UIImage imageNamed:@"companyMessages"] forState:UIControlStateNormal];
            }
        }
        else
        {
            cell.labelMessageCount.hidden =NO;
            
            if (isMessagesStopped)
            {
                [cell.buttonMessageOutlet setImage:[UIImage imageNamed:@"msgStopPort"] forState:UIControlStateNormal];
                cell.labelMessageCount.hidden = YES;
            }
            else
            {
                [cell.buttonMessageOutlet setImage:[UIImage imageNamed:@"companyMessages"] forState:UIControlStateNormal];
                cell.labelMessageCount.hidden = NO;
            }
        }

    }else{
        cell.btnProfile.hidden = YES;
        cell.companyNameLabel.text= [[_arrayData valueForKey:@"company_name"] objectAtIndex:indexPath.row ];
        
        if (![[[_arrayData valueForKey:@"application_due_date"] objectAtIndex:indexPath.row] isEqualToString:@"no"]&&![[[_arrayData valueForKey:@"application_due_date"] objectAtIndex:indexPath.row] isEqualToString:@"Open Until Filled"])
        {
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"yyyy/MM/dd";
            NSDate *yourDate = [dateFormatter dateFromString:[[_arrayData valueForKey:@"application_due_date"] objectAtIndex:indexPath.row]];
            dateFormatter.dateFormat = @"dd-MMM-yyyy";
            cell.labelDate.text = [[dateFormatter stringFromDate:yourDate] uppercaseString];
        }
        else if ([[[_arrayData valueForKey:@"application_due_date"] objectAtIndex:indexPath.row ] isEqualToString:@"Open Until Filled"])
        {
            cell.labelDate.text = @"Open Until Filled";
        }
        else
        {
            cell.labelDate.text = @"Open Until Filled";
        }
        if(isInternetOff)
        {
            if ([[[_arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row] isEqualToString:@"N"]||[[_arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row]==0)
            {
                [cell.logoImageView setImage:[UIImage imageNamed:@"defaultBuilding"]];
            }
            else
            {
                cell.logoImageView.image = [UIImage imageWithContentsOfFile:[[_arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row]];
            }
        }
        else
        {
            if ([[[_arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row] isEqualToString:@"N"]||[[_arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row]==0)
            {
                [cell.logoImageView setImage:[UIImage imageNamed:@"defaultBuilding"]];
            }
            
            if ([[[_arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row] hasPrefix:@"/Users/"])
            {
                [cell.logoImageView setImage:[UIImage imageWithContentsOfFile:[[_arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row]]];
            }
            
            else if ([[[_arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row] hasPrefix:@"/var/mobile/Containers/"])
            {
                [cell.logoImageView setImage:[UIImage imageWithContentsOfFile:[[_arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row]]];
            }
            else
            {
                NSString *stringIm = [NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[[_arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row]]]];
                
                
                cell.logoImageView.imageURL = [NSURL URLWithString:stringIm];
            }
        }
        
        [cell.labelMessageCount setTitle:[NSString stringWithFormat:@"%@",[[_arrayData valueForKey:@"unread_message"] objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        
        //msg007
        BOOL isMessagesStopped=NO;
        isMessagesStopped=[[[_arrayData valueForKey:@"message_status"] objectAtIndex:indexPath.row] boolValue];
        NSLog(@"Messages Stopped:%@",isMessagesStopped?@"YES":@"NO");
        isMessagesStopped=![[MessageSubscriptionManager sharedManager] isMessageSubscribedForId:[NSString stringWithFormat:@"%@",[[_arrayData valueForKey:@"id"] objectAtIndex:indexPath.row]] isCompany:YES];
        NSLog(@"Messages StoppedNN:%@ For CompanyId:%@",isMessagesStopped?@"YES":@"NO",[NSString stringWithFormat:@"%@",[[_arrayData valueForKey:@"id"] objectAtIndex:indexPath.row]]);

        if ([[cell.labelMessageCount.titleLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@"0"])
        {
            cell.labelMessageCount.hidden =YES;
            if (isMessagesStopped)
            {
                [cell.buttonMessageOutlet setImage:[UIImage imageNamed:@"msgStopPort"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.buttonMessageOutlet setImage:[UIImage imageNamed:@"companyMessages"] forState:UIControlStateNormal];
            }
        }
        else
        {
            cell.labelMessageCount.hidden =NO;
            
            if (isMessagesStopped)
            {
                [cell.buttonMessageOutlet setImage:[UIImage imageNamed:@"msgStopPort"] forState:UIControlStateNormal];
                cell.labelMessageCount.hidden = YES;
            }
            else
            {
                [cell.buttonMessageOutlet setImage:[UIImage imageNamed:@"companyMessages"] forState:UIControlStateNormal];
                cell.labelMessageCount.hidden = NO;
            }
        }
        
        if ([[_arrayData valueForKey:@"company_intro"] objectAtIndex:indexPath.row])
        {
            cell.descriptionlabel.text = [NSString stringWithFormat:@"%@",[[_arrayData valueForKey:@"company_intro"] objectAtIndex:indexPath.row]];
        }
        
        [self setAwardPhotos:indexPath cellTable:cell];
    }
    
    
    
    CGRect theRect = cell.companyNameLabel.frame;
    theRect.size.height = height;
    cell.companyNameLabel.frame = theRect;
    
    cell.descriptionlabel.textColor = [UIColor colorWithRed:131.0f/255.0f green:131.0f/255.0f blue:131.0f/255.0f alpha:1.0f];
    
    
    cell.separatorInset = UIEdgeInsetsMake(0,10000,0,0);
    
    return cell;
}


#pragma mark---------UITableViewDelegate-----------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PortfolioTableCell *cell = (PortfolioTableCell *) [tableView cellForRowAtIndexPath:indexPath];
    cell.bgImageView.backgroundColor=[UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.10];
    if ([tableView isEqual:self.tblStudentSociety]) {
        self.tblStudentSociety.userInteractionEnabled = NO;
        [self performSelector:@selector(openLink:) withObject:indexPath afterDelay:0.2];
        
    }else{
        _protfolioTableView.userInteractionEnabled = NO;
        [self performSelector:@selector(moveNextController:) withObject:indexPath afterDelay:0.2];
    }
}
-(void)openLink:(NSIndexPath *)indexPath
{
    PortfolioTableCell *cell = (PortfolioTableCell *) [self.tblStudentSociety cellForRowAtIndexPath:indexPath];
    cell.bgImageView.backgroundColor=[UIColor whiteColor];
    self.tblStudentSociety.userInteractionEnabled = YES;
    NSDictionary *dictSociety = [arrStudnetSocirtyData objectAtIndex:indexPath.row];
    NSString *stringUrl = [dictSociety objectForKey:@"website"];
    
    if (![stringUrl containsString:@"http://"])
    {
        stringUrl = [NSString stringWithFormat:@"http://%@",stringUrl];
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringUrl]];
}

-(void)moveNextController:(NSIndexPath *)indexPath
{
    PortfolioTableCell *cell = (PortfolioTableCell *) [_protfolioTableView cellForRowAtIndexPath:indexPath];
    CompaniesDetailViewController *companiesView = [self.storyboard instantiateViewControllerWithIdentifier:@"CompaniesDetailView"];
    companiesView.strVwChane = @"portfolioVw";
    
    arrayPortDataGlobal = [_arrayData mutableCopy];
    
    if (arrayCompaniesList.count==0)
    {
        arrayCompaniesList =[_arrayData mutableCopy];
    }
    companiesView.dicCompanyDetail = [[NSMutableDictionary alloc] initWithDictionary:[_arrayData objectAtIndex:indexPath.row]];
    
    cell.bgImageView.backgroundColor=[UIColor whiteColor];
    
    [self.navigationController pushViewController:companiesView animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView isEqual:self.tblStudentSociety]){
        if (isEditingEnabledStudent)
        {
            return YES;
        }
    }
    if (isEditingEnabled)
    {
        return YES;
    }
    return NO;
}

#pragma mark----PlaceNumberOfAwards----------

-(void)setAwardPhotos:(NSIndexPath*)index cellTable:(PortfolioTableCell*)cellTable
{
    int xValue=80;
    
    for (int i=0; i<[[[_arrayData valueForKeyPath:@"links.media_logo"] objectAtIndex:index.row] count]; i++)
    {
        __block  AsyncImageView *imageAwards = [[AsyncImageView alloc] initWithFrame:CGRectMake(xValue, 45, 14, 14)];
        xValue = xValue+16;
        
        imageAwards.layer.cornerRadius = 3.0f;
        
        imageAwards.clipsToBounds = YES;
        
        if (isInternetOff)
        {
            imageAwards.image = [UIImage imageWithContentsOfFile:[[[_arrayData valueForKeyPath:@"links.media_logo"]objectAtIndex:index.row] objectAtIndex:i]];
        }
        else
        {
            if ([[[[_arrayData valueForKeyPath:@"links.media_logo"] objectAtIndex:index.row] objectAtIndex:i] hasPrefix:@"/Users/"])
            {
                
                imageAwards.image = [UIImage imageWithContentsOfFile:[[[_arrayData valueForKeyPath:@"links.media_logo"]objectAtIndex:index.row] objectAtIndex:i]];
            }
            else if ([[[[_arrayData valueForKeyPath:@"links.media_logo"] objectAtIndex:index.row] objectAtIndex:i] hasPrefix:@"/var/mobile/Containers/"])
            {
                imageAwards.image = [UIImage imageWithContentsOfFile:[[[_arrayData valueForKeyPath:@"links.media_logo"]objectAtIndex:index.row] objectAtIndex:i]];
            }
            else
            {
                [imageAwards setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[[[_arrayData valueForKeyPath:@"links.media_logo"] objectAtIndex:index.row] objectAtIndex:i]]] placeholderImage:nil];
            }
        }
        [cellTable.contentView addSubview:imageAwards];
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        if ([tableView isEqual:self.tblStudentSociety]) {
            [self webServiceRemoveSociety:indexPath];
        }else{
            [self webServiceRemovePort:indexPath];
        }
        
        [tableView setEditing:NO animated:YES];
    }
}

-(void)editPressed:(id)sender
{
    if (currentPage == 0){
        if (isEditingEnabled)
        {
            [_protfolioTableView setEditing:NO animated:YES];
            isEditingEnabled =NO;
        }
        else
        {
            isEditingEnabled =YES;
            [_protfolioTableView setEditing:YES animated:YES];
        }
    }else{
        if (isEditingEnabledStudent)
        {
            [self.tblStudentSociety setEditing:NO animated:YES];
            isEditingEnabledStudent =NO;
        }
        else
        {
            isEditingEnabledStudent =YES;
            [self.tblStudentSociety setEditing:YES animated:YES];
        }
    }
    
}

- (IBAction)btnSortAction:(id)sender
{
    if(currentPage == 1){
        return;
    }
    if ([sender tag]==11)
    {
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
        
        NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
        
        NSArray *sortedArray = [_arrayData sortedArrayUsingDescriptors:sortDescriptors];
        
        _arrayData = nil;
        
        _arrayData = [[NSArray alloc]init];
        
        _arrayData = [sortedArray copy];
    }
    
    else if ([sender tag]==12)
    {
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"awards" ascending:NO ];
        
        NSArray *stories = [_arrayData sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
        
        _arrayData = nil;
        
        _arrayData = [[NSArray alloc]init];
        
        _arrayData = [stories copy];
    }
    
    else
    {
        NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy/MM/dd"];
        NSMutableArray *arrwithDates,*arrayWithoutDates;
        arrwithDates = [NSMutableArray array];
        arrayWithoutDates = [NSMutableArray array];
        
        for (NSDictionary *dict in _arrayData)
        {
            if ([[dict objectForKey:@"application_due_date"] isEqualToString:@"no"]||[[dict objectForKey:@"application_due_date"] isEqualToString:@"Open Until Filled"])
            {
                [arrayWithoutDates addObject:dict];
            }
            else
            {
                [arrwithDates addObject:dict];
            }
        }
        NSArray  *arrayDatesSorted = [arrwithDates sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2)
                                      {
                                          NSDate *d1 = [formatter dateFromString:obj1[@"application_due_date"]];
                                          NSDate *d2 = [formatter dateFromString:obj2[@"application_due_date"]];
                                          return [d1 compare:d2]; // ascending order
                                          
                                      }];
        
        arrwithDates = [arrayDatesSorted mutableCopy];
        
        [arrwithDates addObjectsFromArray:arrayWithoutDates];
        
        _arrayData = nil;
        
        _arrayData = [[NSArray alloc]init];
        
        _arrayData = [NSArray arrayWithArray:arrwithDates];
        
        [arrwithDates removeAllObjects];
        
        [arrayWithoutDates removeAllObjects];
        
        arrwithDates = nil;
        
        arrayWithoutDates = nil;
    }
    
    [_protfolioTableView reloadSections:[NSIndexSet indexSetWithIndex:0 ] withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark- TabButtonActions

- (IBAction)buttonActionPortfolio:(id)sender
{
}

- (IBAction)buttonActionDiscip:(id)sender
{
    
    arrayPortDataGlobal = [_arrayData mutableCopy];
    
    self.navigationItem.hidesBackButton = YES;
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj = [viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[DisciplineViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    DisciplineViewController *disciplineVw =[self.storyboard instantiateViewControllerWithIdentifier:@"disciplineView"];
    [self.navigationController pushViewController:disciplineVw animated:NO];
}


- (IBAction)buttonSearch:(id)sender
{
    if ([stringFirstCome isEqualToString:@"firstCome"])
    {
        stringFirstCome = @"";
    }
    
    arrayPortDataGlobal = [_arrayData mutableCopy];
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[LocationFilterSearchViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    LocationFilterSearchViewController  *searchVw = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVw"];
    
    [self.navigationController pushViewController:searchVw animated:NO];
}

- (IBAction)buttonMessages:(id)sender
{
    arrayPortDataGlobal = [_arrayData mutableCopy];
    
    MessagesViewController *messageVw = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    
    [self.navigationController pushViewController:messageVw animated:NO];
}

- (IBAction)buttonActionSalary:(id)sender
{
    arrayPortDataGlobal = [_arrayData mutableCopy];
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    
    for( int i=0; i<[viewControllers count]; i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[SalaryGuideViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            
            return;
        }
    }
    
    SalaryGuideViewController *salaryVw = [self.storyboard instantiateViewControllerWithIdentifier:@"salaryVw"];
    [self.navigationController pushViewController:salaryVw animated:NO];
}

#pragma mark- selfCreatedButtonActions

-(void)buttonMessageAction: (UIButton *)sender
{
    arrayPortDataGlobal = [_arrayData mutableCopy];
    
    [self performSelector:@selector(navigateNextView:) withObject:sender afterDelay:0.2];
    sender.userInteractionEnabled = NO;
    
    if ([sender isSelected])
    {
        [sender setSelected:NO];
    }
    
    else
    {
        [sender setSelected:YES];
    }
    [self performSelector:@selector(setState:) withObject:sender afterDelay:1.0];
    
}

-(void)navigateNextView:(id)sender
{
    MessagesViewController*messageVw =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    if (currentPage == 0){
        messageVw.strFromScreen = @"port";
        messageVw.arrComapanyDetail = [_arrayData objectAtIndex:[sender tag]];
        
    }else{
        messageVw.strFromScreen = @"Clubs&Societies";
        messageVw.isFromStudent = YES;
        messageVw.dictStudentSoc = [arrStudnetSocirtyData objectAtIndex:[sender tag]];
        
    }
    
    
    
    
    [self.navigationController pushViewController:messageVw animated:NO];
}

-(void)setState:(UIButton *)sender
{
    [sender setSelected:NO];
    
    sender.userInteractionEnabled = YES;
}

-(void)buttonGroupAction
{
    if (currentPage == 0) {
        [self.PortScrollView scrollRectToVisible:CGRectMake([UIScreen mainScreen].bounds.size.width, 0, self.PortScrollView.frame.size.width, self.PortScrollView.frame.size.height) animated:YES];
    }else{
        [self.PortScrollView scrollRectToVisible:CGRectMake(0, 0, self.PortScrollView.frame.size.width, self.PortScrollView.frame.size.height) animated:YES];
    }
    
}

-(void)buttonPerfectGraduateAction
{
    arrayPortDataGlobal = [_arrayData mutableCopy];
    
    MessagesViewController *messageVw =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    messageVw.strFromScreen = @"PerfectGraduate";
    [self.navigationController pushViewController:messageVw animated:NO];
}

-(void)buttonGami
{
    GamificationVC *gamiView = [self.storyboard instantiateViewControllerWithIdentifier:@"Gamification"];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:gamiView];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

#pragma mark----getUserCompanies---------

-(void)getUserCompanies
{
    if (boolNotifyEditCompanyPort== YES)
    {
        boolNotifyEditCompanyPort = NO;
    }
    else
    {
        [self ShowDataLoader:YES];
    }
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@user_company",baseUrl];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];

         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             firstLogin = YES;
             
             _arrayData = [[NSArray alloc] initWithArray:[responseObject objectForKey:@"data"]];
             
//             
//             [[NSUserDefaults standardUserDefaults] setObject:self.arrayData forKey:@"PortfolioUserData"];
//             [[NSUserDefaults standardUserDefaults] synchronize];
             
             if (_arrayData.count==0)
             {
                 arrayTempValues = nil;
                 
                 arrayTempValues = [NSArray new];
             }
             stringCntryId=[[_arrayData valueForKey:@"country_id"] objectAtIndex:0];
             
             
             if (PREF(LATITUDE_COUNTRY)== nil)
             {
                 [self locationLatitudeLongitude];
             }
             
             [self savePortfolioData];
         }
         
         else if([[responseObject objectForKey:@"status"] isEqualToString:@"false"])
         {
             _arrayData =[[NSArray alloc] initWithArray:[responseObject objectForKey:@"data"]];
             
             if (_arrayData.count==0)
             {
                 arrayTempValues = nil;
                 
                 arrayTempValues = [NSArray new];
             }
             
             firstLogin = NO;
             
             if (PREF(LATITUDE_COUNTRY)== nil)
                 
             {
                 [self locationLatitudeLongitude];
             }
             
             [self savePortfolioData];
         }
         
         else
         {
             [self ShowDataLoader:NO];
         }
         
         
         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         NSLog(@"badge count is %@",[responseObject valueForKey:@"badge_count"] );
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKey:@"badge_count"] forKey:@"badgeCount"];
         
         [UIApplication sharedApplication].applicationIconBadgeNumber = [[responseObject valueForKey:@"badge_count"] intValue];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.country_id"] forKey:@"countryId"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[responseObject valueForKeyPath:@"admin"]] forKey:@"adminCount"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[responseObject valueForKeyPath:@"society"]] forKey:@"societyCount"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.university"] forKey:@"userUniversity"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.uni_id"] forKey:@"userUniversityId"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.score"] forKey:@"userPoints"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.country"] forKey:@"countryName"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.abr"] forKey:@"stateAbr"];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.program_name"] forKey:@"programName"];
         [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[responseObject valueForKeyPath:@"society"]] forKey:@"societyCount"];
         if ([[responseObject valueForKeyPath:@"admin"] integerValue]!=0)
         {
             lblPgMsgCount.text = [NSString stringWithFormat:@"%@",[responseObject valueForKeyPath:@"admin"]];
             
             lblPgMsgCount.hidden = NO;
         }
         else
         {
             lblPgMsgCount.hidden = YES;
         }
         
         NSLog(@"program name %@ abr is %@",[responseObject valueForKeyPath:@"user_profile.program_name"],[responseObject valueForKeyPath:@"user_profile.abr"]);
         
         if ([responseObject valueForKeyPath:@"user_profile.program_name"] == NULL && [responseObject valueForKeyPath:@"user_profile.abr"] == NULL)
         {
             [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"\nPlease update your profile again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             
             [self setDueDateText:@""];
         }
         
         else
         {
             
             [self setDueDateText:[NSString stringWithFormat:@"Application Due Dates \n%@ %@",[responseObject valueForKeyPath:@"user_profile.abr"],[responseObject valueForKeyPath:@"user_profile.program_name"]]];
         }
         
         NSInteger points = [[responseObject valueForKeyPath:@"user_profile.score"] integerValue];
         
         [self gamificationNumbers:points];
         
         [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.state"] forKey:@"StateName"];
         
         if ([[responseObject valueForKeyPath:@"user_discipline.discipline"]count])
             
         {
             NSArray *arrayWithoutDuplicates = [[NSOrderedSet orderedSetWithArray:[responseObject valueForKeyPath:@"user_discipline.discipline"]] array];
             
             [[NSUserDefaults standardUserDefaults]setObject:[arrayWithoutDuplicates componentsJoinedByString:@", "] forKey:@"userDiscipline"];
             
             labelSubTitle.text = [arrayWithoutDuplicates componentsJoinedByString:@", "];
         }
         [self performSelector:@selector(showPortfolioCompanies) withObject:nil afterDelay:1];
         
         [self performSelectorInBackground:@selector(getAllStates) withObject:nil];
     }
     
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         else
         {
             [self ShowDataLoader:NO];
         }
         
         [self showPortfolioCompanies];
         
         [self ShowDataLoader:NO];
     }];
}

-(void)showServerData:(NSDictionary *)responseObject
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[responseObject valueForKeyPath:@"society"]] forKey:@"societyCount"];
    int TotalCount = 0;
    for(NSDictionary *dict in self.arrayData){
        if([dict objectForKey:@"unread_message"]){
            int Count = [[[NSString stringWithFormat:@"%@",[dict objectForKey:@"unread_message"]] removeNull] intValue];
            TotalCount = TotalCount+Count;
        }
    }
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",TotalCount] forKey:@"companyCount"];
    [self setCountLabel];
    if ([[responseObject valueForKeyPath:@"admin"] integerValue]!=0)
    {
        lblPgMsgCount.text = [NSString stringWithFormat:@"%@",[responseObject valueForKeyPath:@"admin"]];
        
        lblPgMsgCount.hidden = NO;
    }
    else
    {
        lblPgMsgCount.hidden = YES;
    }
    
    if ([responseObject valueForKeyPath:@"user_profile.program_name"] == NULL && [responseObject valueForKeyPath:@"user_profile.abr"] == NULL)
    {
        [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"\nPlease update your profile again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
        [self setDueDateText:@""];
    }
    else
    {
        [self setDueDateText:[NSString stringWithFormat:@"Application Due Dates \n%@ %@",[responseObject valueForKeyPath:@"user_profile.abr"],[responseObject valueForKeyPath:@"user_profile.program_name"]]];
    }
    
    NSInteger points = [[responseObject valueForKeyPath:@"user_profile.score"] integerValue];
    
    [self gamificationNumbers:points];
    
    [[NSUserDefaults standardUserDefaults] setObject:[responseObject valueForKeyPath:@"user_profile.state"] forKey:@"StateName"];
    
    if ([[responseObject valueForKeyPath:@"user_discipline.discipline"]count])
    {
        NSArray *arrayWithoutDuplicates = [[NSOrderedSet orderedSetWithArray:[responseObject valueForKeyPath:@"user_discipline.discipline"]] array];
        
        [[NSUserDefaults standardUserDefaults]setObject:[arrayWithoutDuplicates componentsJoinedByString:@", "] forKey:@"userDiscipline"];
        
        labelSubTitle.text = [arrayWithoutDuplicates componentsJoinedByString:@", "];
    }
    
    if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
    {
        firstLogin = YES;
        
        _arrayData = nil;
        
        arrayFinalSaveData = nil;
        
        _arrayData = [[NSArray alloc] initWithArray:[responseObject objectForKey:@"data"]];
        
        
        arrayFinalSaveData = [[NSArray alloc] initWithArray:[responseObject objectForKey:@"data"]];
        
        stringCntryId = [[_arrayData valueForKey:@"country_id"] objectAtIndex:0];
        
        if (PREF(LATITUDE_COUNTRY)== nil)
        {
            [self performSelectorInBackground:@selector(locationLatitudeLongitude) withObject:nil];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
        
        [self savePortfolioData];
    }
    
    else if([[responseObject objectForKey:@"status"] isEqualToString:@"false"])
    {
        _arrayData = nil;
        
        _arrayData =[[NSArray alloc] initWithArray:[responseObject objectForKey:@"data"]];
        
        firstLogin = NO;
        
        if (PREF(LATITUDE_COUNTRY)== nil)
        {
            [self performSelectorInBackground:@selector(locationLatitudeLongitude) withObject:nil];
        }
        
        [self showPortfolioCompanies];
    }
    else
    {
    }
}


-(void)reloadDataTable
{
    [self ShowDataLoader:NO];
    
    
    [_protfolioTableView reloadData];
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"portfolioAlert"])
    {
        [self performSelectorOnMainThread:@selector(alertShowMethod) withObject:nil waitUntilDone:NO];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"portfolioAlert"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)setDueDateText:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    
    NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSString *boldString = @"Application Due Dates";
    
    if ([string isEqualToString:@""])
    {
        yourAttributedString = [[NSMutableAttributedString alloc]initWithString:boldString];
        
        NSRange range = [boldString rangeOfString:boldString];
        
        [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:@"OpenSans-Bold" size:10.0f] range:range];
    }
    
    else
    {
        NSRange boldRange = [string rangeOfString:boldString];
        
        [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:@"OpenSans" size:10.0f] range:NSMakeRange(0,string.length)];
        
        [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:@"OpenSans-Bold" size:10.0f] range:boldRange];
    }
    
    [_labelDueDate setAttributedText: yourAttributedString];
}


-(void)gamificationNumbers:(NSInteger)point
{
    if (point >= 8000)
    {
        [buttonCandidatePoints setBackgroundImage:[UIImage imageNamed:@"yellow_ribbon"] forState:UIControlStateNormal];//yellow_ribbon
        labelPoints.text = [NSString stringWithFormat:@"PERFECT Candidate: %ld Pts",(long)point];
    }
    else if (point>=5000)
    {
        [buttonCandidatePoints setBackgroundImage:[UIImage imageNamed:@"green_ribbon"] forState:UIControlStateNormal];
        labelPoints.text = [NSString stringWithFormat:@"POPULAR Candidate: %li Pts",(long)point];
    }
    else if (point>=2000)
    {
        [buttonCandidatePoints setBackgroundImage:[UIImage imageNamed:@"blue_ribbon"] forState:UIControlStateNormal];
        
        labelPoints.text = [NSString stringWithFormat:@"Good Candidate: %li Pts",(long)point];//points
    }
    else
    {
        
        [buttonCandidatePoints setBackgroundImage:[UIImage imageNamed:@"red_ribbon"] forState: UIControlStateNormal];
        
        labelPoints.text = [NSString stringWithFormat:@"Candidate: %li Pts",(long)point];
    }
}

#pragma mark----getStudentSocieties-----

-(void)getStudentSocieties
{
    NSString* urlStr = [NSString stringWithFormat:@"%@societyDetail",baseUrl];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [[NSUserDefaults standardUserDefaults] setValue:responseObject forKey:@"userSocieties"];
             
             NSArray *array = [responseObject valueForKeyPath:@"data"];
             
             if (array.count ==0)
             {
                 
             }
             else
             {
                 stringSocietyIcon =[NSString stringWithFormat:@"%@", [[array valueForKey:@"logo"] objectAtIndex:0]];
                 
                 NSArray *arrayId =[array valueForKey:@"id"] ;
                 
                 [USERDEFAULT setObject:arrayId forKey:USER_SELECTED_SOCITIES];
             }
         }
         else
         {
         }
     }
     
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
     }];
}

-(void)getAllStates
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parameters = @{@"cid":[USERDEFAULT valueForKey:@"countryId"]};
    [manager POST:@"getAllStates" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [[NSUserDefaults standardUserDefaults]objectForKey:@"stateName"];
         }
         else
         {
         }
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
     }];
}

-(void)locationLatitudeLongitude
{
    //http://maps.google.com/maps/api/geocode/json?sensor=false&address=australia
    
    [self ShowDataLoader:YES];
    
    NSString *string = [NSString stringWithFormat:@"%@%@", @"http://maps.google.com/maps/api/geocode/json?sensor=false&address=",[USERDEFAULT valueForKey:@"countryName"]];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager POST:string parameters: nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSDictionary *dictgeom;
         if([[responseObject valueForKeyPath:@"results.geometry"] count]>0){
             dictgeom = [[responseObject valueForKeyPath:@"results.geometry"]objectAtIndex:0];
         }
         
         
         NSDictionary *dicLoc = [dictgeom valueForKey:@"location"];
         
         [USERDEFAULT removeObjectForKey:LATITUDE_COUNTRY];
         
         [USERDEFAULT removeObjectForKey:LONGITUDE_COUNTRY];
         
         [USERDEFAULT setObject:[dicLoc valueForKey:@"lat"] forKey:LATITUDE_COUNTRY];
         
         [USERDEFAULT setObject:[dicLoc valueForKey:@"lng"] forKey:LONGITUDE_COUNTRY];
         
         NSLog(@"value in cntryLatitude detail %@", PREF(LATITUDE_COUNTRY));
         
         NSLog(@"value in cntryLongitude detail %@", PREF(LONGITUDE_COUNTRY));
         
         [self performSelectorInBackground:@selector(getAllStates) withObject:nil];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
         }
         
         else if ([[responseObject objectForKey:@"status"] isEqualToString:@"false"])
         {
         }
         
         else
         {
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}

-(void)savePortfolioData
{
    if (stringSocietyIcon == nil)
    {
        stringSocietyIcon = @"icon";
    }
    if ([dummy deleteTable:@"delete from PORTFOLIO"])
    {
        if ([dummy deleteTable:@"delete from PortLocationsTable"])
        {
            if ([dummy deleteTable:@"delete from PortAppDueDate"])
            {
                if ([dummy deleteTable:@"delete from PortCompanyAwards"])
                {
                    if ([dummy deleteTable:@"delete from PortDisciplinesTable"])
                    {
                        if ([dummy deleteTable:@"delete from PortSubDisciplinesTable"])
                        {
                            if ([dummy deleteTable:@"delete from PortQuesAnsTable"])
                            {
                                if ([dummy deleteTable:@"delete from PortCompanyDueDate"])
                                {
                                    if ([dummy deleteTable:@"delete from PortCompSocialLinksTable"])
                                    {
                                        for (int i=0; i<[_arrayData count]; i++)
                                        {
                                            NSString *stringINt;
                                            
                                            if ([[[_arrayData valueForKey:@"international"] objectAtIndex:i]isKindOfClass:[NSNull class]])
                                            {
                                                stringINt = @"";
                                            }
                                            
                                            else
                                            {
                                                stringINt =[[_arrayData valueForKey:@"international"] objectAtIndex:i];
                                            }
                                            
                                            NSString *query = [NSString stringWithFormat: @"INSERT INTO PORTFOLIO (id,application_due_date, awards,city,company_address,company_name,description,logo,state_id,unread_message,regstates,company_intro,country,message_status,website,international,perfect_logo,society_logo) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\",\"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")"
                                                               
                                                               ,[[_arrayData valueForKey:@"id"] objectAtIndex:i]
                                                               ,[[_arrayData valueForKey:@"application_due_date"] objectAtIndex:i]
                                                               ,[[_arrayData valueForKey:@"awards"] objectAtIndex:i],[[_arrayData valueForKey:@"city"] objectAtIndex:i]
                                                               ,[[_arrayData valueForKey:@"company_address"] objectAtIndex:i]
                                                               ,[[_arrayData valueForKey:@"company_name"] objectAtIndex:i]
                                                               ,[[_arrayData valueForKey:@"description"] objectAtIndex:i]
                                                               ,[[_arrayData valueForKey:@"logo"] objectAtIndex:i]
                                                               ,[[_arrayData valueForKey:@"state_id"] objectAtIndex:i]
                                                               ,[NSString stringWithFormat:@"%@",[[_arrayData valueForKey:@"unread_message"] objectAtIndex:i]]
                                                               ,[[_arrayData valueForKey:@"regstates"] objectAtIndex:i]
                                                               ,[[_arrayData valueForKey:@"company_intro"] objectAtIndex:i]
                                                               ,[[_arrayData valueForKey:@"country"] objectAtIndex:i]
                                                               ,[[_arrayData valueForKey:@"message_status"] objectAtIndex:i]
                                                               ,[[_arrayData valueForKey:@"website"] objectAtIndex:i],stringINt,[[_arrayData valueForKey:@"perfect_logo"] objectAtIndex:i],stringSocietyIcon];
                                            
                                            [self saveImages:[[NSString stringWithFormat:@"%@%@",[[_arrayData valueForKey:@"id"] objectAtIndex:i],[[_arrayData valueForKey:@"logo"] objectAtIndex:i]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] imageUrl:[[_arrayData valueForKey:@"logo"] objectAtIndex:i]];
                                            
                                            [self saveImages:[[NSString stringWithFormat:@"%@%@",[[_arrayData valueForKey:@"id"] objectAtIndex:i],[[_arrayData valueForKey:@"perfect_logo"] objectAtIndex:i]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] imageUrl:[[_arrayData valueForKey:@"perfect_logo"] objectAtIndex:i]];
                                            
                                            [self saveImages:[[NSString stringWithFormat:@"%@%@",[[_arrayData valueForKey:@"id"] objectAtIndex:i],stringSocietyIcon] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] imageUrl:stringSocietyIcon];
                                            const char *query_stmt = [query UTF8String];
                                            [dummy  runQueries:query_stmt strForscreen:@"port"];
                                            
                                            [self savePortQuestions:i];
                                            [self saveDueDatePortCompanies:i];
                                            [self saveLocationsPortCompanies:i];
                                            [self saveAwardsPortCompanies:i];
                                            [self saveDisciplinesPortCompanies:i];
                                            [self savePortCompanyLinks:i];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"portfolioAlert"])
    {
        [self performSelectorOnMainThread:@selector(alertShowMethod) withObject:nil waitUntilDone:NO];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"portfolioAlert"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}

-(void)alertShowMethod
{
    [[[UIAlertView alloc]initWithTitle:@"Welcome to Perfect Graduate!" message:@"\nTo begin, we’ve included a few suggested companies for your portfolio.\n\nTo find more companies, use the ‘Disciplines’ and ‘Search’ buttons located at the bottom of the page.\n\nAdd companies to your portfolio to easily follow their application due dates, messages and events." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
}


-(void)saveOtherData
{
    for (int i=0; i<[_arrayData count]; i++)
    {
        [self savePortQuestions:i];
        [self saveDueDatePortCompanies:i];
        [self saveLocationsPortCompanies:i];
        [self saveAwardsPortCompanies:i];
        [self saveDisciplinesPortCompanies:i];
        [self savePortCompanyLinks:i];
    }
}


-(void)getAllCountries
{
    NSString* urlStr =[NSString stringWithFormat:@"%@getAllsearchCountries",baseUrl];
    
    NSDictionary *dicParam = @{@"user_id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
             NSArray* sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
             NSArray *arrCountry = [[responseObject objectForKey:@"data"] sortedArrayUsingDescriptors:sortDescriptors];
             
             arrayCountryData = [[responseObject objectForKey:@"data"] sortedArrayUsingDescriptors:sortDescriptors];
             
             if ([dummy deleteTable:@"delete from COUNTRIES"])
             {
                 for (int i=0; i<[arrCountry  count]; i++)
                 {
                     NSString *query = [NSString stringWithFormat: @"INSERT INTO COUNTRIES (id,name,abr) VALUES (\"%@\",\"%@\",\"%@\")"
                                        ,[[arrCountry valueForKey:@"id"] objectAtIndex:i]
                                        ,[[arrCountry valueForKey:@"name"] objectAtIndex:i]
                                        ,[[arrCountry valueForKey:@"abr"] objectAtIndex:i]];
                     
                     const char *query_stmt = [query UTF8String];
                     
                     [dummy  runQueries:query_stmt strForscreen:@"countiesList" ];
                 }
             }
         }
         else
         {
         }
     }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
     }];
}

-(void)savePortCompanyLinks:(int)index
{
    for (int k=0; k<[[[_arrayData valueForKey:@"links"] objectAtIndex:index] count]; k++)
    {
        NSString *subQueryLinks =[NSString stringWithFormat: @"INSERT INTO PortCompSocialLinksTable (company_id,id,media_link,media_logo,media_type) VALUES (\"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                                  ,[[[_arrayData valueForKeyPath:@"links.company_id"] objectAtIndex:index] objectAtIndex:k]
                                  ,[[[_arrayData valueForKeyPath:@"links.id"] objectAtIndex:index] objectAtIndex:k]
                                  ,[[[_arrayData valueForKeyPath:@"links.media_link"] objectAtIndex:index] objectAtIndex:k]
                                  ,[[[_arrayData valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]
                                  ,[[[_arrayData valueForKeyPath:@"links.media_type"] objectAtIndex:index] objectAtIndex:k]];
        
        [self saveImages:[[NSString stringWithFormat:@"%@%@",[[[_arrayData valueForKeyPath:@"links.id"] objectAtIndex:index] objectAtIndex:k],[[[_arrayData valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                imageUrl:[[[_arrayData valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]];
        
        const char *query_stmt_SocialLinks = [subQueryLinks UTF8String];
        
        [dummy runQuerySocialLinksForCompanies:query_stmt_SocialLinks strForscreen:@"Port"];
    }
}

-(void)savePortQuestions:(int)index
{
    for (int k=0; k<[[[_arrayData valueForKey:@"quesAns"] objectAtIndex:index] count]; k++)
    {
        NSString *subQuery = [NSString stringWithFormat: @"INSERT INTO PortQuesAnsTable (answer,company_id,id,question) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                              ,[[[_arrayData valueForKeyPath:@"quesAns.answer"] objectAtIndex:index] objectAtIndex:k]
                              ,[[[_arrayData valueForKeyPath:@"quesAns.company_id"] objectAtIndex:index] objectAtIndex:k]
                              ,[[[_arrayData valueForKeyPath:@"quesAns.id"] objectAtIndex:index] objectAtIndex:k]
                              ,[[[_arrayData valueForKeyPath:@"quesAns.question"] objectAtIndex:index] objectAtIndex:k]];
        
        const char *query_stmt_QuesAns = [subQuery UTF8String];
        [dummy runQueryQuesAnsForCompanies:query_stmt_QuesAns strForscreen:@"Port"];
    }
}

-(void)saveDisciplinesPortCompanies:(int)index
{
    for (int k=0; k<[[[_arrayData valueForKey:@"disciplines"] objectAtIndex:index] count]; k++)
    {
        NSString *subQuery = [NSString stringWithFormat: @"INSERT INTO PortDisciplinesTable (company_id,discipline_id,name) VALUES (\"%@\",\"%@\", \"%@\")"
                              ,[[[_arrayData valueForKeyPath:@"disciplines.company_id"] objectAtIndex:index] objectAtIndex:k]
                              ,[[[_arrayData valueForKeyPath:@"disciplines.discipline_id"] objectAtIndex:index] objectAtIndex:k]
                              ,[[[_arrayData valueForKeyPath:@"disciplines.name"] objectAtIndex:index] objectAtIndex:k]];
        
        const char *query_stmt_disciplines = [subQuery UTF8String];
        [dummy runQueryDispSubDispForCompanies:query_stmt_disciplines strForscreen:@"DisciplinesPort"];
        [self saveSubDisciplinesPort:index disciplineTag:k];
    }
}

-(void)saveSubDisciplinesPort:(int)index disciplineTag:(int)disciplineTag
{
    for (int j=0; j<[[[[_arrayData valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:index] objectAtIndex:disciplineTag] count]; j++)
    {
        NSString *subQuery = [NSString stringWithFormat: @"INSERT INTO PortSubDisciplinesTable (discipline_id,name,company_id,sub_discipline_id) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                              ,[[[[[_arrayData valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:index] objectAtIndex:disciplineTag]valueForKey:@"discipline_id"] objectAtIndex:j]
                              ,[[[[[_arrayData valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:index] objectAtIndex:disciplineTag]valueForKey:@"name"] objectAtIndex:j]
                              ,[[[_arrayData valueForKeyPath:@"disciplines.company_id"] objectAtIndex:index] objectAtIndex:disciplineTag]
                              ,[[[[[_arrayData valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:index] objectAtIndex:disciplineTag]valueForKey:@"sub_discipline_id"] objectAtIndex:j]
                              ];
        
        const char *query_stmt_PortSubDiscip = [subQuery UTF8String];
        [dummy runQuerySubDisciplines:query_stmt_PortSubDiscip strForscreen:@"SubDiscipPort"];
    }
}

-(void)saveLocationsPortCompanies:(int)index
{
    for (int k=0; k<[[[_arrayData valueForKey:@"location"] objectAtIndex:index] count]; k++)
    {
        NSString *subQuery = [NSString stringWithFormat: @"INSERT INTO PortLocationsTable (company_id,id,branch_name, lat,long,location) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\")",[[[_arrayData valueForKeyPath:@"location.company_id"] objectAtIndex:index] objectAtIndex:k],[[[_arrayData valueForKeyPath:@"location.id"] objectAtIndex:index] objectAtIndex:k],[[[_arrayData valueForKeyPath:@"location.branch_name"] objectAtIndex:index] objectAtIndex:k],[[[_arrayData valueForKeyPath:@"location.lat"] objectAtIndex:index] objectAtIndex:k],[[[_arrayData valueForKeyPath:@"location.long"] objectAtIndex:index] objectAtIndex:k],[[[_arrayData valueForKeyPath:@"location.location"] objectAtIndex:index] objectAtIndex:k]];
        
        const char *query_stmtLocation = [subQuery UTF8String];
        [dummy runSubQueryLocations:query_stmtLocation strForscreen:@"port"];
    }
}

-(void)saveAwardsPortCompanies:(int)index
{
    for (int k=0; k<[[[_arrayData valueForKey:@"allAwards"] objectAtIndex:index] count]; k++)
    {
        NSString *subQueryAwards = [NSString stringWithFormat: @"INSERT INTO PortCompanyAwards (awards,company_id,description, id) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                                    ,[[[_arrayData valueForKeyPath:@"allAwards.awards"] objectAtIndex:index] objectAtIndex:k]
                                    ,[[[_arrayData valueForKeyPath:@"allAwards.company_id"] objectAtIndex:index] objectAtIndex:k]
                                    ,[[[_arrayData valueForKeyPath:@"allAwards.description"] objectAtIndex:index] objectAtIndex:k]
                                    ,[[[_arrayData valueForKeyPath:@"allAwards.id"] objectAtIndex:index] objectAtIndex:k]];
        
        const char *query_stmt_Awards= [subQueryAwards UTF8String];
        [dummy runSubQueryToSaveAwards:query_stmt_Awards strForscreen:@"port"];
    }
}

-(void)saveDueDatePortCompanies:(int)index
{
    for (int j=0; j<[[[_arrayData valueForKey:@"application_due_date_all"] objectAtIndex:index] count]; j++)
    {
        NSString *subQueryDueDate = [NSString stringWithFormat:@"INSERT INTO PortCompanyDueDate(close_date,company_id,id,program,program_id,start_date,state,country) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\",\"%@\",\"%@\")",
                                     
                                     [[[_arrayData valueForKeyPath:@"application_due_date_all.close_date"] objectAtIndex:index] objectAtIndex:j]
                                     ,[[[_arrayData valueForKeyPath:@"application_due_date_all.company_id"] objectAtIndex:index] objectAtIndex:j]
                                     ,[[[_arrayData valueForKeyPath:@"application_due_date_all.id"] objectAtIndex:index] objectAtIndex:j]
                                     ,[[[_arrayData valueForKeyPath:@"application_due_date_all.program"] objectAtIndex:index] objectAtIndex:j]
                                     ,[[[_arrayData valueForKeyPath:@"application_due_date_all.program_id"] objectAtIndex:index] objectAtIndex:j]
                                     ,[[[_arrayData valueForKeyPath:@"application_due_date_all.start_date"] objectAtIndex:index] objectAtIndex:j]
                                     ,[[[_arrayData valueForKeyPath:@"application_due_date_all.state"] objectAtIndex:index] objectAtIndex:j]
                                     ,[[[_arrayData valueForKeyPath:@"application_due_date_all.country"] objectAtIndex:index] objectAtIndex:j]];
        
        const char *query_stmt_dueDate = [subQueryDueDate UTF8String];
        [dummy runSubQueryToSaveDueDate:query_stmt_dueDate strForscreen:@"port"];
    }
}

-(void)saveImages:(NSString *)ImageName imageUrl :(NSString *)imageUrl
{
    ImageName = [ImageName stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    if (imageUrl.length>0)
    {
        if (![self imageCheck:ImageName])
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:ImageName];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,imageUrl]]];
            [imageData writeToFile:savedImagePath atomically:NO];
        }
    }
}

-(BOOL)imageCheck :(NSString *)strName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    if ([contents containsObject:strName])
    {
        return YES;
    }
    return NO;
}

-(void)webServiceRemovePort:(NSIndexPath*)index
{
    NSString* urlStr =  [NSString stringWithFormat:@"%@removeCompany",baseUrl];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"cid":[[_arrayData valueForKey:@"id"] objectAtIndex:index.row]};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
            // [self ShowDataLoader:YES];
             
             [self getUserCompanies];
             
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callallCompanyWebService"];
         }
         else
         {
             ZAlertWithParam(@"Alert !", [responseObject valueForKey:@"message"]);
         }
     }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
     }];
}

-(void)webServiceRemoveSociety:(NSIndexPath*)index
{
    NSString* urlStr =  [NSString stringWithFormat:@"%@deleteSociety",baseUrl];
    [self ShowDataLoader:YES];
    NSString *strId2 = [NSString stringWithFormat:@"%@",[[arrStudnetSocirtyData valueForKey:@"id"] objectAtIndex:index.row]];
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"sid":strId2};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];

         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             
             if([[arrStudnetSocirtyData objectAtIndex:index.row] objectForKey:@"unread_message"]){
                 NSInteger MsgCount = [[NSString stringWithFormat:@"%@",[[arrStudnetSocirtyData objectAtIndex:index.row] objectForKey:@"unread_message"]] integerValue];
                 NSInteger PreCount = [[[NSUserDefaults standardUserDefaults] objectForKey:@"societyCount"] integerValue];
                 [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",(PreCount - MsgCount)] forKey:@"societyCount"];
                 
             }
             
             NSMutableDictionary *dicSocieties = [[NSMutableDictionary alloc]initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"userSocieties"]];
             NSMutableArray *arrSocieties = [NSMutableArray arrayWithArray:[dicSocieties objectForKey:@"data"]];
             for (NSDictionary *dict in arrSocieties){
                 NSString *strId = [NSString stringWithFormat:@"%@",[dict objectForKey:@"id"]];
                 if([strId isEqualToString:strId2]){
                     [arrSocieties removeObject:dict];
                 }
             }
             [dicSocieties removeAllObjects];
             [dicSocieties setObject:arrSocieties forKey:@"data"];
             [[NSUserDefaults standardUserDefaults] setObject:dicSocieties forKey:@"userSocieties"];
             [arrStudnetSocirtyData removeObjectAtIndex:index.row];
             NSString *filePath = [self getSocietyArrPath];
             [arrStudnetSocirtyData writeToFile:filePath atomically:YES];
             [self.tblStudentSociety reloadData];
         }
         else
         {
             ZAlertWithParam(@"Alert !", [responseObject valueForKey:@"message"]);
         }
     }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
     }];
}
@end



