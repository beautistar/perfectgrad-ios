
//
//  MessagesViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 2/6/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "MessagesViewController.h"
#import "SWRevealViewController.h"
#import "PortfolioViewController.h"
#import "LocationFilterSearchViewController.h"
#import "SalaryGuideViewController.h"
#import "DisciplineViewController.h"
#import "AppDelegate.h"
#import "Globals.h"

@interface MessagesViewController ()
{
    IBOutlet UIView *textInputView;
    IBOutlet UITextField *textField;
    UILabel *labelTitleName,*labelSubTitle;
    
    NSMutableArray *arrayCompMsgsAndEvents;
    NSMutableArray *arraySocietyMsgsAndEvents;
    NSMutableArray *arrayCompanyInvites;
    NSMutableArray *arraySocietyInvites;
    
    NSMutableArray *arraySocietyEvents;
    NSMutableArray *arrayCompanyEvents;
    
    NSMutableArray *arrAllData;
    NSArray *arrayPaging;
    Database *dummy;
    UIActivityIndicatorView *spinner;
    BOOL isPaging;
    NSTimer *timer;
    UIRefreshControl *refreshControl;
    BOOL isRefreshingMessages , frombutton;
    
    NSInteger intEventsScreen;
    NSInteger intMsgsScreen;
    NSInteger intSocietyMsgsScreen;
    NSInteger intSocietyEventsScreen;
    
    int tagValue;
    UIView *viewLayer;
    
    int intRowsForTable;
    NSString *stringSelectedMsg;
    
    NSMutableArray *arrayAllMessageData;
    BOOL isSort, boolRead;
}

@end

@implementation MessagesViewController
@synthesize bubbleTable,stringFromPrevious;


#pragma mark- callWebServices

-(void)getStudentProfile
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"getProfile" parameters:@{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]} success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             if ([responseObject valueForKeyPath:@"data.country"] == (id)[NSNull null]||[responseObject valueForKeyPath:@"data.state"] == (id)[NSNull null]||[responseObject valueForKeyPath:@"data.university"] == (id)[NSNull null]||[responseObject valueForKeyPath:@"data.program"] == (id)[NSNull null]||[[responseObject valueForKeyPath:@"data.discipline"] count]==0)
             {
                 [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"\nPlease update your profile again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             
         }
         else
         {
             NSLog(@"wrong result");
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         NSLog(@"%@",error.localizedDescription);
     }];
}

-(void)viewWillDisappear:(BOOL)animated
{
    static NSString *CellIdentifier = @"cellID";
    
    MessagesTableViewCell *cell=(MessagesTableViewCell*)[bubbleTable dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.backgroundColor = [UIColor clearColor];
    
    UIButton *btn = (UIButton *)[cell viewWithTag:tagValue];
    
    MessagesTableViewCell *buttonCell = (MessagesTableViewCell *)btn.superview.superview;
    
    buttonCell.backgroundColor = [UIColor redColor];
    
    [super viewWillDisappear:animated];
    
    [timer invalidate];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"messageNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"getComapiness" object:nil];
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, _imageRedStar.frame.origin.y, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    
    arrayAllMessageData = [[NSMutableArray alloc]init];
    
    [self getStudentProfile];
    
    intRowsForTable = 20;
    
    stringMessageDetail = @"";
    
    isRefreshingMessages = NO;
    
    refreshControl = [[UIRefreshControl alloc] init];
    
    refreshControl.backgroundColor = [UIColor clearColor];
    
    refreshControl.tintColor = [UIColor lightGrayColor];
    
    [refreshControl addTarget:self
                       action:@selector(callMessageWebService:)
             forControlEvents:UIControlEventValueChanged];
    
    [bubbleTable addSubview:refreshControl];
    
    dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    arrayCompanyInvites = [[NSMutableArray alloc]init];
    arraySocietyInvites = [[NSMutableArray alloc]init];
    arrayCompanyEvents = [[NSMutableArray alloc]init];
    arraySocietyEvents = [[NSMutableArray alloc]init];
    arrayCompMsgsAndEvents = [[NSMutableArray alloc] init];
    arraySocietyMsgsAndEvents = [[NSMutableArray alloc] init];
    arrAllData = [[NSMutableArray alloc] init];
    
    self.viewBottomBar.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.hidden = NO;
    
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:22.0f],NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    _buttonCmpnyAndClubs.tintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    _buttonCmpnyAndClubs.layer.borderWidth = 0.5f;
    _buttonCmpnyAndClubs.layer.borderColor= [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]CGColor];
    
    _buttonEvents.tintColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    _buttonEvents.layer.borderWidth = 0.5f;
    _buttonEvents.layer.borderColor= [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]CGColor];
    
    [self addVcTitlesAndButtons];
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"messageAlert"])
    {
        [self performSelectorOnMainThread:@selector(alertOnMainThread) withObject:nil waitUntilDone:NO];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"messageAlert"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)callMessageWebService:(NSNotification *)notify
{
    if ([notify isEqual:@"messageReloadBackground"])
    {
        [self performSelectorInBackground:@selector(getMessages) withObject:nil];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"callMsgWebService"];
        isRefreshingMessages = YES;
        intRowsForTable = 20;
        [self checkReachability];
    }
}

-(void)addVcTitlesAndButtons
{
    UIView  *textView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 200 , 40)];
    
    /*----UILabel title Name-------*/
    labelTitleName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 0, CGRectGetWidth(textView.frame), 22)];
    labelTitleName.backgroundColor = [UIColor clearColor];
    labelTitleName.font = [UIFont fontWithName:@"OpenSans" size:18.0f];
    labelTitleName.textAlignment = NSTextAlignmentCenter;
    labelTitleName.textColor = [UIColor whiteColor];
    
    labelTitleName.adjustsFontSizeToFitWidth = YES;
    [labelTitleName setMinimumScaleFactor:8.0/[UIFont labelFontSize]];
    
    /*----UILabel Sub-title Name-------*/
    labelSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(labelTitleName.frame.origin.x , 22, CGRectGetWidth(textView.frame),18)];
    labelSubTitle.backgroundColor = [UIColor clearColor];
    labelSubTitle.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
    labelSubTitle.textAlignment = NSTextAlignmentCenter;
    labelSubTitle.textColor = [UIColor whiteColor];
    [textView setBackgroundColor:[UIColor clearColor]];
    [textView addSubview:labelSubTitle];
    [textView addSubview:labelTitleName];
    
    
    self.navigationItem.titleView = textView;
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.bounds = CGRectMake( 10, 0, 30 , 30 );//set bound as per you want
    [rightButton addTarget:self action:@selector(rightBArButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setImage:[UIImage imageNamed:@"calender"] forState:UIControlStateNormal];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    
    _imageVwStar.hidden = NO;
    
    if ([_strFromScreen isEqualToString:@"port"])
    {
        intMsgsScreen = 10;
        
        intEventsScreen = 10;
        
        strBuubleImage = nil;
        
        labelSubTitle.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userDiscipline"];
        
        [_buttonEvents setTitle:@"Events only" forState:UIControlStateNormal];
        
        labelTitleName.text = [_arrComapanyDetail valueForKey:@"company_name"];
        
        _buttonCmpnyAndClubs.tintColor = [UIColor lightGrayColor];
        
        _buttonCmpnyAndClubs.enabled = NO;
        
        self.navigationItem.rightBarButtonItem = rightBarButton;
        
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
        
        self.navigationItem.leftBarButtonItem = leftItem;
        
        _buttonMessages.backgroundColor = [UIColor whiteColor];
        
        [_buttonMessages setImage:[UIImage imageNamed:@"messagesSelect"] forState:UIControlStateNormal];
        
        _labelmessag.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        
        
        _imageVwStar.hidden = YES;
    }
    
    else if ([_strFromScreen isEqualToString:@"PerfectGraduate"]||[_strFromScreen isEqualToString:@"Clubs&Societies"])
    {
        intMsgsScreen = 10;
        
        NSString *stringFirst1 =[USERDEFAULT objectForKey:@"FirstCome"];
        
        if ([stringFirst1 isEqualToString:@"value"])
        {
        }
        else
        {
            [USERDEFAULT setObject:@"value" forKey:@"FirstCome"];
        }
        
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
        
        self.navigationItem.leftBarButtonItem = leftItem;
        
        if ([_strFromScreen isEqualToString:@"PerfectGraduate"])
        {
            strBuubleImage = @"yellow";
            labelTitleName.text = @"Perfect Graduate";
            labelSubTitle.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userDiscipline"];
        }
        else
        {
            strBuubleImage = nil;
            if(self.isFromStudent){
                NSString *strName = [NSString stringWithFormat:@"%@",[self.dictStudentSoc objectForKey:@"society_name"]];
                labelTitleName.text = strName;
            }else{
                
                labelTitleName.text = @"My Student Societies";
            }
            labelSubTitle.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userUniversity"];
        }
        _buttonCmpnyAndClubs.tintColor = [UIColor lightGrayColor];
        _buttonCmpnyAndClubs.enabled = NO;
        [_buttonCmpnyAndClubs setTitle:@"Companies" forState:UIControlStateNormal];
        [_buttonEvents setTitle:@"Events only" forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = rightBarButton;
        
        _buttonMessages.backgroundColor = [UIColor whiteColor];
        [_buttonMessages setImage:[UIImage imageNamed:@"messagesSelect"] forState:UIControlStateNormal];
        
        _labelmessag.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        
        _imageVwStar.hidden = YES;
    }
    
    else if ([_strFromScreen isEqualToString:@"fromMenu"])
    {
        intMsgsScreen = 10;
        strBuubleImage = nil;
        _buttonMessages.backgroundColor = [UIColor clearColor];
        [_buttonMessages setImage:[UIImage imageNamed:@"messages"] forState:UIControlStateNormal];
        _labelmessag.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        
        self.navigationItem.hidesBackButton = YES;
        labelTitleName.text = @"My Events";
        labelSubTitle.text = @"All requests and responses";
        _buttonCmpnyAndClubs.tintColor = [UIColor lightGrayColor];
        _buttonCmpnyAndClubs.enabled = NO;
        _buttonEvents.tintColor = [UIColor lightGrayColor];
        _buttonEvents.enabled = NO;
        
        
        UIView *viewAddBtnsLeft = [[UIView alloc]initWithFrame:CGRectMake(-5, 0, 30, 40)];
        
        UIButton * buttonMenu = [[UIButton alloc]initWithFrame:CGRectMake(-3, 4, 30, 30) ];
        
        
        [buttonMenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
        
        buttonMenu.backgroundColor = [UIColor clearColor];
        
        [buttonMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        
        [viewAddBtnsLeft addSubview:buttonMenu];
        
        self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:viewAddBtnsLeft];
        
        _buttonMessages.backgroundColor = [UIColor whiteColor];
        [_buttonMessages setImage:[UIImage imageNamed:@"messagesSelect"] forState:UIControlStateNormal];
    }
    
    else if ([_strEventsOfScreen isEqualToString:@"eventsClubs&Societies"] )
    {
        intEventsScreen = 10;
        
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
        
        self.navigationItem.leftBarButtonItem = leftItem;
        
        _buttonCmpnyAndClubs.titleLabel.textColor = [UIColor lightGrayColor];
        _buttonEvents.titleLabel.textColor = [UIColor lightGrayColor];
        
        _buttonEvents.enabled = NO;
        _buttonCmpnyAndClubs.enabled = NO;
        
        _buttonMessages.backgroundColor = [UIColor whiteColor];
        [_buttonMessages setImage:[UIImage imageNamed:@"messagesSelect"] forState:UIControlStateNormal];
        
        _labelmessag.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        
        strBuubleImage = nil;
        labelTitleName.text = @"My Events";
        labelSubTitle.text = @"My Student Societies";
        
        [self methodForPaging:[_arrCompanyEventsPass mutableCopy] numberOfMsgsToLoad:10];
        
        _imageVwStar.hidden = YES;
    }
    
    else if ([_strEventsOfScreen isEqualToString:@"eventsPort"])
    {
        intEventsScreen = 10;
        
        strBuubleImage = nil;
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        _buttonPortfolio.backgroundColor = [UIColor whiteColor];
        [_buttonPortfolio setImage:[UIImage imageNamed:@"portfolio1"] forState:UIControlStateNormal];
        _labelPortfolio.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        
        _buttonCmpnyAndClubs.tintColor = [UIColor lightGrayColor];
        _buttonCmpnyAndClubs.enabled = NO;
        _buttonEvents.tintColor = [UIColor lightGrayColor];
        _buttonEvents.enabled = NO;
        labelTitleName.text = @"My Events";
        labelSubTitle.text = _strNavTitle;
        
        [self methodForPaging:[_arrCompanyEventsPass mutableCopy] numberOfMsgsToLoad:10];
    }
    
    else if ([_strEventsOfScreen isEqualToString:@"eventsPerfectGraduate"])
    {
        intEventsScreen = 10;
        
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        _buttonPortfolio.backgroundColor = [UIColor whiteColor];
        [_buttonPortfolio setImage:[UIImage imageNamed:@"portfolio1"] forState:UIControlStateNormal];
        _labelPortfolio.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        
        strBuubleImage = @"yellow";
        labelTitleName.text = @"My Events";
        labelSubTitle.text = @"Perfect Graduate";
        _buttonCmpnyAndClubs.tintColor = [UIColor lightGrayColor];
        _buttonCmpnyAndClubs.enabled = NO;
        [_buttonCmpnyAndClubs setTitle:@"Companies" forState:UIControlStateNormal];
        [_buttonEvents setTitle:@"Messages & Events " forState:UIControlStateNormal];
        _buttonEvents.tintColor = [UIColor lightGrayColor];
        _buttonEvents.enabled = NO;
        
        [self methodForPaging:[_arrCompanyEventsPass mutableCopy] numberOfMsgsToLoad:10];
        
        _imageVwStar.hidden = YES;
    }
    
    else if ([_strEventsOfScreen isEqualToString:@"eventsCompamyFromTab"]||[_strEventsOfScreen isEqualToString:@"eventsSocietyFromTab"])
    {
        intEventsScreen = 10;
        
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        _buttonMessages.backgroundColor = [UIColor whiteColor];
        [_buttonMessages setImage:[UIImage imageNamed:@"messages"] forState:UIControlStateNormal];
        _labelmessag.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        strBuubleImage = nil;
        
        if ([_strEventsOfScreen isEqualToString:@"eventsCompamyFromTab"])
        {
            labelTitleName.text = @"My Events";
            labelSubTitle.text = @"Companies";
            [_buttonCmpnyAndClubs setTitle:@"Student Societies" forState:UIControlStateNormal];
        }
        else
        {
            labelTitleName.text = @"My Events";
            labelSubTitle.text = @"Student Societies";
            [_buttonCmpnyAndClubs setTitle:@"Companies" forState:UIControlStateNormal];
        }
        
        [_buttonEvents setTitle:@"Messages & Events " forState:UIControlStateNormal];
        _buttonEvents.tintColor = [UIColor lightGrayColor];
        _buttonEvents.enabled = NO;
        
        [self methodForPaging:[_arrCompanyEventsPass mutableCopy] numberOfMsgsToLoad:10];
    }
    
    else
    {
        [[NSUserDefaults standardUserDefaults ]setObject:@"hide" forKey:@"redStar"];
        
        intMsgsScreen = 10;
        intEventsScreen = 10;
        strBuubleImage = nil;
        self.navigationItem.hidesBackButton = YES;
        if(self.isFromStudent){
            UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
            
            self.navigationItem.leftBarButtonItem = leftItem;
        }else{
            
            UIView *viewAddBtnsLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 40)];
            
            UIButton * buttonMenu = [[UIButton alloc]initWithFrame:CGRectMake(-3, 4, 30, 30) ];
            
            
            [buttonMenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
            
            buttonMenu.backgroundColor = [UIColor clearColor];
            
            [buttonMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
            
            [viewAddBtnsLeft addSubview:buttonMenu];
            
            self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:viewAddBtnsLeft];
        }
        
        
        
        
        _strFromScreen = @"fromTab";
        
        stringFromPrevious = @"fromTab";
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        rightButton.bounds = CGRectMake( 10, 0, 30 , 30 );//set bound as per you want
        [rightButton addTarget:self action:@selector(rightBArButtonAction) forControlEvents:UIControlEventTouchUpInside];
        
        [_buttonCmpnyAndClubs setTitle:@"Student Societies" forState:UIControlStateNormal];
        [_buttonEvents setTitle:@"Events only" forState:UIControlStateNormal];
        labelTitleName.text = @"Companies";
        
        labelSubTitle.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userDiscipline"];
        
        [rightButton setImage:[UIImage imageNamed:@"calender"] forState:UIControlStateNormal];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        self.navigationItem.rightBarButtonItem = backButton;
        
        _buttonMessages.backgroundColor = [UIColor whiteColor];
        [_buttonMessages setImage:[UIImage imageNamed:@"messagesSelect"] forState:UIControlStateNormal];
        _labelmessag.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    }
}


-(void)methodForPaging: (NSMutableArray*)arrData numberOfMsgsToLoad:(NSInteger) numberOfMsgsToLoad
{
    
    if (frombutton == YES)
    {
        [bubbleTable setContentOffset:CGPointZero animated:NO];
    }
    
    [arrAllData removeAllObjects];
    
    arrayPaging = [[NSArray alloc] initWithArray:arrData];
    
    if (arrayPaging.count >= 20)
    {
        if (arrayPaging.count == intRowsForTable)
        {
            isPaging =NO;
        }
        else
        {
            isPaging = YES;
        }
        
        if (numberOfMsgsToLoad < arrayPaging.count)
        {
            numberOfMsgsToLoad  = arrayPaging.count;
        }
        
        if (numberOfMsgsToLoad > arrayPaging.count)
        {
            numberOfMsgsToLoad  = arrayPaging.count;
        }
        
        if ( [stringMessageDetail isEqualToString:@"messagedetail"])
        {
            
        }
        
        else
        {
            if (intRowsForTable==0)
            {
                if (arrayPaging.count>20)
                {
                    intRowsForTable = 20;
                }
                else
                {
                    intRowsForTable =  (int) arrayPaging.count;
                }
            }
            
            if (intRowsForTable > arrayPaging.count)
            {
                intRowsForTable =  (int) arrayPaging.count;
            }
            
            if (arrayPaging.count>20)
            {
                intRowsForTable = 20;
            }
        }
        
        isSort = YES;
        
        NSArray *tempArray = [arrayPaging subarrayWithRange:NSMakeRange(0, intRowsForTable)];
        
        arrAllData = [[NSMutableArray alloc] initWithArray:[[tempArray reverseObjectEnumerator] allObjects]];
        
    }
    
    else
    {
        isPaging = NO;
        isSort = YES;
        
        arrAllData = [[NSMutableArray alloc] initWithArray:[[arrData reverseObjectEnumerator] allObjects]];
        
        intRowsForTable = (int) arrAllData.count;
        
    }
    
    [bubbleTable reloadData];
    
    if ( [stringMessageDetail isEqualToString:@"messagedetail"])
        
    {
        stringMessageDetail = @"";
    }
    
    else if (isRefreshingMessages)
        
    {
        [self scrollTableToBottom];
        
    }
    
    else
    {
        if (arrAllData.count>0)
            
        {
            [self scrollTableToBottom];
        }
    }
    
    if ([spinner superview])
        
    {
        [self ShowDataLoader:NO];
    }
    
    NSLog(@"arrAllData is %lu",(unsigned long)arrAllData.count);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    stringForNotifyEditCompany = @"message";
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"])
    {
        self.imageRedStar.hidden = YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    
    isRefreshingMessages = NO;
    
    stringNotifiction = @"frommessage";
    
    self.navigationController.navigationBar.tintColor= [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    if ([_strEventsOfScreen isEqualToString:@"eventsClubs&Societies"])
    {
        [self methodForPaging:[_arrCompanyEventsPass mutableCopy] numberOfMsgsToLoad:intEventsScreen];
    }
    else if ([_strEventsOfScreen isEqualToString:@"eventsPort"])
    {
        [self methodForPaging:[_arrCompanyEventsPass mutableCopy] numberOfMsgsToLoad:intEventsScreen];
    }
    else if ([_strEventsOfScreen isEqualToString:@"eventsPerfectGraduate"])
    {
        [self methodForPaging:[_arrCompanyEventsPass mutableCopy] numberOfMsgsToLoad:intEventsScreen];
    }
    else if ([_strEventsOfScreen isEqualToString:@"eventsCompamyFromTab"]||[_strEventsOfScreen isEqualToString:@"eventsSocietyFromTab"])
    {
        if ([_strEventsOfScreen isEqualToString:@"eventsCompamyFromTab"])
        {
            [self methodForPaging:[_arrCompanyEventsPass mutableCopy] numberOfMsgsToLoad:intEventsScreen];
        }
        
        else
        {
            [self methodForPaging:[_arrSocietyEventsPass mutableCopy] numberOfMsgsToLoad:intEventsScreen];
        }
    }
    else
    {
        
    }
    if ( [stringMessageDetail isEqualToString:@"messagedetail"])
    {
        NSLog(@"arr all data count is %lu", (unsigned long)arrayAllMessageData.count);
        
        int index = (int) [[arrayAllMessageData valueForKey:@"id"]indexOfObject:stringSelectedMsg];
        
        NSLog(@"index is %d",index);
        
        NSMutableDictionary *dictnryTemp = [[NSMutableDictionary alloc]initWithDictionary:arrayAllMessageData[index] copyItems:YES];
        
        if ([stringLike isEqualToString:@"like"])
        {
            stringLike = @"";
            
            boolRead = YES;
            
            if ([[dictnryTemp objectForKey:@"status"]isEqualToString:@"unread"])
            {
                [dictnryTemp removeObjectForKey:@"status"];
                
                [dictnryTemp setObject:@"read" forKey:@"status"];
            }
            [dictnryTemp removeObjectForKey:@"like_status"];
            
            [dictnryTemp setObject:@"1" forKey:@"like_status"];
        }
        
        else
        {
            boolRead = YES;
            
            if ([[dictnryTemp objectForKey:@"status"]isEqualToString:@"unread"])
            {
                [dictnryTemp removeObjectForKey:@"status"];
                
                [dictnryTemp setObject:@"read" forKey:@"status"];
            }
            else
            {
                
            }
        }
        
        if ([stringReport isEqualToString: @"report"])
        {
            if ([stringBlock isEqualToString:@"1"])
            {
                [dictnryTemp removeObjectForKey:@"report"];
                
                [dictnryTemp setObject:@"1" forKey:@"report"];
            }
            
            else
            {
                [dictnryTemp removeObjectForKey:@"report"];
                
                [dictnryTemp setObject:@"0" forKey:@"report"];
            }
            
            boolRead = YES;
            
            stringReport = @"";
            
            stringBlock = @"";
        }
        
        if ([stringFromSocietyMessage isEqualToString:@"added"])
        {
            [dictnryTemp removeObjectForKey:@"addedStatus"];
            
            [dictnryTemp setObject:@"1" forKey:@"addedStatus"];
            
            stringFromSocietyMessage = @"";
        }
        
        else if ([stringFromSocietyMessage isEqualToString:@"removed"])
        {
            [dictnryTemp removeObjectForKey:@"addedStatus"];
            
            [dictnryTemp setObject:@"0" forKey:@"addedStatus"];
            
            stringFromSocietyMessage = @"";
        }
        
        if ([stringRequestInvite isEqualToString: @"requested"])
        {
            [dictnryTemp removeObjectForKey:@"eventconfirmation"];
            
            [dictnryTemp setObject:@"Request Sent" forKey:@"eventconfirmation"];
            
            stringRequestInvite =@"";
        }
        
        int index1 = (int) [[arrayMessageGlobal valueForKey:@"id"]indexOfObject:stringSelectedMsg];
        
        NSLog(@"index is %d",index1);
        
        [arrayMessageGlobal replaceObjectAtIndex:index1 withObject:dictnryTemp];
        
        [arrayAllMessageData replaceObjectAtIndex:index withObject:dictnryTemp];
        
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:arrayAllMessageData,@"data", nil];
        
        [self placeDataOnTable:dict];
    }
    
    else if(arrayMessageGlobal.count>0)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"callMsgWebService"])
        {
            [self getMessages];
        }
        else
        {
            [arrayAllMessageData removeAllObjects];
            
            arrayAllMessageData = [arrayMessageGlobal mutableCopy];
            
            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:arrayAllMessageData,@"data", nil];
            
            [self placeDataOnTable:dict];
            
        }
    }
    
    else
    {
        
        [self checkReachability];
        
    }
    
    static NSString *CellIdentifier = @"cellID";
    
    MessagesTableViewCell *cell=(MessagesTableViewCell*)[bubbleTable dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.backgroundColor = [UIColor clearColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"messageNotification"
                                               object:nil];
    
    [ [NSNotificationCenter defaultCenter]addObserver:self selector:@selector(callMessageWebService:) name:@"messageReloadBackground" object:nil];
    
    [ [NSNotificationCenter defaultCenter]addObserver:self selector:@selector(callMessageReload) name:@"companyEditReload" object:nil];
    
    if(self.isFromStudent)
    {
        [self.buttonCmpnyAndClubs setEnabled:NO];
        [self.buttonCmpnyAndClubs setTitleColor:[UIColor colorWithRed:131.0f/255.0f green:131.0f/255.0f blue:131.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    }
}
-(void)callMessageReload
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"callMsgWebService"];
    isRefreshingMessages = YES;
    [self checkReachability];
}
-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"messageNotification"])
        NSLog (@"Successfully received the test notification!");
    
    [self ShowDataLoader:YES];
    
    [self getMessages];
}

-(void)checkReachability
{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"callMsgWebService"])
    {
        if (boolNotifyEditCompanyMessage == YES)
        {
            [self loadMessageOffline];
            
            [self performSelectorInBackground:@selector(getMessages) withObject:nil];
        }
        else
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(),
                           ^{
                               if ([stringLike isEqualToString:@"like"])
                               {
                                   [self ShowDataLoader:NO];
                               }
                               else
                               {
                                   [self ShowDataLoader:YES];
                               }
                               
                               [self getMessages];
                           });
        }
    }
    else
    {
        if(!isRefreshingMessages)
        {
            NSLog(@"notConnected");
            
            [self loadMessageOffline];
        }
        else
        {
            NSLog(@"connected");
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
                [self getMessages];
            });
        }
    }
}

-(void)backBtnAction
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)loadMessageOffline
{
    isInternetOff = YES;
    [arrayCompanyInvites removeAllObjects];
    [arraySocietyInvites removeAllObjects];
    
    if (self.view)
    {
        if ([[[dummy loadDataMessages:@"Select * From MessagesTable"] copy] count]>0)
        {
            NSArray *arrOffineMessages;
            
            if (arrayMessageGlobal.count>0)
            {
                arrOffineMessages = [ arrayMessageGlobal copy];
            }
            else
            {
                
                arrOffineMessages = [[dummy loadDataMessages:@"Select * From MessagesTable"] copy];
            }
            arrayAllMessageData = [[NSMutableArray alloc]initWithArray:arrOffineMessages];
            [self placeDataOnTable:@{@"data":arrOffineMessages}];
        }
        
        else
        {
            [self getMessages];
        }
        
        [self.navigationController.view setUserInteractionEnabled:YES];
    }
}

#pragma mark- UIActivityIndicatorView

-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        
        spinner.center = self.view.center;
        spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [spinner startAnimating];
        [self.navigationController.navigationBar setUserInteractionEnabled:NO];
        [self.view.window setUserInteractionEnabled:NO];
        [self.view addSubview:spinner];
    }
    
    else
    {
        [spinner stopAnimating];
        [self.view.window setUserInteractionEnabled:YES];
        [self.navigationController.navigationBar setUserInteractionEnabled:YES];
        [spinner removeFromSuperview];
        spinner = nil;
    }
}

#pragma mark- getMessagesWebService Need changes on this screen add data in database and then get that key from data base and do next task.

-(void)getMessages
{
    messageAlrdyLoad = NO;
    
    if ([[[dummy loadDataMessages:@"Select * From MessagesTable"] copy] count]==0||[[NSUserDefaults standardUserDefaults] boolForKey:@"callMsgWebService"])
    {
        if (boolNotifyEditCompanyMessage==YES)
        {
            [self ShowDataLoader:NO];
            
            boolNotifyEditCompanyMessage = NO;
        }
        else
        {
            if ([stringLike isEqualToString:@"like"])
            {
                [self ShowDataLoader:NO];
            }
            else
            {
                [self ShowDataLoader:YES];}
        }
    }
    self.navigationController.view.userInteractionEnabled = NO;
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"getmessages" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     
     {
         NSDictionary *dictResponse = [[NSDictionary alloc]initWithDictionary:responseObject];
         
         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"callMsgWebService"];
         
         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         [arrayCompanyInvites removeAllObjects];
         
         [arraySocietyInvites removeAllObjects];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [self.navigationController.view setUserInteractionEnabled:NO];
             
             
             [self saveMessageData:dictResponse];
             
         }
         else
         {
             if ([spinner isAnimating])
             {
                 [self ShowDataLoader:NO];
             }
         }
         self.navigationController.view.userInteractionEnabled = YES;
         
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         else
         {
             [self ShowDataLoader:NO];
         }
         
         if (isRefreshingMessages)
         {
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"No Internet " message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"No Internet " message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }
         else
         {
             NSLog(@"%@",error.localizedDescription);
         }
         
         isInternetOff = YES;
         
         NSArray *arrOffineMessages;
         
         if (arrayMessageGlobal.count>0)
         {
             arrOffineMessages = [ arrayMessageGlobal copy];
         }
         else
         {
             
             arrOffineMessages = [[dummy loadDataMessages:@"Select * From MessagesTable"] copy];
         }
         
         arrayAllMessageData = [NSMutableArray arrayWithArray:arrOffineMessages];
         
         [self placeDataOnTable:@{@"data":arrOffineMessages}];
         
         self.navigationController.view.userInteractionEnabled = YES;
     }];
}


-(void)saveMessageData:(NSDictionary *)dictResponse
{
    if ([dummy deleteTable:@"delete from MessagesTable"])
    {
        if ([dummy deleteTable:@"delete from messagerAdderDetailTable"])
        {
            for (int i=0; i<[[dictResponse valueForKey:@"data"] count]; i++)
            {
                NSString *strLogo = [[[dictResponse valueForKeyPath:@"data.adder_detail"] valueForKey:@"logo"]objectAtIndex:i];
                if ([strLogo isKindOfClass:[NSNull class]])
                {
                    strLogo = @"";
                }
                NSString *perfectLogo = [[dictResponse valueForKeyPath:@"data.perfect_logo"]objectAtIndex:i];
                
                if ([perfectLogo isKindOfClass:[NSNull class]])
                {
                    perfectLogo = @"";
                }
                NSString *queryMessages = [NSString stringWithFormat: @"INSERT INTO MessagesTable (adder_id,date, event_date,event_title,event_venue,eventtimefrom,eventtimeto,gender,id,message,message_type,status,type,logo,name,eventconfirmation,default_timezone,website,report,like_status,addedStatus,eventstatus,perfect_logo) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\",\"%@\", \"%@\",\"%@\",\"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")"
                                           ,[[dictResponse valueForKeyPath:@"data.adder_id"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.date"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.event_date"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.event_title"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.event_venue"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.eventtimefrom"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.eventtimeto"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.gender"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.id"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.message"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.message_type"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.status"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.type"]objectAtIndex:i]
                                           ,strLogo
                                           ,[[[dictResponse valueForKeyPath:@"data.adder_detail"] valueForKey:@"name"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.eventconfirmation"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.default_timezone"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.website"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.report"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.like_status"]objectAtIndex:i]
                                           ,[[dictResponse valueForKeyPath:@"data.addedStatus"]objectAtIndex:i], [[dictResponse valueForKeyPath:@"data.eventstatus"]objectAtIndex:i],perfectLogo];
                [self saveImages:[NSString stringWithFormat:@"%@%@",[[dictResponse valueForKeyPath:@"data.adder_id"]objectAtIndex:i],strLogo] imageUrl:strLogo];
                
                [self saveImages:[NSString stringWithFormat:@"%@%@",[[dictResponse valueForKeyPath:@"data.adder_id"]objectAtIndex:i],perfectLogo] imageUrl:perfectLogo];
                
                const char *queryMessagesStat = [queryMessages UTF8String];
                
                [dummy runQueries:queryMessagesStat strForscreen:@"messsages"];
            }
        }
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"messageAlert"])
    {
        [self performSelectorOnMainThread:@selector(alertOnMainThread) withObject:nil waitUntilDone:NO];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"messageAlert"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    NSArray *arrOffineMessages;
    arrOffineMessages = [[dummy loadDataMessages:@"Select * From MessagesTable"] copy];
    
    NSLog(@"arr from database is %@", arrOffineMessages);
    
    [arrayMessageGlobal removeAllObjects];
    
    arrayMessageGlobal = [[dummy loadDataMessages:@"Select * From MessagesTable"] mutableCopy];
    
    if ([stringFromPrevious isEqualToString:@"fromTab"])
    {
        if ([[arrOffineMessages valueForKey:@"status"] containsObject:@"unread"])
        {
            
        }
        
        else
        {
            _imageVwStar.hidden = YES;
        }
    }
    arrayAllMessageData = [[NSMutableArray alloc]initWithArray:arrOffineMessages];
    
    [self placeDataOnTable:@{@"data":arrOffineMessages}];
}


-(void)alertOnMainThread
{
    [[[UIAlertView alloc]initWithTitle:@"Messages" message:@"\nTo help you connect with companies and stay active with student societies, you can follow their messages and sign up to their events. \n\nThe more events you sign up for, the more opportunities you’ll have to connect with companies." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
}


#pragma mark- SaveImagesInDataBase
-(void)saveImages:(NSString *)ImageName imageUrl :(NSString *)imageUrl
{
    if (imageUrl.length>0)
    {
        ImageName = [ImageName stringByReplacingOccurrencesOfString:@"/" withString:@""];
        if (![self imageCheck:ImageName])
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:ImageName];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,imageUrl]]];
            [imageData writeToFile:savedImagePath atomically:NO];
        }
    }
}

-(BOOL)imageCheck :(NSString *)strName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    if ([contents containsObject:strName])
    {
        return YES;
    }
    return NO;
}


-(void)placeDataOnTable:(NSDictionary*)dictData
{
    
    if ([_strFromScreen isEqualToString:@"port"])
    {
        [self loadParticularCompanyMessages:dictData];
    }
    else if ([_strFromScreen isEqualToString:@"Clubs&Societies"])
    {
        [self loadAllSocietyMessages:dictData];
    }
    else if ([_strFromScreen isEqualToString:@"PerfectGraduate"])
    {
        [self loadAdminMessagesAndEvents:dictData];
    }
    else if ([_strFromScreen isEqualToString:@"fromMenu"])
    {
        [self loadAdminSocietyEvents:dictData];
    }
    else
        [self loadAllMessages:dictData];
}


#pragma mark- loadMessagesAndFillArrays

-(void)loadAdminMessagesAndEvents:(NSDictionary*)dictResponse
{
    [arrayCompMsgsAndEvents removeAllObjects];
    [arrayCompanyEvents removeAllObjects];
    [arrayCompanyInvites removeAllObjects];
    
    for (int i=0; i<[[dictResponse objectForKey:@"data"] count]; i++)
    {
        if ([[[dictResponse valueForKeyPath:@"data.type"]objectAtIndex:i] isEqualToString:@"admin"])
        {
            if ([[[dictResponse valueForKeyPath:@"data.message_type"]objectAtIndex:i] isEqualToString:@"event"])
            {
                
                if (![[[dictResponse valueForKeyPath:@"data.eventconfirmation"]objectAtIndex:i] isEqualToString:@"Not Sent"])
                {
                    [arrayCompanyInvites addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                }
            }
            [arrayCompMsgsAndEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
            
            if ([[[dictResponse valueForKeyPath:@"data.message_type"]objectAtIndex:i] isEqualToString:@"event"])
            {
                [arrayCompanyEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
            }
        }
    }
    [self loadTableWithAllSocietyMsgsOnRefresh];
    
}


-(void)loadParticularCompanyMessages:(NSDictionary*)dictResponse
{
    NSLog(@"dict response is %@", dictResponse);
    NSArray *arrayCo = [dictResponse objectForKey:@"data"];
    
    NSLog(@"array co is %@", arrayCo);
    
    [arrayCompMsgsAndEvents removeAllObjects];
    [arrayCompanyEvents removeAllObjects];
    [arrayCompanyInvites removeAllObjects];
    
    for (int i=0; i<[arrayCo count]; i++)
    {
        if ([[[dictResponse valueForKeyPath:@"data.message_type"]objectAtIndex:i] isEqualToString:@"event"])
        {
            if ([[_arrComapanyDetail valueForKey:@"id"] isEqualToString:[[[dictResponse valueForKeyPath:@"data.adder_detail"]valueForKey:@"id"]objectAtIndex:i]])
            {
                if (![[[dictResponse valueForKeyPath:@"data.eventconfirmation"]objectAtIndex:i] isEqualToString:@"Not Sent"])
                {
                    [arrayCompanyInvites addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                }
            }
        }
        
        if ([[_arrComapanyDetail valueForKey:@"id"] isEqualToString:[[[dictResponse valueForKeyPath:@"data.adder_detail"]valueForKey:@"id"]objectAtIndex:i]])
        {
            if ([[[dictResponse valueForKeyPath:@"data.type"]objectAtIndex:i] isEqualToString:@"companybehalf"])
            {
                [arrayCompMsgsAndEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                
                if ([[[dictResponse valueForKeyPath:@"data.message_type"]objectAtIndex:i] isEqualToString:@"event"])
                {
                    [arrayCompanyEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                }
            }
        }
    }
    [self loadTableWithAllSocietyMsgsOnRefresh];
}


-(void)loadAdminSocietyEvents:(NSDictionary*)dictResponse
{
    [arrAllData removeAllObjects];
    
    [arrayCompanyInvites removeAllObjects];
    
    for (int i=0; i<[[dictResponse objectForKey:@"data"] count]; i++)
    {
        if ([[[dictResponse valueForKeyPath:@"data.message_type"]objectAtIndex:i] isEqualToString:@"event"])
        {
            
            if (![[[dictResponse valueForKeyPath:@"data.eventconfirmation"]objectAtIndex:i] isEqualToString:@"Not Sent"])
            {
                [arrayCompanyInvites addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
            }
        }
    }
    [self methodForPaging:arrayCompanyInvites numberOfMsgsToLoad:intMsgsScreen];
}

-(void)loadAllSocietyMessages:(NSDictionary*)dictResponse
{
    [arrayCompMsgsAndEvents removeAllObjects];
    [arrayCompanyInvites removeAllObjects];
    [arrayCompanyEvents removeAllObjects];
    
    NSArray *arraySelectedsocities = [[NSUserDefaults standardUserDefaults]objectForKey:USER_SELECTED_SOCITIES];
    
    NSLog(@"arraySelectedsocities is %@", arraySelectedsocities);
    
    for (int i=0; i<[[dictResponse objectForKey:@"data"] count]; i++)
    {
        if ([[[dictResponse valueForKeyPath:@"data.type"]objectAtIndex:i] isEqualToString:@"studentsociety"])
        {
            NSLog(@"id value is %@", [[[dictResponse valueForKeyPath:@"data.adder_detail"]valueForKey:@"id"]objectAtIndex:i]);
            
            if ([[[dictResponse valueForKeyPath:@"data.message_type"]objectAtIndex:i] isEqualToString:@"event"])
            {
                if (([arraySelectedsocities containsObject:[[[dictResponse valueForKeyPath:@"data.adder_detail"]valueForKey:@"id"]objectAtIndex:i]]))
                {
                    
                    if (![[[dictResponse valueForKeyPath:@"data.eventconfirmation"]objectAtIndex:i] isEqualToString:@"Not Sent"])
                    {
                        if (self.isFromStudent) {
                            if ([[self.dictStudentSoc valueForKey:@"id"] isEqualToString:[[[dictResponse valueForKeyPath:@"data.adder_detail"]valueForKey:@"id"]objectAtIndex:i]])
                            {
                                [arrayCompanyInvites addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                            }
                        }else{
                            [arrayCompanyInvites addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                        }
                        
                    }
                }
                
            }
            
            if (([arraySelectedsocities containsObject:[[[dictResponse valueForKeyPath:@"data.adder_detail"]valueForKey:@"id"]objectAtIndex:i]]))
            {
                if (self.isFromStudent) {
                    if ([[self.dictStudentSoc valueForKey:@"id"] isEqualToString:[[[dictResponse valueForKeyPath:@"data.adder_detail"]valueForKey:@"id"]objectAtIndex:i]])
                    {
                        [arrayCompMsgsAndEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                    }
                }else{
                    [arrayCompMsgsAndEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                }
            }
            
            if ([[[dictResponse valueForKeyPath:@"data.message_type"]objectAtIndex:i] isEqualToString:@"event"])
            {
                if (([arraySelectedsocities containsObject:[[[dictResponse valueForKeyPath:@"data.adder_detail"]valueForKey:@"id"]objectAtIndex:i]]))
                {
                    if (self.isFromStudent) {
                        if ([[self.dictStudentSoc valueForKey:@"id"] isEqualToString:[[[dictResponse valueForKeyPath:@"data.adder_detail"]valueForKey:@"id"]objectAtIndex:i]])
                        {
                            [arrayCompanyEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                        }
                    }else{
                        [arrayCompanyEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                    }
                }
            }
        }
    }
    
    [self loadTableWithAllSocietyMsgsOnRefresh];
}


-(void)loadTableWithAllSocietyMsgsOnRefresh
{
    if ([_buttonEvents.titleLabel.text isEqualToString:@"Events only"])
    {
        [self methodForPaging:arrayCompMsgsAndEvents numberOfMsgsToLoad:intMsgsScreen];
    }
    else
    {
        [self methodForPaging:arrayCompanyEvents numberOfMsgsToLoad:intEventsScreen];
    }
}


-(void)loadAllMessages:(NSDictionary*)dictResponse
{
    [arrayCompMsgsAndEvents removeAllObjects];
    [arrayCompanyEvents removeAllObjects];
    [arrayCompanyInvites removeAllObjects];
    
    [arraySocietyMsgsAndEvents removeAllObjects];
    [arraySocietyInvites removeAllObjects];
    [arraySocietyEvents removeAllObjects];
    
    NSMutableArray *arrId = [[NSMutableArray alloc] init];
    for (NSDictionary *dictid in arrayPortDataGlobal){
        if([dictid objectForKey:@"id"]){
            NSString *strid = [[NSString stringWithFormat:@"%@",[dictid objectForKey:@"id"]] removeNull];
            [arrId addObject:strid];
        }
    }
    
    for (int i=0; i<[[dictResponse objectForKey:@"data"] count]; i++)
    {
        if ([[[dictResponse valueForKeyPath:@"data.type"]objectAtIndex:i] isEqualToString:@"companybehalf"])
        {
            if(self.isFromStudent){
                if([dictResponse valueForKeyPath:@"data.event_title"]){
                    NSString *strid2 = [[NSString stringWithFormat:@"%@",[[[[dictResponse objectForKey:@"data"] objectAtIndex:i] objectForKey:@"adder_detail"] objectForKey:@"id"]] removeNull];
                    if([arrId containsObject:strid2]){
                        NSString *titel = [[NSString stringWithFormat:@"%@",[[[[dictResponse objectForKey:@"data"] objectAtIndex:i] objectForKey:@"adder_detail"] objectForKey:@"name"]] removeNull];
                        if([titel isEqualToString:@""] || [titel isEqualToString:@"<null>"]){
                            
                        }else{
                            [arrayCompMsgsAndEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                        }
                    }
                }
            }else{
                [arrayCompMsgsAndEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                
            }
            
            
            if ([[[dictResponse valueForKeyPath:@"data.message_type"]objectAtIndex:i] isEqualToString:@"event"])
            {
                
                if (![[[dictResponse valueForKeyPath:@"data.eventconfirmation"]objectAtIndex:i] isEqualToString:@"Not Sent"])
                {
                    if(self.isFromStudent){
                        if([dictResponse valueForKeyPath:@"data.event_title"]){
                            NSString *strid2 = [[NSString stringWithFormat:@"%@",[[[[dictResponse objectForKey:@"data"] objectAtIndex:i] objectForKey:@"adder_detail"] objectForKey:@"id"]] removeNull];
                            if([arrId containsObject:strid2]){
                                NSString *titel = [[NSString stringWithFormat:@"%@",[[[[dictResponse objectForKey:@"data"] objectAtIndex:i] objectForKey:@"adder_detail"] objectForKey:@"name"]] removeNull];
                                if([titel isEqualToString:@""] || [titel isEqualToString:@"<null>"]){
                                    
                                }else{
                                    [arrayCompanyInvites addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                                }
                            }
                        }
                    }else{
                        [arrayCompanyInvites addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                        
                    }
                }
                if(self.isFromStudent){
                    if([dictResponse valueForKeyPath:@"data.event_title"]){
                        NSString *strid2 = [[NSString stringWithFormat:@"%@",[[[[dictResponse objectForKey:@"data"] objectAtIndex:i] objectForKey:@"adder_detail"] objectForKey:@"id"]] removeNull];
                        if([arrId containsObject:strid2]){
                            NSString *titel = [[NSString stringWithFormat:@"%@",[[[[dictResponse objectForKey:@"data"] objectAtIndex:i] objectForKey:@"adder_detail"] objectForKey:@"name"]] removeNull];
                            if([titel isEqualToString:@""] || [titel isEqualToString:@"<null>"]){
                                
                            }else{
                                [arrayCompanyEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                            }
                        }
                    }
                }else{
                    [arrayCompanyEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                    
                }
            }
        }
        else if ([[[dictResponse valueForKeyPath:@"data.type"]objectAtIndex:i] isEqualToString:@"studentsociety"])
        {
            [arraySocietyMsgsAndEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
            
            if ([[[dictResponse valueForKeyPath:@"data.message_type"]objectAtIndex:i] isEqualToString:@"event"])
            {
                if (![[[dictResponse valueForKeyPath:@"data.eventconfirmation"]objectAtIndex:i] isEqualToString:@"Not Sent"])
                {
                    [arraySocietyInvites addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                }
                [arraySocietyEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
            }
        }
        else if ([[[dictResponse valueForKeyPath:@"data.type"]objectAtIndex:i] isEqualToString:@"admin"])
        {
            if (!self.isFromStudent){
                [arrayCompMsgsAndEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                
                if ([[[dictResponse valueForKeyPath:@"data.message_type"]objectAtIndex:i] isEqualToString:@"event"])
                {
                    if (![[[dictResponse valueForKeyPath:@"data.eventconfirmation"]objectAtIndex:i] isEqualToString:@"Not Sent"])
                    {
                        [arrayCompanyInvites addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                    }
                    
                    [arrayCompanyEvents addObject:[[dictResponse valueForKey:@"data"]objectAtIndex:i]];
                }
            }
            
        }
    }
    
    if (_strFromScreen)
    {
        [self loadTableWithAllMsgsOnReload];
    }
    else
    {
        if ([_strFromScreen isEqualToString:@"fromTab"])
        {
            _arrSocietyEventsPass = arraySocietyInvites;
        }
        _arrCompanyEventsPass = arrayCompanyInvites;
    }
}

-(void)loadTableWithAllMsgsOnReload
{
    if ([_buttonCmpnyAndClubs.titleLabel.text isEqualToString:@"Companies"])
    {
        if (![_buttonEvents.titleLabel.text isEqualToString:@"Events only"])
        {
            [self methodForPaging:arraySocietyEvents numberOfMsgsToLoad:intMsgsScreen];
        }
        else
        {
            [self methodForPaging:arraySocietyMsgsAndEvents numberOfMsgsToLoad:intMsgsScreen];
        }
    }
    else
    {
        if (![_buttonEvents.titleLabel.text isEqualToString:@"Events only"])
        {
            [self methodForPaging:arrayCompanyEvents numberOfMsgsToLoad:intEventsScreen];
        }
        else
        {
            [self methodForPaging:arrayCompMsgsAndEvents numberOfMsgsToLoad:intMsgsScreen];
        }
    }
}


-(void)scrollTableToBottom
{
    NSInteger index = [arrAllData count]-1;
    
    if (index>0)
    {
        timer = [NSTimer scheduledTimerWithTimeInterval:0.4f
                                                 target: self
                                               selector:@selector(abc)
                                               userInfo:nil repeats:NO];
    }
    
    if ([spinner superview])
    {
        [self ShowDataLoader:NO];
    }
}

- (void)abc
{
    [bubbleTable layoutIfNeeded];
    
    if (arrAllData.count>0)
    {
        NSInteger index = intRowsForTable-1;
        NSIndexPath *indexPath1 = [NSIndexPath indexPathForItem:index inSection:0];
        [bubbleTable scrollToRowAtIndexPath:indexPath1 atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)rightBArButtonAction
{
    
    MessagesViewController  *messageVw = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    
    if ([_strFromScreen isEqualToString:@"PerfectGraduate"])
    {
        messageVw.strEventsOfScreen = @"eventsPerfectGraduate";
    }
    
    else if ([_strFromScreen isEqualToString:@"Clubs&Societies"])
    {
        messageVw.strEventsOfScreen = @"eventsClubs&Societies";
    }
    
    else if ([_strFromScreen isEqualToString:@"fromTab"])
    {
        if ([labelTitleName.text isEqualToString:@"Companies"])
        {
            messageVw.strEventsOfScreen = @"eventsCompamyFromTab";
        }
        else
        {
            messageVw.strEventsOfScreen = @"eventsSocietyFromTab";
        }
        messageVw.arrSocietyEventsPass = arraySocietyInvites;
    }
    
    else if ([_strFromScreen isEqualToString:@"port"])
    {
        messageVw.strEventsOfScreen = @"eventsPort";
        
        messageVw.strNavTitle = [_arrComapanyDetail valueForKey:@"company_name"];
    }
    
    messageVw.arrCompanyEventsPass = arrayCompanyInvites;
    
    intMsgsScreen = arrAllData.count;
    
    _imageVwStar.hidden = YES;
    messageVw.isFromStudent = self.isFromStudent;
    [self.navigationController pushViewController:messageVw animated:NO];
}

#pragma mark - Keyboard events

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = textInputView.frame;
        frame.origin.y -= kbSize.height;
        textInputView.frame = frame;
        
        frame = bubbleTable.frame;
        frame.size.height -= kbSize.height;
        bubbleTable.frame = frame;
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = textInputView.frame;
        frame.origin.y += kbSize.height;
        textInputView.frame = frame;
        frame = bubbleTable.frame;
        frame.size.height += kbSize.height;
        bubbleTable.frame = frame;
    }];
}

#pragma mark- segmentButtonActions

- (IBAction)buttonCmpnyAndClubsAction:(id)sender
{
    frombutton = YES;
    
    stringMessageDetail = @"";
    
    isRefreshingMessages = YES;
    
    if ([_strFromScreen isEqualToString:@"fromTab"])
    {
        if ([_buttonCmpnyAndClubs.titleLabel.text isEqualToString:@"Companies"])
        {
            
            [_buttonCmpnyAndClubs setTitle:@"Student Societies" forState:UIControlStateNormal];
            
            labelTitleName.text = @"Companies";
            
            labelSubTitle.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userDiscipline"];
            
            if (![_buttonEvents.titleLabel.text isEqualToString:@"Events only"])
            {
                [self methodForPaging:arrayCompanyEvents numberOfMsgsToLoad:intEventsScreen];
            }
            
            else
            {
                [self methodForPaging:arrayCompMsgsAndEvents numberOfMsgsToLoad:intMsgsScreen];
            }
            _imageVwStar.hidden = NO;
        }
        else
        {
            NSString *stringFirst1 =[USERDEFAULT objectForKey:@"FirstCome"] ;
            
            if ([stringFirst1 isEqualToString:@"value"])
            {
            }
            else
            {
                [USERDEFAULT setObject:@"value" forKey:@"FirstCome"];
            }
            
            [_buttonCmpnyAndClubs setTitle:@"Companies" forState:UIControlStateNormal];
            labelTitleName.text = @"Student Societies";
            labelSubTitle.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userUniversity"];
            if (![_buttonEvents.titleLabel.text isEqualToString:@"Events only"])
            {
                [self methodForPaging:arraySocietyEvents numberOfMsgsToLoad:intEventsScreen];
            }
            else
            {
                [self methodForPaging:arraySocietyMsgsAndEvents numberOfMsgsToLoad:intMsgsScreen];
            }
            _imageVwStar.hidden = YES;
        }
    }
    
    else if ([_strEventsOfScreen isEqualToString:@"eventsCompamyFromTab"]||[_strEventsOfScreen isEqualToString:@"eventsSocietyFromTab"])
    {
        if ([_buttonCmpnyAndClubs.titleLabel.text isEqualToString:@"Companies"])
        {
            [_buttonCmpnyAndClubs setTitle:@"Student Societies" forState:UIControlStateNormal];
            
            labelSubTitle.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userUniversity"];
            
            labelSubTitle.text = @"Companies";
            
            [self methodForPaging:[_arrCompanyEventsPass mutableCopy] numberOfMsgsToLoad:10];
            
        }
        else
        {
            [_buttonCmpnyAndClubs setTitle:@"Companies" forState:UIControlStateNormal];
            
            labelSubTitle.text = @"Student Societies";
            
            [self methodForPaging:[_arrSocietyEventsPass mutableCopy] numberOfMsgsToLoad:10];
            
            _imageVwStar.hidden = YES;
        }
    }
}


- (IBAction)buttonEventsAction:(id)sender
{
    isRefreshingMessages = YES;
    
    frombutton = YES;
    
    if ([_strFromScreen isEqualToString:@"fromTab"])
    {
        if ([_buttonEvents.titleLabel.text isEqualToString:@"Events only"])
        {
            [_buttonEvents setTitle:@"Messages & Events" forState:UIControlStateNormal];
            
            if ([_buttonCmpnyAndClubs.titleLabel.text isEqualToString:@"Companies"])
            {
                [self methodForPaging:arraySocietyEvents numberOfMsgsToLoad:intEventsScreen];
            }
            else
            {
                [self methodForPaging:arrayCompanyEvents numberOfMsgsToLoad:intEventsScreen];
            }
        }
        else
        {
            [_buttonEvents setTitle:@"Events only" forState:UIControlStateNormal];
            if ([_buttonCmpnyAndClubs.titleLabel.text isEqualToString:@"Companies"])
            {
                [self methodForPaging:arraySocietyMsgsAndEvents numberOfMsgsToLoad:intMsgsScreen];
            }
            else
            {
                [self methodForPaging:arrayCompMsgsAndEvents numberOfMsgsToLoad:intMsgsScreen];
            }
        }
    }
    else if ([_strFromScreen isEqualToString:@"port"]||[_strFromScreen isEqualToString:@"Clubs&Societies"]||[_strFromScreen isEqualToString:@"PerfectGraduate"])
    {
        if ([_buttonEvents.titleLabel.text isEqualToString:@"Events only"])
        {
            [self methodForPaging:arrayCompanyEvents numberOfMsgsToLoad:intEventsScreen];
            [_buttonEvents setTitle:@"Messages & Events" forState:UIControlStateNormal];
        }
        else
        {
            [_buttonEvents setTitle:@"Events only" forState:UIControlStateNormal];
            [self methodForPaging:arrayCompMsgsAndEvents numberOfMsgsToLoad:intMsgsScreen];
        }
    }
}


-(void)viewDidUnload
{
    arrayPaging = nil;
    arraySocietyEvents = nil;
    arraySocietyInvites = nil;
    arraySocietyMsgsAndEvents = nil;
    arrayCompMsgsAndEvents =nil;
    arrAllData =nil;
    arrayCompanyEvents = nil;
}


-(void)moveOnAnotherScreen
{
    if (boolRead == YES)
    {
        boolRead = NO;
        
        messageAlrdyLoad = YES;
        
        if(![Utilities CheckInternetConnection])
        {
        }
        else
        {
            [self performSelectorInBackground:@selector(getMessages) withObject:nil];
        }
    }
    else
    {
        
    }
}


#pragma mark- TabButtonActions

- (IBAction)buttonActionPortfolio:(id)sender;
{
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    
    for(int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[PortfolioViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    PortfolioViewController *portfolioView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
    [self.navigationController pushViewController:portfolioView animated:NO];
}

- (IBAction)buttonActionDiscip:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[DisciplineViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(),
                   ^{
                       [self getMessages];
                   });
    
    DisciplineViewController *disciplineVw = [self.storyboard instantiateViewControllerWithIdentifier:@"disciplineView"];
    
    [self.navigationController pushViewController:disciplineVw animated:NO];
}


- (IBAction)buttonSearch:(id)sender;
{
    LocationFilterSearchViewController *searchVw = nil;
    searchVw = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVw"];
    self.navigationController.navigationBar.hidden = NO;
    [self.navigationController pushViewController:searchVw animated:NO];
    
    searchVw = nil;
}


- (IBAction)buttonMessages:(id)sender;
{
    MessagesViewController  *messageVw = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    [self.navigationController pushViewController:messageVw animated:NO];
}


- (IBAction)buttonActionSalary:(id)sender;
{
    _strFromScreen = nil;
    _strEventsOfScreen = nil;
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[SalaryGuideViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    SalaryGuideViewController *salaryVw = [self.storyboard instantiateViewControllerWithIdentifier:@"salaryVw"];
    
    [self.navigationController pushViewController:salaryVw animated:NO];
}

#pragma mark- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (arrAllData.count==0)
    {
        return 0;
    }
    else if (arrAllData.count == intRowsForTable)
    {
        return intRowsForTable;
    }
    return intRowsForTable;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cellID";
    
    MessagesTableViewCell *cell = (MessagesTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MessagesTableViewCell" owner:self options:nil];
        
        cell = [topLevelObjects objectAtIndex:0];
        
        cell.backgroundColor = [UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    UIView *headerView ;
    
    if (indexPath.row == 0)
    {
        if (!isPaging)
        {
            [headerView setHidden:YES];
            
            cell.labelMsgDate.frame = CGRectMake(cell.labelMsgDate.frame.origin.x, 0, cell.labelMsgDate.frame.size.width, cell.labelMsgDate.frame.size.height);
            
            cell.viewEvents.frame = CGRectMake(cell.viewEvents.frame.origin.x, 20, cell.viewEvents.frame.size.width, cell.viewEvents.frame.size.height);
            
            cell.viewMessages.frame = CGRectMake(cell.viewMessages.frame.origin.x, 20, cell.viewMessages.frame.size.width, cell.viewMessages.frame.size.height);
        }
        else
        {
            headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, tableView.frame.size.width ,40)];
            headerView.backgroundColor= [UIColor clearColor];
            UIButton *btnLoadMessages = [[UIButton alloc] initWithFrame:CGRectMake(tableView.frame.size.width/2-50, 5, 100, 30)];
            [btnLoadMessages setTitle:@"Load Earlier" forState:UIControlStateNormal];
            btnLoadMessages.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:10.0f];
            btnLoadMessages.layer.cornerRadius = 5.0f;
            btnLoadMessages.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0] CGColor];
            [btnLoadMessages setTitleColor:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0] forState:UIControlStateNormal];
            btnLoadMessages.layer.borderWidth = 1.0f;
            btnLoadMessages.backgroundColor = [UIColor whiteColor];
            [btnLoadMessages addTarget:self action:@selector(loadMoreMsgs) forControlEvents:UIControlEventTouchUpInside];
            [headerView addSubview:btnLoadMessages];
            
            [cell.contentView addSubview:headerView];
            
            cell.labelMsgDate.frame = CGRectMake(cell.labelMsgDate.frame.origin.x, cell.labelMsgDate.frame.origin.y+40, cell.labelMsgDate.frame.size.width, cell.labelMsgDate.frame.size.height);
            
            cell.viewEvents.frame = CGRectMake(cell.viewEvents.frame.origin.x, cell.viewEvents.frame.origin.y+40, cell.viewEvents.frame.size.width, cell.viewEvents.frame.size.height);
            
            cell.viewMessages.frame = CGRectMake(cell.viewMessages.frame.origin.x, cell.viewMessages.frame.origin.y+40, cell.viewMessages.frame.size.width, cell.viewMessages.frame.size.height);
            
        }
    }
    
scrollRectToVisible:CGRectMake(0, 0, 1, 1);
    
    if ([[[arrAllData valueForKey:@"type"] objectAtIndex:indexPath.row] isEqualToString:@"admin"])
    {
        [self setAdminImages:indexPath.row cell:cell];
    }
    
    else if ([[[arrAllData valueForKey:@"type"] objectAtIndex:indexPath.row] isEqualToString:@"companybehalf"])
    {
        [self setCompanyImages:indexPath.row cell:cell];
    }
    
    else
    {
        [self setStudentSocietyImages:indexPath.row cell:cell];
    }
    
    cell.viewLogoEvents.tag = indexPath.row +100;
    
    cell.viewLogoMsgs.tag = indexPath.row +100;
    
    cell.imageVwLogoMsgs.userInteractionEnabled =YES;
    cell.imageVwLogoEvents.userInteractionEnabled =YES;
    
    cell.viewLogoEvents.layer.cornerRadius = 5.0f;
    cell.viewLogoEvents.clipsToBounds = YES;
    cell.viewLogoEvents.layer.borderWidth = 2.0f;
    cell.viewLogoEvents.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0] CGColor];
    
    cell.viewLogoMsgs.layer.cornerRadius = 5.0f;
    cell.viewLogoMsgs.clipsToBounds = YES;
    cell.viewLogoMsgs.layer.borderWidth = 2.0f;
    cell.viewLogoMsgs.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0] CGColor];
    
    cell.viewLogoEvents.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0] CGColor];
    
    cell.viewLogoEvents.layer.borderWidth = 2.0f;
    
    cell.btnMsg.tag = indexPath.row;
    
    cell.btnEvent.tag = indexPath.row;
    
    
    [cell.btnMsg addTarget:self action:@selector(ImageTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btnEvent addTarget:self action:@selector(ImageTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    NSLog(@"indexPath.row::::%ld",(long)indexPath.row);
    
    if ([[[arrAllData valueForKey:@"message_type"] objectAtIndex:indexPath.row] isEqualToString:@"event"])
    {
        cell.imageVwLogoEvents.tag = indexPath.row;
        
        cell.labelDate.text = [NSString stringWithFormat:@"%@",[[arrAllData valueForKey:@"date"] objectAtIndex:indexPath.row]];
        
        cell.labelEventName.text = [[arrAllData valueForKey:@"event_title"] objectAtIndex:indexPath.row];
        
        cell.labelEventCompanyName.text = [[arrAllData valueForKeyPath:@"adder_detail.name"] objectAtIndex:indexPath.row];
        
        cell.labelEventDate.text = [NSString stringWithFormat:@"%@", [[arrAllData valueForKey:@"event_date"] objectAtIndex:indexPath.row]];
        
        NSDateFormatter *dtFormatter1=[[NSDateFormatter alloc]init];
        [dtFormatter1 setDateFormat:@"dd/mm/yyyy"];
        NSDateFormatter *dtFormatter2=[[NSDateFormatter alloc]init];
        [dtFormatter2 setDateFormat:@"dd-MMM-yyyy"];
        if ([dtFormatter1 dateFromString:[NSString stringWithFormat:@"%@", [[arrAllData valueForKey:@"event_date"] objectAtIndex:indexPath.row]]]) {
            cell.labelEventDate.text =[dtFormatter2 stringFromDate:[dtFormatter1 dateFromString:[NSString stringWithFormat:@"%@", [[arrAllData valueForKey:@"event_date"] objectAtIndex:indexPath.row]]]];
        }
        
        //vijay007
        
        
        if ([[[arrAllData valueForKey:@"status"] objectAtIndex:indexPath.row] isEqualToString:@"unread"])
        {
            cell.imageViewEventReadStatus.hidden = NO;
            
            cell.imageViewEventReadStatus.layer.cornerRadius = cell.imageViewEventReadStatus.frame.size.width /2;
            
            cell.imageViewEventReadStatus.clipsToBounds = YES;
        }
        else
        {
            cell.imageViewEventReadStatus.hidden = YES;
        }
    }
    else
    {
        cell.imageVwLogoMsgs.tag = indexPath.row;
        
        cell.labelNameMsgs.text =[[arrAllData valueForKeyPath:@"adder_detail.name"] objectAtIndex:indexPath.row];
        
        cell.labelMsgDetail.text = [[arrAllData valueForKey:@"message"] objectAtIndex:indexPath.row];
        
        if ([[arrAllData valueForKey:@"type"][indexPath.row]isEqualToString:@"admin"])
        {
            cell.labelMsgDetail.textColor = [UIColor blueColor];
        }
        else
        {
            cell.labelMsgDetail.textColor = [UIColor blackColor];
        }
        
        if ([[[arrAllData valueForKey:@"status"] objectAtIndex:indexPath.row] isEqualToString:@"unread"])
        {
            cell.imageViewMessageReadStatus.hidden = NO;
            
            cell.imageViewMessageReadStatus.layer.cornerRadius = cell.imageViewEventReadStatus.frame.size.width /2;
            
            cell.imageViewMessageReadStatus.clipsToBounds = YES;
        }
        else
        {
            cell.imageViewMessageReadStatus.hidden = YES;
        }
    }
    
    NSString *string1 =[NSString stringWithFormat:@"%@",[[arrAllData valueForKey:@"date"]objectAtIndex:indexPath.row]];
    
    string1 = [string1 stringByReplacingOccurrencesOfString:@"PM" withString:@""];
    
    string1 = [string1 stringByReplacingOccurrencesOfString:@"AM" withString:@""];
    
    NSLog(@"date after pm is %@", string1);
    
    cell.labelMsgDate.text = [self calculateDateAccordingToTimeZone2:[[arrAllData valueForKey:@"date"] objectAtIndex:indexPath.row ] isDateType:nil timeZoneName:[[arrAllData valueForKey:@"default_timezone"] objectAtIndex:indexPath.row ]];
    
    return cell;
}


#pragma mark- TableMethodsToPlaceData

-(void)setStudentSocietyImages:(NSInteger)index  cell:(MessagesTableViewCell*)cell
{
    if(isInternetOff)
    {
        if (![[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index] isEqualToString:@"N"])
        {
            cell.imageVwLogoEvents.backgroundColor = [UIColor whiteColor];
            
            cell.imageVwLogoMsgs.backgroundColor = [UIColor whiteColor];
            
            if ([[[arrAllData valueForKey:@"message_type"] objectAtIndex:index] isEqualToString:@"event"])
            {
                
                if ([[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index] hasPrefix:@"/Users/"])
                {
                    [cell.imageVwLogoEvents setImage:[UIImage imageWithContentsOfFile:[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]]];
                }
                
                else if ([[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index] hasPrefix:@"/var/mobile/Containers/"])
                {
                    [cell.imageVwLogoEvents setImage:[UIImage imageWithContentsOfFile:[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]]];
                }
                else
                {
                    UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]]]]];
                    
                    [cell.imageVwLogoEvents setImage:imge];
                }
            }
            else
            {
                if ([[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index] hasPrefix:@"/Users/"])
                {
                    [cell.imageVwLogoMsgs setImage:[UIImage imageWithContentsOfFile:[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]]];
                }
                
                else if ([[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index] hasPrefix:@"/var/mobile/Containers/"])
                {
                    [cell.imageVwLogoMsgs setImage:[UIImage imageWithContentsOfFile:[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]]];
                }
                else
                {
                    UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]]]]];
                    
                    [cell.imageVwLogoMsgs setImage:imge];
                }
                
            }
            
        }
        else
        {
            cell.imageVwLogoEvents.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
            
            cell.imageVwLogoMsgs.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
            
            cell.imageVwLogoMsgs.image = [UIImage imageNamed:@"eventLogo"];
        }
    }
    else
    {
        if (![[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index] isEqual:[NSNull null]])
        {
            cell.imageVwLogoEvents.backgroundColor = [UIColor whiteColor];
            cell.imageVwLogoMsgs.backgroundColor = [UIColor whiteColor];
            
            if ([[[arrAllData valueForKey:@"message_type"] objectAtIndex:index] isEqualToString:@"event"])
            {
                
                cell.imageVwLogoEvents.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
                
                cell.imageVwLogoMsgs.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
                
                if ([[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index] hasPrefix:@"/Users/"])
                {
                    [cell.imageVwLogoEvents setImage:[UIImage imageWithContentsOfFile:[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]]];
                }
                
                else if ([[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index] hasPrefix:@"/var/mobile/Containers/"])
                {
                    [cell.imageVwLogoEvents setImage:[UIImage imageWithContentsOfFile:[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]]];
                }
                else
                {
                    UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]]]]];
                    
                    [cell.imageVwLogoEvents setImage:imge];
                }
            }
            else
            {
                cell.imageVwLogoEvents.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
                
                cell.imageVwLogoMsgs.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
                
                if ([[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index] hasPrefix:@"/Users/"])
                {
                    [cell.imageVwLogoMsgs setImage:[UIImage imageWithContentsOfFile:[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]]];
                }
                
                else if ([[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index] hasPrefix:@"/var/mobile/Containers/"])
                {
                    [cell.imageVwLogoMsgs setImage:[UIImage imageWithContentsOfFile:[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]]];
                }
                else
                {
                    UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]]]]];
                    
                    [cell.imageVwLogoMsgs setImage:imge];
                }
                
            }
        }
        else
        {
            cell.imageVwLogoEvents.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
            cell.imageVwLogoMsgs.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
            
            
        }
    }
    
    UILabel*lblReqStatus = (UILabel*)[cell.contentView viewWithTag:963];
    
    if ([[[arrAllData valueForKey:@"message_type"] objectAtIndex:index] isEqualToString:@"message"])
        
    {
        cell.viewEvents.hidden = YES;
        cell.viewMessages.hidden = NO;
        cell.imageViewMsgBg.image = [UIImage imageNamed:@"societyMsg"];
    }
    
    else
    {
        cell.viewEvents.hidden = NO;
        cell.viewMessages.hidden = YES;
        cell.imageVwEventBack.image = [UIImage imageNamed:@"societyEvent"];
        
        if ([[[arrAllData valueForKey:@"eventconfirmation"] objectAtIndex:index] isEqualToString:@"Request Sent"])
        {
            lblReqStatus.text = @"Invitation requested";
            lblReqStatus.textColor = [UIColor whiteColor];
        }
        else if ([[[arrAllData valueForKey:@"eventconfirmation"] objectAtIndex:index] isEqualToString:@"Confirmed"])
        {
            lblReqStatus.text = @"Congratulations";
            lblReqStatus.textColor = [UIColor whiteColor];
            cell.imageVwEventBack.image = [UIImage imageNamed:@"pinkRightArrow"];
        }
        else
        {
            lblReqStatus.text = @"Request an invitation today";
            lblReqStatus.textColor = [UIColor whiteColor];
        }
        
        if ([[[arrAllData valueForKey:@"eventStatus"] objectAtIndex:index] isEqualToString:@"1"])
        {
            
        }
        
        else if ([[[arrAllData valueForKey:@"eventStatus"] objectAtIndex:index] isEqualToString:@"0"])
        {
            lblReqStatus.text = @"Event cancelled";
            lblReqStatus.textColor = [UIColor whiteColor];
        }
        
        else if ([[[arrAllData valueForKey:@"eventStatus"] objectAtIndex:index] isEqualToString:@"2"])
        {
            lblReqStatus.text = @"Event closed";
            lblReqStatus.textColor = [UIColor whiteColor];
        }
    }
}

-(void)setCompanyImages:(NSInteger)index  cell:(MessagesTableViewCell*)cell
{
    cell.imageVwLogoMsgs.image = [UIImage imageNamed:@"defaultBuilding"];
    cell.imageVwLogoEvents.backgroundColor = [UIColor whiteColor];
    cell.imageVwLogoMsgs.backgroundColor = [UIColor whiteColor];
    
    if(isInternetOff)
    {
        if (![[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index] isEqualToString:@"N"])
            
        {
            if ([[[arrAllData valueForKey:@"message_type"] objectAtIndex:index] isEqualToString:@"event"])
                
            {
                cell.imageVwLogoEvents.image = [UIImage imageWithContentsOfFile:[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]];
            }
            else
                
            {
                cell.imageVwLogoMsgs.image = [UIImage imageWithContentsOfFile:[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index]];
            }
        }
        
        else
        {
            cell.imageVwLogoEvents.image = [UIImage imageNamed:@"defaultBuilding"];
            cell.imageVwLogoMsgs.image = [UIImage imageNamed:@"defaultBuilding"];
        }
    }
    else
    {
        if (![[[arrAllData valueForKeyPath:@"adder_detail.logo"] objectAtIndex:index] isEqual:[NSNull null]])
        {
            if ([[[arrAllData valueForKey:@"message_type"] objectAtIndex:index] isEqualToString:@"event"])
            {
                
                cell.imageVwLogoEvents.image = [UIImage imageWithContentsOfFile:[[arrAllData valueForKeyPath:@"adder_detail.logo"]objectAtIndex:index]];
                
            }
            else
            {
                
                cell.imageVwLogoMsgs.image = [UIImage imageWithContentsOfFile:[[arrAllData valueForKeyPath:@"adder_detail.logo"]objectAtIndex:index]];
            }
        }
        else
        {
            cell.imageVwLogoEvents.image = [UIImage imageNamed:@"defaultBuilding"];
            cell.imageVwLogoMsgs.image = [UIImage imageNamed:@"defaultBuilding"];
        }
    }
    
    UILabel*lblReqStatus = (UILabel*)[cell.contentView viewWithTag:963];
    
    if ([[[arrAllData valueForKey:@"message_type"] objectAtIndex:index] isEqualToString:@"message"])
    {
        cell.viewEvents.hidden = YES;
        cell.viewMessages.hidden = NO;
    }
    else
        
    {
        cell.viewEvents.hidden = NO;
        cell.viewMessages.hidden = YES;
        
        if ([[[arrAllData valueForKey:@"eventconfirmation"] objectAtIndex:index] isEqualToString:@"Request Sent"])
        {
            lblReqStatus.text = @"Invitation requested";
            cell.imageVwEventBack.image = [UIImage imageNamed:@"greenRightArrow"];
            lblReqStatus.textColor = [UIColor blackColor];
        }
        else if ([[[arrAllData valueForKey:@"eventconfirmation"] objectAtIndex:index] isEqualToString:@"Confirmed"])
        {
            lblReqStatus.text = @"Congratulations";
            lblReqStatus.textColor = [UIColor whiteColor];
            cell.imageVwEventBack.image = [UIImage imageNamed:@"pinkRightArrow"];
        }
        else
        {
            lblReqStatus.text = @"Request an invitation today";
            lblReqStatus.textColor = [UIColor blackColor];
        }
        
        if ([[[arrAllData valueForKey:@"eventStatus"] objectAtIndex:index] isEqualToString:@"1"])
        {
            
        }
        
        else if ([[[arrAllData valueForKey:@"eventStatus"] objectAtIndex:index] isEqualToString:@"0"])
        {
            lblReqStatus.text = @"Event cancelled";
            lblReqStatus.textColor = [UIColor blackColor];
        }
        
        else if ([[[arrAllData valueForKey:@"eventStatus"] objectAtIndex:index] isEqualToString:@"2"])
        {
            lblReqStatus.text = @"Event closed";
            lblReqStatus.textColor = [UIColor blackColor];
        }
    }
}


-(void)setAdminImages:(NSInteger)index  cell:(MessagesTableViewCell*)cell
{
    UILabel*lblReqStatus = (UILabel*)[cell.contentView viewWithTag:963];
    
    cell.imageVwLogoEvents.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
    cell.imageVwLogoMsgs.backgroundColor = [UIColor colorWithRed:59.0f/255.0 green:123.0f/255.0f blue:195.0f/255.0f alpha:1.0];
    
    if ([[[arrAllData valueForKey:@"message_type"] objectAtIndex:index] isEqualToString:@"message"])
    {
        cell.imageViewMsgBg.image = [UIImage imageNamed:@"whiteLeftArrow"];   // yellowLeftArrow
        
        stringAdminLogo=[NSString stringWithFormat:@"%@",[[arrAllData valueForKey:@"perfect_logo"]objectAtIndex:index]];
        
        if (isInternetOff)
        {
            [cell.imageVwLogoMsgs setImage:[UIImage imageWithContentsOfFile:stringAdminLogo]] ;
        }
        else
        {
            if ([stringAdminLogo hasPrefix:@"/Users/"])
            {
                [cell.imageVwLogoMsgs setImage:[UIImage imageWithContentsOfFile:stringAdminLogo]];
            }
            
            else if ([stringAdminLogo hasPrefix:@"/var/mobile/Containers/"])
                
            {
                [cell.imageVwLogoMsgs setImage:[UIImage imageWithContentsOfFile:stringAdminLogo]];
            }
            
            else
            {
                UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,stringAdminLogo]]]];
                
                [cell.imageVwLogoMsgs setImage:imge];
            }
        }
        
        cell.viewEvents.hidden=YES;
        cell.viewMessages.hidden = NO;
    }
    
    else
    {
        
        stringAdminLogo = [NSString stringWithFormat:@"%@",[[arrAllData valueForKey:@"perfect_logo"]objectAtIndex:index]];
        
        cell.imageVwEventBack.image = [UIImage imageNamed:@"whiteRightArrow"];
        
        if (isInternetOff)
        {
            [cell.imageVwLogoEvents setImage:[UIImage imageWithContentsOfFile:stringAdminLogo]] ;
        }
        else
        {
            if ([stringAdminLogo hasPrefix:@"/Users/"])
            {
                [cell.imageVwLogoEvents setImage:[UIImage imageWithContentsOfFile:stringAdminLogo]];
            }
            
            else if ([stringAdminLogo hasPrefix:@"/var/mobile/Containers/"])
            {
                [cell.imageVwLogoEvents setImage:[UIImage imageWithContentsOfFile:stringAdminLogo]];
            }
            
            else
            {
                UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,stringAdminLogo]]]];
                
                [cell.imageVwLogoEvents setImage:imge];
            }
        }
        
        cell.viewEvents.hidden = NO;
        cell.viewMessages.hidden = YES;
        
        if ([[[arrAllData valueForKey:@"eventconfirmation"] objectAtIndex:index] isEqualToString:@"Request Sent"])
        {
            lblReqStatus.text = @"Invitation requested";
            cell.imageVwEventBack.image = [UIImage imageNamed:@"whiteRightArrow"];
            lblReqStatus.textColor = [UIColor blueColor];
        }
        
        else if ([[[arrAllData valueForKey:@"eventconfirmation"] objectAtIndex:index] isEqualToString:@"Confirmed"])
        {
            lblReqStatus.text = @"Congratulations";
            lblReqStatus.textColor = [UIColor whiteColor];
            cell.imageVwEventBack.image = [UIImage imageNamed:@"pinkRightArrow"];
        }
        
        else
        {
            lblReqStatus.text = @"Request an invitation today";
            lblReqStatus.textColor = [UIColor blueColor];
        }
        
        if ([[[arrAllData valueForKey:@"eventStatus"] objectAtIndex:index] isEqualToString:@"1"])
        {
            
        }
        else if ([[[arrAllData valueForKey:@"eventStatus"] objectAtIndex:index] isEqualToString:@"0"])
        {
            lblReqStatus.text = @"Event cancelled";
            lblReqStatus.textColor = [UIColor blueColor];
        }
        else if ([[[arrAllData valueForKey:@"eventStatus"] objectAtIndex:index] isEqualToString:@"2"])
        {
            lblReqStatus.text = @"Event closed";
            lblReqStatus.textColor = [UIColor blueColor];
        }
    }
}


#pragma mark- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if ([[[arrAllData valueForKey:@"message_type"] objectAtIndex:indexPath.row] isEqualToString:@"event"])
        
    {
        if (indexPath.row ==0)
            
        {
            if (!isPaging)
                
            {
                return  170;
            }
            
            return  170+40;
        }
        
        if(indexPath.row == arrAllData.count-1)
            
        {
            return 210;
        }
        
        return 170;
    }
    else
    {
        if (indexPath.row ==0)
        {
            if (!isPaging)
            {
                return  115;
            }
            return  115+40;
        }
        
        if(indexPath.row == arrAllData.count-1)
            
        {
            return 160;
        }
        
        return 115;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (isPaging){
        return 40;
    }
    else{
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    frombutton = NO;
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    isRefreshingMessages = YES;
    
    messageDetailVc *messageView = [self.storyboard instantiateViewControllerWithIdentifier:@"messageDetailId"];
    
    if ([_strFromScreen isEqualToString:@"PerfectGraduate"]||[_strEventsOfScreen isEqualToString:@"eventsPerfectGraduate"]||[_strFromScreen isEqualToString:@"port"]||[_strEventsOfScreen isEqualToString:@"eventsPort"]||[_strFromScreen isEqualToString:@"Clubs&Societies"]||[_strEventsOfScreen isEqualToString:@"eventsClubs&Societies"])
    {
        messageView.strDetailtype = @"Yellow";
    }
    stringSelectedMsg = [[arrAllData valueForKey:@"id"]objectAtIndex:indexPath.row];
    
    NSLog(@"selcted id is %@", stringSelectedMsg);
    
    dictSelectedMessage=[[NSMutableDictionary alloc] initWithDictionary:[arrAllData objectAtIndex:indexPath.row]];
    
    messageView.dicMessageDetail = [[NSMutableDictionary alloc] initWithDictionary:[arrAllData objectAtIndex:indexPath.row]];
    
    intMsgsScreen = arrAllData.count;
    
    intEventsScreen = arrAllData.count;
    
    [self.navigationController pushViewController:messageView animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

-(void)loadMoreMsgs
{
    [self loadMessagesWithCount:20];
}

-(void)loadMessagesWithCount :(NSInteger ) msgCount
{
    NSInteger atRow = 0;
    
    if (isPaging)
    {
        if (arrayPaging.count - arrAllData.count >= 20)
        {
            if (arrayPaging.count - arrAllData.count == 20)
            {
                isPaging = NO;
            }
            
            else
            {
                isPaging = YES;
            }
            
            NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray: [arrayPaging subarrayWithRange:NSMakeRange(arrAllData.count,20)]];
            
            tempArray = [[[tempArray reverseObjectEnumerator] allObjects] mutableCopy];
            atRow = tempArray.count;
            atRow+=2;
            
            [tempArray addObjectsFromArray:arrAllData];
            [arrAllData removeAllObjects];
            [arrAllData addObjectsFromArray:tempArray];
        }
        else
        {
            NSInteger arrAllDataCount = arrAllData.count;
            atRow = arrayPaging.count-arrAllDataCount;
            NSMutableArray *tempArray = [[NSMutableArray alloc] initWithArray:[arrayPaging subarrayWithRange:NSMakeRange(arrAllDataCount, arrayPaging.count-arrAllDataCount)]];
            tempArray = [[[tempArray reverseObjectEnumerator] allObjects] mutableCopy];
            atRow = tempArray.count;
            atRow+=2;
            [tempArray addObjectsFromArray:arrAllData];
            [arrAllData removeAllObjects];
            [arrAllData addObjectsFromArray:tempArray];
            
            isPaging = NO;
        }
    }
    
    if (_strFromScreen)
    {
        if ([_buttonEvents.titleLabel.text isEqualToString:@"Events only"])
        {
            intMsgsScreen = arrAllData.count;
        }
        else
        {
            intEventsScreen= arrAllData.count;
        }
    }
    else
    {
        intEventsScreen= arrAllData.count;
    }
    
    if (arrAllData.count-1 == intRowsForTable)
    {
        
    }
    else if (intRowsForTable < arrayPaging.count-20)
    {
        intRowsForTable = intRowsForTable + 20;
    }
    else
    {
        intRowsForTable = (int) arrayPaging.count;
    }
    
    NSLog(@"intRowsForTable is %d", intRowsForTable);
    
    NSLog(@"arrAllData.count is %lu", (unsigned long)arrAllData.count);
    
    [bubbleTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    if (self.view.frame.size.height == 568)
    {
        [bubbleTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:atRow inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    else if (self.view.frame.size.height == 667)
    {
        [bubbleTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:atRow+1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    else if (self.view.frame.size.height == 736)
    {
        [bubbleTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:atRow+2 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
}

#pragma mark- calculateTime

-(NSString *)calculateDateAccordingToTimeZone1:(NSString *)stringfromDate isDateType:(NSString*)isDateType timeZoneName:(NSString*)timeZoneName
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    
    NSArray *array = [stringfromDate componentsSeparatedByString:@" "];
    
    if ([array containsObject:@"AM"])
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    }
    
    else if ([array containsObject:@"PM"])
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    }
    
    else
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    }
    
    NSDate *date = [dateFormatter1 dateFromString:stringfromDate];
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"dd-MMM-yyyy hh:mm a"];
    [dateFormatters setDateStyle:NSDateFormatterShortStyle];
    [dateFormatters setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatters setDoesRelativeDateFormatting:YES];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    stringfromDate = [dateFormatters stringFromDate: date];
    NSLog(@"DateString : %@", stringfromDate);
    return stringfromDate;
}
-(NSString *)calculateDateAccordingToTimeZone2:(NSString *)stringfromDate isDateType:(NSString*)isDateType timeZoneName:(NSString*)timeZoneName
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    
    NSArray *array = [stringfromDate componentsSeparatedByString:@" "];
    
    if ([array containsObject:@"AM"])
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    }
    
    else if ([array containsObject:@"PM"])
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
    }
    
    else
    {
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    }
    
    NSDate *date = [dateFormatter1 dateFromString:stringfromDate];
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"dd-MMM-yyyy hh:mm a"];
//    [dateFormatters setDateStyle:NSDateFormatterShortStyle];
//    [dateFormatters setTimeStyle:NSDateFormatterShortStyle];
//    [dateFormatters setDoesRelativeDateFormatting:YES];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    stringfromDate = [dateFormatters stringFromDate: date];
    NSLog(@"DateString : %@", stringfromDate);
    return stringfromDate;
}

-(NSString *)calculateDateAccordingToTimeZone:(NSString *)stringfromDate isDateType:(NSString*)isDateType timeZoneName:(NSString*)timeZoneName
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    
    if ([isDateType isEqualToString:@"event"])
    {
        [dateFormatter1 setDateFormat:@"yyyy/MM/dd"];
    }
    else
    {
        [dateFormatter1 setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    }
    
    NSDate *date = [dateFormatter1 dateFromString:stringfromDate];
    
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithName:timeZoneName];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:date];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:date];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    
    if ([isDateType isEqualToString:@"event"])
    {
        [dateFormatters setDateFormat:@"dd/MM/yyyy"];
    }
    else
    {
        dateFormatters.dateFormat = @"dd/MM/yyyy hh:mm a";
    }
    
    [dateFormatters setDateStyle:NSDateFormatterShortStyle];
    
    if (!isDateType)
    {
        [dateFormatters setTimeStyle:NSDateFormatterShortStyle];
    }
    
    [dateFormatters setDoesRelativeDateFormatting:YES];
    
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSLog(@"DateString : %@", [dateFormatters stringFromDate: destinationDate]);
    
    return [NSString stringWithFormat:@"%@",[dateFormatters stringFromDate: destinationDate]];
}

#pragma mark- UITapGestureRecognizer

-(void)highlightBor:(UIButton *)sender
{
    MessagesTableViewCell *buttonCell = (MessagesTableViewCell *)sender.superview.superview;
    tagValue = (int) sender.tag;
    
    viewLayer = (UIView *)[buttonCell viewWithTag:tagValue+100];
    
    viewLayer.layer.cornerRadius = 5.0f;
    viewLayer.clipsToBounds = YES;
    viewLayer.layer.borderWidth = 2.0f;
    viewLayer.layer.borderColor = [[UIColor colorWithRed:215.0f/255.0f green:215.0f/255.0f blue:215.0f/255.0f alpha:1] CGColor];
}

-(void)ImageTapped:(UIButton *)sender
{
    sender.userInteractionEnabled = NO;
    
    if ([sender isSelected])
    {
        [sender setSelected:NO];
        NSLog(@"sender state is no ");
    }
    else
    {
        [sender setSelected:YES];
        NSLog(@"sender state is yes ");
    }
    tagValue = (int) sender.tag;
    [self performSelector:@selector(afterTap:) withObject:sender afterDelay:0.2];
}


-(void)afterTap:(UIButton *)sender
{
    if ([[[arrAllData objectAtIndex:tagValue] valueForKey:@"type"] isEqualToString:@"companybehalf"])
    {
        CompaniesDetailViewController *companiesView = [self.storyboard instantiateViewControllerWithIdentifier:@"CompaniesDetailView"];
        
        companiesView.strVwChane = @"fromMessage";
        
        companiesView.dicCompanyDetail=[[NSMutableDictionary alloc]initWithDictionary:[arrAllData objectAtIndex:tagValue]];
        [self.navigationController pushViewController:companiesView animated:NO];
    }
    else if ([[[arrAllData objectAtIndex:tagValue] valueForKey:@"type"] isEqualToString:@"studentsociety"])
    {
        NSString *stringUrl = [[arrAllData objectAtIndex:tagValue] objectForKey:@"website"];
        
        if (![stringUrl containsString:@"http://"])
        {
            stringUrl = [NSString stringWithFormat:@"http://%@",stringUrl];
        }
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringUrl]];
    }
    else
    {
        
    }
    
    [self performSelector:@selector(labelClearColor:) withObject:sender afterDelay:0.1];
}

-(void)labelClearColor :(UIButton *)sender
{
    [sender setSelected:NO];
    
    sender.userInteractionEnabled = YES;
    
    viewLayer.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0] CGColor];
}


@end



