//
//  SalaryGuideViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 2/3/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalaryGuideViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *salaryGuideTableView;
@property (strong, nonatomic) IBOutlet UIView *occupView;
@property (strong, nonatomic) IBOutlet UILabel *labelOccupation;
@property (strong, nonatomic) IBOutlet UIView *viewBottomBar;
@property (strong, nonatomic) IBOutlet UIButton *buttonPortfolio;
@property (strong, nonatomic) IBOutlet UIButton *buttonDiscipline;
@property (strong, nonatomic) IBOutlet UIButton *buttonSearch;
@property (strong, nonatomic) IBOutlet UIButton *buttonMessages;
@property (strong, nonatomic) IBOutlet UIButton *buttonSalaryguide;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVwTableContainer;

@property (strong, nonatomic) IBOutlet UIView *viewSelectIndustry;
@property (strong, nonatomic) IBOutlet UITextField *textFldIndustry;
@property (strong, nonatomic) IBOutlet UIButton *buttonSelectIndustry;
@property (strong, nonatomic) IBOutlet UILabel *labelInstructions;
@property (strong, nonatomic) IBOutlet UITableView *tableViewSelectIndustry;
@property (strong, nonatomic) IBOutlet UILabel *labelPortfolio;
@property (strong, nonatomic) IBOutlet UILabel *labelDiscipline;
@property (strong, nonatomic) IBOutlet UILabel *labelSearchVw;
@property (strong, nonatomic) IBOutlet UILabel *labelSalaryGuide;
@property (strong, nonatomic) IBOutlet UILabel *labelMessages;

@property (strong, nonatomic) IBOutlet UILabel *label3To5Exp;
@property (strong, nonatomic) IBOutlet UILabel *label0To3Exp;
@property (strong, nonatomic) IBOutlet UILabel *label5To7Exp;
@property (strong, nonatomic) IBOutlet UIImageView *image3to5;
@property (strong, nonatomic) IBOutlet UIImageView *image0to3;
@property (strong, nonatomic) IBOutlet UIImageView *imageTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imageOcc;
@property(strong,nonatomic) IBOutlet UIImageView *imageBannerView;

@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;


@property(strong,nonatomic) IBOutlet  UIView *viewBack;

@property(strong,nonatomic)IBOutlet UILabel *lblOccu, *lblTile;

- (IBAction)buttonActionPortfolio:(id)sender;
- (IBAction)buttonActionDiscip:(id)sender;
- (IBAction)buttonSearch:(id)sender;
- (IBAction)buttonMessages:(id)sender;
- (IBAction)buttonActionSalary:(id)sender;

@end

