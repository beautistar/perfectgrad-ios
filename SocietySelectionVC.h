//
//  SocietySelectionVC.h
//  PerfectGraduate
//
//  Created by NetSet on 07/04/16.
//  Copyright © 2016 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "AFNetworking.h"
#import "Reachability.h"
#import "Utilities.h"
#import "PortfolioViewController.h"
#import "TermsConditionViewController.h"


@interface SocietySelectionVC : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>

@property (strong, nonatomic) UIPickerView *societyPicker;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewSociety;
@property(nonatomic) NSString *isFromScreen;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong,nonatomic) NSDictionary *dictUserInfoFromUsrPg;

- (IBAction)btnSubmitAction:(id)sender;

@end
