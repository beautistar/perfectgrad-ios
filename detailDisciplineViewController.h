//
//  detailDisciplineViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 2/6/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PortfolioTableCell.h"
#import "SearchTableViewCell.h"

@interface detailDisciplineViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITabBarControllerDelegate>{
    NSArray *arrFinalStateData;
    NSArray *arrFinalCoutryData;
    BOOL isAllLocationSelected;
}

@property (nonatomic) NSArray *arrDiscipline;
@property (weak, nonatomic) IBOutlet UITableView *tableViewDetail;
@property (strong,nonatomic) NSString *strNavTitle;
@property (strong,nonatomic) NSString *strNavSubTitle;
@property (nonatomic) NSString *strIndex;
@property (strong, nonatomic) IBOutlet UIView *viewBottomBar;
@property (strong, nonatomic) IBOutlet UIButton *buttonPortfolio;
@property (strong, nonatomic) IBOutlet UIButton *buttonDiscipline;
@property (strong, nonatomic) IBOutlet UIButton *buttonSearch;
@property (strong, nonatomic) IBOutlet UIButton *buttonMessages;
@property (strong, nonatomic) IBOutlet UIButton *buttonSalaryguide;
@property (strong, nonatomic) IBOutlet UILabel *labelPortfolio;
@property (strong, nonatomic) IBOutlet UILabel *labelDiscipline;
@property (strong, nonatomic) IBOutlet UILabel *labelSearchVw;
@property (strong, nonatomic) IBOutlet UILabel *labelSalaryGuide;
@property (strong, nonatomic) IBOutlet UILabel *labelMessages;
@property (strong, nonatomic) IBOutlet UIButton *companyBtn;
@property (strong, nonatomic) IBOutlet UIButton *awardsBtn;
@property (strong, nonatomic) IBOutlet UIButton *duebtn;
@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;

@property (strong, nonatomic) IBOutlet UILabel *labelCompany;
@property (strong, nonatomic) IBOutlet UILabel *labelDueDate;

@property(strong,nonatomic) NSArray *arrayComp;

@property(assign,nonatomic)int indexPath;

@property(assign,nonatomic)int row;

@property (assign,nonatomic) NSUInteger SelectedSection;
@property (assign,nonatomic) NSUInteger SelectedRow;

@property(strong,nonatomic)NSArray *arrayCompanies;

- (IBAction)btnSortCompaniesAction:(id)sender;

- (IBAction)buttonActionPortfolio:(id)sender;
- (IBAction)buttonActionDiscip:(id)sender;
- (IBAction)buttonSearch:(id)sender;
- (IBAction)buttonMessages:(id)sender;
- (IBAction)buttonActionSalary:(id)sender;




@end
