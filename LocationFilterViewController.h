//
//  LocationFilterViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 2/6/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "LocationFilterSearchViewController.h"
#import "SelectCountryViewController.h"

@interface LocationFilterViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate>{
    NSMutableArray *NewCountryIdArr;
    NSMutableArray *NewStateIdArr;
    BOOL isAllSelected;
    
    NSMutableArray *arrTempStateData;
    NSMutableArray *arrTempCountryData;
    BOOL isAllLocationSelected;
    
}

@property (nonatomic) NSString *strCountryIds;

@property (strong, nonatomic) IBOutlet UITableView *tableViewFilter;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarLocation;
@property (strong, nonatomic) IBOutlet UIView *viewBottomBar;
@property (strong, nonatomic) IBOutlet UIButton *buttonPortfolio;
@property (strong, nonatomic) IBOutlet UIButton *buttonDiscipline;
@property (strong, nonatomic) IBOutlet UIButton *buttonSearch;
@property (strong, nonatomic) IBOutlet UIButton *buttonMessages;
@property (strong, nonatomic) IBOutlet UIButton *buttonSalaryguide;
@property (strong, nonatomic) IBOutlet UILabel *labelPortfolio;
@property (strong, nonatomic) IBOutlet UILabel *labelDiscipline;
@property (strong, nonatomic) IBOutlet UILabel *labelSearchVw;
@property (strong, nonatomic) IBOutlet UILabel *labelSalaryGuide;
@property (strong, nonatomic) IBOutlet UILabel *labelmessag;
@property (strong, nonatomic) IBOutlet UIButton *buttonSelectAllStates;

@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;



- (IBAction)buttonSelectAllStatesAction:(id)sender;
- (IBAction)buttonActionPortfolio:(id)sender;
- (IBAction)buttonActionDiscip:(id)sender;
- (IBAction)buttonSearch:(id)sender;
- (IBAction)buttonMessages:(id)sender;
- (IBAction)buttonActionSalary:(id)sender;




@end
