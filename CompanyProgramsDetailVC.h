//
//  CompanyProgramsDetailVC.h
//  PerfectGraduate
//
//  Created by netset on 6/3/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyProgramsDetailVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableViewCompnyInfo;

@property (strong,nonatomic) NSString *strCompanyName;
@property (strong,nonatomic) NSString *strCountryame;

@property (strong,nonatomic) NSDictionary *dicData;

@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;


@end
