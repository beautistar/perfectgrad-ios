//
//  MenuViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 2/17/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "MenuViewController.h"
#import "SWRevealViewController.h"
#import "PortfolioViewController.h"
#import "MessagesViewController.h"
#import "ViewController.h"
#import "DisciplineViewController.h"
#import "LocationFilterViewController.h"
#import "SearchLocationFilterViewController.h"
#import "SalaryGuideViewController.h"
#import "StudentDetailsViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface MenuViewController ()
{
    NSArray *arrayMenuItem;
    NSArray *arrayMenuImage;
    LIALinkedInHttpClient *_client;
    UIActivityIndicatorView *spinner;
    Database *dummy;
}

@end

@implementation MenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    _client = [self client];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    stringForNotifyEditCompany = @"";
    self.navigationController.navigationBarHidden  = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:80.0f/255.0f green:89.0f/255.0f blue:123.0f/255.0f alpha:1.0f];
    
    UIView *iv = [[UIView alloc] initWithFrame:CGRectMake(0,0,180,30)];
    [iv setBackgroundColor:[UIColor clearColor]];
    
    UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 180, 30)];
    labelTitle.text = @"Perfect Graduate";
    labelTitle.textColor = [UIColor whiteColor];
    labelTitle.font = [UIFont fontWithName:@"OpenSans" size:16.0f];
    [iv addSubview:labelTitle];
    self.navigationItem.titleView = iv;
    
    [self fillArrayToLoadTable];
}


-(void)fillArrayToLoadTable

{
    arrayMenuImage = nil;
    arrayMenuImage = nil;
    
    NSArray *arrayMenuAllTitles = [NSArray arrayWithObjects:@"Portfolio", @"My Events", @"Student Details",@"Student Societies",@"LinkedIn Log In",@"Save My Portfolio",@"Log Out", nil];
    
    NSArray *  arrayMenuAllImages=[[NSArray alloc]initWithObjects:@"menu_portfolio",@"menu_myevents",@"menu_graduate-details",@"menu_social_society",@"menu_linkdin",@"fbLogin",@"menu_logout", nil];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"FbLogIn"] boolValue]&&[[[NSUserDefaults standardUserDefaults] objectForKey:@"LinkedInLogIn"] boolValue])
    {
        arrayMenuItem = [NSArray arrayWithObjects:@"Portfolio", @"My Events", @"Student Details",@"Student Societies",@"Log Out", nil];
        arrayMenuImage=[[NSArray alloc]initWithObjects:@"menu_portfolio",@"menu_myevents",@"menu_graduate-details",@"menu_social_society",@"menu_logout", nil];
    }
    
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"FbLogIn"] boolValue]&&![[[NSUserDefaults standardUserDefaults] objectForKey:@"LinkedInLogIn"] boolValue])
    {
        arrayMenuItem = [NSArray arrayWithObjects:@"Portfolio", @"My Events", @"Student Details",@"Student Societies",@"LinkedIn Log In",@"Log Out", nil];
        arrayMenuImage=[[NSArray alloc]initWithObjects:@"menu_portfolio",@"menu_myevents",@"menu_graduate-details",@"menu_social_society",@"menu_linkdin",@"menu_logout", nil];
    }
    
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"LinkedInLogIn"] boolValue]&&![[[NSUserDefaults standardUserDefaults] objectForKey:@"FbLogIn"] boolValue])
    {
        arrayMenuItem = [NSArray arrayWithObjects:@"Portfolio", @"My Events", @"Student Details",@"Student Societies",@"Save My Portfolio",@"Log Out", nil];
        arrayMenuImage=[[NSArray alloc]initWithObjects:@"menu_portfolio",@"menu_myevents",@"menu_graduate-details",@"menu_social_society",@"fbLogin",@"menu_logout", nil];
    }
    
    else
    {
        arrayMenuItem = [arrayMenuAllTitles copy];
        arrayMenuImage = [arrayMenuAllImages copy];
    }
    
    [_tableViewMenu reloadData];
}


#pragma mark - UITableViewDataSource------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayMenuItem count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    cell.backgroundColor =[UIColor clearColor];
    UIImageView* imgView = [[UIImageView alloc] init];
    imgView.image=[UIImage imageNamed:[arrayMenuImage objectAtIndex:indexPath.row]];
    [cell addSubview:imgView];
    
    if (indexPath.row==3)
    {
        imgView.frame = CGRectMake(10, 12, 22, 22);
    }
    else
    {
        imgView.frame = CGRectMake(10, 10, 22, 22);
    }
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(imgView.frame.origin.x+imgView.frame.size.width+13, 7, 150, 30)];
    label.text = [arrayMenuItem objectAtIndex:indexPath.row];
    label.font =[UIFont fontWithName:@"OpenSans" size:14.0f];
    label.textColor = [UIColor whiteColor];
    [cell addSubview:label];
    
    cell.imageView.contentMode=UIViewContentModeCenter;
    self.tableViewMenu.separatorInset= UIEdgeInsetsMake(cell.bounds.size.height, 0, 0, 0);
    return cell;
}


#pragma mark-----UITableViewDelegate---------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([[arrayMenuItem objectAtIndex:indexPath.row] isEqualToString:@"Portfolio"])
    {
        PortfolioViewController *portView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
        UINavigationController * navPort = [[UINavigationController alloc]initWithRootViewController:portView];
        [self.revealViewController setFrontViewController:navPort animated:YES];
    }
    
    else if ([[arrayMenuItem objectAtIndex:indexPath.row] isEqualToString:@"My Events"])
    {
        MessagesViewController *messageView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
        messageView.strFromScreen = @"fromMenu";
        UINavigationController * navMesg = [[UINavigationController alloc]initWithRootViewController:messageView];
        [self.revealViewController setFrontViewController:navMesg animated:YES];
    }
    
    else if ([[arrayMenuItem objectAtIndex:indexPath.row] isEqualToString:@"Student Details"])
    {
        StudentDetailsViewController *StudentDetailVw = [self.storyboard instantiateViewControllerWithIdentifier:@"detailsView"];
        strFromButton = @"menu";
        UINavigationController * navDis = [[UINavigationController alloc]initWithRootViewController:StudentDetailVw];
        [self.revealViewController setFrontViewController:navDis animated:YES];
    }
    
    else if ([[arrayMenuItem objectAtIndex:indexPath.row] isEqualToString:@"Student Societies"])
    {
        SocietySelectionVC *StudentDetailVw = [self.storyboard instantiateViewControllerWithIdentifier:@"SocietySelectionVC"];
        StudentDetailVw.isFromScreen = @"menu";
        UINavigationController * navDis = [[UINavigationController alloc]initWithRootViewController:StudentDetailVw];
        [self.revealViewController setFrontViewController:navDis animated:YES];
        
    }
    
    else if ([[arrayMenuItem objectAtIndex:indexPath.row] isEqualToString:@"LinkedIn Log In"])
    {
        
        if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"FbLogIn"] boolValue])
        {
            UIAlertView *alertFbLogin = [[UIAlertView alloc] initWithTitle:nil message:@"Please save your portfolio before continuing." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil, nil];
            
            alertFbLogin.tag = 5800;
            
            [alertFbLogin show];
        }
        
        else
        {
            [self addAlertToShowLinkedIn];
        }
    }
    
    else if ([[arrayMenuItem objectAtIndex:indexPath.row] isEqualToString:@"Save My Portfolio"])
    {
        [self savePortfolioAlert];
    }
    
    else if ([[arrayMenuItem objectAtIndex:indexPath.row] isEqualToString:@"Log Out"])
    {
        
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"Perfect Graduate" message:@"\nAre you sure you want to log out?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"No",@"Yes",nil];
        
        [alertView show ];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}


-(void)savePortfolioAlert
{
    UIAlertView *alertFbLogin = [[UIAlertView alloc] initWithTitle:@"Save My Portfolio" message:@"\nPlease confirm if you would like to save your portfolio using your Facebook login?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Sign In",@"No", nil];
    
    alertFbLogin.tag = 5858;
    
    [alertFbLogin show];
}


-(void)addAlertToShowLinkedIn
{
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:@"LinkedIn Sign In"
                                        message:@"\nPlease Sign in to LinkedIn to request invitations to company events."
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *leftBtnAction =
    [UIAlertAction actionWithTitle:@"Sign In"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction *action)
     {
         [self loginLinkedIn];
     }];
    
    UIAlertAction *rightBtnAction =
    [UIAlertAction actionWithTitle:@"No"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action)
     {
         
     }];
    
    [alert addAction:leftBtnAction];
    [alert addAction:rightBtnAction];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark----UIAlertViewDelegate----

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==5858)
    {
        if (buttonIndex==0)
        {
            [self loginWithFacebook];
        }
    }
    else if (alertView.tag == 5800)
    {
        if (buttonIndex==0)
        {
            [self savePortfolioAlert];
        }
    }
    else
    {
        if (buttonIndex==1)
        {
            if(![Utilities CheckInternetConnection])
            {
                
                ZAlertWithParam(@"Alert", @"Please connect to the internet before logging out.");
            }
            else
            {
                [self logOutWebService];
            }
        }
    }
}

#pragma mark----WebServices----

-(void)logOutWebService
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arryCountry"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arryStates"];
    
    [arrayCompaniesList removeAllObjects];
    
    stringPortCompanyScreen = @"";
    
    checkCopmanyApiCall = NO;
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *dictParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [manager POST:@"logout" parameters:dictParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callPortWebService"];
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callMsgWebService"];
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callallCompanyWebService"];
             
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callDesciplineWebService"];
             
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"salaryGuide"];
             
             [arrCountryIds removeAllObjects];
             [arrStateIds removeAllObjects];
             [FBSession.activeSession closeAndClearTokenInformation];
             [FBSession.activeSession close];
             [FBSession setActiveSession:nil];
             
             NSArray *arrayTables = [NSArray arrayWithObjects:@"PORTFOLIO",@"PortLocationsTable",@"PortAppDueDate",@"PortCompanyAwards",@"PortDisciplinesTable",@"PortSubDisciplinesTable",@"PortQuesAnsTable",@"PortCompanyDueDate",@"PortCompSocialLinksTable",@"SALARIES",@"COMPANIES",@"COMPANYLOCATIONS",@"DispSubDispCompaniesTable",@"SearchCompSubDisciplineTable",@"SearchCompanyAwards",@"SearchCompanyDueDate",@"SearchQuesAnsTable",@"SearchCompSocialLinksTable",@"COUNTRIES",@"STATES",@"MessagesTable",@"messagerAdderDetailTable",@"DISCIPLINES",@"SubDisciplines", nil];
             
             for (int i=0; i<[arrayTables count]; i++)
             {
                 [dummy deleteTable:[NSString stringWithFormat:@"delete from %@",[arrayTables objectAtIndex:i]]];
             }
             
             NSDictionary *defaultsDictionary = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
             
             for (NSString *key in [defaultsDictionary allKeys])
                 
             {
                 {
                     [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
                 }
             }
             
             [USERDEFAULT removeObjectForKey:LATITUDE_COUNTRY];
             
             [USERDEFAULT removeObjectForKey:LONGITUDE_COUNTRY];
             
             badge = 0;
             
             [UIApplication sharedApplication].applicationIconBadgeNumber = badge;
             
             ViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"PushMainView"];
             
             UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
             
             [navController setViewControllers: @[view] animated: NO];
             
             [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
         }
         else
         {
             ZAlertWithParam(@"Alert", [responseObject objectForKey:@"message"]);
         }
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     
     {
         ZAlertWithParam(@"Alert", error.localizedDescription);
         
     }];
    
}


-(void)loginWithFacebook
{
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        [self getfacebookuserInfo];
    }
    else
    {
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile,email"]
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error)
         {
             if (!error)
             {
                 [self getfacebookuserInfo];
             }
             else
             {
                 
             }
         }];
    }
}

-(void)getfacebookuserInfo
{
    [self ShowDataLoader:YES];
    
    [FBRequestConnection startWithGraphPath:@"/me"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error)
     {
         if (!error)
         {
             [self callRegisterFbAPI:result];
         }
         else
         {
             [self ShowDataLoader:NO];
             
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }
     }];
}

-(void)callRegisterFbAPI:(NSDictionary*)dictInfoFB
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    NSDictionary *parametersFb = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"fb_id":[dictInfoFB objectForKey:@"id"],@"email":[dictInfoFB objectForKey:@"email"],@"name":[NSString stringWithFormat:@"%@ %@",[dictInfoFB objectForKey:@"first_name"],[dictInfoFB objectForKey:@"last_name"]],@"device":strDeviceToken,@"device type":@"I"};
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@register_fb",baseUrl];
    
    [manager POST:urlStr parameters:parametersFb success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] boolValue])
         {
             [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"FbLogIn"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             ZAlertWithParam(@"Confirmation",@"Your portfolio has been succesfully saved.");
             
             [self fillArrayToLoadTable];
         }
         else
         {
             if ([[responseObject valueForKey:@"message"] isEqualToString:@"User Already Registered"])
             {
                 ZAlertWithParam(@"Please try again",@"Please note you are already registered.");
             }
             else{
                 ZAlertWithParam(@"Please try again",[responseObject objectForKey:@"message"]);
             }         }
         
         [self ShowDataLoader:NO];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}

- (void)loginLinkedIn
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"LinkedInLogIn"])
    {
        [self.client getAuthorizationCode:^(NSString *code)
         {
             [self.client getAccessToken:code success:^(NSDictionary *accessTokenData)
              {
                  NSString *accessToken = [accessTokenData objectForKey:@"access_token"];
                  
                  [self requestMeWithToken:accessToken];
                  
              }                   failure:^(NSError *error)
              {
                  NSLog(@"Quering accessToken failed %@", error);
              }];
         }
                                   cancel:^
         {
             NSLog(@"Authorization was cancelled by user");
         }
                                  failure:^(NSError *error)
         {
             NSLog(@"Authorization failed %@", error);
         }];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Your Linkedin account is alraedy registerd with us." message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil]show];
    }
}

- (LIALinkedInHttpClient *)client
{
    LIALinkedInApplication *application = [LIALinkedInApplication applicationWithRedirectURL:@"http://netsetsoftware.com/"
                                                                                    clientId:@"75yp653ipmtqm3"
                                                                                clientSecret:@"mGsFOpD2Ssn1Ey05"
                                                                                       state:@"DCEEFWF45453sdffef424"
                                                                               grantedAccess:@[@"r_basicprofile", @"r_emailaddress"]];
    
    NSLog(@"%@",[LIALinkedInHttpClient clientForApplication:application presentingViewController:nil]);
    
    return [LIALinkedInHttpClient clientForApplication:application presentingViewController:nil];
}


- (void)requestMeWithToken:(NSString *)accessToken
{
    // https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,formatted-name,email-address,location:(name),industry,public-profile-url,picture-url,primary-twitter-account,network,skills,summary,phone-numbers,date-of-birth,main-address,positions:(title,company:(name)),educations:(school-name,field-of-study,start-date,end-date,degree,activities))"
    
    
    [self.client GET:[NSString stringWithFormat:@"https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,formatted-name,email-address,public-profile-url,picture-url,picture-urls::(original))?oauth2_access_token=%@&format=json", accessToken] parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *result)
     {
         NSLog(@"current user %@", result);
         
         
         [self callWebservice:result];
         
         
     }        failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"failed to fetch current user %@", error);
     }];
}

-(void)callWebservice:(NSDictionary*)dic
{
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"image":[dic valueForKey:@"pictureUrl"],@"url":[dic valueForKey:@"publicProfileUrl"]};
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:@"studentLinkedIn" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject valueForKey:@"status"] isEqualToString:@"true"])
         {
             [[[UIAlertView alloc]initWithTitle:@"Confirmation" message:@"\nYou have succesfully signed in to LinkedIn." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
             
             [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"LinkedInLogIn"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             [self fillArrayToLoadTable];
         }
         
         NSLog(@"%@",responseObject);
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}


-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        spinner.center = self.view.center;
        spinner.color = [UIColor whiteColor];
        [spinner startAnimating];
        [self.view.window setUserInteractionEnabled:NO];
        
        [self.view addSubview:spinner];
        
    }
    else
    {
        [spinner stopAnimating];
        [self.view.window setUserInteractionEnabled:YES];
        
        [spinner removeFromSuperview];
        spinner = nil;
    }
    
}

@end

