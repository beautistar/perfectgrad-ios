//
//  LocationFilterSearchViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 2/3/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "LocationFilterViewController.h"
#import "PortfolioTableCell.h"
#import "SearchTableViewCell.h"
#import "AppDelegate.h"

@interface LocationFilterSearchViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate>
{
    NSArray *arrFinalStateData;
    NSArray *arrFinalCoutryData;
    BOOL isAllLocationSelected;
}

@property (weak, nonatomic) IBOutlet UITableView *locationFilterTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *locationFilterSearchBar;
@property (strong, nonatomic) IBOutlet UIButton *companyBtn;
@property (strong, nonatomic) IBOutlet UIButton *awardsBtn;
@property (strong, nonatomic) IBOutlet UIButton *duebtn;
@property (strong,nonatomic) NSMutableArray *mutableArrayFilteredCompanyName;
@property (strong,nonatomic) NSArray *arraysectionTitle;
@property (strong, nonatomic) IBOutlet UIView *viewBottomBar;
@property (strong, nonatomic) IBOutlet UIButton *buttonPortfolio;
@property (strong, nonatomic) IBOutlet UIButton *buttonDiscipline;
@property (strong, nonatomic) IBOutlet UIButton *buttonSearch;
@property (strong, nonatomic) IBOutlet UIButton *buttonMessages;
@property (strong, nonatomic) IBOutlet UIButton *buttonSalaryguide;

@property (strong, nonatomic) IBOutlet UILabel *labelCompany;
@property (strong, nonatomic) IBOutlet UILabel *labelDueDate;


@property (strong, nonatomic) IBOutlet UILabel *labelPortfolio;
@property (strong, nonatomic) IBOutlet UILabel *labelDiscipline;
@property (strong, nonatomic) IBOutlet UILabel *labelSearchTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelSalaryGuide;
@property (strong, nonatomic) IBOutlet UILabel *labelMessages;
@property(strong,nonatomic)NSMutableArray *arrayRemoveSelected;

@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;




- (IBAction)btnSortingTableAction:(id)sender;

- (IBAction)buttonActionPortfolio:(id)sender;
- (IBAction)buttonActionDiscip:(id)sender;
- (IBAction)buttonSearch:(id)sender;
- (IBAction)buttonMessages:(id)sender;
- (IBAction)buttonActionSalary:(id)sender;

@end
