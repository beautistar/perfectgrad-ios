//
//  detailDisciplineViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 2/6/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "detailDisciplineViewController.h"
#import "AFNetworking.h"
#import "SWRevealViewController.h"
#import "CompaniesDetailViewController.h"
#import "PortfolioViewController.h"
#import "LocationFilterSearchViewController.h"
#import "MessagesViewController.h"
#import "SalaryGuideViewController.h"
#import "DisciplineViewController.h"

@interface detailDisciplineViewController ()
{
    NSArray *arrayData;
    Database *dummy;
    int imgCount;
    NSArray *arrDummyImgs;
    NSArray * arrayAllCompanies;
    UIRefreshControl *refreshControl;
    UIActivityIndicatorView *spinner;
    
    UILabel *labelSubTitle;
    
    BOOL checkFilter;
}

@end

@implementation detailDisciplineViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arraySubDisciCountryId = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountrySubDiscipline"]];
    
    arraySubDisciStateId = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryStatesSubDiscipline"]];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"callallCompanyWebService"])
    {
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"callallCompanyWebService"];
        
    }
    
    if (arraySubDisciCountryId.count==0)
    {
        arraySubDisciCountryId = [[NSMutableArray alloc]init];
    }
    if (arraySubDisciStateId.count==0)
    {
        arraySubDisciStateId = [[NSMutableArray alloc]init];
    }
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, _imageRedStar.frame.origin.y, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    
    self.imageRedStar.hidden = YES;
    
    refreshControl = [[UIRefreshControl alloc] init];
    
    refreshControl.backgroundColor = [UIColor clearColor];
    
    refreshControl.tintColor = [UIColor lightGrayColor];
    
    [refreshControl addTarget:self
                       action:@selector(refreshData)
             forControlEvents:UIControlEventValueChanged];
    
    [_tableViewDetail addSubview:refreshControl];
    
    isInternetOff = NO;
    
    self.automaticallyAdjustsScrollViewInsets =  NO;
    
    arrDummyImgs = [[NSArray alloc] initWithObjects:@"agl",@"amcor",@"amp",@"ab", nil];
    
    dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    [_tableViewDetail setBackgroundColor:[UIColor clearColor]];
    
    [self.view setBackgroundColor:[UIColor yellowColor]];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"state"])
    {
        
        [self setDueDateText: [NSString stringWithFormat:@"Application Due Dates \n%@ %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"stateAbr"],[[NSUserDefaults standardUserDefaults] objectForKey:@"programName"]]];
    }
    
    _labelCompany.layer.borderWidth = 0.5;
    _labelDueDate.layer.borderWidth = 0.5;
    
    _labelCompany.layer.borderColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f].CGColor;
    _labelDueDate.layer.borderColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f].CGColor;
    
    self.viewBottomBar.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationItem.hidesBackButton = YES;
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
    
    self.navigationItem.leftBarButtonItem = leftItem;
    
    /*Set layout of CandidatePoints button*/
    UIButton *buttonCandidatePoints = [[UIButton alloc]init];
    
    [buttonCandidatePoints setImage:[UIImage imageNamed:@"candidatepoints"] forState:UIControlStateNormal];
    
    buttonCandidatePoints.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-40, self.view.frame.size.width,50);
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    UIView  *textView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 200 , 40)];
    UILabel *labelTitleName;
    labelTitleName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 0, CGRectGetWidth(textView.frame), 22)];
    labelTitleName.backgroundColor = [UIColor clearColor];
    labelTitleName.font = [UIFont fontWithName:@"OpenSans" size:18.0f];
    labelTitleName.textAlignment = NSTextAlignmentCenter;
    labelTitleName.textColor = [UIColor whiteColor];
    labelTitleName.text = [[[_arrDiscipline valueForKeyPath:@"subdiscipline.name"]objectAtIndex:0] objectAtIndex:[_strIndex integerValue]];
    
    labelSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 22, CGRectGetWidth(textView.frame), 18)];
    labelSubTitle.backgroundColor = [UIColor clearColor];
    labelSubTitle.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
    labelSubTitle.textAlignment = NSTextAlignmentCenter;
    labelSubTitle.textColor = [UIColor whiteColor];
    [textView addSubview:labelTitleName];
    [textView addSubview:labelSubTitle];
    self.navigationItem.titleView=textView;
    
    UIButton * buttonSearch = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.origin.x-135,15, 30, 30)];
    
    UIImage *imageSearchButton = [UIImage imageNamed:@"top-icon"];
    buttonSearch.backgroundColor = [UIColor clearColor];/*--------------*/
    
    [buttonSearch setImage:imageSearchButton forState:UIControlStateNormal];
    [buttonSearch addTarget:self action:@selector(btnFilter)forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:buttonSearch];
}

-(void)refreshData
{
    if ([refreshControl isRefreshing])
    {
        [refreshControl endRefreshing];
    }
    [self filterLocally];
}

-(void)btnFilter
{
    checkFilter = YES;
    
    checkSubDisciFilterClick = YES;
    
    stringBackScreen = @"";
    
    stringTakeDataFromPreviousScreen = @"";
    
    LocationFilterViewController *detailsView = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationFilterView"];
    [self.navigationController pushViewController:detailsView animated:NO];
}


-(void)viewWillAppear:(BOOL)animated
{
    if ([stringBackScreen isEqualToString:@"companyProfile"])
    {
        
    }
    else
    {
        arrayData = nil;
        
        [_tableViewDetail reloadData];
    }
    
    stringNotifiction = @"";
    
    stringCheckDisciplines = @"fromSubDisciplines";
    
    [super viewWillAppear:YES];
    
    stringForNotifyEditCompany = @"discipline";
    
    if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"] )
    {
        self.imageRedStar.hidden = YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    [self SetHeader];
    
    _buttonDiscipline.backgroundColor = [UIColor clearColor];
    [_buttonDiscipline setImage:[UIImage imageNamed:@"sisciplines"] forState:UIControlStateNormal];
    _labelDiscipline.textColor=[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    _companyBtn.layer.borderWidth = 1;
    _awardsBtn.layer.borderWidth = 1;
    _duebtn.layer.borderWidth = 1;
    
    _companyBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _awardsBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _duebtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:@"Application Due Dates\n  (Victoria Graduates)"];
    
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:@"OpenSans-Bold" size:9.0f]
                  range:NSMakeRange(24, 11)];
    
    _duebtn.titleLabel.font =[UIFont fontWithName:@"OpenSans-Bold" size:9.0f];
    
    [_duebtn setAttributedTitle:hogan forState:UIControlStateNormal];
    
    [[_duebtn titleLabel] setNumberOfLines:0];
    
    [[_duebtn titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];
    
    [_duebtn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"])
    {
        self.imageRedStar.hidden = YES;
    }
    
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(redStarShow)
                                                 name:@"redStarShow"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refrshAutoData)
                                                 name:@"refreshAuto"
                                               object:nil];
    [self filterLocally];
    
}

-(void)refrshAutoData
{
    [self getAllCompanies];
}

-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];
    
}

-(void)setDueDateText:(NSString *)string
{
    NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:string];
    
    [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:@"OpenSans" size:10.0f] range:NSMakeRange(0,string.length)];
    
    NSString *boldString = @"Application Due Dates";
    
    NSRange boldRange = [string rangeOfString:boldString];
    
    [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:@"OpenSans-Bold" size:10.0f] range:boldRange];
    
    [_labelDueDate setAttributedText: yourAttributedString];
}

-(void)sortDataOfCompanies
{
    
    
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    
    NSArray *sortedArray = [arrayData sortedArrayUsingDescriptors:sortDescriptors];
    
    arrayData = nil;
    
    arrayData = [[NSArray alloc]init];
    
    arrayData = [sortedArray copy];
}

-(void)filterLocally{
    NSArray *arrAllDisp = [[NSUserDefaults standardUserDefaults] objectForKey:@"allDiscipline"];
    NSDictionary *dictSubDisp = [[[arrAllDisp objectAtIndex:_SelectedSection] objectForKey:@"subdiscipline"] objectAtIndex:_SelectedRow];
    NSString *strSubSidp = [[NSString stringWithFormat:@"%@",[dictSubDisp objectForKey:@"name"]] removeNull];
    NSArray *arrAllComp = [[dictSubDisp objectForKey:strSubSidp] objectForKey:@"allcompanys"];
    if(isAllLocationSelected){
        arrayData = arrAllComp;
    }else{
        NSArray *arrSIds = [arrFinalStateData valueForKey:@"id"];
        NSArray *arrCIds = [arrFinalCoutryData valueForKey:@"id"];
        NSMutableArray *arrNewComp = [[NSMutableArray alloc]init];
        for (NSDictionary *dictComp in _arrayComp) {
            if([dictComp objectForKey:@"opration_country"] && [dictComp objectForKey:@"opration_state"]){
                NSString *strCId = [[NSString stringWithFormat:@"%@",[dictComp objectForKey:@"opration_country"]] removeNull];
                NSString *strSId = [[NSString stringWithFormat:@"%@",[dictComp objectForKey:@"opration_state"]] removeNull];
                
                
                BOOL isDataAdd = NO;
                if(![strSId isEqualToString:@""]){
                    NSArray *arrayRegState = [[strSId componentsSeparatedByString:@","] mutableCopy];
                    
                    for (NSString *strState in arrayRegState){
                        if([arrSIds containsObject:strState]){
                            if(![arrNewComp containsObject:dictComp]){
                                isDataAdd = YES;
                            }
                        }
                    }
                }
                if(![strCId isEqualToString:@""]){
                    NSArray *arrayInternational = [[strCId componentsSeparatedByString:@","] mutableCopy];
                    for (NSString *strInter in arrayInternational){
                        if([arrCIds containsObject:strInter]){
                            if(![arrNewComp containsObject:dictComp]){
                                isDataAdd = YES;
                            }
                        }
                    }
                }
                if(isDataAdd){
                    
                    [arrNewComp addObject:dictComp];
                }
            }
        }
        arrayData = [NSArray arrayWithArray:arrNewComp];
    }
    
    [self sortDataOfCompanies];
    
    [self.tableViewDetail reloadData];
}

-(void)SetHeader{
    NSArray *arrOriginalS = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalStateData"];
    NSArray *arrOriginalC = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalCountryData"];
    arrFinalStateData = [[NSUserDefaults standardUserDefaults] objectForKey:@"FinalStateDataDisp"];
    arrFinalCoutryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"FinalCountryDataDisp"];
    if((arrOriginalS.count == arrFinalStateData.count && arrOriginalC.count == arrFinalCoutryData.count) || (arrFinalStateData.count == 0 && arrFinalCoutryData.count == 0) || (arrOriginalS.count == 0 && arrOriginalC.count == 0)){
        labelSubTitle.text = @"All Locations";
        isAllLocationSelected = YES;
    }else{
        isAllLocationSelected = NO;
        NSString *strIds = @"";
        for (NSDictionary *dict in arrFinalStateData) {
            if([dict objectForKey:@"abr"]){
                NSString *strName = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"abr"]] removeNull];
                if([strIds isEqualToString:@""]){
                    strIds = [NSString stringWithFormat:@"%@",strName];
                }else{
                    strIds = [NSString stringWithFormat:@"%@,%@",strIds,strName];
                }
            }
        }
        for (NSDictionary *dict in arrFinalCoutryData) {
            if([dict objectForKey:@"abr"]){
                NSString *strName = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"abr"]] removeNull];
                if([strIds isEqualToString:@""]){
                    strIds = [NSString stringWithFormat:@"%@",strName];
                }else{
                    strIds = [NSString stringWithFormat:@"%@,%@",strIds,strName];
                }
            }
        }
        labelSubTitle.text = strIds;
    }
}

#pragma mark- selfCreatedButtonAction

-(void)backBtnAction
{
    fromLoadDisc = NO;
    
    if (checkFilter == YES)
    {
        checkFilter = NO;
        
        stringCheckDisciplines = @"";
        
        if ([stringFilterScreen isEqualToString:@"nothing"])
        {
            stringFilterScreen = @"nothing";
        }
        else
        {
            stringFilterScreen = @"filterSubDiscipline";
        }
        
        
    }
    
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)getAllCompanies
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"getAllCompanies" parameters:@{@"id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]} success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         else
         {
             [self ShowDataLoader:NO];
         }
         
         [self ShowDataLoader:NO];
         
         [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"callPortWebService"];
         
         [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"callCompy"];
         
         if ([[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountry"] == nil || [[NSUserDefaults standardUserDefaults]objectForKey:@"arryStates"] == nil)
         {
             
         }
         
         else
         {
             [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"fromDiscipline"];
         }
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             arrayAllCompanies = [responseObject valueForKey:@"data"];
             
             [self saveSearchCompanies:responseObject];
             
             NSArray *arrCompanies = [[NSArray alloc]initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]];
             
             NSArray *arrayTemp = [dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"];
             
             NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
             
             NSArray* sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
             
             NSArray *arraySorted = [[NSArray alloc] initWithArray:[arrayTemp sortedArrayUsingDescriptors:sortDescriptors]];
             
             [arrayCompaniesList removeAllObjects];
             
             arrayCompaniesList = [arraySorted mutableCopy];
             
             [[NSUserDefaults standardUserDefaults]setObject:arrCompanies forKey:@"allCompanies"];
             
         }
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         
         else
         {
             [self ShowDataLoader:NO];
         }
         
         [refreshControl endRefreshing];
         
         [self ShowDataLoader:NO];
         
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             //[[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }];
}



#pragma mark - FilterDataBase

-(void)saveSearchCompaniesFilter :(NSDictionary *)dicData
{
    if ([dummy deleteTable:@"delete from COMPANIESFILTER"])
    {
        if ([dummy deleteTable:@"delete from COMPANYLOCATIONSFILTER"])
        {
            if ([dummy deleteTable:@"delete from DispSubDispCompaniesTableFILTER"])
            {
                if ([dummy deleteTable:@"delete from SearchCompSubDisciplineTableFILTER"])
                {
                    if ([dummy deleteTable:@"delete from SearchCompanyAwardsFILTER"])
                    {
                        if ([dummy deleteTable:@"delete from SearchCompanyDueDateFILTER"])
                        {
                            if ([dummy deleteTable:@"delete from SearchQuesAnsTableFILTER"])
                            {
                                if ([dummy deleteTable:@"delete from SearchCompSocialLinksTableFILTER"])
                                {
                                    for (int i=0; i<[arrayAllCompanies count]; i++)
                                    {
                                        
                                        
                                        NSString *query = [NSString stringWithFormat: @"INSERT INTO COMPANIESFILTER (id,application_due_date, awards,city,company_address,company_name,description,logo,state_id,regstates,country_id,company_intro,country,portfolio_status,message_state,message_status,website,international) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")"
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"id"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"application_due_date"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"awards"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"city"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"company_address"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"company_name"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"description"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"logo"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"state_id"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"regstates"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"country_id"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"company_intro"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"country"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"portfolio_status"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"message_state"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"message_status"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"website"],[[arrayAllCompanies objectAtIndex:i] valueForKey:@"international"]];
                                        NSLog(@"query is %@", query);
                                        
                                        [self saveImages:[NSString stringWithFormat:@"%@%@",[[arrayAllCompanies  objectAtIndex:i] valueForKeyPath:@"id"],[[arrayAllCompanies  objectAtIndex:i] valueForKeyPath:@"logo"]] imageUrl:[[arrayAllCompanies  objectAtIndex:i] valueForKeyPath:@"logo"]];
                                        
                                        const char *query_stmt = [query UTF8String];
                                        [dummy  runQueries:query_stmt strForscreen:@"searchFilter"];
                                        
                                        [self saveDueDateSearchCompaniesFilter:i];
                                        [self saveCompanyLocationsFilter:i];
                                        [self saveCompanyDisciplinesFilter:i];
                                        [self saveSearchCompanyAwardsFilter:i];
                                        [self saveQuestionsSearchCompFilter:i];
                                        [self saveSearchCompanyLinksFilter:i];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    NSArray *arrCompanies = [[NSArray alloc]initWithArray:[dummy loaddata:@"Select * From COMPANIESFILTER" strForscreen:@"searchFilter"]];
    
    NSLog(@"filter companies are %@", arrCompanies);
    
    [self ShowDataLoader:NO];
    
    arrayData = [arrCompanies copy];
    
    [self sortDataOfCompanies];
    
    [_tableViewDetail reloadData];
}

-(void)saveDueDateSearchCompaniesFilter:(int)index
{
    for (int j=0; j<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all"] count]; j++)
    {
        NSString *subQueryDueDate;
        
        subQueryDueDate = [NSString stringWithFormat:@"INSERT INTO SearchCompanyDueDateFILTER(close_date,company_id,id,program,program_id,start_date,state,country) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.close_date"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.company_id"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.id"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.program"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.program_id"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.start_date"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.state"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.country"] objectAtIndex:j]];
        
        const char *query_stmt_dueDate = [subQueryDueDate UTF8String];
        
        [dummy runSubQueryToSaveDueDate:query_stmt_dueDate strForscreen:@"SearchFilter"];
    }
}

-(void)saveCompanyLocationsFilter:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location"] count]; k++)
    {
        NSString *subQuery = [NSString stringWithFormat: @"INSERT INTO COMPANYLOCATIONSFILTER (company_id,id,branch_name, lat,long,location) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\")"
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.company_id"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.id"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.branch_name"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.lat"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.long"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.location"] objectAtIndex:k]];
        
        const char *query_stmtLocation = [subQuery UTF8String];
        [dummy runSubQueryLocations:query_stmtLocation strForscreen:@"COMPANYLOCATIONSFilter"];
    }
}

-(void)saveCompanyDisciplinesFilter:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines"] count]; k++)
    {
        NSString *subQuery;
        
        NSArray *arraySubDisciplineId = [[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline.sub_discipline_id"] objectAtIndex:k];
        
        for (int i=0; i<arraySubDisciplineId.count; i++)
        {
            
            subQuery = [NSString stringWithFormat: @"INSERT INTO DispSubDispCompaniesTable (company_id,discipline_id,name,type,sub_discipline_id) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\")"
                        ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:k]
                        ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.discipline_id"] objectAtIndex:k]
                        ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.name"] objectAtIndex:k],[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.type"] objectAtIndex:k],[arraySubDisciplineId objectAtIndex:i]];
            
            const char *query_stmt_disciplines = [subQuery UTF8String];
            [dummy runQueryDispSubDispForCompanies:query_stmt_disciplines strForscreen:@"searchCompanyDisciplinesFilter"];
            [self saveSubDisciplinesPortFiter:index disciplineTag:k];
        }
    }
}

-(void)saveSubDisciplinesPortFiter:(int)index disciplineTag:(int)disciplineTag
{
    for (int j=0; j<[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag] count]; j++)
    {
        NSString *subQuery;
        
        subQuery = [NSString stringWithFormat: @"INSERT INTO SearchCompSubDisciplineTableFILTER (discipline_id,name,company_id,sub_discipline_id) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                    ,[[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"discipline_id"] objectAtIndex:j]
                    ,[[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"name"] objectAtIndex:j]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:disciplineTag]
                    ,[[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"sub_discipline_id"] objectAtIndex:j]
                    ];
        
        const char *query_stmt_PortSubDiscip = [subQuery UTF8String];
        
        
        [dummy runQuerySubDisciplines:query_stmt_PortSubDiscip strForscreen:@"SubDiscipSearchCompaniesFilter"];
    }
}

-(void)saveSearchCompanyAwardsFilter:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKey:@"allAwards"] count]; k++)
    {
        NSString *subQueryAwards;
        
        
        subQueryAwards = [NSString stringWithFormat: @"INSERT INTO SearchCompanyAwardsFILTER (awards,company_id,description, id) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.awards"] objectAtIndex:k]
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.company_id"] objectAtIndex:k]
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.description"] objectAtIndex:k]
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.id"] objectAtIndex:k]];
        
        const char *query_stmt_Awards= [subQueryAwards UTF8String];
        [dummy runSubQueryToSaveAwards:query_stmt_Awards strForscreen:@"SearchVwFilter"];
    }
}


-(void)saveQuestionsSearchCompFilter:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns"] count]; k++)
    {
        NSString *subQuery;
        subQuery = [NSString stringWithFormat: @"INSERT INTO SearchQuesAnsTableFILTER (answer,company_id,id,question) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.answer"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.company_id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.question"] objectAtIndex:k]];
        
        const char *query_stmt_QuesAns = [subQuery UTF8String];
        [dummy runQueryQuesAnsForCompanies:query_stmt_QuesAns strForscreen:@"searchFilter"];
    }
}

-(void)saveSearchCompanyLinksFilter:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies valueForKey:@"links"] objectAtIndex:index] count]; k++)
    {
        NSString *subQueryLinks;
        
        subQueryLinks = [NSString stringWithFormat: @"INSERT INTO SearchCompSocialLinksTableFILTER (company_id,id,media_link,media_logo,media_type) VALUES (\"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.company_id"] objectAtIndex:k]
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.id"] objectAtIndex:k]
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.media_link"] objectAtIndex:k]
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.media_logo"] objectAtIndex:k]
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.media_type"] objectAtIndex:k]];
        
        [self saveImages:[[NSString stringWithFormat:@"%@%@",[[[arrayAllCompanies valueForKeyPath:@"links.id"] objectAtIndex:index] objectAtIndex:k],[[[arrayAllCompanies valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                imageUrl:[[[arrayAllCompanies valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]];
        
        const char *query_stmt_SocialLinks = [subQueryLinks UTF8String];
        [dummy runQuerySocialLinksForCompanies:query_stmt_SocialLinks strForscreen:@"searchFilter"];
    }
}




#pragma mark - ALLComapniesDataBase

-(void)saveSearchCompanies :(NSDictionary *)dicData
{
    if ([dummy deleteTable:@"delete from COMPANIES"])
    {
        if ([dummy deleteTable:@"delete from COMPANYLOCATIONS"])
        {
            if ([dummy deleteTable:@"delete from DispSubDispCompaniesTable"])
            {
                if ([dummy deleteTable:@"delete from SearchCompSubDisciplineTable"])
                {
                    if ([dummy deleteTable:@"delete from SearchCompanyAwards"])
                    {
                        if ([dummy deleteTable:@"delete from SearchCompanyDueDate"])
                        {
                            if ([dummy deleteTable:@"delete from SearchQuesAnsTable"])
                            {
                                if ([dummy deleteTable:@"delete from SearchCompSocialLinksTable"])
                                {
                                    for (int i=0; i<[arrayAllCompanies count]; i++)
                                    {
                                        
                                        NSString *query = [NSString stringWithFormat: @"INSERT INTO COMPANIES (id,application_due_date, awards,city,company_address,company_name,description,logo,state_id,regstates,country_id,company_intro,country,portfolio_status,message_state,message_status,website,international,unread_message) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")"
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"id"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"application_due_date"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"awards"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"city"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"company_address"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"company_name"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"description"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"logo"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"state_id"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"regstates"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"country_id"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"company_intro"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"country"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"portfolio_status"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"message_state"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"message_status"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"website"],[[arrayAllCompanies objectAtIndex:i] valueForKey:@"international"],[[arrayAllCompanies valueForKey:@"unread_message"] objectAtIndex:i]];
                                        
                                        [self saveImages:[NSString stringWithFormat:@"%@%@",[[arrayAllCompanies  objectAtIndex:i] valueForKeyPath:@"id"],[[arrayAllCompanies  objectAtIndex:i] valueForKeyPath:@"logo"]] imageUrl:[[arrayAllCompanies  objectAtIndex:i] valueForKeyPath:@"logo"]];
                                        
                                        const char *query_stmt = [query UTF8String];
                                        [dummy  runQueries:query_stmt strForscreen:@"search"];
                                        
                                        [self saveDueDateSearchCompanies:i];
                                        [self saveCompanyLocations:i];
                                        [self saveCompanyDisciplines:i];
                                        [self saveSearchCompanyAwards:i];
                                        [self saveQuestionsSearchComp:i];
                                        [self saveSearchCompanyLinks:i];
                                        [self saveCompanySubDisciplines:i];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    [self ShowDataLoader:NO];
    
}


-(void)saveDueDateSearchCompanies:(int)index
{
    for (int j=0; j<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all"] count]; j++)
    {
        NSString *subQueryDueDate;
        
        
        subQueryDueDate = [NSString stringWithFormat:@"INSERT INTO SearchCompanyDueDate(close_date,company_id,id,program,program_id,start_date,state,country) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.close_date"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.company_id"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.id"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.program"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.program_id"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.start_date"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.state"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.country"] objectAtIndex:j]];
        
        const char *query_stmt_dueDate = [subQueryDueDate UTF8String];
        
        [dummy runSubQueryToSaveDueDate:query_stmt_dueDate strForscreen:@"Search"];
    }
}


-(void)saveCompanyLocations:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location"] count]; k++)
    {
        NSString *subQuery = [NSString stringWithFormat: @"INSERT INTO COMPANYLOCATIONS (company_id,id,branch_name, lat,long,location) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\")"
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.company_id"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.id"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.branch_name"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.lat"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.long"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.location"] objectAtIndex:k]];
        
        const char *query_stmtLocation = [subQuery UTF8String];
        [dummy runSubQueryLocations:query_stmtLocation strForscreen:@"COMPANYLOCATIONS"];
        
    }
}

-(void)saveCompanySubDisciplines:(int)index
{
    NSLog(@"index is %d", index);
    
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines"] count]; k++)
    {
        NSString *subQuery;
        
        NSArray *arraySubDisciplineId = [[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline.sub_discipline_id"] objectAtIndex:k];
        
        for (int i=0; i<arraySubDisciplineId.count; i++)
        {
            subQuery = [NSString stringWithFormat: @"INSERT INTO DispSubDispDataTable (company_id,discipline_id,name,sub_discipline_id) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                        ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:k]
                        ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.discipline_id"] objectAtIndex:k]
                        ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.name"] objectAtIndex:k],[arraySubDisciplineId objectAtIndex:i]];
            
            const char *query_stmt_disciplines = [subQuery UTF8String];
            [dummy runQueryDispSubDispFordisp:query_stmt_disciplines strForscreen:@"searchCompanyDisciplines"];
        }
    }
}


-(void)saveCompanyDisciplines:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines"] count]; k++)
    {
        NSString *subQuery;
        
        
        subQuery = [NSString stringWithFormat: @"INSERT INTO DispSubDispCompaniesTable (company_id,discipline_id,name,type) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.discipline_id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.name"] objectAtIndex:k],[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.type"] objectAtIndex:k]];
        
        const char *query_stmt_disciplines = [subQuery UTF8String];
        [dummy runQueryDispSubDispForCompanies:query_stmt_disciplines strForscreen:@"searchCompanyDisciplines"];
        [self saveSubDisciplinesPort:index disciplineTag:k];
        
    }
}

-(void)saveSubDisciplinesPort:(int)index disciplineTag:(int)disciplineTag
{
    for (int j=0; j<[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag] count]; j++)
    {
        NSString *subQuery;
        
        subQuery = [NSString stringWithFormat: @"INSERT INTO SearchCompSubDisciplineTable (discipline_id,name,company_id,sub_discipline_id) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                    ,[[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"discipline_id"] objectAtIndex:j]
                    ,[[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"name"] objectAtIndex:j]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:disciplineTag]
                    ,[[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"sub_discipline_id"] objectAtIndex:j]
                    ];
        
        const char *query_stmt_PortSubDiscip = [subQuery UTF8String];
        
        
        [dummy runQuerySubDisciplines:query_stmt_PortSubDiscip strForscreen:@"SubDiscipSearchCompanies"];
    }
}

-(void)saveSearchCompanyAwards:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKey:@"allAwards"] count]; k++)
    {
        NSString *subQueryAwards;
        
        
        subQueryAwards = [NSString stringWithFormat: @"INSERT INTO SearchCompanyAwards (awards,company_id,description, id) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.awards"] objectAtIndex:k]
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.company_id"] objectAtIndex:k]
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.description"] objectAtIndex:k]
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.id"] objectAtIndex:k]];
        
        const char *query_stmt_Awards= [subQueryAwards UTF8String];
        [dummy runSubQueryToSaveAwards:query_stmt_Awards strForscreen:@"SearchVw"];
    }
}


-(void)saveQuestionsSearchComp:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns"] count]; k++)
    {
        NSString *subQuery;
        subQuery = [NSString stringWithFormat: @"INSERT INTO SearchQuesAnsTable (answer,company_id,id,question) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.answer"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.company_id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.question"] objectAtIndex:k]];
        
        const char *query_stmt_QuesAns = [subQuery UTF8String];
        [dummy runQueryQuesAnsForCompanies:query_stmt_QuesAns strForscreen:@"search"];
    }
}

-(void)saveSearchCompanyLinks:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies valueForKey:@"links"] objectAtIndex:index] count]; k++)
    {
        NSString *subQueryLinks;
        
        subQueryLinks = [NSString stringWithFormat: @"INSERT INTO SearchCompSocialLinksTable (company_id,id,media_link,media_logo,media_type) VALUES (\"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.company_id"] objectAtIndex:k]
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.id"] objectAtIndex:k]
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.media_link"] objectAtIndex:k]
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.media_logo"] objectAtIndex:k]
                         ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.media_type"] objectAtIndex:k]];
        
        [self saveImages:[[NSString stringWithFormat:@"%@%@",[[[arrayAllCompanies valueForKeyPath:@"links.id"] objectAtIndex:index] objectAtIndex:k],[[[arrayAllCompanies valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                imageUrl:[[[arrayAllCompanies valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]];
        const char *query_stmt_SocialLinks = [subQueryLinks UTF8String];
        [dummy runQuerySocialLinksForCompanies:query_stmt_SocialLinks strForscreen:@"search"];
    }
}

-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        [self.view.window setUserInteractionEnabled:NO];
        spinner.center = self.view.center;
        spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [spinner startAnimating];
        [self.view addSubview:spinner];
    }
    else
    {
        [spinner stopAnimating];
        [self.view.window setUserInteractionEnabled:YES];
        [spinner removeFromSuperview];
        spinner = nil;
    }
}

-(void)saveImages:(NSString *)ImageName imageUrl :(NSString *)imageUrl
{
    ImageName = [ImageName stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    if (imageUrl.length>0)
    {
        if (![self imageCheck:ImageName])
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:ImageName];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,imageUrl]]];
            
            [imageData writeToFile:savedImagePath atomically:NO];
        }
    }
}

#pragma mark- CheckForImageInDataBase

-(BOOL)imageCheck :(NSString *)strName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    if ([contents containsObject:strName])
    {
        return YES;
    }
    return NO;
}

#pragma mark- UITableViewDataSource---------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView setBackgroundColor:[UIColor clearColor]];
    
    static NSString *CellIdentifier = @"cellID";
    
    tableView.backgroundColor = [UIColor clearColor];
    
    SearchTableViewCell *cell = (SearchTableViewCell*) [_tableViewDetail dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SearchTableViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    if ([[[arrayData valueForKey:@"application_due_date"] objectAtIndex:indexPath.row ] isEqualToString:@"no"]||[[[arrayData valueForKey:@"application_due_date"] objectAtIndex:indexPath.row ] isEqualToString:@"Open Until Filled"])
    {
        cell.labelDate.text = @"Open Until Filled";
    }
    
    else
    {
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSDate *yourDate = [dateFormatter dateFromString:[[arrayData valueForKey:@"application_due_date"] objectAtIndex:indexPath.row ]];
        
        NSDateFormatter* myDateFormatter = [[NSDateFormatter alloc] init];
        myDateFormatter.dateFormat = @"dd-MMM-yyyy";
        //vijay007
        cell.labelDate.text = [[NSString stringWithFormat:@"%@",[myDateFormatter stringFromDate:yourDate]] uppercaseString];
    }
    
    cell.labelCompanyName.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    self.tableViewDetail.contentInset = UIEdgeInsetsMake(-1.0f, -1.0f, 0.0f, 0.0);
    
    cell.labelCompanyName.text= [[arrayData valueForKey:@"company_name"] objectAtIndex:indexPath.row ];
    
    if(![Utilities CheckInternetConnection])
    {
        if ([[[arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row] isEqualToString:@"N"] || [[[arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row] isEqualToString:@""])
        {
            [cell.imageVwLogo setImage:[UIImage imageNamed:@"ICO_Company"]];
        }
        else
        {
            if ([[[arrayData valueForKey:@"logo"] objectAtIndex:indexPath.row]  hasPrefix:@"/Users/"])
            {
                cell.imageVwLogo.image = [UIImage imageWithContentsOfFile:[[arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row] ];
            }
            
            else if ([[[arrayData valueForKey:@"logo"] objectAtIndex:indexPath.row] hasPrefix:@"/var/mobile/Containers/"])
            {
                cell.imageVwLogo.image = [UIImage imageWithContentsOfFile:[[arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row]];
            }
            
            else
            {
                cell.imageVwLogo.image = [UIImage imageWithContentsOfFile:[[arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row]];
            }
            
            if (!cell.imageVwLogo.image) {
                
                NSLog(@"logo007");
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld%@",(long)[[[arrayData valueForKey:@"id"]objectAtIndex:indexPath.row] integerValue],[[NSString stringWithFormat:@"%@",[[arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row]] stringByReplacingOccurrencesOfString:@"/" withString:@""]]];
                cell.imageVwLogo.image = [UIImage imageWithContentsOfFile:savedImagePath];
                if (!cell.imageVwLogo.image) {
                    NSLog(@"logo008");
                    [cell.imageVwLogo setImage:[UIImage imageNamed:@"ICO_Company"]];
                }
            }
        }
    }
    
    else
        
    {
        if ([[[arrayData valueForKey:@"logo"] objectAtIndex:indexPath.row] isEqualToString:@""])
        {
            [cell.imageVwLogo setImage:[UIImage imageNamed:@"ICO_Company"]];
        }
        
        if ([[[arrayData valueForKey:@"logo"] objectAtIndex:indexPath.row] hasPrefix:@"/Users/"])
        {
            cell.imageVwLogo.image = [UIImage imageWithContentsOfFile:[[arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row] ];
        }
        else if ([[[arrayData valueForKey:@"logo"] objectAtIndex:indexPath.row] hasPrefix:@"/var/mobile/Containers/"])
        {
            cell.imageVwLogo.image = [UIImage imageWithContentsOfFile:[[arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row]];
        }
        else
        {
            cell.imageVwLogo.imageURL =[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl, [[arrayData valueForKey:@"logo"]objectAtIndex:indexPath.row]]];
        }
    }
    
    cell.imageVwLogo.layer.cornerRadius = 5.0f;
    cell.imageVwLogo.clipsToBounds = YES;
    cell.imageVwLogo.layer.borderWidth = 2.0f;
    cell.imageVwLogo.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor whiteColor]);
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.10];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}


#pragma mark-----UITableViewDelegate----------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*Open view detailDescription view controller*/
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CompaniesDetailViewController *companiesView = [self.storyboard instantiateViewControllerWithIdentifier:@"CompaniesDetailView"];
    
    companiesView.strVwChane = @"CompaniesVw";
    
    companiesView.dicCompanyDetail = [[NSMutableDictionary alloc] initWithDictionary:[arrayData objectAtIndex:indexPath.row]];
    
    self.navigationController.navigationBarHidden = NO;
    
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    [self.navigationController pushViewController:companiesView animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.001f;
}

-(void)setAwardPhotos:(NSIndexPath*)index cellTable:(SearchTableViewCell*)cellTable
{
    int xValue=71;
    
    for (int i=0; i<5; i++)
    {
        UIImageView *imageAwards = [[UIImageView alloc] initWithFrame:CGRectMake(xValue, 39, 12, 12)];
        
        xValue = xValue+16;
        
        if (i<[[[arrayData valueForKey:@"allAwards"] objectAtIndex:index.row] count])
        {
            if ([[[[arrayData valueForKeyPath:@"allAwards.awards"] objectAtIndex:index.row] objectAtIndex:i] isEqualToString:@"2"])
            {
                imageAwards.image = [UIImage imageNamed:@"yellow-cup"];
            }
            
            else if ([[[[arrayData valueForKeyPath:@"allAwards.awards"] objectAtIndex:index.row] objectAtIndex:i] isEqualToString:@"3"])
            {
                imageAwards.image = [UIImage imageNamed:@"silverCup"];
            }
            
            else if ([[[[arrayData valueForKeyPath:@"allAwards.awards"] objectAtIndex:index.row] objectAtIndex:i] isEqualToString:@"1"])
            {
                imageAwards.image = [UIImage imageNamed:@"honour"];
            }
            
            else if ([[[[arrayData valueForKeyPath:@"allAwards.awards"] objectAtIndex:index.row] objectAtIndex:i] isEqualToString:@"0"])
            {
                imageAwards.image = [UIImage imageNamed:@"noAward"];
            }
            else
            {
                imageAwards.image = [UIImage imageNamed:@"light_silver"];
            }
        }
        else
        {
            imageAwards.image = [UIImage imageNamed:@"light_silver"];
        }
        
        [cellTable.contentView addSubview:imageAwards];
    }
}


#pragma mark- IbButtonActions
- (IBAction)btnSortCompaniesAction:(id)sender
{
    if ([sender tag]==11)
    {
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
        
        NSArray*  sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
        
        NSArray*  sortedArray = [arrayData sortedArrayUsingDescriptors:sortDescriptors];
        
        arrayData = [sortedArray copy];
    }
    else if ([sender tag]==12)
    {
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"awards" ascending:NO];
        
        NSArray *stories = [arrayData sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
        
        arrayData = [stories copy];
    }
    else
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy/MM/dd"];
        
        NSMutableArray*arrwithDates,*arrayWithoutDates;
        arrwithDates = [NSMutableArray array];
        arrayWithoutDates = [NSMutableArray array];
        
        for (NSDictionary*dict in arrayData) {
            if ([[dict objectForKey:@"application_due_date"] isEqualToString:@"no"]||[[dict objectForKey:@"application_due_date"] isEqualToString:@"Open Until Filled"])
            {
                [arrayWithoutDates addObject:dict];
            }
            else{
                [arrwithDates addObject:dict];
            }
        }
        
        NSArray *arrayDatesSorted = [arrwithDates sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2)
                                     {
                                         NSDate *d1 = [formatter dateFromString:obj1[@"application_due_date"]];
                                         NSDate *d2 = [formatter dateFromString:obj2[@"application_due_date"]];
                                         return [d1 compare:d2]; // ascending order
                                         //                 return [d2 compare:d1]; // descending order
                                     }];
        
        arrwithDates = [arrayDatesSorted mutableCopy];
        [arrwithDates addObjectsFromArray:arrayWithoutDates];
        
        arrayData = [NSArray arrayWithArray:arrwithDates];
        [arrwithDates removeAllObjects];
        [arrayWithoutDates removeAllObjects];
        arrwithDates = nil;
        arrayWithoutDates = nil;
    }
    
    [self sortDataOfCompanies];
    
    [_tableViewDetail reloadData];
}


#pragma mark- tabBarButtonAction
- (IBAction)buttonActionPortfolio:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[PortfolioViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    PortfolioViewController *portfolioView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
    [self.navigationController pushViewController:portfolioView animated:NO];
}
- (IBAction)buttonActionDiscip:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[DisciplineViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    DisciplineViewController *disciplineVw = [self.storyboard instantiateViewControllerWithIdentifier:@"disciplineView"];
    [self.navigationController pushViewController:disciplineVw animated:NO];
}

- (IBAction)buttonSearch:(id)sender;
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"callallCompanyWebService"];
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:[LocationFilterSearchViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;}}
    LocationFilterSearchViewController  *searchVw = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVw"];
    [self.navigationController pushViewController:searchVw animated:NO];
}

- (IBAction)buttonMessages:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[MessagesViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    MessagesViewController  *messageVw = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    [self.navigationController pushViewController:messageVw animated:NO];
    
}
- (IBAction)buttonActionSalary:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[SalaryGuideViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    SalaryGuideViewController *salaryVw = [self.storyboard instantiateViewControllerWithIdentifier:@"salaryVw"];
    [self.navigationController pushViewController:salaryVw animated:NO];
}


@end
