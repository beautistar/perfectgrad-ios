//
//  SearchTableViewCell.h
//  PerfectGraduate
//
//  Created by NSPL on 2/3/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"


@interface SearchTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelCompanyName;
@property (strong, nonatomic) IBOutlet UIImageView *imageVwLogo;
@property (strong, nonatomic) IBOutlet UIImageView *imageAwardOne;
@property (strong, nonatomic) IBOutlet UIImageView *imageAwardTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imageAwardThree;
@property (strong, nonatomic) IBOutlet UIImageView *imageAwardFour;
@property (strong, nonatomic) IBOutlet UIImageView *imageAwardFive;
@property (strong, nonatomic) IBOutlet UILabel *labelDate;

@end
