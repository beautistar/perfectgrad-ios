//
//  SearchLocationFilterTableViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 2/3/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchLocationFilterTableViewController : UITableViewController<UISearchBarDelegate,UISearchDisplayDelegate>
@property (strong,nonatomic) NSArray *countryArray;
@property (strong,nonatomic) NSMutableArray *filteredcountryArray;
@property IBOutlet UISearchBar *countrySearchBar;

@end
