
//
//  DisciplineViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 2/2/15.
//  Copyright (c) 2015 netset. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Utilities.h"

@interface DisciplineViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    int currentExpandedIndex;
    BOOL isExpanded;
    UIImage *imgView;
    NSArray *arrFinalStateData;
    NSArray *arrFinalCoutryData;
    BOOL isAllLocationSelected;
    //
    NSMutableArray *arraydiscipline;
}
@property (weak, nonatomic) IBOutlet UITableView *disciplineTableView;
@property (weak, nonatomic) IBOutlet UIButton *disciplineNameButton;
@property (nonatomic, strong) NSMutableArray *selectedIndexPaths;
@property (weak, nonatomic) IBOutlet UIButton *bButton;
@property (strong, nonatomic) IBOutlet UIView *viewBottomBar;
@property (strong, nonatomic) IBOutlet UIButton *buttonPortfolio;
@property (strong, nonatomic) IBOutlet UIButton *buttonDiscipline;
@property (strong, nonatomic) IBOutlet UIButton *buttonSearch;
@property (strong, nonatomic) IBOutlet UIButton *buttonMessages;
@property (strong, nonatomic) IBOutlet UIButton *buttonSalaryguide;
- (IBAction)buttonActionPortfolio:(id)sender;
- (IBAction)buttonActionDiscip:(id)sender;
- (IBAction)buttonSearch:(id)sender;
- (IBAction)buttonMessages:(id)sender;
- (IBAction)buttonActionSalary:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *labelPortfolio;
@property (strong, nonatomic) IBOutlet UILabel *labelDiscipline;
@property (strong, nonatomic) IBOutlet UILabel *labelSearchVw;
@property (strong, nonatomic) IBOutlet UILabel *labelSalaryGuide;
////
@property (strong, nonatomic) IBOutlet NSMutableArray *arraydiscipline;
@property (strong, nonatomic) IBOutlet UILabel *labelMessages;
@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;


@end
