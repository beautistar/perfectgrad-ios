//
//  CompanyMoreInfoVC.m
//  PerfectGraduate
//
//  Created by netset on 4/9/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "CompanyMoreInfoVC.h"
#import "AppDelegate.h"


@interface CompanyMoreInfoVC ()
{
    UIActivityIndicatorView* spinner;
}

@end

@implementation CompanyMoreInfoVC

-(void)viewWillAppear:(BOOL)animated
{
    stringNotifiction = @"";
    
    stringForNotifyEditCompany = @"";
    
    if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"] )
    {
        self.imageRedStar.hidden = YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(redStarShow)
                                                 name:@"redStarShow"
                                               object:nil];
}

-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imageRedStar.hidden = YES;
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, _imageRedStar.frame.origin.y, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    
    
    _webViewSocial.delegate =self;
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:18.0f],NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil]];

    NSCharacterSet *whitespace = [NSCharacterSet whitespaceCharacterSet];
    NSString *urlAddress = [[_arrLink valueForKey:@"media_link"] stringByTrimmingCharactersInSet:whitespace];
    
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webViewSocial loadRequest:requestObj];

    self.navigationItem.hidesBackButton = YES;
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnAction)];
    self.navigationItem.leftBarButtonItem = leftItem;

    self.title = [_arrLink valueForKey:@"media_type"];
    
    // Do any additional setup after loading the view.
}


-(void)backBtnAction
{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark--UIWebViewDelegate-----

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self ShowDataLoader:YES];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self ShowDataLoader:NO];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self ShowDataLoader:NO];
    [[[UIAlertView alloc] initWithTitle:@"Error !!" message:@"\nSorry failed to load the page,try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil, nil]show];
}

-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        spinner.center = self.view.center;
        spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [spinner startAnimating];
        [self.view addSubview:spinner];
    }
    else
    {
        [spinner stopAnimating];
        [spinner removeFromSuperview];
        spinner = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
