//
//  SelectCountryViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 2/18/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol senddataProtocol <NSObject>

-(void)sendDataToStateLoc:(NSMutableArray *)array;

@end

@interface SelectCountryViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate>{
    NSArray *NewCountryIdArr;
    
}

@property (strong,nonatomic) NSMutableArray *arrTempCountryData;
@property (strong, nonatomic) IBOutlet UITableView *tableViewSelectCountry;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarCountry;
@property (strong, nonatomic) IBOutlet UIView *viewBottomBar;
@property (strong, nonatomic) IBOutlet UIButton *buttonPortfolio;
@property (strong, nonatomic) IBOutlet UIButton *buttonDiscipline;
@property (strong, nonatomic) IBOutlet UIButton *buttonSearch;
@property (strong, nonatomic) IBOutlet UIButton *buttonMessages;
@property (strong, nonatomic) IBOutlet UIButton *buttonSalaryguide;

@property (strong, nonatomic) IBOutlet UILabel *labelPortfolio;
@property (strong, nonatomic) IBOutlet UILabel *labelDiscipline;
@property (strong, nonatomic) IBOutlet UILabel *labelSearchVw;
@property (strong, nonatomic) IBOutlet UILabel *labelSalaryGuide;
@property (strong, nonatomic) IBOutlet UILabel *labelmessag;
@property(nonatomic,assign)id delegate;

@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;



- (IBAction)buttonActionPortfolio:(id)sender;
- (IBAction)buttonActionDiscip:(id)sender;
- (IBAction)buttonSearch:(id)sender;
- (IBAction)buttonMessages:(id)sender;
- (IBAction)buttonActionSalary:(id)sender;

-(void)sendDataBack:(NSArray *)array;

@end
