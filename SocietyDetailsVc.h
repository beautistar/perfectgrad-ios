//
//  SocietyDetailsVc.h
//  PerfectGraduate
//
//  Created by netset on 3/26/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TermsConditionViewController.h"
#import "AFNetworking.h"
#import "SWRevealViewController.h"
#import "Reachability.h"
#import "Utilities.h"
#import "PortfolioViewController.h"

@interface SocietyDetailsVc : UIViewController<UIAlertViewDelegate>

@property(nonatomic) NSString *isFromScreen;
@property (strong,nonatomic) NSDictionary *dictUserInfoFromUsrPg;

@property (nonatomic)NSArray *arrSocieties;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerVwSociety;
@property (strong, nonatomic) IBOutlet UITextField *textFldSocietyOne;
@property (strong, nonatomic) IBOutlet UITextField *textFldSocietyTwo;
@property (strong, nonatomic) IBOutlet UITextField *textFldSocietyThree;
@property (strong, nonatomic) IBOutlet UITextField *textFldSocietyFour;
@property (strong, nonatomic) IBOutlet UITextField *textFldSocietyFive;
@property (strong, nonatomic) IBOutlet UITextField *textFldSocietySix;
@property (strong, nonatomic) IBOutlet UIView *viewCompContainer;
@property (strong, nonatomic) IBOutlet UIButton *buttonSubmit;


- (IBAction)buttonOpenPickerAction:(id)sender;

- (IBAction)buttonSubmitAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewButtons;
- (IBAction)buttonCancelAction:(id)sender;
- (IBAction)buttonDoneAction:(id)sender;

@end
