//
//  MessageSubscriptionManager.h
//  PerfectGraduate
//
//  Created by Mubin Lakadia on 3/3/17.
//  Copyright © 2017 netset. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageSubscriptionManager : NSObject

+(id)sharedManager;

-(void)refreshData;
-(BOOL)isMessageSubscribedForId:(NSString *)companyId isCompany:(BOOL)isCompany;
-(void)updateSubscriptionMessagesForId:(NSString *)companyId isCompany:(BOOL)isCompany shouldSubscribe:(BOOL)shouldSubscribe;

@end
