//
//  SocietySelectionVC.m
//  PerfectGraduate
//
//  Created by NetSet on 07/04/16.
//  Copyright © 2016 netset. All rights reserved.
//


#import "SocietySelectionVC.h"
#import "Globals.h"

@interface SocietySelectionVC ()
{
    NSMutableArray *arraySocieties;
    NSMutableArray *arraySocietyIds;
    NSString *stringSocietyName,*strId;
    int replace;
    UIActivityIndicatorView*spinner;
}

@end

@implementation SocietySelectionVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arraySocietyIds = [NSMutableArray new];
    
    arraySocieties = [NSMutableArray new];
    
    [self addBackground];
    [self addNavTitle];
    [self getAllStudentSocieties];
    [self addButtons];
    
    NSLog(@"value in cntryLatitude detail %@", PREF(LATITUDE_COUNTRY));
    
    NSLog(@"value in cntryLongitude detail %@", PREF(LONGITUDE_COUNTRY));
}

-(void)viewWillAppear:(BOOL)animated{
    NSDictionary *dicSocieties;
    if ([_isFromScreen isEqualToString:@"uniChange"])
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userSocieties"];
        
        [self addTextfields:1 count:4 arraySociety:[dicSocieties valueForKey:@"data"] yPostion:21];
    }
    else
    {
        dicSocieties = [[NSUserDefaults standardUserDefaults] objectForKey:@"userSocieties"];
        
        [arraySocietyIds addObjectsFromArray:[dicSocieties valueForKeyPath:@"data.id"]];
        
        if ([[dicSocieties valueForKey:@"data"] count]<4)
        {
            [self addTextfields:1 count:4 arraySociety:[dicSocieties valueForKey:@"data"] yPostion:21];
        }
        else
        {
            [self addTextfields:1 count:[[dicSocieties valueForKey:@"data"] count] arraySociety:[dicSocieties valueForKey:@"data"] yPostion:21];
        }
    }
}

-(void)addButtons
{
    _btnSubmit.layer.cornerRadius = 5.0f;
    _btnSubmit.layer.borderWidth = 2.0f;
    
    _btnSubmit.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]  CGColor];
    [_btnSubmit setTitleColor:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    
    UIView *viewAddBtnsLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 40)];
    
    UIButton * buttonMenu = [[UIButton alloc]initWithFrame:CGRectMake(-3, 4, 30, 30) ];
    
    buttonMenu.backgroundColor = [UIColor clearColor];
    
    [buttonMenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    
    [buttonMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    [viewAddBtnsLeft addSubview:buttonMenu];
    
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:viewAddBtnsLeft];
    
    if ([_isFromScreen isEqualToString:@"menu"])
    {
        [self.navigationItem setHidesBackButton:NO];
        
        UIView *viewAddBtnsLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 40)];
        
        UIButton * buttonMenu = [[UIButton alloc]initWithFrame:CGRectMake(-3, 4, 30, 30) ];
        
        [buttonMenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
        
        [buttonMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        
        [viewAddBtnsLeft addSubview:buttonMenu];
    }
    else if ([_isFromScreen isEqualToString:@"uniChange"])
    {
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationController.navigationItem.hidesBackButton = YES;
    }
    else
    {
        self.navigationItem.hidesBackButton = YES;
        
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrowBackWhite"] style:UIBarButtonItemStylePlain target:self action:@selector(backToStudentPageBtnAction)];
        
        self.navigationItem.leftBarButtonItem = leftItem;
        
        [[[UIAlertView alloc]initWithTitle:@"Almost there..." message:@"\nShow your support for student societies by selecting them.\n\nSocieties will be able to inform you of upcoming company and social events." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
    }
}

-(void)addBackground
{
    UIImageView *imageBg = [[UIImageView alloc] initWithFrame:self.view.frame];
    [imageBg setImage:[UIImage imageNamed:@"appBack"]];
    imageBg.contentMode = UIViewContentModeScaleAspectFill;
    imageBg.clipsToBounds = YES;
    [self.view addSubview:imageBg];
    [self.view sendSubviewToBack:imageBg];
}

-(void)addNavTitle
{
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    UIView *viewTitles = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    UILabel *lblNavTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(viewTitles.frame), 22)];
    
    lblNavTitle.textAlignment = NSTextAlignmentCenter;
    lblNavTitle.text = @"Student Society";
    lblNavTitle.font = [UIFont fontWithName:@"OpenSans" size:18.0f];
    lblNavTitle.textColor = [UIColor whiteColor];
    
    UILabel *lblNavSubTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 22, CGRectGetWidth(viewTitles.frame), 18)];
    lblNavSubTitle.textAlignment = NSTextAlignmentCenter;
    
    lblNavSubTitle.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"userUniversity"];
    lblNavSubTitle.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
    lblNavSubTitle.textColor = [UIColor whiteColor];
    
    [viewTitles addSubview:lblNavTitle];
    [viewTitles addSubview:lblNavSubTitle];
    self.navigationItem.titleView = viewTitles;
}

#pragma mark- ButtonActions

-(void)backToStudentPageBtnAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addTextfields:(int)tag count:(int)count arraySociety:(NSArray*)arraySociety  yPostion:(int)yPostion
{
    CGFloat y = yPostion;
    
    for (int i=tag ; i<=count; i++)
    {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, y, 80, 31)];
        label.font =  [UIFont fontWithName:@"OpenSans-Bold" size:12.0f];
        label.text = [NSString stringWithFormat:@"%i) Society",i];
        
        UITextField *textFld = [[UITextField alloc] initWithFrame:CGRectMake(120, y, self.view.frame.size.width-130, 31)];
        
        textFld.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
        
        textFld.tintColor = [UIColor clearColor];
        
        textFld.backgroundColor = [UIColor whiteColor];
        textFld.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        
        textFld.delegate = self;
        
        if (i<=arraySociety.count)
        {
            textFld.text = [[arraySociety valueForKey:@"society_name"] objectAtIndex:i-1];
        }
        
        textFld.tag = i;
        y = CGRectGetMaxY(textFld.frame)+20;
        [_scrollViewSociety addSubview:label];
        [_scrollViewSociety addSubview:textFld];
        
        [self setPickerView:textFld];
        [self setRightViewFortextField:textFld];
        
        if (i==count)
        {
            UIButton *addMoreBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, y, 80, 30)];
            [addMoreBtn setTitle:@"Add More..." forState:UIControlStateNormal];
            [addMoreBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:12.0f]];
            [addMoreBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            [addMoreBtn addTarget:self action:@selector(addMoreAction:) forControlEvents:UIControlEventTouchUpInside];
            addMoreBtn.tag = count+1;
            [_scrollViewSociety addSubview:addMoreBtn];
            
            int yMinSubmitBtn = CGRectGetMinY(_btnSubmit.frame);
            int yMaxAddBtn = CGRectGetMaxY(addMoreBtn.frame);
            
            if (yMaxAddBtn>yMinSubmitBtn||yMaxAddBtn>yMinSubmitBtn)
            {
                CGRect submitBtnRect = _btnSubmit.frame;
                submitBtnRect.origin.y = CGRectGetMaxY(addMoreBtn.frame)+30;
                _btnSubmit.frame = submitBtnRect;
            }
            _scrollViewSociety.contentSize = CGSizeMake(0, CGRectGetMaxY(_btnSubmit.frame)+20);
        }
    }
}

- (void)setRightViewFortextField:(UITextField *)textField
{
    UIImageView *imgV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetHeight(textField.frame), CGRectGetHeight(textField.frame))];
    imgV.contentMode = UIViewContentModeCenter;
    imgV.image = [UIImage imageNamed:@"FIRST"];
    textField.rightView = imgV;
    textField.rightViewMode = UITextFieldViewModeAlways;
    UIImageView *imgVLeft = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 10, 5)];
    textField.leftView = imgVLeft;
    textField.leftViewMode = UITextFieldViewModeAlways;
    
    CALayer *layer1 = [[CALayer alloc]init];
    layer1.frame = CGRectMake(0, 0, CGRectGetWidth(textField.frame)-CGRectGetHeight(textField.frame)+1, CGRectGetHeight(textField.frame));
    layer1.borderColor = [[UIColor lightGrayColor] CGColor];
    layer1.borderWidth = 1.0;
    
    [textField.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc]init];
    layer2.frame = CGRectMake(CGRectGetWidth(textField.frame)-CGRectGetHeight(textField.frame), 0,CGRectGetHeight(textField.frame), CGRectGetHeight(textField.frame));
    layer2.borderColor = [[UIColor lightGrayColor] CGColor];
    layer2.borderWidth = 1.0;
    [textField.layer addSublayer:layer2];
}

-(void)setPickerView :(UITextField *)textFld
{
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0)];
    
    pickerToolbar.translucent=NO;
    
    pickerToolbar.barTintColor=[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    pickerToolbar.barStyle = UIBarStyleDefault;
    
    [pickerToolbar sizeToFit];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed:)];
    doneBtn.tag = textFld.tag;
    
    doneBtn.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonPressed)];
    [pickerToolbar setItems:@[cancelBtn, flexSpace, doneBtn] animated:YES];
    
    cancelBtn.tintColor = [UIColor whiteColor];
    
    textFld.inputAccessoryView = pickerToolbar;
    
    _societyPicker = [[UIPickerView alloc] init];
    _societyPicker.frame = CGRectMake(0, 0, 320, 162);
    [_societyPicker setDataSource: self];
    [_societyPicker setDelegate: self];
    _societyPicker.showsSelectionIndicator = YES;
    _societyPicker.backgroundColor =[UIColor colorWithRed:233.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.0f];
    
    textFld.inputView = _societyPicker;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (arraySocieties.count>0)
    {
        [self.societyPicker selectRow:0 inComponent:0 animated:YES];
        // The delegate method isn't called if the row is selected programmatically
        [self pickerView:self.societyPicker didSelectRow:0 inComponent:0];
        
        [_societyPicker reloadAllComponents];
        
        return YES;
    }
    return NO;
}

-(void)doneButtonPressed:(id)sender
{
    UITextField *txtFld = [self.view viewWithTag:[sender tag]];
    
    replace = (int) [sender tag];
    
    if ([stringSocietyName isEqualToString:@"None"])
    {
        if (arraySocietyIds.count == 0)
        {
            
        }
        else
        {
            if (![txtFld.text isEqualToString:@""])
            {
                if (arraySocietyIds.count==replace-1)
                {
                    [arraySocietyIds removeObjectAtIndex:arraySocietyIds.count-1];
                }
                else
                {
                    [arraySocietyIds removeObjectAtIndex:replace-1];
                }
            }
        }
        txtFld.text = @"";
    }
    
    else if (strId!=nil)
    {
        if (![txtFld.text isEqualToString:@""])
        {
            if (arraySocietyIds.count==replace-1)
            {
                [arraySocietyIds replaceObjectAtIndex:arraySocietyIds.count-1 withObject:strId];
            }
            else
            {
                [arraySocietyIds replaceObjectAtIndex:replace-1 withObject:strId];
            }
            txtFld.text = stringSocietyName;
        }
        else
        {
            
            txtFld.text = stringSocietyName;
            
            [arraySocietyIds addObject:strId];
            
        }
    }
    
    [self.view endEditing:YES];
}

-(void)cancelButtonPressed
{
    [self.view endEditing:YES];
}

-(void)addMoreAction:(id)sender
{
    UIButton *btn = [self.view viewWithTag:[sender tag]];
    int y = CGRectGetMinY(btn.frame);
    [btn removeFromSuperview];
    [self addTextfields:[sender tag] count:[sender tag]+1 arraySociety:nil yPostion:y];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    // Handle the selection
    NSLog(@"%@",[[arraySocieties valueForKey:@"society_name"] objectAtIndex:row]);
    
    NSLog(@"%ld",(long)row);
    
    
    if ([arraySocietyIds containsObject:[[arraySocieties valueForKey:@"id"] objectAtIndex:row]])
    {
        stringSocietyName=nil;
        strId=nil;
    }
    
    else
    {
        if ([[[arraySocieties valueForKey:@"society_name"] objectAtIndex:row]isEqualToString:@"None"])
            
        {
            stringSocietyName = @"None";
        }
        
        else
            
        {
            
            stringSocietyName = [[arraySocieties valueForKey:@"society_name"] objectAtIndex:row];
            
            strId=[[arraySocieties valueForKey:@"id"] objectAtIndex:row];
            
        }
    }
    
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return arraySocieties.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [[arraySocieties valueForKey:@"society_name"] objectAtIndex:row];
    
}

-(NSMutableArray *)sortArrayData:(NSArray *)array
{
    NSArray *aray;
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"society_name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    aray=[array sortedArrayUsingDescriptors:@[sort]];
    NSMutableArray *arrayM = [[NSMutableArray alloc]init];
    arrayM = [aray mutableCopy];
    
    NSDictionary *dictNone = @{@"society_name":@"None"};
    
    [arrayM insertObject:dictNone atIndex:0];
    
    return arrayM;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSString *title = [[arraySocieties valueForKey:@"society_name"] objectAtIndex:row];
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f]}];
    
    return attString;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    
    return pickerView.frame.size.width;
}

#pragma mark- webservice

-(void)getStudentSocieties
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"societyDetail" parameters:@{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]} success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [[NSUserDefaults standardUserDefaults] setValue:responseObject forKey:@"userSocieties"];
             [[NSUserDefaults standardUserDefaults]synchronize];
         }
         else
         {
             NSLog(@"wrong result");
             
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"%@",error.localizedDescription);
     }];
}


-(void)getAllStudentSocieties
{
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSLog(@"uni id id %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userUniversityId"] );
    
    [manager POST:@"getAllStudentSocieties" parameters:@{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userUniversityId"]} success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             arraySocieties = [[responseObject valueForKey:@"data"] mutableCopy];
             
             arraySocieties = [[self sortArrayData:(NSArray *)[arraySocieties copy]] mutableCopy];
             
             [_societyPicker reloadAllComponents];
         }
         else
         {
             NSLog(@"wrong result");
         }
         
         [self ShowDataLoader:NO];
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"%@",error.localizedDescription);
         
         [self ShowDataLoader:NO];
     }];
}

-(void)updateSocieties

{
    
    if (arraySocietyIds.count>0)
    {
        [arraySocietyIds removeObject:@"0"];
    }
    [self ShowDataLoader:YES];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"sid":[arraySocietyIds componentsJoinedByString:@","]};
    
    NSLog(@"dic param is %@", dicParam);
    
    [manager POST:@"addAllSociety" parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self ShowDataLoader:NO];
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [self getStudentSocieties];
             
             [USERDEFAULT setObject:arraySocietyIds forKey:USER_SELECTED_SOCITIES];
             
             if ([_isFromScreen isEqualToString:@"menu"]||[_isFromScreen isEqualToString:@"uniChange"])
             {
                 [[NSNotificationCenter defaultCenter]
                  postNotificationName:@"facebookCompnies"
                  object:self];
                 
                 [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callPortWebService"];
                 [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callMsgWebService"];
                 [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callallCompanyWebService"];
                 
                 PortfolioViewController *portView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
                 
                 UINavigationController * navPort = [[UINavigationController alloc]initWithRootViewController:portView];
                 
                 [self.revealViewController setFrontViewController:navPort animated:NO];
                 
                 [self performSelectorOnMainThread:@selector(showAlertMethod) withObject:nil waitUntilDone:NO];
             }
             else
             {
                 
                 [[[UIAlertView alloc]initWithTitle:@"Student Society Selections" message:@"\nYour student society selections have been successfully updated" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }
         else
         {
             NSLog(@"wrong result");
             
             [USERDEFAULT setObject:arraySocietyIds forKey:USER_SELECTED_SOCITIES];
         }
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         
         else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         
         else
         {
             [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
         
     }];
}

-(void)showAlertMethod
{
    [[[UIAlertView alloc]initWithTitle:@"Student Details Updated" message:@"\nYour student details have been successfully updated." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
}

#pragma mark- UIActivityIndicatorView

-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        
        [self.view.window setUserInteractionEnabled:NO];
        spinner.center = self.view.center;
        spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [spinner startAnimating];
        [self.view addSubview:spinner];
    }
    else
    {
        [spinner stopAnimating];
        [self.view.window setUserInteractionEnabled:YES];
        [spinner removeFromSuperview];
        spinner = nil;
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btnSubmitAction:(id)sender
{
    stringPortCompanyScreen = @"";
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callPortWebService"];
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callallCompanyWebService"];
    
    if ([_isFromScreen isEqualToString:@"menu"]||[_isFromScreen isEqualToString:@"uniChange"])
    {
        [self updateSocieties];
    }
    else
    {
        TermsConditionViewController *termsConditionView = [self.storyboard instantiateViewControllerWithIdentifier:@"termsConditionView"];
        termsConditionView.arrSocietiesFromScPg = arraySocietyIds;
        termsConditionView.dicUserInfoFromScPg = _dictUserInfoFromUsrPg;
        [self.navigationController pushViewController:termsConditionView animated:YES];
    }
}

@end



