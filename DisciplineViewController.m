
//
//  DisciplineViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 2/2/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "DisciplineViewController.h"
#import "DisciplineTableViewCell.h"
#import "detailDisciplineViewController.h"
#import "PortfolioViewController.h"
#import "LocationFilterSearchViewController.h"
#import "MessagesViewController.h"
#import "SalaryGuideViewController.h"
#import "SWRevealViewController.h"
#import "AFNetworking.h"
#import "AppDelegate.h"

@interface DisciplineViewController ()

{
    NSInteger expandedIndex;
    NSMutableArray *disciplineNameArray;
    NSMutableArray *arrayForBool;
    NSArray *disciplineSubNameArray;
    NSMutableDictionary *sectionContentDict;
    NSDictionary *dictDis;
    NSDictionary *dictDisInfo;
    NSDictionary *dictData;
    Database *dummy;
    UIActivityIndicatorView *spinner;
    UIRefreshControl *refreshControl;
    BOOL refreshManually;
    UILabel *labelSubTitle;
    
    BOOL isAPICalling;
    
    NSMutableArray *arrayAllDiscipline;
    
    NSMutableArray *arrayResponseData;
    
    NSMutableArray *arrayCompnieId;
}

@end

@implementation DisciplineViewController

@synthesize arraydiscipline;


- (void)viewDidLoad
{
    [super viewDidLoad];
    expandedIndex=-1;
    
    checkSubDisciFilterClick = YES;
    stringFromDesciplineView = @"desciplineScreen";
    
    arrayCompnieId = [NSMutableArray new];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"callDesciplineWebService"])
    {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"callSUBDES"];
    }
    
    if ([[[[NSUserDefaults standardUserDefaults] objectForKey:@"allDiscipline"] mutableCopy] count]>0)
    {
        arrayResponseData = [[[NSUserDefaults standardUserDefaults] objectForKey:@"allDiscipline"] mutableCopy];
    }
    
    
    arrayDisciplineCountryId = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountryDiscipline"]];
    
    arrayDisciplineStateId = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryStatesDiscipline"]];
    
    if (arrayDisciplineCountryId.count == 0)
    {
        arrayDisciplineCountryId = [[NSMutableArray alloc] init];
    }
    
    if (arrayDisciplineStateId.count == 0)
    {
        arrayDisciplineStateId = [[NSMutableArray alloc] init];
    }
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, _imageRedStar.frame.origin.y, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    
    self.imageRedStar.hidden = YES;
    
    refreshControl = [[UIRefreshControl alloc] init];
    
    refreshControl.backgroundColor = [UIColor clearColor];
    
    refreshControl.tintColor = [UIColor lightGrayColor];
    
    [refreshControl addTarget:self
                       action:@selector(callDiscplinesWebService)
             forControlEvents:UIControlEventValueChanged];
    
    [_disciplineTableView addSubview:refreshControl];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:18.0f],NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
    
    /*------Set the title name in navigation Bar-----*/
    
    UIView  *textView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 200 , 40)];
    UILabel *labelTitleName;
    labelTitleName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 0, CGRectGetWidth(textView.frame), 22)];
    labelTitleName.backgroundColor = [UIColor clearColor];
    labelTitleName.font = [UIFont fontWithName:@"OpenSans" size:18.0f];
    labelTitleName.textAlignment = NSTextAlignmentCenter;
    labelTitleName.textColor = [UIColor whiteColor];
    labelTitleName.text = @"Disciplines";
    
    labelSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 22, CGRectGetWidth(textView.frame), 18)];
    labelSubTitle.backgroundColor = [UIColor clearColor];
    labelSubTitle.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
    labelSubTitle.textAlignment = NSTextAlignmentCenter;
    labelSubTitle.textColor = [UIColor whiteColor];
    [textView addSubview:labelTitleName];
    [textView addSubview:labelSubTitle];
    self.navigationItem.titleView=textView;
    
    dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    self.viewBottomBar.backgroundColor = [UIColor whiteColor];
    
    UIView *viewAddBtnsLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 40)];
    
    UIButton * buttonMenu = [[UIButton alloc]initWithFrame:CGRectMake(-3, 4, 30, 30) ];
    
    [buttonMenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    
    buttonMenu.backgroundColor = [UIColor clearColor];
    
    [buttonMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    [viewAddBtnsLeft addSubview:buttonMenu];
    
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:viewAddBtnsLeft];
    
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"disciplineAlert"])
    {
        [self performSelectorOnMainThread:@selector(disciplineAlertMethod) withObject:nil waitUntilDone:NO];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"disciplineAlert"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    UIButton *buttonSearch =[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.origin.x-135,15, 30, 30)];
    
    UIImage *imageSearchButton = [UIImage imageNamed:@"top-icon"];
    buttonSearch.backgroundColor = [UIColor clearColor];/*--------------*/
    
    [buttonSearch setImage:imageSearchButton forState:UIControlStateNormal];
    [buttonSearch addTarget:self action:@selector(btnFilter)forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:buttonSearch];
}

-(void)disciplineAlertMethod
{
    [[[UIAlertView alloc]initWithTitle:@"University Disciplines" message:@"\nTo help you find companies that are interested in your field of study, you can search for companies using the Disciplines page. \n\nSelect from a wide range of Disciplines and Sub Disciplines to learn more about the companies that are interested in your field of study." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
}

-(void)SetHeader{
    NSArray *arrOriginalS = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalStateData"];
    NSArray *arrOriginalC = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalCountryData"];
    arrFinalStateData = [[NSUserDefaults standardUserDefaults] objectForKey:@"FinalStateDataDisp"];
    arrFinalCoutryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"FinalCountryDataDisp"];
    if((arrOriginalS.count == arrFinalStateData.count && arrOriginalC.count == arrFinalCoutryData.count) || (arrFinalStateData.count == 0 && arrFinalCoutryData.count == 0) || (arrOriginalS.count == 0 && arrOriginalC.count == 0)){
        labelSubTitle.text = @"All Locations";
        isAllLocationSelected = YES;
    }else{
        isAllLocationSelected = NO;
        NSString *strIds = @"";
        for (NSDictionary *dict in arrFinalStateData) {
            if([dict objectForKey:@"abr"]){
                NSString *strName = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"abr"]] removeNull];
                if([strIds isEqualToString:@""]){
                    strIds = [NSString stringWithFormat:@"%@",strName];
                }else{
                    strIds = [NSString stringWithFormat:@"%@,%@",strIds,strName];
                }
            }
        }
        for (NSDictionary *dict in arrFinalCoutryData) {
            if([dict objectForKey:@"abr"]){
                NSString *strName = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"abr"]] removeNull];
                if([strIds isEqualToString:@""]){
                    strIds = [NSString stringWithFormat:@"%@",strName];
                }else{
                    strIds = [NSString stringWithFormat:@"%@,%@",strIds,strName];
                }
            }
        }
        labelSubTitle.text = strIds;
    }
}

//Locally Filter Data

-(void)filterLocally{
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"allDiscipline"]){
        return;
    }
    NSArray *arrAllDisp = [[NSUserDefaults standardUserDefaults] objectForKey:@"allDiscipline"];
    arrayResponseData = [arrAllDisp mutableCopy];
    [_disciplineTableView  setHidden:NO];
    if(isAllLocationSelected){
        dictData = [NSDictionary dictionaryWithObject:arrayResponseData forKey:@"data"];
        [arrayForBool removeAllObjects];
        NSLog(@"");
        [self SetBoolToOpenSection];
    }else{
        NSArray *arrSIds = [arrFinalStateData valueForKey:@"id"];
        NSArray *arrCIds = [arrFinalCoutryData valueForKey:@"id"];
        NSMutableArray *arrNewAllDisp = [[NSMutableArray alloc]init];
        for(NSMutableDictionary *dictDisp in arrayResponseData){
            NSArray *arrSDisp = [dictDisp objectForKey:@"subdiscipline"];
            NSMutableArray *arrSubDispline = [[NSMutableArray alloc]initWithArray:arrSDisp];
            for (NSDictionary *dictSub in arrSDisp) {
                NSString *strName = [NSString stringWithFormat:@"%@",[dictSub objectForKey:@"name"]];
                
                
                NSArray *arrComp = [[dictSub objectForKey:strName] objectForKey:@"allcompanys"];
                NSMutableArray *arrNewComp = [[NSMutableArray alloc]init];
                for (NSDictionary *dictComp in arrComp) {
                    if([dictComp objectForKey:@"opration_country"] && [dictComp objectForKey:@"opration_state"]){
                        NSString *strCId = [[NSString stringWithFormat:@"%@",[dictComp objectForKey:@"opration_country"]] removeNull];
                        NSString *strSId = [[NSString stringWithFormat:@"%@",[dictComp objectForKey:@"opration_state"]] removeNull];
                        BOOL isDataAdd = NO;
                        if(![strSId isEqualToString:@""]){
                            NSArray *arrayRegState = [[strSId componentsSeparatedByString:@","] mutableCopy];
                            
                            for (NSString *strState in arrayRegState){
                                if([arrSIds containsObject:strState]){
                                    if(![arrNewComp containsObject:dictComp]){
                                        isDataAdd = YES;
                                    }
                                }
                            }
                        }
                        if(![strCId isEqualToString:@""]){
                            NSArray *arrayInternational = [[strCId componentsSeparatedByString:@","] mutableCopy];
                            for (NSString *strInter in arrayInternational){
                                if([arrCIds containsObject:strInter]){
                                    if(![arrNewComp containsObject:dictComp]){
                                        isDataAdd = YES;
                                    }
                                }
                            }
                        }
                        if(isDataAdd){
                            [arrNewComp addObject:dictComp];
                        }
                    }
                }
                
                NSMutableDictionary *dictAllComp = [[NSMutableDictionary alloc]init];
                [dictAllComp setObject:[arrNewComp copy] forKey:@"allcompanys"];
                [dictAllComp setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[arrNewComp count]] forKey:@"companys"];
                NSMutableDictionary *dictNewSub = [[NSMutableDictionary alloc]initWithDictionary:dictSub];
                [dictNewSub setObject:dictAllComp forKey:strName];
                NSUInteger index = [arrSubDispline indexOfObject:dictSub];
                [arrSubDispline removeObjectAtIndex:index];
                [arrSubDispline insertObject:dictNewSub atIndex:index];
            }
            
            NSMutableDictionary *dictNewSup = [[NSMutableDictionary alloc]initWithDictionary:dictDisp];
            [dictNewSup setObject:arrSubDispline forKey:@"subdiscipline"];
            [arrNewAllDisp addObject:dictNewSup];
        }
        dictData = [NSDictionary dictionaryWithObject:arrNewAllDisp forKey:@"data"];
        [arrayForBool removeAllObjects];
        
        //NSLog(@"theeeee data:%@",arrNewAllDisp);
        
        [self SetBoolToOpenSection];
    }
    
}

-(void)setTextToNavTitles
{
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"allLocationDes"]isEqualToString:@"allLocation"])
    {
        labelSubTitle.text = @"All Locations";
    }
    else
    {
        if (arrayDisciplineCountryId.count>0&&arrayDisciplineStateId.count>0)
        {
            labelSubTitle.text = [NSString stringWithFormat:@"%@, %@",[[arrayDisciplineStateId valueForKey:@"abr"] componentsJoinedByString:@", "],[[arrayDisciplineCountryId valueForKey:@"abr"] componentsJoinedByString:@", "]];
        }
        
        else if (arrayDisciplineStateId.count==0&&[arrayDisciplineCountryId count]>0)
        {
            labelSubTitle.text = [[arrayDisciplineCountryId valueForKey:@"abr"] componentsJoinedByString:@", "];
        }
        else if (arrayDisciplineStateId.count==0&&[arrayDisciplineCountryId count]==0)
        {
            labelSubTitle.text = @"All Locations";
        }
        else if (arrayDisciplineStateId.count>0)
        {
            labelSubTitle.text = [[arrayDisciplineStateId valueForKey:@"abr"] componentsJoinedByString:@", "];
        }
    }
}

#pragma mark- ButtonActions

-(void)btnFilter
{
    fromLoad = NO;
    
    LocationFilterViewController *detailsView = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationFilterView"];
    [self.navigationController pushViewController:detailsView animated:NO];
}


-(void)viewWillAppear:(BOOL)animated
{
    
    stringNotifiction = @"";
    
    stringForNotifyEditCompany = @"";
    stringCheckDisciplines = @"fromDisciplines";
    
    [_disciplineTableView  setHidden:YES];
    expandedIndex=-1;
    [_disciplineTableView reloadData];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"])
    {
        self.imageRedStar.hidden = YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    
    [super viewWillAppear:animated];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden =  NO;
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    /*------Set the fonts,Size in navigation Bar title-------*/
    
    _buttonDiscipline.backgroundColor = [UIColor clearColor];
    [_buttonDiscipline setImage:[UIImage imageNamed:@"sisciplines"] forState:UIControlStateNormal];
    _labelDiscipline.textColor=[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"])
    {
        self.imageRedStar.hidden = YES;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(redStarShow)
                                                 name:@"redStarShow"
                                               object:nil];
    [self SetHeader];
    if(![Utilities CheckInternetConnection])
    {
        NSLog(@"notConnected");
        
    }
    else
    {
        [self getDisciplineAndSubDiscp];
    }
    [self filterLocally];
}

-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    fromLoad = NO;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];
}


#pragma mark- MethodCallDiscplinesWebService

-(void)callDiscplinesWebService
{
    refreshManually = YES;
    isAPICalling = YES;
    if(![Utilities CheckInternetConnection])
    {
        NSLog(@"notConnected");
        
        [self filterLocally];
        isAPICalling = NO;
        if ([refreshControl isRefreshing])
        {
            [refreshControl endRefreshing];
        }
    }
    else
    {
        [self getDisciplineAndSubDiscp];
        
    }
}

#pragma mark- UIActivityIndicatorView

-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        spinner.center = self.view.center;
        spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [spinner startAnimating];
        [self.view addSubview:spinner];
        [self.view.window setUserInteractionEnabled:NO];
    }
    else
    {
        [spinner stopAnimating];
        [self.view.window setUserInteractionEnabled:YES];
        
        [spinner removeFromSuperview];
        spinner = nil;
    }
}


-(NSMutableDictionary *)replaceDisciplineArray :(NSMutableDictionary *)dict :(NSString *)cmpny_count
{
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    
    [newDict addEntriesFromDictionary:dict];
    [newDict setObject: cmpny_count forKey: @"companys"];
    
    return  newDict;
}

-(void)getDisciplineAndSubDiscp
{
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"allDiscipline"]){
        [self ShowDataLoader:YES];
    }
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:@"getAlldisciplines" parameters:@{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"],@"cid":@""} success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [_disciplineTableView  setHidden:NO];
         isAPICalling = NO;
         isInternetOff = NO;
         
         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         NSMutableDictionary *dictDiscA = [[NSMutableDictionary alloc]init];
         
         NSMutableArray *arrayCompyFinal= [[NSMutableArray alloc]init];
         
         NSMutableArray *arrayComp = [[NSMutableArray alloc]init];
         
         int indexDisci = 0;
         
         BOOL boolFirst = false;
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"callDesciplineWebService"];
             
             NSDictionary *dictDatas = [[NSDictionary alloc]initWithDictionary:responseObject];
             
             arrayAllDiscipline = [NSMutableArray new];
             
             arrayAllDiscipline = [[responseObject valueForKey:@"data"] mutableCopy];
             
             [arrayResponseData removeAllObjects];
             
             arrayResponseData = [[responseObject valueForKey:@"data"] mutableCopy];
             
             for (int c =0; c < [[dictDatas valueForKey:@"data"] count]; c++)
             {
                 
                 for (int k = 0; k<[[[dictDatas valueForKeyPath:@"data.subdiscipline"]objectAtIndex:c] count]; k++)
                 {
                     if (indexDisci <= [[dictDatas valueForKeyPath:@"data.subdiscipline"] count]-1)
                     {
                         if (boolFirst == YES)
                         {
                             if (k==0)
                             {
                                 indexDisci ++;
                                 
                                 arrayComp = [[NSMutableArray alloc]init];
                             }
                         }
                         else
                         {
                             boolFirst = YES;
                         }
                     }
                     
                     NSString *stringName = [[[[dictDatas valueForKeyPath:@"data.subdiscipline"] valueForKey:@"name"]objectAtIndex:c]objectAtIndex:k];
                     
                     NSMutableArray *arrayDes = [[NSMutableArray alloc]init];
                     
                     NSMutableDictionary *dictE = [[NSMutableDictionary alloc]init];
                     
                     if ([[[[[[dictDatas valueForKeyPath:@"data.subdiscipline"] valueForKey:stringName]objectAtIndex:c]objectAtIndex:k]valueForKey:@"allcompanys"] count] == 0)
                     {
                         [arrayDes addObject:@"000"];
                         
                         [dictE setObject:arrayDes forKey:[NSString stringWithFormat:@"%d", k]];
                         
                         [arrayComp addObject:dictE];
                     }
                     else
                     {
                         
                         for (int j=0; j<[[[[[[dictDatas valueForKeyPath:@"data.subdiscipline"] valueForKey:stringName]objectAtIndex:c]objectAtIndex:k]valueForKey:@"allcompanys"] count]; j++)
                         {
                             NSString *stringCompanyName = [[[[[[[dictDatas valueForKeyPath:@"data.subdiscipline"] valueForKey:stringName]objectAtIndex:c]objectAtIndex:k]valueForKey:@"allcompanys"]valueForKey:@"company_id"]objectAtIndex:j];
                             
                             [arrayDes addObject:stringCompanyName];
                             
                         }
                         
                         [dictE setObject:arrayDes forKey:[NSString stringWithFormat:@"%d", k]];
                         
                         [arrayComp addObject:dictE];
                         
                     }
                     
                     [dictDiscA setObject:arrayComp forKey:[NSString stringWithFormat:@"%d", indexDisci]];
                     
                 }
             }
             
             [arrayCompyFinal addObject:dictDiscA];
             
             [[NSUserDefaults standardUserDefaults]setObject:arrayCompyFinal forKey:@"companyDisciplinearray"];
             
             [[NSUserDefaults standardUserDefaults]setObject:arrayAllDiscipline forKey:@"allDiscipline"];
             
             [[NSUserDefaults standardUserDefaults]synchronize];
             
             arrayResponseData = [[[NSUserDefaults standardUserDefaults] objectForKey:@"allDiscipline"] mutableCopy];
             
             
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 
                 if ([dummy deleteTable:@"delete from DISCIPLINES"])
                 {
                     if ([dummy deleteTable:@"delete from SubDisciplines"])
                     {
                         for (int i=0; i<[[dictDatas valueForKey:@"data"] count]; i++)
                         {
                             NSString *query;
                             query = [NSString stringWithFormat: @"INSERT INTO DISCIPLINES (id,name) VALUES (\"%@\",\"%@\")",[[dictDatas valueForKeyPath:@"data.id"] objectAtIndex:i],[[dictDatas valueForKeyPath:@"data.name"] objectAtIndex:i]];
                             
                             const char *query_stmt = [query UTF8String];
                             [dummy  runQueries:query_stmt strForscreen:@"Disciplines" ];
                             
                             for (int k=0; k<[[[dictDatas valueForKeyPath:@"data.subdiscipline"]objectAtIndex:i] count]; k++)
                             {
                                 NSString *stringName = [[[[dictDatas valueForKeyPath:@"data.subdiscipline"] valueForKey:@"name"]objectAtIndex:i]objectAtIndex:k];
                                 
                                 NSString *stringCom = [NSString stringWithFormat:@"%@",[[[[[dictDatas valueForKeyPath:@"data.subdiscipline"] valueForKey:stringName]valueForKey:@"companys"]objectAtIndex:i]objectAtIndex:k]];
                                 
                                 NSString *subQuery;
                                 subQuery = [NSString stringWithFormat: @"INSERT INTO SubDisciplines (date,id,name,discipline_id,companys) VALUES (\"%@\",\"%@\", \"%@\",\"%@\",\"%@\")",[[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:@"date"]objectAtIndex:i]objectAtIndex:k],[[[[dictDatas valueForKeyPath:@"data.subdiscipline"] valueForKey:@"id"]objectAtIndex:i]objectAtIndex:k],[[[[dictDatas valueForKeyPath:@"data.subdiscipline"] valueForKey:@"name"]objectAtIndex:i]objectAtIndex:k],[[[[dictDatas valueForKeyPath:@"data.subdiscipline"] valueForKey:@"discipline_id"]objectAtIndex:i]objectAtIndex:k],stringCom];
                                 
                                 const char *query_stmtSubDiscp = [subQuery UTF8String];
                                 
                                 [dummy runQuerySubDisciplines:query_stmtSubDiscp strForscreen:@"DisciplinesVw"];
                                 
                             }
                         }
                     }
                 }
                 
                 if ([spinner isAnimating])
                     
                 {
                     [self ShowDataLoader:NO];
                 }
             });
             
             [arrayForBool removeAllObjects];
             if (![dictData isEqualToDictionary:dictDatas]) {
                 [self filterLocally];
             }else{
                 dictData = [[NSDictionary alloc]initWithDictionary:dictDatas];
             }
             
             
             [self ShowDataLoader:NO];
         }
         
         else
         {
             if ([spinner isAnimating])
             {
                 [self ShowDataLoader:NO];
             }
             
             [[[UIAlertView alloc]initWithTitle:@"Error" message:@"\nNo result to display." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
         }
     }
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         NSLog(@"errorrrrrr is %@", error);
         
         
         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         else if ([spinner isAnimating])
         {
             [self ShowDataLoader:NO];
         }
         
         if (refreshManually)
         {
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 //[[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }
         
         [self ShowDataLoader:NO];
     }];
}

-(void)SetBoolToOpenSection
{
    if (arrayForBool.count == 0)
    {
        arrayForBool = [[NSMutableArray alloc]init];
        for (int i=0; i<[[dictData valueForKeyPath:@"data.name"] count]+1; i++)
        {
            [arrayForBool addObject:[NSNumber numberWithBool:NO]];
        }
    }
    [_disciplineTableView  reloadData];
}


#pragma mark- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([[dictData valueForKeyPath:@"data.name"] count]!=0)
    {
        return [[dictData valueForKeyPath:@"data.name"] count]+1;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //if ([[arrayForBool objectAtIndex:section] boolValue])
    if (expandedIndex==section)
    {
        return [[[dictData valueForKeyPath:@"data.subdiscipline"]objectAtIndex:section] count];
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    /*remove cell line in table view*/
    tableView.backgroundColor = [UIColor whiteColor];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    
    //BOOL manyCells  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
    BOOL manyCells  = (indexPath.section==expandedIndex);
    if (!manyCells)
    {
        cell.textLabel.text = @"click to enlarge";
    }
    else
    {
        NSArray *content = [[[dictData valueForKeyPath:@"data.subdiscipline"]objectAtIndex:indexPath.section] valueForKey:@"name"];
        
        cell.textLabel.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
        
        NSString *stringName = [[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:@"name"]objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
        
        NSString *stringCom = [NSString stringWithFormat:@"%@", [[[[[dictData valueForKeyPath:@"data.subdiscipline"] valueForKey:stringName]valueForKey:@"companys"]objectAtIndex:indexPath.section]objectAtIndex:indexPath.row]];
        
        if (stringCom == nil||[stringCom isKindOfClass:[NSNull class]]||[stringCom isEqualToString:@"<null>"]||[stringCom isEqualToString:@""])
        {
            cell.textLabel.text =[NSString stringWithFormat:@"              %@ (%@)", [content objectAtIndex:indexPath.row],[[[[dictData valueForKeyPath:@"data.subdiscipline"]objectAtIndex:indexPath.section] valueForKey:@"companys"] objectAtIndex:indexPath.row]];
        }
        else
        {
            cell.textLabel.text =[NSString stringWithFormat:@"              %@ (%@)", [content objectAtIndex:indexPath.row],stringCom];
        }
        
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.10];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

#pragma mark- UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    stringCheckSubDesc = @"Desc";
    
    NSArray *content = [[[dictData valueForKeyPath:@"data.subdiscipline"]objectAtIndex:indexPath.section] valueForKey:@"id"];
    
    
    NSUInteger cellInt = [content indexOfObject:[content objectAtIndex:indexPath.row]];
    
    UITableViewCell*cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel .textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    detailDisciplineViewController *detailsView = [self.storyboard instantiateViewControllerWithIdentifier:@"detailsDisciplineVIew"];
    NSLog(@"the aadata:%@",[[dictData valueForKeyPath:@"data"]objectAtIndex:indexPath.section]);
    detailsView.arrDiscipline = [[NSArray alloc]initWithObjects:[[dictData valueForKeyPath:@"data"]objectAtIndex:indexPath.section],nil];
    
    detailsView.SelectedSection = indexPath.section;
    detailsView.SelectedRow = indexPath.row;
    
    detailsView.strIndex = [NSString stringWithFormat:@"%lu",(unsigned long)cellInt];
    NSArray *arrAllDisp = [[NSUserDefaults standardUserDefaults] objectForKey:@"allDiscipline"];
    NSDictionary *dictSubDisp = [[[arrAllDisp objectAtIndex:indexPath.section]  objectForKey:@"subdiscipline"] objectAtIndex:indexPath.row];
    NSString *strSubSidp = [[NSString stringWithFormat:@"%@",[dictSubDisp objectForKey:@"name"]] removeNull];
    NSArray *arrAllComp = [[dictSubDisp objectForKey:strSubSidp] objectForKey:@"allcompanys"];
    detailsView.arrayComp = arrAllComp;
    detailsView.indexPath = (int) indexPath.section;
    
    detailsView.row = (int) indexPath.row;
    
    [self.navigationController pushViewController:detailsView animated:NO];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, _disciplineTableView.frame.size.width, 60)];
    headerView.tag = section;
    headerView.backgroundColor= [UIColor whiteColor];
    UILabel *headerString = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, self.view.frame.size.width-20-50, headerView.frame.size.height)];
    
    if (section!=[[dictData valueForKeyPath:@"data.name"] count])
    {
        //BOOL manyCells = [[arrayForBool objectAtIndex:section] boolValue];
        BOOL manyCells = (section==expandedIndex);
        
        headerString.text = [NSString stringWithFormat:@"%@", [[dictData valueForKeyPath:@"data.name"] objectAtIndex:section]];
        
        headerString.font = [UIFont fontWithName:@"OpenSans" size:15.0f];
        headerString.textAlignment = NSTextAlignmentLeft;
        headerString.textColor = [UIColor colorWithRed:42.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        headerString.backgroundColor = [UIColor clearColor];
        
        [headerView addSubview:headerString];
        
        UIImageView *upDownArrow = [[UIImageView alloc] initWithImage:manyCells ? [UIImage imageNamed:@"-ve"] : [UIImage imageNamed:@"add"]];
        upDownArrow.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        upDownArrow.frame = CGRectMake(self.view.frame.origin.x+10, self.view.frame.origin.y+20,20,20);
        [headerView addSubview:upDownArrow];
        UITapGestureRecognizer  *headerTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
        [headerView addGestureRecognizer:headerTapped];
    }
    
    UIImageView *titleImage = [[UIImageView alloc] initWithFrame: CGRectMake(0.0,  0.0,  headerView.frame.size.width , 0.5f)];
    titleImage.backgroundColor = [UIColor lightGrayColor];
    
    [headerView addSubview:titleImage];
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footer  = [[UIView alloc] initWithFrame:CGRectZero];
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section!=[[dictData valueForKeyPath:@"data.name"] count])
    {
        return 60;
    }
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if ([[arrayForBool objectAtIndex:indexPath.section] boolValue])
    if (indexPath.section==expandedIndex)
    {
        return 40;
    }
    return 0;
}

// Customize the appearance of table view cells.

#pragma mark- discpline_company_search

-(void)Calldiscpline_company_search
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *parameters = @{@"did":@"1",@"sdid":@"1"};
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@discpline_company_search",baseUrl];
    
    [manager POST:urlStr parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             dictDisInfo=[responseObject objectForKey:@"data"];
         }
         else
         {NSLog(@"wrong result");
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error.localizedDescription);
     }];
}

#pragma mark- UITapGestureRecognizer

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    
    if (indexPath.row == 0)
    {
       //BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        BOOL collapsed  = (indexPath.section==expandedIndex);
        collapsed       = !collapsed;
        [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:collapsed]];
        if (!collapsed) {
            expandedIndex=-1;
        }
        else
        {
            expandedIndex=indexPath.section;
        }
        
        //reload specific section animated
        NSRange range   = NSMakeRange(indexPath.section, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.disciplineTableView reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark- tabBarButtonAction
- (IBAction)buttonActionPortfolio:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[PortfolioViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    PortfolioViewController *portfolioView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
    [self.navigationController pushViewController:portfolioView animated:NO];
}

- (IBAction)buttonActionDiscip:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[DisciplineViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    DisciplineViewController *disciplineVw = [self.storyboard instantiateViewControllerWithIdentifier:@"disciplineView"];
    [self.navigationController pushViewController:disciplineVw animated:NO];
}

- (IBAction)buttonSearch:(id)sender;
{
    fromLoadDisc = YES;
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[LocationFilterSearchViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    LocationFilterSearchViewController  *searchVw = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVw"];
    [self.navigationController pushViewController:searchVw animated:NO];
}


- (IBAction)buttonMessages:(id)sender;
{
    MessagesViewController  *messageVw = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    [self.navigationController pushViewController:messageVw animated:NO];
}


- (IBAction)buttonActionSalary:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[SalaryGuideViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    SalaryGuideViewController *salaryVw = [self.storyboard instantiateViewControllerWithIdentifier:@"salaryVw"];
    [self.navigationController pushViewController:salaryVw animated:NO];
}


@end

