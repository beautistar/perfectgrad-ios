//
//  MessagesViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 2/6/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "messageDetailVc.h"
#import "MessagesTableViewCell.h"
#import "CompaniesDetailViewController.h"

@interface MessagesViewController : UIViewController

@property BOOL isFromStudent;
@property (nonatomic) NSMutableDictionary *dictStudentSoc;

@property (nonatomic) NSString *strFromScreen;
@property (nonatomic) NSArray *arrComapanyDetail;
@property (nonatomic) NSArray *arrCompanyEventsPass;
@property (nonatomic) NSArray *arrSocietyEventsPass;
@property (nonatomic) NSString *strEventsOfScreen;
@property (nonatomic) NSString *strNavTitle;
@property(nonatomic)NSString *stringFromPrevious;
@property (strong, nonatomic) IBOutlet UITableView *bubbleTable;
@property (strong, nonatomic) IBOutlet UIView *viewBottomBar;
@property (strong, nonatomic) IBOutlet UIButton *buttonPortfolio;
@property (strong, nonatomic) IBOutlet UIButton *buttonDiscipline;
@property (strong, nonatomic) IBOutlet UIButton *buttonSearch;
@property (strong, nonatomic) IBOutlet UIButton *buttonMessages;
@property (strong, nonatomic) IBOutlet UIButton *buttonSalaryguide;
@property (strong, nonatomic) IBOutlet UILabel *labelPortfolio;
@property (strong, nonatomic) IBOutlet UILabel *labelDiscipline;
@property (strong, nonatomic) IBOutlet UILabel *labelSearchVw;
@property (strong, nonatomic) IBOutlet UILabel *labelSalaryGuide;
@property (strong, nonatomic) IBOutlet UILabel *labelmessag;

@property (strong, nonatomic) IBOutlet UIImageView *imageVwStar;

@property (strong, nonatomic) IBOutlet UIButton *buttonCmpnyAndClubs;
@property (strong, nonatomic) IBOutlet UIButton *buttonEvents;

@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;

- (IBAction)buttonEventsAction:(id)sender;
- (IBAction)buttonCmpnyAndClubsAction:(id)sender;
- (IBAction)buttonActionPortfolio:(id)sender;
- (IBAction)buttonActionDiscip:(id)sender;
- (IBAction)buttonSearch:(id)sender;
- (IBAction)buttonMessages:(id)sender;
- (IBAction)buttonActionSalary:(id)sender;


@end




