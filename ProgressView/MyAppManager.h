//
//  MyAppManager.h
//  SmartWinners
//
//  Created by Sevenstar Infotech on 18/06/14.
//  Copyright (c) 2014 Vijay Hirpara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "KVNProgress.h"

#define AppHeaderColor [UIColor colorWithRed:65.0/255.0 green:99.0/255.0 blue:179.0/255.0 alpha:1.0]

@interface MyAppManager : NSObject
+(id)sharedManager;
-(void)showLoaderInMainThread;
-(void)hideLoaderInMainThread;
-(void)showLoader;
-(void)hideLoader;

@property (nonatomic) KVNProgressConfiguration *basicConfiguration;
-(void)setConfiguration;
@end
