//
//  MyAppManager.m
//  SmartWinners
//
//  Created by Sevenstar Infotech on 18/06/14.
//  Copyright (c) 2014 Vijay Hirpara. All rights reserved.
//

#import "MyAppManager.h"


@implementation MyAppManager

+(id)sharedManager
{
    static MyAppManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(id)init
{
    if (self = [super init])
    {
    }
    return self;
}

#pragma mark - LOADER METHODS

-(void)setConfiguration{
    self.basicConfiguration = [self customKVNProgressUIConfiguration];
    [KVNProgress setConfiguration:self.basicConfiguration];
}

- (KVNProgressConfiguration *)customKVNProgressUIConfiguration
{
    KVNProgressConfiguration *configuration = [[KVNProgressConfiguration alloc] init];
    
    // See the documentation of KVNProgressConfiguration
    configuration.circleStrokeForegroundColor = AppHeaderColor;
    configuration.circleStrokeBackgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    configuration.circleFillBackgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    configuration.backgroundFillColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    configuration.backgroundTintColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    configuration.circleSize = 60.0f;
    configuration.lineWidth = 1.0f;
    
    configuration.tapBlock = ^(KVNProgress *progressView) {
        [KVNProgress dismiss];
    };
    
    return configuration;
}

-(void)showLoaderInMainThread
{
    [self performSelectorOnMainThread:@selector(showLoader) withObject:nil waitUntilDone:NO];
}

-(void)hideLoaderInMainThread
{
    [self performSelectorOnMainThread:@selector(hideLoader) withObject:nil waitUntilDone:NO];
}

-(void)showLoader
{
    self.basicConfiguration.tapBlock = ^(KVNProgress *progressView) {
        _basicConfiguration.tapBlock = nil;
        [KVNProgress dismiss];
    };
    
    [KVNProgress show];
}

-(void)hideLoader
{
    self.basicConfiguration.tapBlock = nil;
    [KVNProgress dismiss];
}




@end
