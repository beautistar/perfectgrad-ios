//
//  messageDetailVc.h
//  PerfectGraduate
//
//  Created by NSPL on 2/21/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LIALinkedInApplication.h"
#import "LIALinkedInHttpClient.h"
#import "OAuthLoginView.h"

@interface messageDetailVc : UIViewController

@property (strong, nonatomic) IBOutlet UIView *viewBottomBar;
@property (strong, nonatomic) IBOutlet UIButton *buttonPortfolio;
@property (strong, nonatomic) IBOutlet UIButton *buttonDiscipline;
@property (strong, nonatomic) IBOutlet UIButton *buttonSearch;
@property (strong, nonatomic) IBOutlet UIButton *buttonMessages;
@property (strong, nonatomic) IBOutlet UIButton *buttonSalaryguide;
@property (strong, nonatomic) IBOutlet UILabel *labelPortfolio;
@property (strong, nonatomic) IBOutlet UILabel *labelDiscipline;
@property (strong, nonatomic) IBOutlet UILabel *labelSearchVw;
@property (strong, nonatomic) IBOutlet UILabel *labelSalaryGuide;
@property (strong, nonatomic) IBOutlet UILabel *labelmessag;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewComp;
@property (strong, nonatomic) IBOutlet UILabel *labelName;
@property (strong, nonatomic) IBOutlet UILabel *labelDate;
@property (strong, nonatomic) IBOutlet UILabel *labelEventName;
@property (strong, nonatomic) IBOutlet UIView *viewMsgDetails;

@property (strong, nonatomic) IBOutlet UIButton *buttonRequest;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewBackGd;
@property (strong,nonatomic) NSString *strDetailtype;
@property (strong, nonatomic) IBOutlet UILabel *labelEventDate;
@property (strong, nonatomic) IBOutlet UILabel *LabelEventTime;
@property (strong, nonatomic) IBOutlet UILabel * labelEventLocation;

@property (strong, nonatomic) IBOutlet UILabel *staticLabelEventName;
@property (strong, nonatomic) IBOutlet UILabel *staticLabelEventDate;
@property (strong, nonatomic) IBOutlet UILabel *staticLabelEventTime;
@property (strong, nonatomic) IBOutlet UILabel *staticLabelEventLocation;

@property (strong, nonatomic) IBOutlet UILabel *labelResponseConfirmation;
@property (strong, nonatomic) IBOutlet UIView *viewImageContainer;
@property (strong, nonatomic) IBOutlet UIButton *btnLikeMsg, *btnLinkWebsite;

@property (strong, nonatomic) IBOutlet UITextView *textViewDetail;
@property (nonatomic) NSMutableDictionary *dicMessageDetail;
@property(strong,nonatomic)IBOutlet UIButton *btnOpenLink;

@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;


- (IBAction)buttonActionPortfolio:(id)sender;
- (IBAction)buttonActionDiscip:(id)sender;
- (IBAction)buttonSearch:(id)sender;
- (IBAction)buttonMessages:(id)sender;
- (IBAction)buttonActionSalary:(id)sender;
- (IBAction)buttonRequestInvitationAction:(id)sender;
- (IBAction)btnLikeMsgAction:(id)sender;
-(IBAction)btnlinkSite:(id)sender;
-(IBAction)btnOpenMap:(id)sender;

@end
