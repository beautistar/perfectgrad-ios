//
//  InformationVC.h
//  PerfectGraduate
//
//  Created by netset on 3/25/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "AFNetworking.h"
#import "StudentDetailsViewController.h"
#import "PortfolioViewController.h"
#import "SWRevealViewController.h"
#import "MenuViewController.h"
#import "AppDelegate.h"

@interface InformationVC : UIViewController
@property (strong, nonatomic) SWRevealViewController *viewController;
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) IBOutlet UIButton *buttonTempSignin;
@property (strong, nonatomic) IBOutlet UIButton *buttonFbSignIn;
@property (strong, nonatomic) IBOutlet UILabel *labelYear;



- (IBAction)btnFacebookAction:(id)sender;
- (IBAction)btnTempSignin:(id)sender;
- (IBAction)buttonBackAction:(id)sender;

@end
