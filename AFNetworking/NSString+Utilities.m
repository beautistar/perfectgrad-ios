//
//  NSString+Utilities.m
//  Suvi
//
//  Created by Gagan Mishra on 2/20/13.
//
//

#import "NSString+Utilities.h"

@implementation NSString (Utilities)
-(NSAttributedString *)attributedStringWithFontName:(UIFont *)font color:(UIColor *)color
{
    //[attribStr addAttribute:NSFontAttributeName value:font range:NSMakeRange(0,[attribStr length])];
    
    NSMutableAttributedString *attribStr=[[NSMutableAttributedString alloc] initWithData:[self dataUsingEncoding:NSUTF8StringEncoding]options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
    [attribStr addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0,[attribStr length])];
    [attribStr addAttribute:NSFontAttributeName value:font range:NSMakeRange(0,[attribStr length])];
    return attribStr;
    // return [[NSAttributedString alloc]initWithData:[[NSString stringWithFormat:@"<font face=\"%@\" size=\"%ld\">%@</font>",strFontName,(long)fontSize,self] dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil  error:nil];
}

-(BOOL)isValidEmail
{
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:self options:0 range:NSMakeRange(0,[self length])];
    return (regExMatches == 0)?NO:YES;
}
-(NSString *)formattedTime
{
    float timediff=[[NSDate date] timeIntervalSince1970]-[self floatValue];
    
    if ((timediff<=0.0) || ([self floatValue]==0.0))
    {
        return @"0m";
    }
    else if(timediff<3600.0)
    {
        return [NSString stringWithFormat:@"%.fm",(timediff/60.0)];
    }
    else if(timediff<43200.0)
    {
        return [NSString stringWithFormat:@"%.fh",(timediff/3600.0)];
    }
    else if(timediff<302400.0)
    {
        return [NSString stringWithFormat:@"%.fd",(timediff/43200.0)];
    }
    else
    {
        return [NSString stringWithFormat:@"%.fw",(timediff/302400.0)];
    }
}
-(NSString *)removeNull
{
    if ([self isKindOfClass:[NSNull class]])
    {
        return @"";
    }

    if (![self isKindOfClass:[NSString class]])
    {
        return self;
    }
    
    if (!self || (self == nil))
    {
        return @"";
    }
    else if([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"<null>"]
            || [[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@"(null)"]
            ||(self == (id)[NSNull null])
            || [self caseInsensitiveCompare:@"(null)"] == NSOrderedSame
            || [self caseInsensitiveCompare:@"<null>"] == NSOrderedSame
            || [self caseInsensitiveCompare:@""] == NSOrderedSame
            || [self caseInsensitiveCompare:@"<nil>"] == NSOrderedSame
            || [self length]==0)
    {
        return @"";
    }
    else
    {
        return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
}

-(NSString *)convertToSmilies
{
    NSString *strSmilies=[[NSString alloc]initWithFormat:@"%@",self];
    strSmilies=[self stringByReplacingOccurrencesOfString:@" :P" withString:@" 😋"];
    strSmilies=[self stringByReplacingOccurrencesOfString:@" :(" withString:@" 😒"];
    strSmilies=[self stringByReplacingOccurrencesOfString:@" :)" withString:@" 😃"];
    
    
    return [self stringByReplacingOccurrencesOfString:@" :P" withString:@" 😋"];
}

-(NSString *)encryptIntoROT13
{
	const char *_string = [self cStringUsingEncoding:NSASCIIStringEncoding];
	int stringLength = (int)[self length];
	char newString[stringLength+1];
    
	int x;
	for( x=0; x<stringLength; x++ )
	{
		unsigned int aCharacter = _string[x];
        
		if( 0x40 < aCharacter && aCharacter < 0x5B ) // A - Z
			newString[x] = (((aCharacter - 0x41) + 0x0D) % 0x1A) + 0x41;
		else if( 0x60 < aCharacter && aCharacter < 0x7B ) // a-z
			newString[x] = (((aCharacter - 0x61) + 0x0D) % 0x1A) + 0x61;
		else  // Not an alpha character
			newString[x] = aCharacter;
	}
    
	newString[x] = '\0';
    
	NSString *rotString = [NSString stringWithCString:newString encoding:NSASCIIStringEncoding];
	return rotString;
}


-(NSMutableDictionary *)getDataFromFile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
	NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:self];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    NSMutableDictionary *dictFileData=[[NSMutableDictionary alloc]init];
    
    if (fileExists)
    {
        [dictFileData setObject:@"1" forKey:@"success"];
        NSError *error;
        NSData *fileData = [NSData dataWithContentsOfFile:filePath];
        NSMutableDictionary *dictJson = [NSJSONSerialization JSONObjectWithData:fileData options:NSJSONReadingMutableLeaves error:&error];
        [dictFileData addEntriesFromDictionary:dictJson];
    }
    else
    {
        [dictFileData setObject:@"0" forKey:@"success"];
    }
    
    return dictFileData;
}

-(NSString*)stringBetweenString:(NSString *)start andString:(NSString *)end
{
    NSRange startRange = [self rangeOfString:start];
    if (startRange.location != NSNotFound) {
        NSRange targetRange;
        targetRange.location = startRange.location + startRange.length;
        targetRange.length = [self length] - targetRange.location;
        NSRange endRange = [self rangeOfString:end options:0 range:targetRange];
        if (endRange.location != NSNotFound) {
            targetRange.length = endRange.location - targetRange.location;
            return [self substringWithRange:targetRange];
        }
    }
    return nil;
}
+ (NSString *) getCurrentTime {
    
	NSDate *nowUTC = [NSDate date];
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
	[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
	return [dateFormatter stringFromDate:nowUTC];
	
}

- (NSString *) substituteEmoticons {
	
	//See http://www.easyapns.com/iphone-emoji-alerts for a list of emoticons available
	
	NSString *res = [self stringByReplacingOccurrencesOfString:@":)" withString:@"\ue415"];
	res = [res stringByReplacingOccurrencesOfString:@":(" withString:@"\ue403"];
	res = [res stringByReplacingOccurrencesOfString:@";-)" withString:@"\ue405"];
	res = [res stringByReplacingOccurrencesOfString:@":-x" withString:@"\ue418"];
	
	return res;
	
}
- (NSString *)encodedURLString {
	NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)self,
                                                                                             NULL,                   // characters to leave unescaped (NULL = all escaped sequences are replaced)
                                                                                             CFSTR("?=&+"),          // legal URL characters to be escaped (NULL = all legal characters are replaced)
                                                                                             kCFStringEncodingUTF8)); // encoding
	return result ;
}

- (NSString *)encodedURLParameterString {
    NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)self,NULL,CFSTR(":/=,!$&'()*+;[]@#?"),kCFStringEncodingUTF8));
	return result;
}

- (NSString *)decodedURLString
{
	NSString *result = (NSString*)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,(CFStringRef)self,CFSTR(""),kCFStringEncodingUTF8));
	return result;
}

-(NSString *)removeQuotes
{
	NSUInteger length = [self length];
	NSString *ret = self;
	if ([self characterAtIndex:0] == '"') {
		ret = [ret substringFromIndex:1];
	}
	if ([self characterAtIndex:length - 1] == '"') {
		ret = [ret substringToIndex:length - 2];
	}
	
	return ret;
}
#pragma mark - Not Null String
-(NSString *)RemoveNull
{
    if(self == nil)
    {
        return @"";
    }
    else if(self == (id)[NSNull null] || [self caseInsensitiveCompare:@"(null)"] == NSOrderedSame || [self caseInsensitiveCompare:@"<null>"] == NSOrderedSame || [self caseInsensitiveCompare:@""] == NSOrderedSame || [self length]==0)
    {
        return @"";
    }
    else
    {
        return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
}


#pragma mark - Validate Email
-(BOOL)StringIsValidEmail
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

#pragma mark - Validate for Integer Number string
-(BOOL)StringIsIntigerNumber
{
    NSRegularExpression *regex = [[NSRegularExpression alloc]
                                  initWithPattern:@"[0-9]" options:0 error:NULL];
    NSUInteger matches = [regex numberOfMatchesInString:self options:0
                                                  range:NSMakeRange(0, [self length])];
    if (matches == [self length])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


#pragma mark - Validate for Float Number string
-(BOOL)StringIsFloatNumber
{
    if([self rangeOfString:@"."].location == NSNotFound)
    {
        return NO;
    }
    else
    {
        
        NSString *regx = @"(-){0,1}(([0-9]+)(.)){0,1}([0-9]+)";
        NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regx];
        return [test evaluateWithObject:self];
    }
}


#pragma mark - Complete Number string
-(BOOL)StringIsComplteNumber
{
    NSString *regx = @"(-){0,1}(([0-9]+)(.)){0,1}([0-9]+)";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regx];
    return [test evaluateWithObject:self];
}


#pragma mark - alpha numeric string
-(BOOL)StringIsAlphaNumeric
{
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
    
    if ([self rangeOfCharacterFromSet:set].location != NSNotFound)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}


#pragma mark - illegal char in string
-(BOOL)StringWithIlligalChar
{
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
    if ([self rangeOfCharacterFromSet:set].location != NSNotFound)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark -Strong Password
-(BOOL)StringWithStrongPassword:(int)minimumLength
{
    if([self length] <minimumLength)
    {
        return NO;
    }
    BOOL isCaps = FALSE;
    BOOL isNum = FALSE;
    BOOL isSymbol = FALSE;
    
    NSRegularExpression *regexCaps = [[NSRegularExpression alloc]
                                      initWithPattern:@"[A-Z]" options:0 error:NULL];
    NSUInteger intMatchesCaps = [regexCaps numberOfMatchesInString:self options:0
                                                             range:NSMakeRange(0, [self length])];
    if (intMatchesCaps > 0)
    {
        isCaps = TRUE;
    }
    
    NSRegularExpression *regexNum = [[NSRegularExpression alloc]
                                     initWithPattern:@"[0-9]" options:0 error:NULL];
    NSUInteger intMatchesNum = [regexNum numberOfMatchesInString:self options:0
                                                           range:NSMakeRange(0, [self length])];
    if (intMatchesNum > 0)
    {
        isNum = TRUE;
    }
    
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789"] invertedSet];
    if ([self rangeOfCharacterFromSet:set].location != NSNotFound)
    {
        isSymbol = TRUE;
    }
    
    if(isCaps == TRUE && isNum == TRUE && isSymbol == TRUE)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}


-(NSString *)FormateDate_withCurrentFormate:(NSString *)currentFormate newFormate:(NSString *)dateFormatter
{
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setTimeZone:[NSTimeZone systemTimeZone]];
    [df setDateFormat:currentFormate];
    NSDate *date = [df dateFromString:self];
    [df setTimeZone:[NSTimeZone systemTimeZone]];
    [df setDateFormat:dateFormatter];
    return [df stringFromDate:date];
}

-(NSString *)removeHTMLTags
{
    NSScanner *theScanner;
    NSString *text = nil;
    NSString *strFormatted=[NSString stringWithFormat:@"%@",self];
    theScanner = [NSScanner scannerWithString:self];
    while ([theScanner isAtEnd] == NO) {
        // find start of tag
        [theScanner scanUpToString: @"<" intoString: NULL];
        // find end of tag
        [theScanner scanUpToString: @">" intoString: &text];
        // replace the found tag with a space
        //(you can filter multi-spaces out later if you wish)
        strFormatted = [strFormatted stringByReplacingOccurrencesOfString:
                [NSString stringWithFormat: @"%@>", text] withString: @" "];
    } // while //
    return strFormatted;
}

-(BOOL)hasImage
{
    NSString *strOriginal=[self removeNull];
    if ([strOriginal length]==0)
    {
        return NO;
    }
    else
    {
        NSArray *arrComps=[strOriginal componentsSeparatedByString:@"src="];
        
        if ([arrComps count]>1)
        {
            NSArray *arrComp2=[[arrComps objectAtIndex:0] componentsSeparatedByString:@" "];
            
            if ([arrComp2 count]>0)
            {
                return YES;
            }
            else
            {
                return NO;
            }
        }
        else
        {
            return NO;
        }
    }
}


@end
