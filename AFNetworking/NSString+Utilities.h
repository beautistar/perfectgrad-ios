//
//  NSString+Utilities.h
//  Suvi
//
//  Created by Gagan Mishra on 2/20/13.
//
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface NSString (Utilities)
-(NSAttributedString *)attributedStringWithFontName:(UIFont *)font color:(UIColor *)color;

-(NSString *)removeNull;
-(BOOL)isValidEmail;
-(NSString *)convertToSmilies;
-(NSString *)formattedTime;
-(NSMutableDictionary *)getDataFromFile;
-(NSString *)encryptIntoROT13;
+ (NSString *) getCurrentTime;
- (NSString *) substituteEmoticons;
- (NSString *)encodedURLString;
- (NSString *)encodedURLParameterString;
- (NSString *)decodedURLString;
- (NSString *)removeQuotes;
-(NSString *)RemoveNull;
-(BOOL)StringIsValidEmail;
-(BOOL)StringIsIntigerNumber;
-(BOOL)StringIsFloatNumber;
-(BOOL)StringIsComplteNumber;
-(BOOL)StringIsAlphaNumeric;
-(BOOL)StringWithIlligalChar;
-(BOOL)StringWithStrongPassword:(int)minimumLength;
-(NSString *)FormateDate_withCurrentFormate:(NSString *)currentFormate newFormate:(NSString *)dateFormatter;
-(NSString *)removeHTMLTags;
-(BOOL)hasImage;

@end
