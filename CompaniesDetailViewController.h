//
//  CompaniesDetailViewController.h
//  PerfectGraduate
//
//  Created by NSPL on 2/19/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "AsyncImageView.h"
#import "CompanyMoreInfoVC.h"
#import "UIKit+AFNetworking.h"
#import "CompanyProgramsDetailVC.h"
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>


@import GoogleMaps;

@interface CompaniesDetailViewController : UIViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIRefreshControl* refreshControl;

}

@property (strong,nonatomic) NSString *strNavTitle;
@property (strong,nonatomic) NSString *strVwChane;
@property (nonatomic) NSMutableDictionary *dicCompanyDetail;

@property (strong, nonatomic) IBOutlet UIView *viewTitle;
@property (strong, nonatomic) IBOutlet UIView *viewMapLocations;

@property (strong, nonatomic) IBOutlet UILabel *labelCompaniesTitle;
@property (strong, nonatomic) IBOutlet UITextView *textViewCompanyDescrip;
@property (strong, nonatomic) IBOutlet UILabel *labelOurLocation;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewOutlet;
@property (strong, nonatomic) IBOutlet UILabel *labelMoreinformation;
@property (strong, nonatomic) IBOutlet UILabel *labelfblink;
@property (strong, nonatomic) IBOutlet UITableView *tableViewCompany;
@property (strong, nonatomic) IBOutlet UIButton *buttonLinkdin;
@property (strong, nonatomic) IBOutlet UIButton *buttonFacebook;
@property (strong, nonatomic) IBOutlet UIView *viewBottomBar;
@property (strong, nonatomic) IBOutlet UIButton *buttonPortfolio;
@property (strong, nonatomic) IBOutlet UIButton *buttonDiscipline;
@property (strong, nonatomic) IBOutlet UIButton *buttonSearch;
@property (strong, nonatomic) IBOutlet UIButton *buttonMessages;
@property (strong, nonatomic) IBOutlet UIButton *buttonSalaryguide;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewLogo;
@property (strong, nonatomic) IBOutlet UIButton *buttonAreaOfOprtn;
@property (strong,nonatomic) IBOutlet UILabel *lblTop;

@property (strong, nonatomic) IBOutlet UIImageView *imageRedStar;



- (IBAction)buttonActionPortfolio:(id)sender;
- (IBAction)buttonActionDiscip:(id)sender;
- (IBAction)buttonSearch:(id)sender;
- (IBAction)buttonMessages:(id)sender;
- (IBAction)buttonActionSalary:(id)sender;
- (IBAction)buttonAreaOfOprtnAction:(id)sender;
- (IBAction)btnOpenWebSite:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *labelPortfolio;
@property (strong, nonatomic) IBOutlet UILabel *labelDiscipline;
@property (strong, nonatomic) IBOutlet UILabel *labelSearchTitle;
@property (strong, nonatomic) IBOutlet UILabel *labelSalaryGuide;
@property (strong, nonatomic) IBOutlet UILabel *labelmessag;
@property (strong, nonatomic) IBOutlet GMSMapView *mapViewGoogle;
@property(nonatomic,strong)CLLocationManager * locationManeger1;



@end

