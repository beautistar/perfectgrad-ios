

//
//  SalaryGuideViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 2/3/15.
//  Copyright (c) 2015 netset. All rights reserved.
//


#import "SalaryGuideViewController.h"
#import "SalaryGuideTableViewCell.h"
#import "PortfolioViewController.h"
#import "DisciplineViewController.h"
#import "MessagesViewController.h"
#import "LocationFilterSearchViewController.h"
#import "SalaryGuideViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AFNetworking.h"
#import "ApiClass.h"


#define cellHeight 80
#define cellWidth 320


@interface SalaryGuideViewController ()<UIGestureRecognizerDelegate>

{
    NSArray *arrayData;
    NSMutableArray *arraySalaries;
    NSIndexPath* idOfsa;
    Database *dummy;
    UIBarButtonItem *rightBarButton;
    UIActivityIndicatorView *spinner;
    UILabel *labelSubTitle;
    NSMutableArray *arrSelectIndustry;
    NSString *stringSelectedIndustry;
    NSString *stringSelectedIndustryName;
    
    UIRefreshControl *refreshControl;
    BOOL isRefreshingSalary;
    NSString *stringSalaryType;
    CGRect rectTableSalary;
    
    NSArray *arrayBanner;
    
    int rotateIndex;
    
    NSArray *arrayDataBanner;
    
    NSString *stringInfo;
    
    NSString *stringBannerUrl;
    
    UIImageView *imageHeaderBlue;
}

@end

@implementation SalaryGuideViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, _imageRedStar.frame.origin.y, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    self.imageRedStar.hidden = YES;
    
    _viewBack.hidden = YES;
    
    isInternetOff = NO;
    _scrollVwTableContainer.hidden = YES;
    
    _lblTile.hidden = YES;
    _lblOccu.hidden = YES;
    _label0To3Exp.hidden = YES;
    _label3To5Exp.hidden = YES;
    _label5To7Exp.hidden = YES;
    _imageOcc.hidden = YES;
    _image0to3.hidden = YES;
    _image3to5.hidden=YES;
    _imageTitle.hidden =YES;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor lightGrayColor];
    [refreshControl addTarget:self
                       action:@selector(callSalaryWebService)
             forControlEvents:UIControlEventValueChanged];
    [_salaryGuideTableView addSubview:refreshControl];
    
    arrSelectIndustry = [[NSMutableArray alloc] init];
    
    dummy=[[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    _tableViewSelectIndustry.layer.borderWidth = 1.0f;
    _tableViewSelectIndustry.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] CGColor];
    
    _viewSelectIndustry.layer.borderWidth = 1.0f;
    _viewSelectIndustry.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] CGColor];
    _viewSelectIndustry.layer.cornerRadius = 5.0f;
    
    _buttonSelectIndustry.layer.borderWidth = 1.0f;
    _buttonSelectIndustry.layer.borderColor = [[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f] CGColor];
    
    _tableViewSelectIndustry.backgroundColor = [UIColor whiteColor];
    _salaryGuideTableView.backgroundColor = [UIColor clearColor];
    
    NSString *stringtxt = @"Disclaimer: The following Salary indicators are based on averaged salaries recorded within each country. Users should only refer to this table as a guide and not as a precise indication of their future salary.";
    
    NSMutableAttributedString *attString =
    [[NSMutableAttributedString alloc]
     initWithString:stringtxt];
    
    [attString addAttribute: NSForegroundColorAttributeName
                      value: [UIColor blackColor]
                      range: NSMakeRange(0,attString.length)];
    
    [attString addAttribute: NSFontAttributeName
                      value:  [UIFont fontWithName:@"OpenSans-Bold" size:10]
                      range: NSMakeRange(0,11)];
    
    [attString addAttribute: NSFontAttributeName
                      value:  [UIFont fontWithName:@"OpenSans" size:10]
                      range: NSMakeRange(12,attString.length-12)];
    
    _labelInstructions.attributedText = attString;
    _labelInstructions.numberOfLines = 0;
    _labelInstructions.hidden = YES;
    
    self.viewBottomBar.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:20.0f],     NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.bounds = CGRectMake( 10, 0, 30 , 30 );
    [rightButton addTarget:self action:@selector(infoButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setImage:[UIImage imageNamed:@"info"] forState:UIControlStateNormal];
    rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    /*Company Names Label in title*/
    _salaryGuideTableView.layer.borderWidth = 1;
    _salaryGuideTableView.layer.borderColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"horizontal"]].CGColor;
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0, 0, 20, 320);
    bottomBorder.borderColor = [UIColor redColor].CGColor;
    
    [self addNavTitles];
    [self addLeftButtons];
    
    // Do any additional setup after loading the view.
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"salaryAlert"])
    {
        [self performSelectorOnMainThread:@selector(showAlert) withObject:nil waitUntilDone:NO];
    }
    
    _imageBannerView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(sectionHeaderTapped:)];
    
    tapGesture1.numberOfTapsRequired = 1;
    
    [tapGesture1 setDelegate:self];
    
    [_imageBannerView addGestureRecognizer:tapGesture1];
    
}

-(void)showAlert
{
    [[[UIAlertView alloc]initWithTitle:@"Salary Guide" message:@"\nTo help you learn more about graduate and future salaries, you can search for occupations using the Salary Guide page. \n\nNote: The salary indicators are based on averaged salaries recorded within each country and are to be used as a guide only." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"salaryAlert"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)viewWillAppear:(BOOL)animated
{
    stringForNotifyEditCompany = @"";
    
    [super viewWillAppear:YES];
    
    if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"] )
    {
        self.imageRedStar.hidden = YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    stringNotifiction = @"";
    
    rectTableSalary = _salaryGuideTableView.bounds;
    
    if (stringSelectedIndustry)
    {
        _textFldIndustry.text = stringSelectedIndustryName;
    }
    
    else
    {
        _textFldIndustry.text = @"Press to select an industry";
    }
    
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.hidesBackButton = YES;
    _buttonSalaryguide.backgroundColor = [UIColor clearColor];
    [_buttonSalaryguide setImage:[UIImage imageNamed:@"salary-gude-activ"] forState:UIControlStateNormal];
    _labelSalaryGuide.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    [self reachabilityCheck];
    
    _occupView.hidden = YES;
    _salaryGuideTableView.frame = CGRectMake(_salaryGuideTableView.frame.origin.x, _occupView.frame.origin.y, _salaryGuideTableView.frame.size.width, _salaryGuideTableView.frame.size.height);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(redStarShow)
                                                 name:@"redStarShow"
                                               object:nil];
}

-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    idOfsa = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];
}

#pragma mark- SelfCreatedButtons

-(void)addLeftButtons
{
    UIView *viewAddBtnsLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 40)];
    
    UIButton * buttonMenu = [[UIButton alloc]initWithFrame:CGRectMake(-3, 4, 30, 30) ];
    
    
    [buttonMenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    
    buttonMenu.backgroundColor = [UIColor clearColor];
    
    [buttonMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    [viewAddBtnsLeft addSubview:buttonMenu];
    
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:viewAddBtnsLeft];
    
}


-(void)addNavTitles
{
    UIView  *textView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 130, 40)];
    
    UILabel *labelTitleName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 0, CGRectGetWidth(textView.frame), 22)];
    labelTitleName.backgroundColor = [UIColor clearColor];
    labelTitleName.font = [UIFont fontWithName:@"OpenSans" size:18.0f];
    labelTitleName.textAlignment = NSTextAlignmentCenter;
    labelTitleName.textColor = [UIColor whiteColor];
    labelTitleName.text = @"Salary Guide";
    
    /*----UILabel Sub-title Name-------*/
    labelSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 22, CGRectGetWidth(textView.frame), 18)];
    labelSubTitle.backgroundColor = [UIColor clearColor];
    labelSubTitle.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
    labelSubTitle.textAlignment = NSTextAlignmentCenter;
    labelSubTitle.textColor = [UIColor whiteColor];
    
    [textView setBackgroundColor:[UIColor clearColor]];
    [textView addSubview:labelTitleName];
    [textView addSubview:labelSubTitle];
    self.navigationItem.titleView = textView;
}

#pragma mark- methodToCallWebService

-(void)callSalaryWebService
{
    isRefreshingSalary = YES;
    [self reachabilityCheck];
}

#pragma mark- UIActivityIndicatorView
-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        spinner.center = self.view.center;
        spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [spinner startAnimating];
        [self.view.window setUserInteractionEnabled:NO];
        [self.view addSubview:spinner];
    }
    else
    {
        [spinner stopAnimating];
        [spinner removeFromSuperview];
        [self.view.window setUserInteractionEnabled:YES];
        spinner = nil;
    }
}

#pragma mark- InternetConnectionCheck

-(void)reachabilityCheck
{
    {
        
        if ([refreshControl isRefreshing])
        {
            [refreshControl endRefreshing];
        }
        
        if([Utilities CheckInternetConnection])
        {
            if ([[NSUserDefaults standardUserDefaults]boolForKey:@"salaryGuide"])
            {
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"salaryGuide"];
                
                [self getSalariesList];
            }
            else
            {
                if ([[[dummy loaddataSalaries:@"Select * From SALARIES"] copy] count]>0)
                {
                    [self loadSalariesOffline];
                }
                else
                {
                    NSLog(@"connected");
                    [self getSalariesList];
                }
            }
        }
        else
        {
            [self loadSalariesOffline];
        }
    }
}

#pragma mark- loadDataOffline

-(void)getRefreshData
{
    [arraySalaries removeAllObjects];
    
    for (int i=0; i<[arrayData count]; i++)
    {
        if ([stringSelectedIndustry isEqualToString:[[arrayData objectAtIndex:i] objectForKey:@"sector_id"]] )
        {
            [arraySalaries addObject:[arrayData objectAtIndex:i]];
        }
    }
    
    [self sortDataOfSalary];
    
    [_salaryGuideTableView reloadData];
}

-(void)loadSalariesOffline
{
    if ([[[dummy loaddataSalaries:@"Select * From SALARIES"] copy] count]>0)
    {
        isInternetOff = YES;
        arrayData = [[NSArray alloc]initWithArray:[dummy loaddataSalaries:@"Select * From SALARIES"]];
        
        if (isRefreshingSalary == YES)
        {
            [self getRefreshData];
        }
        else
        {
            arraySalaries = [[dummy loaddataSalaries:@"Select * From SALARIES"] mutableCopy];
            
            NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryName"]];
            
            if ([str isEqualToString:@"(null)"])
            {
                labelSubTitle.text = [NSString stringWithFormat:@"%@",[[arrayData valueForKey:@"year"] objectAtIndex:0]];
            }
            else
            {
                labelSubTitle.text = [NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryName"],[[arrayData valueForKey:@"year"] objectAtIndex:0]];
            }
            
            [self getFilteredSectors];
            
            [self sortDataOfSalary];
            
        }
        
        if([spinner isAnimating])
        {
            [self ShowDataLoader:NO];
        }
    }
    
    if ([[[dummy loaddataBanners:@"Select * From SALARIESBANNERS"] copy] count]>0)
    {
        arrayDataBanner = [NSArray new];
        
        isInternetOff = YES;
        arrayDataBanner = [[NSArray alloc]initWithArray:[dummy loaddataBanners: @"Select * From SALARIESBANNERS"]];
        NSLog(@"bannerrrrrrrrrr is %@", arrayDataBanner);
        
        [self showBanner];
    }
}

-(void)showBanner
{
    NSString *stringImage = [NSString stringWithFormat:@"%@",[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",[[arrayDataBanner valueForKey:@"image_name"] objectAtIndex:0]]]];
    
    stringBannerUrl = [[arrayDataBanner valueForKey:@"bannerUrl"]objectAtIndex:0];
    
    rotateIndex = 0;
    
    if (isInternetOff)
    {
        [_imageBannerView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",[[arrayDataBanner valueForKey:@"image_name"] objectAtIndex:rotateIndex]]]];
    }
    else
    {
        if ([stringImage hasPrefix:@"/Users/"])
        {
            [_imageBannerView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",[[arrayDataBanner valueForKey:@"image_name"] objectAtIndex:rotateIndex]]]];
        }
        
        else if ([stringImage hasPrefix:@"/var/mobile/Containers/"])
        {
            [_imageBannerView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",[[arrayDataBanner valueForKey:@"image_name"] objectAtIndex:rotateIndex]]]];
        }
        else
        {
            UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",[[arrayDataBanner valueForKey:@"image_name"] objectAtIndex:rotateIndex]]]]]]];
            
            [_imageBannerView setImage:imge];
        }
    }
    
    if (arrayDataBanner.count>1)
    {
        int delay = [[[arrayDataBanner valueForKey:@"rotation_time"] objectAtIndex:rotateIndex]intValue];
        
        [self performSelector:@selector(changeImage) withObject:nil afterDelay:delay];
    }
}

-(void)sectionHeaderTapped:(UITapGestureRecognizer *)gesture
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringBannerUrl]];
}

-(void)changeImage
{
    if (rotateIndex== arrayDataBanner.count-1)
    {
        rotateIndex = 0;
    }
    
    else
    {
        rotateIndex++;
    }
    CATransition *transition1 = [CATransition animation];
    
    transition1.subtype = kCATransitionFromRight;
    
    transition1.duration = 0.5;
    
    transition1.type = kCATransitionPush;
    
    [_imageBannerView.layer addAnimation:transition1 forKey:nil];
    
    NSString *stringImage = [NSString stringWithFormat:@"%@",[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",[[arrayDataBanner valueForKey:@"image_name"] objectAtIndex:rotateIndex]]]];
    
    stringBannerUrl = [[arrayDataBanner valueForKey:@"bannerUrl"]objectAtIndex:rotateIndex];
    
    if (isInternetOff)
    {
        [_imageBannerView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",[[arrayDataBanner valueForKey:@"image_name"] objectAtIndex:rotateIndex]]]] ;
    }
    else
    {
        if ([stringImage hasPrefix:@"/Users/"])
        {
            [_imageBannerView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",[[arrayDataBanner valueForKey:@"image_name"] objectAtIndex:rotateIndex]]]];
        }
        
        else if ([stringImage hasPrefix:@"/var/mobile/Containers/"])
        {
            [_imageBannerView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",[[arrayDataBanner valueForKey:@"image_name"] objectAtIndex:rotateIndex]]]];
        }
        
        else
        {
            UIImage *imge = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",[[arrayDataBanner valueForKey:@"image_name"] objectAtIndex:rotateIndex]]]]]]];
            
            [_imageBannerView setImage:imge];
        }
    }
    int delay = [[[arrayDataBanner valueForKey:@"rotation_time"] objectAtIndex:rotateIndex]intValue];
    
    [self performSelector:@selector(changeImage) withObject:nil afterDelay:delay];
}

-(void)getSalariesList
{
    [self ShowDataLoader:YES];
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@getAllSalaries",baseUrl];
    
    NSDictionary *dicParam = @{@"uid":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"salaryGuide"];
         
         if ([refreshControl isRefreshing])
         {
             [refreshControl endRefreshing];
         }
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             arrayData = nil;
             
             arrayData = [[NSArray alloc]initWithArray:[responseObject valueForKeyPath:@"data.salary"]];
             
             arrayBanner = [responseObject valueForKeyPath:@"data.banners"];
             
             arraySalaries = [arrayData mutableCopy];
             
             [self getFilteredSectors];
             
             if (arrayBanner.count == 0)
             {
                 
             }
             else
             {
                 if ([dummy deleteTable:@"delete from SALARIESBANNERS"])
                 {
                     NSString *string = [NSString stringWithFormat:@"%@",[[arrayBanner valueForKey:@"msg"] objectAtIndex:0]];
                     
                     if ([string isEqualToString:@"Nothing found"])
                     {
                         
                     }
                     else
                     {
                         NSString *stringId;
                         
                         NSDate *dateCurrent = [NSDate date];
                         
                         NSDate *dateGreater ;
                         
                         NSComparisonResult result = [dateCurrent compare:dateGreater];
                         
                         if(result==NSOrderedAscending)
                         {
                             NSLog(@"Date1 is in the future");
                         }
                         else if(result==NSOrderedDescending)
                         {
                             NSLog(@"Date1 is in the past");
                         }
                         else
                         {
                             NSLog(@"Both dates are the same");
                         }
                         
                         for (int i=0; i<[arrayBanner count]; i++)
                         {
                             NSString *query;
                             
                             if (![arrayBanner valueForKey:@"id"])
                             {
                                 stringId = @"0189654";
                             }
                             else
                             {
                                 stringId = [[arrayBanner valueForKey:@"id"] objectAtIndex:i];
                             }
                             query = [NSString stringWithFormat: @"INSERT INTO SALARIESBANNERS (image_name,rotation_time,id,name,bannerUrl) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",[[arrayBanner valueForKey:@"image_name"] objectAtIndex:i],[[arrayBanner valueForKey:@"rotation_time"] objectAtIndex:i],stringId,[[arrayBanner valueForKey:@"name"]objectAtIndex:i],[[arrayBanner valueForKey:@"banner_url"]objectAtIndex:i]];
                             
                             [self saveImages:[[NSString stringWithFormat:@"%@%@",[[arrayBanner valueForKey:@"id"] objectAtIndex:i],[[arrayBanner valueForKey:@"image_name"] objectAtIndex:i]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] imageUrl:[[arrayBanner valueForKey:@"image_name"] objectAtIndex:i]];
                             
                             const char *query_stmt = [query UTF8String];
                             [dummy  runQueries:query_stmt strForscreen:@"SalariesBanners"];
                         }
                         
                         if ([[[dummy loaddataBanners:@"Select * From SALARIESBANNERS"] copy] count]>0)
                         {
                             arrayDataBanner = [NSArray new];
                             
                             isInternetOff = YES;
                             arrayDataBanner = [[NSArray alloc]initWithArray:[dummy loaddataBanners: @"Select * From SALARIESBANNERS"]];
                             
                             NSLog(@"bannerrrrrrrrrr is %@", arrayDataBanner);
                             
                             [self showBanner];
                         }
                     }
                 }
                 
             }
             
             NSString *str = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryName"]];
             
             if ([str isEqualToString:@"(null)"])
             {
                 
                 labelSubTitle.text = [NSString stringWithFormat:@"%@",[[arrayData valueForKey:@"year"] objectAtIndex:0]];
                 
                 [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"\nPlease update your profile again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
                 
             }
             else
             {
                 labelSubTitle.text = [NSString stringWithFormat:@"%@ %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"countryName"],[[arrayData valueForKey:@"year"] objectAtIndex:0]];
             }
             [self ShowDataLoader:NO];
             
             [self performSelectorInBackground:@selector(saveSalaryGuide) withObject:nil];
         }
     }
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         [self loadSalariesOffline];
         [self ShowDataLoader:NO];
     }];
}


-(void)saveSalaryGuide
{
    {
        if ([dummy deleteTable:@"delete from SALARIES"])
        {
            for (int i=0; i<[arrayData count]; i++)
            {
                NSString *query;
                
                query = [NSString stringWithFormat: @"INSERT INTO SALARIES (id,occupation,occupation_id,salary0to3,title,sector,sector_id,salary3to5,salary5to7,year,large,smallMedium,type) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")"
                         ,[[arrayData valueForKey:@"id"] objectAtIndex:i]
                         ,[[arrayData valueForKey:@"occupation"] objectAtIndex:i]
                         ,[[arrayData valueForKey:@"occupation_id"] objectAtIndex:i]
                         ,[[arrayData valueForKey:@"salary0to3"] objectAtIndex:i]
                         ,[[arrayData valueForKey:@"title"] objectAtIndex:i]
                         ,[[arrayData valueForKey:@"sector"] objectAtIndex:i]
                         ,[[arrayData valueForKey:@"sector_id"] objectAtIndex:i]
                         ,[[arrayData valueForKey:@"salary3to5"] objectAtIndex:i]
                         ,[[arrayData valueForKey:@"salary5to7"] objectAtIndex:i]
                         ,[[arrayData valueForKey:@"year"] objectAtIndex:i]
                         ,[[arrayData valueForKey:@"large"] objectAtIndex:i]
                         ,[[arrayData valueForKey:@"smallMedium"] objectAtIndex:i]
                         ,[[arrayData valueForKey:@"type"] objectAtIndex:i]];
                
                const char *query_stmt = [query UTF8String];
                
                [dummy  runQueries:query_stmt strForscreen:@"Salaries" ];
            }
        }
        
        if ([dummy deleteTable:@"delete from SALARIESBANNERS"])
        {
            NSString *string = [NSString stringWithFormat:@"%@",[[arrayBanner valueForKey:@"msg"] objectAtIndex:0]];
            
            if ([string isEqualToString:@"Nothing found"])
            {
                
            }
            else
            {
                NSString *stringId;
                
                for (int i=0; i<[arrayBanner count]; i++)
                {
                    NSString *query;
                    
                    if (![arrayBanner valueForKey:@"id"])
                    {
                        stringId = @"0189654";
                    }
                    else
                    {
                        stringId = [[arrayBanner valueForKey:@"id"] objectAtIndex:i];
                    }
                    
                    query = [NSString stringWithFormat: @"INSERT INTO SALARIESBANNERS (image_name,rotation_time,id,name,bannerUrl) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",[[arrayBanner valueForKey:@"image_name"] objectAtIndex:i],[[arrayBanner valueForKey:@"rotation_time"] objectAtIndex:i],stringId,[[arrayBanner valueForKey:@"name"]objectAtIndex:i],[[arrayBanner valueForKey:@"banner_url"]objectAtIndex:i]];
                    
                    [self saveImages:[[NSString stringWithFormat:@"%@%@",[[arrayBanner valueForKey:@"id"] objectAtIndex:i],[[arrayBanner valueForKey:@"image_name"] objectAtIndex:i]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] imageUrl:[[arrayBanner valueForKey:@"image_name"] objectAtIndex:i]];
                    
                    const char *query_stmt = [query UTF8String];
                    [dummy  runQueries:query_stmt strForscreen:@"SalariesBanners"];
                }
            }
        }
    }
}

-(void)saveImages:(NSString *)ImageName imageUrl :(NSString *)imageUrl
{
    ImageName = [ImageName stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    if (imageUrl.length>0)
    {
        if (![self imageCheck:ImageName])
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:ImageName];
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,imageUrl]]];
            
            [imageData writeToFile:savedImagePath atomically:NO];
        }
    }
}

-(BOOL)imageCheck :(NSString *)strName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    if ([contents containsObject:strName])
    {
        return YES;
    }
    return NO;
}

-(void)getFilteredSectors
{
    [arrSelectIndustry removeAllObjects];
    
    for (int i=0; i<arrayData.count; i++)
    {
        if (arrSelectIndustry.count>0)
        {
            for (int k=0; k<arrSelectIndustry.count; k++)
            {
                if (![[arrSelectIndustry valueForKey:@"sector_id"]  containsObject:[[arrayData valueForKey:@"sector_id"] objectAtIndex:i]])
                {
                    NSDictionary *dicInds = @{@"sector":[[arrayData valueForKey:@"sector"] objectAtIndex:i],@"sector_id":[[arrayData valueForKey:@"sector_id"] objectAtIndex:i]};
                    [arrSelectIndustry addObject:dicInds];
                }
            }
        }
        else
        {
            NSDictionary *dicInds = @{@"sector":[[arrayData valueForKey:@"sector"] objectAtIndex:i],@"sector_id":[[arrayData valueForKey:@"sector_id"] objectAtIndex:i]};
            [arrSelectIndustry addObject:dicInds];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView ==_tableViewSelectIndustry)
    {
        return [arrSelectIndustry count];
    }
    else
    {
        if (arraySalaries.count==0)
        {
            return [arraySalaries count];
        }
        else
        {
            return [arraySalaries count];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SalaryGuideCell";
    
    UITableViewCell *cell = (UITableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:nil];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    if (tableView == _tableViewSelectIndustry)
    {
        cell.textLabel.text = [[arrSelectIndustry valueForKey:@"sector"] objectAtIndex:indexPath.row];
        
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
        
        if ([idOfsa isEqual:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    else
    {
        UILabel * labelOccupation = [[UILabel alloc]init];
        UILabel * labelTitleName = [[UILabel alloc]init];
        UILabel * labelExperience = [[UILabel alloc]init];
        UILabel * labelExperience3to5Yrs = [[UILabel alloc]init];
        UILabel * labelExperience5to7Yrs = [[UILabel alloc]init];
        
        UIImageView *imageVwOccupationTitleSep = [[UIImageView alloc]init];
        UIImageView *imageVwExprnceSep = [[UIImageView alloc]init];
        UIImageView *imageVwExprnce3To5Sep = [[UIImageView alloc]init];
        UIImageView *imageVwTitleExperienceSep = [[UIImageView alloc]init];
        
        UIImageView *imgOcc = [[UIImageView alloc]init];
        
        UIImageView *imageHeader = [[UIImageView alloc]init];
        
        labelTitleName.numberOfLines=0;
        labelOccupation.numberOfLines=0;
        labelExperience.numberOfLines = 0;
        
        if ([stringSalaryType isEqualToString:@"csize"])
        {
            labelExperience5to7Yrs.hidden = YES;
            
            imageVwExprnce3To5Sep.hidden = YES;
            
            {
                imageHeader.frame = CGRectMake(-5, 0, tableView.frame.size.width+5, 50);
                
                labelOccupation.frame =  CGRectMake(0, 2, 123, 52);
                
                labelTitleName.frame = CGRectMake(125, 2, 123, 52);
                
                labelExperience.frame = CGRectMake(250, 2, 123, 52);
                
                labelExperience3to5Yrs.frame = CGRectMake(375, 3, 123, 52);
                
                imageVwOccupationTitleSep.frame = CGRectMake(124, 0, 1, 54);;
                
                imageVwTitleExperienceSep.frame = CGRectMake(249, 0, 1, 54);
                
                imageVwExprnceSep.frame = CGRectMake(374, 0, 1, 54);
                
                imageHeader.hidden = YES;
                
                labelOccupation.text = [[arraySalaries valueForKey:@"occupation"] objectAtIndex:indexPath.row];
                labelExperience.text = [[arraySalaries valueForKey:@"smallMedium"] objectAtIndex:indexPath.row];
                labelExperience3to5Yrs.text = [[arraySalaries valueForKey:@"large"] objectAtIndex:indexPath.row];
                
                NSCharacterSet *whitespace = [NSCharacterSet whitespaceCharacterSet];
                NSString *trimmedString = [[[arraySalaries valueForKey:@"title"] objectAtIndex:indexPath.row] stringByTrimmingCharactersInSet:whitespace];
                
                labelTitleName.backgroundColor = [UIColor clearColor];
                labelTitleName.text = trimmedString;
            }
        }
        else
        {
            labelExperience5to7Yrs.numberOfLines=0;
            labelExperience3to5Yrs.numberOfLines=0;
            labelExperience.numberOfLines = 0;
            
            labelExperience5to7Yrs.hidden =NO;
            
            imageVwExprnce3To5Sep.hidden = NO;
            
            {
                imageHeader.frame = CGRectMake(-5, 0, tableView.frame.size.width+5, 50);
                
                labelOccupation.frame =  CGRectMake(0, 0, 118, 52);
                
                labelTitleName.frame = CGRectMake(120, 0, 118, 52);
                
                labelExperience.frame = CGRectMake(240, 0, 100, 52);
                
                labelExperience3to5Yrs.frame = CGRectMake(342, 0, 100, 52);
                
                labelExperience5to7Yrs.frame = CGRectMake(444, 0, 100, 52);
                
                imageVwOccupationTitleSep.frame = CGRectMake(119, 0, 1, 54);
                
                imageVwTitleExperienceSep.frame = CGRectMake(239, 0, 1, 54);
                
                imageVwExprnceSep.frame = CGRectMake(341, 0, 1, 54);
                
                imageVwExprnce3To5Sep.frame = CGRectMake(443, 0, 1, 54);
                
                imageHeader.hidden = YES;
                
                labelOccupation.text = [[arraySalaries valueForKey:@"occupation"] objectAtIndex:indexPath.row];
                
                labelExperience5to7Yrs.text =[[arraySalaries valueForKey:@"salary5to7"] objectAtIndex:indexPath.row];
                
                labelExperience3to5Yrs.text = [[arraySalaries valueForKey:@"salary3to5"] objectAtIndex:indexPath.row];
                
                labelExperience.text =[[arraySalaries valueForKey:@"salary0to3"] objectAtIndex:indexPath.row];
                
                NSCharacterSet *whitespace = [NSCharacterSet whitespaceCharacterSet];
                NSString *trimmedString = [[[arraySalaries valueForKey:@"title"] objectAtIndex:indexPath.row] stringByTrimmingCharactersInSet:whitespace];
                
                labelTitleName.backgroundColor = [UIColor clearColor];
                labelTitleName.text = trimmedString;
            }
        }
        
        imageVwOccupationTitleSep.image = [UIImage imageNamed:@"v-line"];
        imageVwTitleExperienceSep.image = [UIImage imageNamed:@"v-line"];
        imageVwExprnceSep.image = [UIImage imageNamed:@"v-line"];
        imageVwExprnce3To5Sep.image = [UIImage imageNamed:@"v-line"];
        [cell.contentView addSubview:imageHeader];
        
        [cell.contentView addSubview:imageVwOccupationTitleSep];
        [cell.contentView addSubview:imageVwTitleExperienceSep];
        [cell.contentView addSubview:imageVwExprnceSep];
        [cell.contentView addSubview:imageVwExprnce3To5Sep];
        [cell.contentView addSubview:imgOcc];
        imgOcc.backgroundColor = [UIColor redColor];
        
        labelOccupation.backgroundColor = [UIColor clearColor];
        labelOccupation.textAlignment = NSTextAlignmentCenter;
        labelOccupation.font = [UIFont fontWithName:@"OpenSans" size:(12.0f)];
        [cell.contentView addSubview:labelOccupation];
        
        labelTitleName.font = [UIFont fontWithName:@"OpenSans" size:(12.0f)];
        labelTitleName.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:labelTitleName];
        
        labelExperience.font = [UIFont fontWithName:@"OpenSans" size:(12.0f)];
        labelExperience.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:labelExperience];
        
        labelExperience3to5Yrs.font = [UIFont fontWithName:@"OpenSans" size:(12.0f)];
        labelExperience3to5Yrs.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:labelExperience3to5Yrs];
        
        labelExperience5to7Yrs.font = [UIFont fontWithName:@"OpenSans" size:(12.0f)];
        labelExperience5to7Yrs.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:labelExperience5to7Yrs];
        
        /* ---add cell seperator line image */
        
        UIImageView *separator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"separator-1"]];
        
        separator.frame = CGRectMake(cell.contentView.frame.origin.x,54,tableView.frame.size.width ,1);
        
        [cell.contentView addSubview:separator];
        
        if (indexPath.row%2 == 0)
        {
            cell.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:240.0f/255.0f blue:244.0f/255.0f alpha:1.0f];
        }
        else
        {
            cell.backgroundColor = [UIColor whiteColor];
        }
    }
    
    return cell;
}

#pragma mark- UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView ==_tableViewSelectIndustry)
    {
        _scrollVwTableContainer.hidden = NO;
        
        _textFldIndustry.text = [[arrSelectIndustry objectAtIndex:indexPath.row] objectForKey:@"sector"];
        
        stringSelectedIndustry = [[arrSelectIndustry objectAtIndex:indexPath.row] objectForKey:@"sector_id"];
        
        stringSelectedIndustryName = [[arrSelectIndustry objectAtIndex:indexPath.row] objectForKey:@"sector"];
        
        NSInteger indexOfObject = [[arrayData valueForKey:@"sector_id"]indexOfObject:stringSelectedIndustry];
        
        stringSalaryType = [[arrayData valueForKey:@"type"] objectAtIndex:indexOfObject];
        
        if ([stringSalaryType isEqualToString:@"csize"])
        {
            CGRect scrollRectMake = _scrollVwTableContainer.frame;
            scrollRectMake.size.width = 500;
            _scrollVwTableContainer.frame = scrollRectMake;
            _scrollVwTableContainer.contentOffset = CGPointMake(0, 0);
            
            CGRect tableRectMake = _salaryGuideTableView.frame;
            
            tableRectMake = CGRectMake(5, CGRectGetMaxY(_occupView.frame)+1, 500, tableRectMake.size.height);
            
            CGRect viewRectMake = _occupView.frame;
            
            viewRectMake.size.width = 500;
            
            _occupView.frame = viewRectMake;
            
            if ([stringInfo isEqualToString:@"info"])
            {
                _salaryGuideTableView.frame = CGRectMake(_salaryGuideTableView.frame.origin.x, _occupView.frame.origin.y, _salaryGuideTableView.frame.size.width, scrollRectMake.size.height-10-70);
            }
            else
            {
                _salaryGuideTableView.frame = CGRectMake(_salaryGuideTableView.frame.origin.x, _occupView.frame.origin.y, _salaryGuideTableView.frame.size.width, scrollRectMake.size.height-10);
            }
            
            if (self.view.frame.size.height<=568)
            {
                _scrollVwTableContainer.contentSize = CGSizeMake(684, _salaryGuideTableView.frame.size.height-100);
            }
            
            else if(self.view.frame.size.height==667)
            {
                _scrollVwTableContainer.contentSize = CGSizeMake(631, _salaryGuideTableView.frame.size.height-100);
            }
            
            else if(self.view.frame.size.height==736)
            {
                _scrollVwTableContainer.contentSize = CGSizeMake(595, _salaryGuideTableView.frame.size.height-100);
            }
        }
        
        else
        {
            CGRect scrollRectMake = _scrollVwTableContainer.frame;
            scrollRectMake.size.width = 544;
            _scrollVwTableContainer.frame = scrollRectMake;
            
            CGRect tableRectMake = _salaryGuideTableView.frame;
            
            tableRectMake = CGRectMake(5, CGRectGetMaxY(_occupView.frame)+1, 544, _salaryGuideTableView.frame.size.height);
            
            CGRect viewRectMake = _occupView.frame;
            viewRectMake.size.width = 544;
            _occupView.frame = viewRectMake;
            _scrollVwTableContainer.contentOffset = CGPointMake(0, 0);
            
            if ([stringInfo isEqualToString:@"info"])
            {
                _salaryGuideTableView.frame = CGRectMake(_salaryGuideTableView.frame.origin.x, _occupView.frame.origin.y, _salaryGuideTableView.frame.size.width, scrollRectMake.size.height-10-70);
            }
            
            else
            {
                _salaryGuideTableView.frame = CGRectMake(_salaryGuideTableView.frame.origin.x, _occupView.frame.origin.y, _salaryGuideTableView.frame.size.width, scrollRectMake.size.height-10);
            }
            
            if (self.view.frame.size.height<=568)
            {
                _scrollVwTableContainer.contentSize = CGSizeMake(774, _salaryGuideTableView.frame.size.height-100);
            }
            
            else if(self.view.frame.size.height==667)
            {
                _scrollVwTableContainer.contentSize = CGSizeMake(720, _salaryGuideTableView.frame.size.height-100);
            }
            
            else if(self.view.frame.size.height==736)
            {
                _scrollVwTableContainer.contentSize = CGSizeMake(680, _salaryGuideTableView.frame.size.height-100);
            }
            
            _scrollVwTableContainer.contentOffset = CGPointMake(0, 0);
        }
        
        [self filteredSalaryList];
        
        [_salaryGuideTableView setUserInteractionEnabled:YES];
        
        self.navigationItem.rightBarButtonItem.enabled = YES;
        
        idOfsa = indexPath;
        
        _tableViewSelectIndustry.hidden = YES;
        
        _viewBack.hidden = YES;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == _tableViewSelectIndustry)
    {
        return 1;
    }
    return 1;
}

-(void)filteredSalaryList
{
    [arraySalaries removeAllObjects];
    
    for (int i=0; i<[arrayData count]; i++)
    {
        if ([stringSelectedIndustry isEqualToString:[[arrayData objectAtIndex:i] objectForKey:@"sector_id"]] )
        {
            [arraySalaries addObject:[arrayData objectAtIndex:i]];
        }
    }
    [self setlabelOnTheTable];
    
    [self sortDataOfSalary];
    [_salaryGuideTableView reloadSections:[NSIndexSet indexSetWithIndex:0 ] withRowAnimation:UITableViewRowAnimationAutomatic];
}


-(void)setlabelOnTheTable
{
    [imageHeaderBlue removeFromSuperview];
    
    UILabel * labelOccupation = [[UILabel alloc]init];
    UILabel * labelTitleName = [[UILabel alloc]init];
    UILabel * labelExperience = [[UILabel alloc]init];
    UILabel * labelExperience3to5Yrs = [[UILabel alloc]init];
    UILabel * labelExperience5to7Yrs = [[UILabel alloc]init];
    
    UIImageView *imageVwOccupationTitleSep = [[UIImageView alloc]init];
    UIImageView *imageVwExprnceSep = [[UIImageView alloc]init];
    UIImageView *imageVwExprnce3To5Sep = [[UIImageView alloc]init];
    UIImageView *imageVwTitleExperienceSep = [[UIImageView alloc]init];
    
    imageHeaderBlue = [[UIImageView alloc]init];
    
    labelTitleName.numberOfLines=0;
    labelOccupation.numberOfLines=0;
    labelExperience.numberOfLines = 0;
    
    labelExperience5to7Yrs.numberOfLines=0;
    labelExperience3to5Yrs.numberOfLines=0;
    labelExperience.numberOfLines = 0;
    
    if ([stringSalaryType isEqualToString:@"csize"])
    {
        labelExperience5to7Yrs.hidden = YES;
        
        imageVwExprnce3To5Sep.hidden = YES;
        
        imageHeaderBlue.frame = CGRectMake(-2, 10, _scrollVwTableContainer.frame.size.width+5, 62);
        
        labelOccupation.frame =  CGRectMake(5, 1, 123, 52);
        
        labelTitleName.frame = CGRectMake(131, 1, 123, 52);
        
        labelExperience.frame = CGRectMake(255, 1, 123, 52);
        
        labelExperience3to5Yrs.frame = CGRectMake(380, 1, 123, 52);
        
        labelExperience5to7Yrs.frame = CGRectMake(455, 1, 100, 55);
        
        imageVwOccupationTitleSep.frame = CGRectMake(129, 0, 1, 54);;
        
        imageVwTitleExperienceSep.frame = CGRectMake(253, 0, 1, 54);
        
        imageVwExprnceSep.frame = CGRectMake(379, 0, 1, 54);
        
        labelOccupation.text= @"Occupation";
        labelTitleName.text = @"Title";
        labelExperience.text = @"Small/Medium Company";
        labelExperience3to5Yrs.text = @"Large Company";
    }
    else
    {
        labelExperience5to7Yrs.hidden = NO;
        
        imageVwExprnce3To5Sep.hidden = NO;
        
        imageHeaderBlue.frame = CGRectMake(-2, 10, _scrollVwTableContainer.frame.size.width+5, 62);
        
        labelOccupation.frame =  CGRectMake(5, 0, 118, 52);
        
        labelTitleName.frame = CGRectMake(125, 0, 118, 52);
        
        labelExperience.frame = CGRectMake(245, 0, 100, 52);
        
        labelExperience3to5Yrs.frame = CGRectMake(348, 0, 100, 52);
        
        labelExperience5to7Yrs.frame = CGRectMake(450, 0, 100, 52);
        
        imageVwOccupationTitleSep.frame = CGRectMake(123, 0, 1, 54);
        
        imageVwTitleExperienceSep.frame = CGRectMake(243, 0, 1, 54);
        
        imageVwExprnceSep.frame = CGRectMake(346, 0, 1, 54);
        
        imageVwExprnce3To5Sep.frame = CGRectMake(448, 0, 1, 54);
        
        labelOccupation.text= @"Occupation";
        labelTitleName.text = @"Title";
        labelExperience.text = @"0 to 3 years Experience";
        labelExperience3to5Yrs.text = @"3 to 5 years Experience";
        labelExperience5to7Yrs.text = @"5 to 7 years Experience";
    }
    
    labelOccupation.textAlignment = NSTextAlignmentCenter;
    labelOccupation.font = [UIFont fontWithName:@"OpenSans" size:(12.0f)];
    
    labelTitleName.font = [UIFont fontWithName:@"OpenSans" size:(12.0f)];
    labelTitleName.textAlignment = NSTextAlignmentCenter;
    
    labelExperience.font = [UIFont fontWithName:@"OpenSans" size:(12.0f)];
    labelExperience.textAlignment = NSTextAlignmentCenter;
    
    labelExperience3to5Yrs.font = [UIFont fontWithName:@"OpenSans" size:(12.0f)];
    labelExperience3to5Yrs.textAlignment = NSTextAlignmentCenter;
    
    labelExperience5to7Yrs.font = [UIFont fontWithName:@"OpenSans" size:(12.0f)];
    labelExperience5to7Yrs.textAlignment = NSTextAlignmentCenter;
    
    imageHeaderBlue.image = [UIImage imageNamed:@"occupation"];
    
    labelOccupation.textColor = [UIColor whiteColor];
    labelTitleName.textColor = [UIColor whiteColor];
    labelExperience.textColor = [UIColor whiteColor];
    labelExperience3to5Yrs.textColor = [UIColor whiteColor];
    labelExperience5to7Yrs.textColor = [UIColor whiteColor];
    
    imageVwOccupationTitleSep.image = [UIImage imageNamed:@"v-line"];
    imageVwTitleExperienceSep.image = [UIImage imageNamed:@"v-line"];
    imageVwExprnceSep.image = [UIImage imageNamed:@"v-line"];
    imageVwExprnce3To5Sep.image = [UIImage imageNamed:@"v-line"];
    
    [imageHeaderBlue addSubview:labelOccupation];
    [imageHeaderBlue addSubview:labelTitleName];
    [imageHeaderBlue addSubview:labelExperience];
    [imageHeaderBlue addSubview:labelExperience3to5Yrs];
    
    [imageHeaderBlue addSubview:imageVwOccupationTitleSep];
    [imageHeaderBlue addSubview:imageVwTitleExperienceSep];
    [imageHeaderBlue addSubview:imageVwExprnceSep];
    [imageHeaderBlue addSubview:imageVwExprnce3To5Sep];
    [imageHeaderBlue addSubview:labelExperience5to7Yrs];
    
    [_scrollVwTableContainer addSubview:imageHeaderBlue];
    
    if ([stringInfo isEqualToString:@"info"])
    {
        _salaryGuideTableView.frame = CGRectMake(_salaryGuideTableView.frame.origin.x,_salaryGuideTableView.frame.origin.y-10, _salaryGuideTableView.frame.size.width, _salaryGuideTableView.frame.size.height+10);
    }
    else
    {
        _salaryGuideTableView.frame = CGRectMake(_salaryGuideTableView.frame.origin.x, 65, _salaryGuideTableView.frame.size.width, _salaryGuideTableView.frame.size.height-58);
    }
    
}

-(void)sortDataOfSalary
{
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"occupation" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    
    NSArray *sortedArray = [arraySalaries sortedArrayUsingDescriptors:sortDescriptors];
    
    arraySalaries = nil;
    
    arraySalaries = [[NSMutableArray alloc]init];
    
    arraySalaries = [sortedArray mutableCopy];
    
    [_salaryGuideTableView reloadData];
}

#pragma mark- UITextFieldDelegate---

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==_textFldIndustry)
    {
        _viewBack.hidden = NO;
        _tableViewSelectIndustry.hidden = NO;
        [_tableViewSelectIndustry reloadData];
        [_salaryGuideTableView setUserInteractionEnabled:NO];
        self.navigationItem.rightBarButtonItem.enabled = NO;
        
        UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableViewSelectIndustry.frame.size.width, 40)];
        
        viewHeader.backgroundColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0];
        CGRect rectLabel = viewHeader.frame;
        rectLabel.size.width -= 100;
        
        UILabel *labeltitle = [[UILabel alloc] initWithFrame: rectLabel];
        labeltitle.text = @"   Select an Industry";
        labeltitle.font = [UIFont fontWithName:@"OpenSans" size:15.0f];
        labeltitle.textColor = [UIColor whiteColor];
        [viewHeader addSubview:labeltitle];
        
        UIButton *btnCancel = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(labeltitle.frame)+20, 0, 80, 40)];
        [btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnCancel.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:(12.0f)];
        [btnCancel addTarget:self action:@selector(removeTable) forControlEvents:UIControlEventTouchUpInside];
        [viewHeader addSubview:btnCancel];
        [_viewBack addSubview:viewHeader];
    }
    return NO;
}

#pragma mark- SelfCreatedButtonAction

-(void)removeTable
{
    _viewBack.hidden = YES;
    _tableViewSelectIndustry.hidden = YES;
    self.navigationItem.rightBarButtonItem.enabled = YES;
    _salaryGuideTableView.userInteractionEnabled = YES;
}

-(void)infoButtonAction
{
    CGRect rectViewOcc = _occupView.frame;
    CGRect rectTable = _scrollVwTableContainer.frame;
    rectTable.origin.y = _scrollVwTableContainer.frame.origin.y+70;
    rectViewOcc.origin.y = _occupView.frame.origin.y+70;
    rectTable.size.height = _scrollVwTableContainer.frame.size.height-70;
    
    [UIView animateWithDuration:0.4 animations:^
     {
         _scrollVwTableContainer.frame = rectTable;
         _occupView.frame = rectViewOcc;
         _labelInstructions.hidden = NO;
         self.navigationItem.rightBarButtonItem = nil;
     }];
    
    [self performSelector:@selector(infoAction) withObject:nil afterDelay:7.0];
    
    stringInfo = @"info";
    
}


-(void)infoAction
{
    CGRect rectViewOcc = _occupView.frame;
    CGRect rectTable = _scrollVwTableContainer.frame;
    rectTable.origin.y = _scrollVwTableContainer.frame.origin.y-70;
    rectViewOcc.origin.y = _occupView.frame.origin.y-70;
    rectTable.size.height = _scrollVwTableContainer.frame.size.height+70;
    
    [UIView animateWithDuration:0.4 animations:^
     {
         _scrollVwTableContainer.frame = rectTable;
         _occupView.frame = rectViewOcc;
         _labelInstructions.hidden = YES;
         self.navigationItem.rightBarButtonItem = rightBarButton;
         stringInfo = @"";
     }];
}


#pragma mark- tabButtonActions
- (IBAction)buttonActionPortfolio:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[PortfolioViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    PortfolioViewController *portfolioView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
    [self.navigationController pushViewController:portfolioView animated:NO];
}


- (IBAction)buttonActionDiscip:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[DisciplineViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    DisciplineViewController *disciplineVw = [self.storyboard instantiateViewControllerWithIdentifier:@"disciplineView"];
    [self.navigationController pushViewController:disciplineVw animated:NO];
}

- (IBAction)buttonSearch:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[LocationFilterSearchViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    LocationFilterSearchViewController  *searchVw = [self.storyboard instantiateViewControllerWithIdentifier:@"searchVw"];
    [self.navigationController pushViewController:searchVw animated:NO];
}


- (IBAction)buttonMessages:(id)sender;
{
    MessagesViewController  *messageVw = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    [self.navigationController pushViewController:messageVw animated:NO];
}

- (IBAction)buttonActionSalary:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    
    for( int i=0;i<[viewControllers count];i++)
        
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[SalaryGuideViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    
    SalaryGuideViewController *salaryVw = [self.storyboard instantiateViewControllerWithIdentifier:@"salaryVw"];
    [self.navigationController pushViewController:salaryVw animated:NO];
    
}


@end

