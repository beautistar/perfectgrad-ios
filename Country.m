//
//  Country.m
//  PerfectGraduate
//
//  Created by NSPL on 2/3/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "Country.h"

@implementation Country
@synthesize category;
@synthesize name;


    + (id)countryOfCategory:(NSString *)category name:(NSString *)name
    {
        Country *newCountry = [[self alloc] init];
        [newCountry setCategory:category];
        [newCountry setName:name];
        return newCountry;
    }

@end
