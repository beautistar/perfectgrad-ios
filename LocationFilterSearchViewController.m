
//
//  LocationFilterSearchViewController.m
//  PerfectGraduate
//
//  Created by NSPL on 2/3/15.
//  Copyright (c) 2015 netset. All rights reserved.
//

#import "LocationFilterSearchViewController.h"
#import "SearchTableViewCell.h"
#import "LocationFilterSearchViewController.h"
#import "SelectCountryViewController.h"
#import "CompaniesDetailViewController.h"
#import "PortfolioViewController.h"
#import "MessagesViewController.h"
#import "SalaryGuideViewController.h"
#import "DisciplineViewController.h"
#import "UIImageView+AFNetworking.h"
#import "ApiClass.h"
#import "Globals.h"


@interface LocationFilterSearchViewController ()

{
    NSArray *arraysectionTitle;
    NSArray *arrayCompanyImageLogo;
    NSArray *arrayCompanyImageAwards;
    NSMutableArray *arrayCompanyName;
    NSArray *arrayApplicationFormStatus;
    NSMutableArray *arrayCompanylIndexTitles;
    NSDictionary *dictionaryCompanyName;
    NSMutableArray *mutableArrayFilteredCompanyName;
    BOOL searchEnableBool;
    BOOL sortEnableBool;
    NSDictionary *dicSorted;
    NSMutableArray *keyArray;
    NSMutableDictionary *dic;
    NSArray * arrayData;
    NSInteger indexOfCell;
    Database *dummy;
    int imgCount;
    UIActivityIndicatorView *spinner;
    UILabel *labelSubTitle;
    UIRefreshControl *refreshControlSearch;
    BOOL isBoolBasicCompanies;
    NSMutableArray *arrUserComapanies;
    NSArray * arrayAllCompanies;
    NSMutableArray *arrFilteredCompanies ;
    NSArray *arrayCountryListData;
}


@end

@implementation LocationFilterSearchViewController
@synthesize mutableArrayFilteredCompanyName;
@synthesize arraysectionTitle,locationFilterSearchBar,duebtn,companyBtn,awardsBtn;


- (void)viewDidLoad
{
    [ super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dummy = [[Database alloc] initWithDatabaseFilename:@"dummyOne.sqlite"];
    
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"filterConties"])
        
    {
        NSMutableArray *temp = [NSMutableArray new];
        
        temp = [[[NSUserDefaults standardUserDefaults]valueForKey:@"filterConties"]copy];
        
        NSMutableArray *tempAdd = [NSMutableArray new];
        
        arrayAllCompanies = [[NSArray alloc] initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]];
        
        for (int i=0; i<arrayAllCompanies.count; i++)
        {
            if ([[temp valueForKey:@"id"] containsObject:[arrayAllCompanies valueForKey:@"id"][i]])
            {
                [tempAdd addObject:arrayAllCompanies[i]];
            }
        }
        
        [[NSUserDefaults standardUserDefaults ]setObject:tempAdd forKey:@"filterConties"];
    }
    
    stringMesagFirstCome = @"stringFirst";
    
    stringFilterScreen = @"";
    
    arrCountryIds=[NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountry"]];
    
    arrStateIds =[NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryStates"]];
    
    if (arrCountryIds.count==0)
    {
        arrCountryIds = [[NSMutableArray alloc]init];
    }
    
    if (arrStateIds.count==0)
    {
        arrStateIds = [[NSMutableArray alloc]init];
    }
    
    if (self.view.frame.size.height == 736)
    {
        _imageRedStar.frame = CGRectMake(_imageRedStar.frame.origin.x-1.5, _imageRedStar.frame.origin.y, _imageRedStar.frame.size.width, _imageRedStar.frame.size.height);
    }
    
    self.imageRedStar.hidden = YES;
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"searchAlert"])
    {
        [self performSelectorOnMainThread:@selector(companyAlertMethod) withObject:nil waitUntilDone:NO];
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"searchAlert"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    arrUserComapanies = [NSMutableArray new];
    
    refreshControlSearch = [[UIRefreshControl alloc] init];
    
    refreshControlSearch.backgroundColor = [UIColor clearColor];
    
    refreshControlSearch.tintColor = [UIColor lightGrayColor];
    
    [refreshControlSearch addTarget:self
                             action:@selector(refershCompanies)
                   forControlEvents:UIControlEventValueChanged];
    
    [_locationFilterTableView addSubview:refreshControlSearch];
    
    [self addLeftButtons];
    
    [self addNavTitles];
    
    _locationFilterTableView.backgroundColor = [UIColor clearColor];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    sortEnableBool = YES;
    
    isInternetOff = NO;
    
    imgCount = 0;
    
    self.viewBottomBar.backgroundColor = [UIColor whiteColor];
    
    arrayCompanylIndexTitles = [[NSMutableArray alloc] init];
    companyBtn.layer.borderWidth = 1;
    awardsBtn.layer.borderWidth = 1;
    duebtn.layer.borderWidth = 1;
    companyBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    awardsBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    duebtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:@"Application Due Dates\n  (Victoria Graduates)"];
    
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:@"OpenSans" size:10.0f]
                  range:NSMakeRange(24, 11)];
    
    duebtn.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:10.0f];
    [duebtn setAttributedTitle:hogan forState:UIControlStateNormal];
    [[duebtn titleLabel] setNumberOfLines:0];
    [[duebtn titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];
    
    _labelCompany.layer.borderWidth = 0.5;
    _labelDueDate.layer.borderWidth = 0.5;
    
    _labelCompany.layer.borderColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f].CGColor;
    _labelDueDate.layer.borderColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f].CGColor;
    
    /*----Search bar frame allocation-----------*/
    CGRect rect = locationFilterSearchBar.frame;
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, rect.size.height-2,rect.size.width, 2)];
    lineView.backgroundColor = [UIColor whiteColor];
    [locationFilterSearchBar addSubview:lineView];
    
    /*-----Search Button----------*/
    UIButton * buttonSearch = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.origin.x-135,15, 30, 30)];
    
    UIImage *imageSearchButton = [UIImage imageNamed:@"top-icon"];
    buttonSearch.backgroundColor = [UIColor clearColor];/*--------------*/
    
    [buttonSearch setImage:imageSearchButton forState:UIControlStateNormal];
    [buttonSearch addTarget:self action:@selector(btnAction)forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:buttonSearch];
    
    searchEnableBool=NO;
    
    // add alphabets in array.....
    
    for (char a = 'A'; a <= 'Z'; a++)
    {
        [arrayCompanylIndexTitles addObject:[NSString stringWithFormat:@"%c", a]];
    }
    
    [arrayCompanylIndexTitles insertObject:@"#" atIndex:0];
    
    [self setDueDateText:[NSString stringWithFormat:@"Application Due Dates \n%@ %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"stateAbr"],[[NSUserDefaults standardUserDefaults] objectForKey:@"programName"]]];
    
    mutableArrayFilteredCompanyName=[[NSMutableArray alloc] init];
    
    [self ShowDataLoader:NO];
    [[self locationFilterTableView] reloadData];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor lightGrayColor]];
    
}


-(void)companyAlertMethod
{
    [[[UIAlertView alloc]initWithTitle:@"Company Search" message:@"\nTo help you learn more about companies and their hiring programs, you can scroll or search for companies of interest. \n\nUse the filter in the top corner to display companies based on location (i.e., local as well as international)." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
}



-(void)viewWillAppear:(BOOL)animated
{
    
    stringGlobalScreen = @"loactionScreen";
    
    stringNotifiction = @"fromCompany";
    
    stringViewGet= @"";
    
    stringForNotifyEditCompany = @"location";
    
    stringCheckDisciplines = @"";
    
    [super viewWillAppear:YES];
    
    if (fromLoadDisc == YES)
    {
        fromLoadDisc = NO;
        
        arrCountryIds=[NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountry"]];
        
        arrStateIds =[NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryStates"]];
        
        if (arrCountryIds.count==0)
        {
            arrCountryIds = [[NSMutableArray alloc]init];
        }
        
        if (arrStateIds.count==0)
        {
            arrStateIds = [[NSMutableArray alloc]init];
        }
    }
    
    if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"redStar"]isEqualToString:@"hide"] )
    {
        self.imageRedStar.hidden = YES;
    }
    else
    {
        self.imageRedStar.hidden = NO;
    }
    arrayCompanyName = [[NSMutableArray alloc] init];
    
    self.navigationController.navigationBarHidden =  NO;
    
    self.navigationItem.hidesBackButton = YES;
    
    self.navigationController.navigationBar.barTintColor =[UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    _locationFilterTableView.sectionIndexColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    
    [companyBtn setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+64, self.view.frame.size.width/3,38)];
    
    [awardsBtn setFrame:CGRectMake(companyBtn.frame.origin.x+companyBtn.frame.size.width, self.view.frame.origin.y+64, self.view.frame.size.width/3,38)];
    
    [duebtn setFrame:CGRectMake(awardsBtn.frame.origin.x+awardsBtn.frame.size.width, self.view.frame.origin.y+64, self.view.frame.size.width/3,38)];
    
    duebtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:@"Application Due Dates\n  (Victoria Graduates)"];
    
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:@"OpenSans-Bold" size:9.0f]
                  range:NSMakeRange(24, 11)];
    
    duebtn.titleLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:9.0f];
    [duebtn setAttributedTitle:hogan forState:UIControlStateNormal];
    [[duebtn titleLabel] setNumberOfLines:0];
    [[duebtn titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];
    [duebtn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    
    [_buttonSearch setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    
    _buttonSearch.backgroundColor = [UIColor clearColor];
    
    _labelSearchTitle.textColor = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
    NSArray *arrCountryName = (NSArray*)[dummy loaddataCountries:@"Select * From COUNTRIES" strForscreen:@"countiesList"];
    NSArray *arrStateName = [[NSArray alloc] initWithArray:[dummy loaddataStates:@"Select * From STATES"]];
    if(arrStateName.count == arrStateIds.count && arrCountryName.count == arrCountryIds.count){
        [[NSUserDefaults standardUserDefaults] setValue:@"allLocation" forKey:@"allLocation"];
    }
    
    for (UIView *subView in locationFilterSearchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews)
        {
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                
                searchBarTextField.textColor = [UIColor colorWithRed:0.0f/255.0f green:122.0f/255.0f blue:255.0f/255.0f alpha:1];
                break;
            }
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveTestNotification:) name:@"companyAddedNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(redStarShow)
                                                 name:@"redStarShow"
                                               object:nil];
    
    [self refershCompanies];
    
}


-(void)getDataAccordingFilterValues
{
    NSMutableArray *arrayTemp = [[NSMutableArray alloc]init];
    
    arrayTemp = [[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountry"];
    
    if (arrayTemp.count==0)
    {
        for (int i =0; i<arrayCountryListData.count; i++)
        {
            
        }
        
        [[NSUserDefaults standardUserDefaults]setObject:arrCountryIds forKey:@"arryCountry"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arryCountry"];
        
        [[NSUserDefaults standardUserDefaults]setObject:arrCountryIds forKey:@"arryCountry"];
    }
    
    NSLog(@"user default is %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountry"]);
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    arrCountryIds = [[NSMutableArray alloc]init];
    
    arrCountryIds=[NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryCountry"]];
    
    NSMutableArray *arrayTemp1 = [[NSMutableArray alloc]init];
    
    arrayTemp1 = [[NSUserDefaults standardUserDefaults]objectForKey:@"arryStates"];
    
    if (arrayTemp1.count==0)
    {
        [[NSUserDefaults standardUserDefaults]setObject:arrStateIds forKey:@"arryStates"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"arryStates"];
        
        [[NSUserDefaults standardUserDefaults]setObject:arrStateIds forKey:@"arryStates"];
    }
    
    NSLog(@"user default is %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"arryStates"]);
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    arrStateIds = [[NSMutableArray alloc]init];
    
    arrStateIds =[NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"arryStates"]];
}

-(void)redStarShow
{
    self.imageRedStar.hidden = NO;
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"companyAddedNotification"])
        NSLog (@"Successfully received the test notification!");
    if (arrCountryIds.count > 0 || arrStateIds.count>0)
    {
        [self getFilterCompaniesSearch];
    }
    else
    {
        [self getAllCompaniesSearch];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    arrayCompanyName = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"companyAddedNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"redStarShow" object:nil];
}

-(void)setTextToNavTitles
{
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"allLocation"]isEqualToString:@"allLocation"])
    {
        labelSubTitle.text = @"All Locations";
    }
    else
    {
        if(arrStateIds.count==[arrayData count]&&[arrCountryIds count]==[arrayCountryData count]){
            labelSubTitle.text = @"All Locations";
        }
        else if ([arrCountryIds count]>0&&arrStateIds.count>0)
        {
            labelSubTitle.text = [NSString stringWithFormat:@"%@, %@",[[arrStateIds valueForKey:@"abr"] componentsJoinedByString:@", "],[[arrCountryIds valueForKey:@"abr"] componentsJoinedByString:@", "]];
        }
        else if (arrStateIds.count==0&&[arrCountryIds count]>0)
        {
            labelSubTitle.text = [[arrCountryIds valueForKey:@"abr"] componentsJoinedByString:@", "];
        }
        else if (arrStateIds.count==0&&[arrCountryIds count]==0)
        {
            labelSubTitle.text = @"All Locations";
        }
        else if (arrStateIds.count>0)
        {
            labelSubTitle.text = [[arrStateIds valueForKey:@"abr"] componentsJoinedByString:@", "];
        }
    }
}

-(void)setDueDateText:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    
    NSString *boldString = @"Application Due Dates";
    if ([string isEqualToString:@"Application Due Dates \n( )"])
    {
        string = [string stringByReplacingOccurrencesOfString:@"\n( )" withString:@""];
        
        [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"\nPlease update your profile again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
    }
    
    NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:string];
    
    if ([string isEqualToString:@""])
    {
        yourAttributedString = [[NSMutableAttributedString alloc]initWithString:boldString];
    }
    else
    {
        NSRange boldRange = [string rangeOfString:boldString];
        
        [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:@"OpenSans" size:10.0f] range:NSMakeRange(0,string.length)];
        
        [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont fontWithName:@"OpenSans-Bold" size:10.0f] range:boldRange];
    }
    [_labelDueDate setAttributedText: yourAttributedString];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadStatesAndCountry
{
    NSArray *arrAllStates = [[NSArray alloc] initWithArray:[dummy loaddataStates:@"Select * From STATES"]];
    
    NSMutableArray *arrSortedStates = [[NSMutableArray alloc] init];
    
    for (int i=0; i<[arrAllStates count]; i++)
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"countryId"] integerValue]==[[[arrAllStates valueForKey:@"country_id"] objectAtIndex:i] integerValue])
        {
            [arrSortedStates addObject:[arrAllStates objectAtIndex:i]];
        }
    }
    
    
    NSArray *arrSStates = [arrSortedStates sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES]]];
    [[NSUserDefaults standardUserDefaults] setObject:arrSStates forKey:@"OriginalStateData"];
    NSArray *arrayCountryName = (NSArray*)[dummy loaddataCountries:@"Select * From COUNTRIES" strForscreen:@"countiesList"];
    [[NSUserDefaults standardUserDefaults]setObject:arrayCountryName forKey:@"OriginalCountryData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self filterArray];
}

-(void)SetHeader{
    NSArray *arrOriginalS = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalStateData"];
    NSArray *arrOriginalC = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalCountryData"];
    arrFinalStateData = [[NSUserDefaults standardUserDefaults] objectForKey:@"FinalStateDataSearch"];
    arrFinalCoutryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"FinalCountryDataSearch"];
    if((arrOriginalS.count == arrFinalStateData.count && arrOriginalC.count == arrFinalCoutryData.count) || (arrFinalStateData.count == 0 && arrFinalCoutryData.count == 0) || (arrOriginalS.count == 0 && arrOriginalC.count == 0)){
        labelSubTitle.text = @"All Locations";
        isAllLocationSelected = YES;
    }else{
        NSString *strIds = @"";
        isAllLocationSelected = NO;
        for (NSDictionary *dict in arrFinalStateData) {
            if([dict objectForKey:@"abr"]){
                NSString *strName = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"abr"]] removeNull];
                if([strIds isEqualToString:@""]){
                    strIds = [NSString stringWithFormat:@"%@",strName];
                }else{
                    strIds = [NSString stringWithFormat:@"%@,%@",strIds,strName];
                }
            }
        }
        for (NSDictionary *dict in arrFinalCoutryData) {
            if([dict objectForKey:@"abr"]){
                NSString *strName = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"abr"]] removeNull];
                if([strIds isEqualToString:@""]){
                    strIds = [NSString stringWithFormat:@"%@",strName];
                }else{
                    strIds = [NSString stringWithFormat:@"%@,%@",strIds,strName];
                }
            }
        }
        labelSubTitle.text = strIds;
    }
}

-(void)refershCompanies
{
    [self ShowDataLoader:YES];
    
    [self loadStatesAndCountry];
    if(![Utilities CheckInternetConnection])
    {
        if ([refreshControlSearch isRefreshing])
        {
            [self ShowDataLoader:NO];
            [refreshControlSearch endRefreshing];
        }
        
    }else{
        if(arrayData.count==0){
            [self getAllCompaniesSearch];
        }else{
            if ([refreshControlSearch isRefreshing])
            {
                [self ShowDataLoader:NO];
                [refreshControlSearch endRefreshing];
            }
        }
    }
}

-(void)filterArray{
    [self SetHeader];
    
    NSArray *arrComapnies = [[NSArray alloc] initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]];
    NSLog(@"tttdata:%@",arrComapnies);
    if(arrComapnies.count==0){
        [self getAllCompaniesSearch];
        return;
    }
    NSMutableArray *arrDataCom = [[NSMutableArray alloc]init];
    NSArray *arrSIds;
    NSArray *arrCIds;
    NSArray *arrOriginalS = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalStateData"];
    NSArray *arrOriginalC = [[NSUserDefaults standardUserDefaults] objectForKey:@"OriginalCountryData"];
    if((arrFinalStateData.count == 0 && arrFinalCoutryData.count == 0)){
        arrSIds = [arrOriginalS valueForKey:@"name"];
        arrCIds = [arrOriginalC valueForKey:@"name"];
    }else{
        arrSIds = [arrFinalStateData valueForKey:@"name"];
        arrCIds = [arrFinalCoutryData valueForKey:@"name"];
    }
    for(NSDictionary *dict in arrComapnies){
        NSString *strCId = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"internationl"]] removeNull];
        NSString *strSId = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"regstates"]] removeNull];
        BOOL isDataAdd = NO;
        if(![strSId isEqualToString:@""]){
            NSArray *arrayRegState = [[strSId componentsSeparatedByString:@","] mutableCopy];
            
            for (NSString *strState in arrayRegState){
                if([arrSIds containsObject:strState]){
                    if(![arrDataCom containsObject:dict]){
                        isDataAdd = YES;
                    }
                }
            }
        }else{
            if(isAllLocationSelected){
                if(![arrDataCom containsObject:dict]){
                    isDataAdd = YES;
                }
            }
        }
        if(![strCId isEqualToString:@""]){
            NSArray *arrayInternational = [[strCId componentsSeparatedByString:@","] mutableCopy];
            for (NSString *strInter in arrayInternational){
                NSArray *arrInter = [strInter componentsSeparatedByString:@" ("];
                if (arrInter.count>0) {
                    NSString *strCountry = [arrInter objectAtIndex:0];
                    if([arrCIds containsObject:strCountry]){
                        if(![arrDataCom containsObject:dict]){
                            isDataAdd = YES;
                        }
                    }
                }
            }
            
            if(isAllLocationSelected){
                if(![arrDataCom containsObject:dict]){
                    isDataAdd = YES;
                }
            }
            //vijay008
        }else{
            if(isAllLocationSelected){
                if(![arrDataCom containsObject:dict]){
                    isDataAdd = YES;
                }
            }
        }
        if(isDataAdd){
            [arrDataCom addObject:dict];
        }
    }
    [self sortUserCompanies:arrDataCom];
    [self ShowDataLoader:NO];
    
}

-(void)sortingMethods{
    NSArray *arrayWithoutDuplicates = [[NSOrderedSet orderedSetWithArray:[arrUserComapanies valueForKey:@"company_name"]] array];
    arrayCompanyName = [arrayWithoutDuplicates mutableCopy];
    NSArray*  sortedArray = [arrayCompanyName sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    arrayData = [arrUserComapanies copy];
    dicSorted = [self fillingDictionary:[sortedArray mutableCopy]];
}

#pragma mark- reachabilityCheck
-(void)reachabilityCheck
{
    if(![Utilities CheckInternetConnection])
    {
        NSLog(@"notConnected");
        
        arrayAllCompanies = nil;
        
        arrayAllCompanies = [[NSArray alloc] initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]];
        
        [self sortUserCompanies:arrayAllCompanies];
    }
    else
    {
        NSLog(@"connected");
        
        [self loadAllCompaniesOffline];
    }
}

#pragma mark- loadAllCompaniesOfflineWebService

-(void)loadAllCompaniesOffline
{
    isInternetOff = YES;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"callallCompanyWebService"])
    {
        if (boolNotifyEditCompanyList==YES)
        {
            arrayAllCompanies = nil;
            
            arrayAllCompanies = [[NSArray alloc] initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]];
            
            [self sortUserCompanies:arrayAllCompanies];
            
            if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"allLocation"]isEqualToString:@"allLocation"])
            {
                
                if([[NSArray alloc] initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]].count>0)
                {
                    
                    arrayAllCompanies = nil;
                    
                    arrayAllCompanies = [[NSArray alloc] init];
                    
                    arrayAllCompanies = [[NSUserDefaults standardUserDefaults]objectForKey:@"allCompanies"];
                    
                    [self sortUserCompanies:arrayAllCompanies];
                }
                else
                {
                    [self getAllCompaniesSearch];
                }
            }
            else if (arrCountryIds.count>0||arrStateIds.count>0)
            {
                arrayAllCompanies = nil;
                
                arrayAllCompanies = [[[NSUserDefaults standardUserDefaults]valueForKey:@"filterConties"]copy];
                
                [self sortUserCompanies:arrayAllCompanies];
                
            }
            else
            {
                [self performSelectorInBackground:@selector(getAllCompaniesSearch) withObject:nil];
            }
        }
        else
        {
            if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"allLocation"]isEqualToString:@"allLocation"])
            {
                if([[NSArray alloc] initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]].count>0)
                {
                    arrayAllCompanies = nil;
                    
                    arrayAllCompanies = [[NSArray alloc] init];
                    
                    arrayAllCompanies = [[NSUserDefaults standardUserDefaults]objectForKey:@"allCompanies"];
                    
                    [self sortUserCompanies:arrayAllCompanies];
                }
                else
                {
                    [self getAllCompaniesSearch];
                }
            }
            
            else if (arrCountryIds.count>0||arrStateIds.count>0)
            {
                arrayAllCompanies = nil;
                
                arrayAllCompanies = [[[NSUserDefaults standardUserDefaults]valueForKey:@"filterConties"]copy];
                
                [self sortUserCompanies:arrayAllCompanies];
                
            }
            else
            {
                [self getAllCompaniesSearch];
            }
        }
    }
    else
    {
        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"allLocation"]isEqualToString:@"allLocation"])
        {
            if([[NSArray alloc] initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]].count>0)
            {
                arrayAllCompanies = nil;
                
                arrayAllCompanies = [[NSArray alloc] init];
                
                arrayAllCompanies = [[NSUserDefaults standardUserDefaults]objectForKey:@"allCompanies"];
                
                [self sortUserCompanies:arrayAllCompanies];
            }
            else
            {
                [self getAllCompaniesSearch];
            }
        }
        
        else  if (arrCountryIds.count>0||arrStateIds.count>0)
        {
            arrayAllCompanies = nil;
            
            arrayAllCompanies = [[[NSUserDefaults standardUserDefaults]valueForKey:@"filterConties"]copy];
            
            [self sortUserCompanies:arrayAllCompanies];
            
        }
        
        else
        {
            
            if ([[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"] count]>0)
            {
                arrayAllCompanies = nil;
                
                arrayAllCompanies = [[NSArray alloc] init];
                
                arrayAllCompanies = [[NSUserDefaults standardUserDefaults]objectForKey:@"allCompanies"];
                
                [self sortUserCompanies:arrayAllCompanies];
            }
            else
            {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"allLocation"]isEqualToString:@"allLocation"])
                    {
                        if([[NSArray alloc] initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]].count>0)
                        {
                            arrayAllCompanies = nil;
                            
                            arrayAllCompanies = [[NSArray alloc] init];
                            
                            arrayAllCompanies = [[NSUserDefaults standardUserDefaults]objectForKey:@"allCompanies"];
                            
                            [self sortUserCompanies:arrayAllCompanies];
                        }
                        else
                        {
                            [self getAllCompaniesSearch];
                        }
                    }
                    if (arrCountryIds.count>0||arrStateIds.count>0)
                    {
                        arrayAllCompanies = nil;
                        
                        arrayAllCompanies = [[[NSUserDefaults standardUserDefaults]valueForKey:@"filterConties"]copy];
                        
                        [self sortUserCompanies:arrayAllCompanies];
                        
                    }
                    else
                    {
                        [self getAllCompaniesSearch];
                    }
                });
            }
            
        }
    }
    
    [self sortingMethods];
}


#pragma mark- addIndicatorToView

-(void)ShowDataLoader:(BOOL)set
{
    if (set)
    {
        if (!spinner)
        {
            spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        [self.view.window setUserInteractionEnabled:NO];
        spinner.center = self.view.center;
        spinner.color = [UIColor colorWithRed:10.0f/255.0f green:77.0f/255.0f blue:170.0f/255.0f alpha:1.0f];
        [spinner startAnimating];
        [self.view addSubview:spinner];
    }
    else
    {
        [self.view.window setUserInteractionEnabled:YES];
        [spinner stopAnimating];
        [spinner removeFromSuperview];
        spinner = nil;
    }
}

#pragma mark- AddNavigationTitles

-(void)addNavTitles
{
    UIView  *textView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    UILabel *labelTitleName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 0, CGRectGetWidth(textView.frame), 22)];
    labelTitleName.backgroundColor = [UIColor clearColor];
    labelTitleName.font = [UIFont fontWithName:@"OpenSans" size:18.0f];
    labelTitleName.textAlignment = NSTextAlignmentCenter;
    labelTitleName.textColor = [UIColor whiteColor];
    labelTitleName.text = @"Company Search";
    
    /*----UILabel Sub-title Name-------*/
    labelSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(textView.frame), 22, CGRectGetWidth(textView.frame), 18)];
    labelSubTitle.backgroundColor = [UIColor clearColor];
    labelSubTitle.font = [UIFont fontWithName:@"OpenSans" size:12.0f];
    labelSubTitle.textAlignment = NSTextAlignmentCenter;
    labelSubTitle.textColor = [UIColor whiteColor];
    
    
    [textView addSubview:labelTitleName];
    [textView addSubview:labelSubTitle];
    self.navigationItem.titleView=textView;
}

#pragma mark- SelfCreatedButtons
-(void)addLeftButtons
{
    UIView *viewAddBtnsLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 40)];
    
    UIButton * buttonMenu = [[UIButton alloc]initWithFrame:CGRectMake(-3, 4, 30, 30) ];
    
    
    [buttonMenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    
    buttonMenu.backgroundColor = [UIColor clearColor];
    
    [buttonMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    [viewAddBtnsLeft addSubview:buttonMenu];
    
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:viewAddBtnsLeft];
    
}

#pragma mark- WebServiceGetCompanies

-(void)getFilterCompaniesSearch
{
    if (boolNotifyEditCompanyList==YES)
    {
        [self ShowDataLoader:NO];
        
        boolNotifyEditCompanyList = NO;
    }
    else
    {
        if (![refreshControlSearch isRefreshing])
            
        { [self ShowDataLoader:YES];
        }
    }
    
    NSMutableString *stringCntry = [[NSMutableString alloc]init];
    
    NSMutableString *stringState = [[NSMutableString alloc]init];
    
    for (int i = 0; i<arrStateIds.count; i++)
    {
        if (i == arrStateIds.count-1)
        {
            [stringState appendString:[NSString stringWithFormat:@"%@",[[arrStateIds valueForKey:@"id"] objectAtIndex:i]]];
        }
        else
        {
            [stringState appendString:[NSString stringWithFormat:@"%@",[[arrStateIds valueForKey:@"id"] objectAtIndex:i]]];
            [stringState appendString:@","];
        }
    }
    
    if (arrStateIds.count==0)
    {
        stringState = [NSMutableString stringWithString:@""];
    }
    
    for (int i = 0; i<arrCountryIds.count; i++)
    {
        if (i == arrCountryIds.count-1)
        {
            [stringCntry appendString:[NSString stringWithFormat:@"%@",[[arrCountryIds valueForKey:@"id"] objectAtIndex:i]]];
        }
        
        else
        {
            [stringCntry appendString:[NSString stringWithFormat:@"%@",[[arrCountryIds valueForKey:@"id"] objectAtIndex:i]]];
            [stringCntry appendString:@","];
        }
        
    }
    
    NSLog(@"string cntry id %@", stringCntry);
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:[NSURL URLWithString:baseUrl]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *dict =@{@"country_id":stringCntry,@"state_id":stringState,@"user_id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [manager POST:@"companyFilter" parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     {
         if ([refreshControlSearch isRefreshing])
         {
             [refreshControlSearch endRefreshing];
         }
         else
         {
             [self ShowDataLoader:NO];
         }
         
         [arrayCompanyName removeAllObjects];
         
         arrayData = nil;
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             arrayAllCompanies = nil;
             
             arrayAllCompanies = [NSArray new];
             
             arrayAllCompanies = [responseObject valueForKeyPath:@"data"];
             
             
             [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"callallCompanyWebService"];
             
             [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"fromDiscipline"];
             
         }
         else
         {
             if ([[responseObject objectForKey:@"data"]isEqualToString:@"no result"])
             {
                 arrayAllCompanies = [[NSArray alloc]init];
                 
                 arrayData = [[NSArray alloc]init];
             }
         }
         
         [self ShowDataLoader:NO];
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         isInternetOff = YES;
         
         arrayAllCompanies = nil;
         
         arrayAllCompanies = [[NSArray alloc] initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]];
         
         [self sortUserCompanies:arrayAllCompanies];
         
         {
             [refreshControlSearch endRefreshing];
             
             if ([error.localizedDescription isEqualToString:@"The network connection was lost."])
             {
                 //[[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your connection and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:@"\nPlease check your internet settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
             else
             {
                 [[[UIAlertView alloc]initWithTitle:@"Please try again" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil]show];
             }
         }
         [self ShowDataLoader:NO];
     }];
}

-(void)getAllCompaniesSearch
{
    if (boolNotifyEditCompanyList==YES)
    {
    }
    else
    {
        
        if (![refreshControlSearch isRefreshing])
        {
            [self ShowDataLoader:YES];
        }
        
    }
    
    NSString* urlStr =  [NSString stringWithFormat:@"%@getAllCompanies",baseUrl];
    
    NSDictionary *dicParam = @{@"id":[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]};
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:dicParam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([refreshControlSearch isRefreshing])
         {
             [refreshControlSearch endRefreshing];
         }
         [arrayCompanyName removeAllObjects];
         
         arrayData = nil;
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             arrayAllCompanies = nil;
             
             arrayAllCompanies = [NSArray new];
             
             arrayAllCompanies = [responseObject valueForKeyPath:@"data"];
             
             [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"callallCompanyWebService"];
             
             [self saveSearchCompanies];
             
             NSArray *arrCompanies = [[NSArray alloc]initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]];
             
             NSArray *arrayTemp = [dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"];
             
             NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
             
             NSArray* sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
             
             NSArray *arraySorted = [[NSArray alloc] initWithArray:[arrayTemp sortedArrayUsingDescriptors:sortDescriptors]];
             
             [arrayCompaniesList removeAllObjects];
             
             arrayCompaniesList = [arraySorted mutableCopy];
             
             [[NSUserDefaults standardUserDefaults]setObject:arrCompanies forKey:@"allCompanies"];
             
             for (int i=0; i<[arrayAllCompanies count]; i++)
             {
                 for (int k=0; k<[[[arrayAllCompanies objectAtIndex:i] valueForKeyPath:@"disciplines"] count]; k++)
                 {
                     for (int j=0; j<[[[[arrayAllCompanies objectAtIndex:i] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:k] count]; j++)
                     {
                         
                     }
                 }
             }
         }
         [self ShowDataLoader:NO];
     }
     
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [self ShowDataLoader:NO];
         
         isInternetOff = YES;
         
         arrayAllCompanies = nil;
         
         arrayAllCompanies = [[NSArray alloc] initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]];
         
         [self sortUserCompanies:arrayAllCompanies];
         
         if ([spinner isAnimating])
         {
             [self ShowDataLoader:NO];
         }
     }];
    
}

-(void)sortUserCompanies :(NSArray *)arrAllData
{
    [arrUserComapanies removeAllObjects];
    arrUserComapanies = [arrAllData mutableCopy];
    
    NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
    NSArray* sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    arrayData = [[NSArray alloc] initWithArray:[arrUserComapanies sortedArrayUsingDescriptors:sortDescriptors]];
    
    if (arrayData.count>0)
    {
        [self sortCompaniesByName];
        [self sortingMethods];
        
        [self ShowDataLoader:NO];
        [_locationFilterTableView reloadData];
    }
    
    else if(arrAllData.count == 0)
    {
        sortEnableBool = NO;
        
        [self ShowDataLoader:NO];
        [_locationFilterTableView reloadData];
    }
}

-(void)getAllStates
{
    NSString* urlStr =  [NSString stringWithFormat:@"%@getAllS",baseUrl];
    
    [[ApiClass getSharedInstance] PostWebserviceCall:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"true"])
         {
             NSArray *arraySt = [[NSArray alloc] initWithArray:[responseObject objectForKey:@"data"]];
             if ([dummy deleteTable:@"delete from STATES"])
             {
                 for (int i=0; i<[arraySt count]; i++)
                 {
                     NSString *query;
                     query = [NSString stringWithFormat: @"INSERT INTO STATES (id,name,country_id,abr) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                              ,[[arraySt valueForKey:@"id"] objectAtIndex:i]
                              ,[[arraySt valueForKey:@"name"] objectAtIndex:i]
                              ,[[arraySt valueForKey:@"country_id"] objectAtIndex:i]
                              ,[[arraySt valueForKey:@"abr"] objectAtIndex:i]];
                     
                     const char *query_stmt = [query UTF8String];
                     [dummy  runQueries:query_stmt strForscreen:@"StatesList" ];
                 }
             }
             
         }
         else
         {
             NSLog(@"wrong result");
         }
     }
     
                                             failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
     }];
    
}

-(void)saveSearchCompanies
{
    if ([dummy deleteTable:@"delete from COMPANIES"])
    {
        if ([dummy deleteTable:@"delete from COMPANYLOCATIONS"])
        {
            if ([dummy deleteTable:@"delete from DispSubDispCompaniesTable"])
            {
                if ([dummy deleteTable:@"delete from SearchCompSubDisciplineTable"])
                {
                    if ([dummy deleteTable:@"delete from SearchCompanyAwards"])
                    {
                        if ([dummy deleteTable:@"delete from SearchCompanyDueDate"])
                        {
                            if ([dummy deleteTable:@"delete from SearchQuesAnsTable"])
                            {
                                if ([dummy deleteTable:@"delete from SearchCompSocialLinksTable"])
                                {
                                    for (int i=0; i<[arrayAllCompanies count]; i++)
                                    {
                                        
                                        NSString *query = [NSString stringWithFormat: @"INSERT INTO COMPANIES (id,application_due_date, awards,city,company_address,company_name,description,logo,state_id,regstates,country_id,company_intro,country,portfolio_status,message_state,message_status, website,international,unread_message) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")"
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"id"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"application_due_date"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"awards"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"city"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"company_address"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"company_name"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"description"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"logo"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"state_id"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"regstates"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"country_id"]
                                                           ,[[arrayAllCompanies  objectAtIndex:i] valueForKey:@"company_intro"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"country"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"portfolio_status"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"message_state"]
                                                           ,[[arrayAllCompanies objectAtIndex:i] valueForKey:@"message_status"],
                                                           [[arrayAllCompanies objectAtIndex:i] valueForKey:@"website"],[[arrayAllCompanies valueForKey:@"international"] objectAtIndex:i],[[arrayAllCompanies valueForKey:@"unread_message"] objectAtIndex:i]];
                                        
                                        [self saveImages:[NSString stringWithFormat:@"%@%@",[[arrayAllCompanies  objectAtIndex:i] valueForKeyPath:@"id"],[[arrayAllCompanies  objectAtIndex:i] valueForKeyPath:@"logo"]] imageUrl:[[arrayAllCompanies  objectAtIndex:i] valueForKeyPath:@"logo"]];
                                        
                                        const char *query_stmt = [query UTF8String];
                                        [dummy  runQueries:query_stmt strForscreen:@"search"];
                                        
                                        [self saveDueDateSearchCompanies:i];
                                        [self saveCompanyLocations:i];
                                        [self saveCompanyDisciplines:i];
                                        [self saveSearchCompanyAwards:i];
                                        [self saveQuestionsSearchComp:i];
                                        [self saveSearchCompanyLinks:i];
                                        [self saveCompanySubDisciplines:i];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self getAllStates];
        [self loadStatesAndCountry];
    });
    
    isInternetOff = YES;
    
    arrayAllCompanies = nil;
    
    arrayAllCompanies = [[NSArray alloc] initWithArray:[dummy loaddata:@"Select * From COMPANIES" strForscreen:@"search"]];
    
    
    if (locationFilterSearchBar.text.length>0)
    {
        [self searchBar:locationFilterSearchBar textDidChange:locationFilterSearchBar.text];
    }
    
}

-(void)saveSearchCompanyLinks:(int)index
{
    if ([[[arrayAllCompanies valueForKey:@"links"] objectAtIndex:index]isKindOfClass:[NSNull class]])
    {
        
    }
    else
    {
        for (int k=0; k<[[[arrayAllCompanies valueForKey:@"links"] objectAtIndex:index] count]; k++)
        {
            NSString *subQueryLinks;
            
            subQueryLinks = [NSString stringWithFormat: @"INSERT INTO SearchCompSocialLinksTable (company_id,id,media_link,media_logo,media_type) VALUES (\"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                             ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.company_id"] objectAtIndex:k]
                             ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.id"] objectAtIndex:k]
                             ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.media_link"] objectAtIndex:k]
                             ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.media_logo"] objectAtIndex:k]
                             ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"links.media_type"] objectAtIndex:k]];
            
            [self saveImages:[[NSString stringWithFormat:@"%@%@",[[[arrayAllCompanies valueForKeyPath:@"links.id"] objectAtIndex:index] objectAtIndex:k],[[[arrayAllCompanies valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                    imageUrl:[[[arrayAllCompanies valueForKeyPath:@"links.media_logo"] objectAtIndex:index] objectAtIndex:k]];
            
            const char *query_stmt_SocialLinks = [subQueryLinks UTF8String];
            [dummy runQuerySocialLinksForCompanies:query_stmt_SocialLinks strForscreen:@"search"];
        }
    }
}


-(void)saveSearchCompanyAwards:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKey:@"allAwards"] count]; k++)
    {
        NSString *subQueryAwards;
        
        
        subQueryAwards = [NSString stringWithFormat: @"INSERT INTO SearchCompanyAwards (awards,company_id,description, id) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.awards"] objectAtIndex:k]
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.company_id"] objectAtIndex:k]
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.description"] objectAtIndex:k]
                          ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"allAwards.id"] objectAtIndex:k]];
        
        const char *query_stmt_Awards= [subQueryAwards UTF8String];
        
        [dummy runSubQueryToSaveAwards:query_stmt_Awards strForscreen:@"SearchVw"];
    }
}

-(void)saveDueDateSearchCompanies:(int)index
{
    for (int j=0; j<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all"] count]; j++)
    {
        NSString *subQueryDueDate;
        
        subQueryDueDate = [NSString stringWithFormat:@"INSERT INTO SearchCompanyDueDate(close_date,company_id,id,program,program_id,start_date,state,country) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\", \"%@\", \"%@\")"
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.close_date"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.company_id"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.id"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.program"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.program_id"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.start_date"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.state"] objectAtIndex:j]
                           ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"application_due_date_all.country"] objectAtIndex:j]];
        
        const char *query_stmt_dueDate = [subQueryDueDate UTF8String];
        
        [dummy runSubQueryToSaveDueDate:query_stmt_dueDate strForscreen:@"Search"];
    }
}


-(void)saveCompanySubDisciplines:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines"] count]; k++)
    {
        NSString *subQuery;
        
        NSArray *arraySubDisciplineId = [[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline.sub_discipline_id"] objectAtIndex:k];
        
        for (int i=0; i<arraySubDisciplineId.count; i++)
        {
            subQuery = [NSString stringWithFormat: @"INSERT INTO DispSubDispDataTable (company_id,discipline_id,name,sub_discipline_id) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                        ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:k]
                        ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.discipline_id"] objectAtIndex:k]
                        ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.name"] objectAtIndex:k],[arraySubDisciplineId objectAtIndex:i]];
            
            const char *query_stmt_disciplines = [subQuery UTF8String];
            [dummy runQueryDispSubDispFordisp:query_stmt_disciplines strForscreen:@"searchCompanyDisciplines"];
        }
    }
}


-(void)saveCompanyDisciplines:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines"] count]; k++)
    {
        NSString *subQuery;
        subQuery = [NSString stringWithFormat: @"INSERT INTO DispSubDispCompaniesTable (company_id,discipline_id,name,type) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.discipline_id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.name"] objectAtIndex:k],[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.type"] objectAtIndex:k]];
        
        const char *query_stmt_disciplines = [subQuery UTF8String];
        [dummy runQueryDispSubDispForCompanies:query_stmt_disciplines strForscreen:@"searchCompanyDisciplines"];
        [self saveSubDisciplinesPort:index disciplineTag:k];
    }
}

-(void)saveSubDisciplinesPort:(int)index disciplineTag:(int)disciplineTag
{
    for (int j=0; j<[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag] count]; j++)
    {
        NSString *subQuery;
        
        subQuery = [NSString stringWithFormat: @"INSERT INTO SearchCompSubDisciplineTable (discipline_id,name,company_id,sub_discipline_id) VALUES (\"%@\",\"%@\",\"%@\",\"%@\")"
                    ,[[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"discipline_id"] objectAtIndex:j]
                    ,[[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"name"] objectAtIndex:j]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.company_id"] objectAtIndex:disciplineTag],
                    [[[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"disciplines.sub_discipline"] objectAtIndex:disciplineTag]valueForKey:@"sub_discipline_id"] objectAtIndex:j]];
        
        const char *query_stmt_PortSubDiscip = [subQuery UTF8String];
        
        
        [dummy runQuerySubDisciplines:query_stmt_PortSubDiscip strForscreen:@"SubDiscipSearchCompanies"];
    }
}

-(void)saveCompanyLocations:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location"] count]; k++)
    {
        NSString *subQuery = [NSString stringWithFormat: @"INSERT INTO COMPANYLOCATIONS (company_id,id,branch_name, lat,long,location) VALUES (\"%@\",\"%@\", \"%@\", \"%@\",\"%@\", \"%@\")"
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.company_id"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.id"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.branch_name"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.lat"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.long"] objectAtIndex:k]
                              ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"location.location"] objectAtIndex:k]];
        
        const char *query_stmtLocation = [subQuery UTF8String];
        [dummy runSubQueryLocations:query_stmtLocation strForscreen:@"COMPANYLOCATIONS"];
    }
}

-(void)saveQuestionsSearchComp:(int)index
{
    for (int k=0; k<[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns"] count]; k++)
    {
        NSString *subQuery;
        subQuery = [NSString stringWithFormat: @"INSERT INTO SearchQuesAnsTable (answer,company_id,id,question) VALUES (\"%@\",\"%@\", \"%@\", \"%@\")"
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.answer"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.company_id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.id"] objectAtIndex:k]
                    ,[[[arrayAllCompanies objectAtIndex:index] valueForKeyPath:@"quesAns.question"] objectAtIndex:k]];
        
        const char *query_stmt_QuesAns = [subQuery UTF8String];
        [dummy runQueryQuesAnsForCompanies:query_stmt_QuesAns strForscreen:@"search"];
    }
}

-(void)saveImages:(NSString *)ImageName imageUrl :(NSString *)imageUrl
{
    ImageName = [ImageName stringByReplacingOccurrencesOfString:@"/" withString:@""];
    
    if (imageUrl.length>0)
    {
        if (![self imageCheck:ImageName])
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:ImageName];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,imageUrl]]];
            
            [imageData writeToFile:savedImagePath atomically:NO];
        }
    }
}

#pragma mark- CheckForImageInDataBase

-(BOOL)imageCheck :(NSString *)strName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    if ([contents containsObject:strName])
    {
        return YES;
    }
    return NO;
}

#pragma mark- Filling dictionary show data according to section name--------

-(NSMutableDictionary *)fillingDictionary:(NSMutableArray *)ary
{
    keyArray=[[NSMutableArray alloc]init];
    [keyArray removeAllObjects];
    dic=[[NSMutableDictionary alloc]init];
    
    NSArray *arryDigit =[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"0", nil];
    
    for(NSString *str in ary)
    {
        char charval=[str characterAtIndex:0];   // Get the first character of your string which will be your key
        NSString *new=[NSString stringWithFormat:@"%c",charval];
        NSString *charStr=[NSString stringWithFormat:@"%@",new];
        BOOL isUppercase = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:[charStr characterAtIndex:0]];
        
        if (isUppercase!=YES)
        {
            NSString *uppercase = [charStr uppercaseString];
            charStr=uppercase;
        }
        
        if ([arryDigit containsObject:charStr])
        {
            charStr = @"#";
        }
        
        if(![keyArray containsObject:charStr])
        {
            NSMutableArray *charArray=[[NSMutableArray alloc]init];
            // Code for show # when digit name comes in company
            
            [charArray addObject:str];
            [keyArray addObject:charStr];
            [dic setValue:charArray forKey:charStr];
        }
        
        else
        {
            NSMutableArray *prevArray=(NSMutableArray *)[dic valueForKey:charStr];
            [prevArray addObject:str];
            NSLog(@"%@", [[arrayData valueForKey:@"id"] objectAtIndex:[[arrayData valueForKey:@"company_name"] indexOfObject:str]]);
            [dic setValue:prevArray forKey:charStr];
        }
    }
    return dic;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark- UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (searchEnableBool)
    {
        return 1;
    }
    else
    {
        if (sortEnableBool)
        {
            return [[[dicSorted allKeys]sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)] count];
        }
        
        else
            return 1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (searchEnableBool)
    {
        return [mutableArrayFilteredCompanyName count];
    }
    else
    {
        if (sortEnableBool)
        {
            NSString *sectionTitle = [[[dicSorted allKeys]sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)] objectAtIndex:section];
            NSArray *sectionName = [dicSorted objectForKey:sectionTitle];
            return [sectionName count];
        }
        else
        {
            return  [arrayData count];
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cellLocationSearchId";
    
    SearchTableViewCell *cell = (SearchTableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SearchTableViewCell" owner:self options:nil]lastObject];
        
        tableView.backgroundColor = [UIColor clearColor];
        
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    if (searchEnableBool)
    {
        cell.labelCompanyName.text = [[mutableArrayFilteredCompanyName valueForKey:@"company_name" ] objectAtIndex:indexPath.row];
        
        if ([[[mutableArrayFilteredCompanyName valueForKey:@"application_due_date"] objectAtIndex:indexPath.row] isEqualToString:@"no"]||[[[mutableArrayFilteredCompanyName valueForKey:@"application_due_date"] objectAtIndex:indexPath.row] isEqualToString:@"Open Until Filled"])
        {
            cell.labelDate.text = @"Open Untill Filled";
        }
        else
        {
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"yyyy-MM-dd";
            NSDate *yourDate = [dateFormatter dateFromString:[[mutableArrayFilteredCompanyName valueForKey:@"application_due_date"] objectAtIndex:indexPath.row ]];
            
            NSDateFormatter* myDateFormatter = [[NSDateFormatter alloc] init];
            myDateFormatter.dateFormat = @"dd-MMM-yyyy";
            //vijay007
            cell.labelDate.text = [[NSString stringWithFormat:@"%@",[myDateFormatter stringFromDate:yourDate]] uppercaseString];
        }
        
        if ([[[mutableArrayFilteredCompanyName valueForKey:@"logo"]objectAtIndex:indexPath.row] isEqualToString:@"N"])
        {
            [cell.imageVwLogo setImage:[UIImage imageNamed:@"defaultBuilding"]];
        }
        else
        {
            cell.imageVwLogo.image = [UIImage imageWithContentsOfFile:[[mutableArrayFilteredCompanyName valueForKey:@"logo"]objectAtIndex:indexPath.row]];
        }
    }
    else
    {
        if (sortEnableBool)
        {
            NSString *sectionTitle = [[[dicSorted allKeys]sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)] objectAtIndex:indexPath.section];
            NSArray *sectionName = [dicSorted  objectForKey:sectionTitle];
            NSString *names = [sectionName objectAtIndex:indexPath.row];
            cell.labelCompanyName.text = names;
            NSInteger anIndex = [[arrayData valueForKey:@"company_name" ] indexOfObject:names];
            
            if ([[[arrayData valueForKey:@"application_due_date"] objectAtIndex:anIndex ] isEqualToString:@"no"])
            {
                cell.labelDate.text = @"Open Until Filled";
            }
            
            else if ([[[arrayData valueForKey:@"application_due_date"] objectAtIndex:anIndex ] isEqualToString:@"Open Until Filled"])
            {
                cell.labelDate.text = @"Open Until Filled";
            }
            
            else
            {
                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.dateFormat = @"yyyy-MM-dd";
                NSDate *yourDate = [dateFormatter dateFromString:[[arrayData valueForKey:@"application_due_date"] objectAtIndex:anIndex ]];
                
                NSDateFormatter* myDateFormatter = [[NSDateFormatter alloc] init];
                myDateFormatter.dateFormat = @"dd-MMM-yyyy";
                //vijay007
                
                cell.labelDate.hidden = NO;
                cell.labelDate.text = [[NSString stringWithFormat:@"%@",[myDateFormatter stringFromDate:yourDate]] uppercaseString];
            }
            
            [self setCompanyImages:cell index:indexPath];
        }
        
        else
        {
            cell.labelCompanyName.text = [[arrayData valueForKey:@"company_name" ] objectAtIndex:indexPath.row];
            
            if ([[[arrayData valueForKey:@"application_due_date"] objectAtIndex:indexPath.row] isEqualToString:@"no"]||[[[arrayData valueForKey:@"application_due_date"] objectAtIndex:indexPath.row] isEqualToString:@"Open Until Filled"])
            {
                cell.labelDate.text = @"Open Until Filled";
            }
            
            else
            {
                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.dateFormat = @"yyyy-MM-dd";
                NSDate *yourDate = [dateFormatter dateFromString:[[arrayData valueForKey:@"application_due_date"] objectAtIndex:indexPath.row ]];
                
                NSDateFormatter* myDateFormatter = [[NSDateFormatter alloc] init];
                myDateFormatter.dateFormat = @"dd-MMM-yyyy";
                //vijay007
                cell.labelDate.text = [[NSString stringWithFormat:@"%@",[myDateFormatter stringFromDate:yourDate]] uppercaseString];
            }
            
            if ([[[arrayData valueForKey:@"logo" ] objectAtIndex:indexPath.row] isEqualToString:@""])
            {
                [cell.imageVwLogo setImage:[UIImage imageNamed:@"defaultBuilding"]];
            }
            
            else
            {
                NSString *strUrl = [NSString stringWithFormat:@"%@",[[arrayData valueForKey:@"logo"] objectAtIndex:indexPath.row]];
                cell.imageVwLogo.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",strImageUrl,[strUrl stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
            }
            
            [self setCompanyImages:cell index:indexPath];
        }
    }
    
    cell.imageVwLogo.layer.cornerRadius = 5.0f;
    cell.imageVwLogo.clipsToBounds = YES;
    cell.imageVwLogo.layer.borderWidth = 2.0f;
    cell.imageVwLogo.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor whiteColor]);
    
    self.locationFilterTableView.contentInset = UIEdgeInsetsMake(-1.0f, -1.0f, 0.0f, 0.0);
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor =[UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.10];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

-(void)setCompanyImages :(SearchTableViewCell*)cell index:(NSIndexPath*)index
{
    isInternetOff = YES;
    
    if(isInternetOff)
    {
        if (sortEnableBool)
        {
            NSString *sectionTitle = [[[dicSorted allKeys]sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)] objectAtIndex:index.section];
            NSArray *sectionName = [dicSorted  objectForKey:sectionTitle];
            NSString *names = [sectionName objectAtIndex:index.row];
            NSInteger anIndex = [[arrayData valueForKey:@"company_name" ] indexOfObject:names];
            
            if ([[[arrayData valueForKey:@"logo"]objectAtIndex:anIndex] isEqualToString:@"N"])
            {
                [cell.imageVwLogo setImage:[UIImage imageNamed:@"defaultBuilding"]];
            }
            else
            {
                cell.imageVwLogo.image = [UIImage imageWithContentsOfFile:[[arrayData valueForKey:@"logo"]objectAtIndex:anIndex]];
            }
        }
        else
        {
            if ([[[arrayData valueForKey:@"logo"]objectAtIndex:index.row] isEqualToString:@"N"])
            {
                [cell.imageVwLogo setImage:[UIImage imageNamed:@"defaultBuilding"]];
            }
            else
            {
                cell.imageVwLogo.image = [UIImage imageWithContentsOfFile:[[arrayData valueForKey:@"logo"]objectAtIndex:index.row]];
            }
        }
    }
    else
    {
        if (sortEnableBool)
        {
            NSString *sectionTitle = [[[dicSorted allKeys]sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)] objectAtIndex:index.section];
            NSArray *sectionName = [dicSorted  objectForKey:sectionTitle];
            NSString *names = [sectionName objectAtIndex:index.row];
            NSInteger anIndex = [[arrayData valueForKey:@"company_name" ] indexOfObject:names];
            
            if ([[[arrayData valueForKey:@"logo" ] objectAtIndex:anIndex] length] ==0)
            {
                [cell.imageVwLogo setImage:[UIImage imageNamed:@"defaultBuilding"]];
            }
            else
            {
                NSString *strUrl1 = [NSString stringWithFormat:@"%@%@",strImageUrl,[[arrayData valueForKey:@"logo"]objectAtIndex:anIndex]];
                [cell.imageVwLogo setImageWithURL:[NSURL URLWithString:strUrl1]];
            }
        }
        else
        {
            if ([[[arrayData valueForKey:@"logo" ] objectAtIndex:index.row] length] ==0)
            {
                [cell.imageVwLogo setImage:[UIImage imageNamed:@"defaultBuilding"]];
            }
            else
            {
                NSString *strUrl1 = [NSString stringWithFormat:@"%@%@",strImageUrl,[[arrayData valueForKey:@"logo"]objectAtIndex:index.row]];
                [cell.imageVwLogo setImageWithURL:[NSURL URLWithString:strUrl1]];
            }
        }
    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (!sortEnableBool||searchEnableBool)
    {
        return 0;
    }
    else
    {
        return arrayCompanylIndexTitles;
    }
}


- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [[[dicSorted allKeys]sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)]  indexOfObject:title];
}


#pragma mark- UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (!searchEnableBool)
    {
        if (sortEnableBool)
        {
            UIView *headerView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-20, 20)];
            headerView.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:232.0f/255.0f blue:232.0f/255.0f alpha:0.4];
            UILabel *headerString = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
            headerString.text = [NSString stringWithFormat:@"%@",[[[dicSorted allKeys]sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)] objectAtIndex:section]];
            headerString.font = [UIFont fontWithName:@"OpenSans-Bold" size:16.0f];
            headerString.textAlignment = NSTextAlignmentCenter;
            headerString.textColor = [UIColor blackColor];
            [headerView addSubview:headerString];
            
            return headerView;
        }
        else
        {
            return nil;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (!sortEnableBool||searchEnableBool)
    {
        return 1;
    }
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*Open view detailDescription view controller*/
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger anIndex;
    
    CompaniesDetailViewController *companiesView = [self.storyboard instantiateViewControllerWithIdentifier:@"CompaniesDetailView"];
    
    if (searchEnableBool)
    {
        companiesView.dicCompanyDetail = [[NSMutableDictionary alloc]initWithDictionary:[mutableArrayFilteredCompanyName objectAtIndex:indexPath.row]];
    }
    else if(sortEnableBool)
    {
        NSString *sectionTitle = [[[dicSorted allKeys]sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSArray *sectionName = [dicSorted  objectForKey:sectionTitle];
        NSString *names = [sectionName objectAtIndex:indexPath.row];
        NSInteger currentRow;
        NSInteger sumSections = 0;
        
        for (int i = 0; i < indexPath.section; i++)
        {
            NSInteger rowsInSection = [tableView numberOfRowsInSection:i];
            sumSections += rowsInSection;
        }
        
        currentRow = sumSections + indexPath.row;
        anIndex = [[arrayData valueForKey:@"company_name" ] indexOfObject:names];
        companiesView.dicCompanyDetail = [[NSMutableDictionary alloc] initWithDictionary:[arrayData objectAtIndex:anIndex]];
    }
    else
    {
        companiesView.dicCompanyDetail = [[NSMutableDictionary alloc] initWithDictionary:[arrayData objectAtIndex:indexPath.row]];
    }
    if (arrayCompaniesList.count==0)
    {
        arrayCompaniesList = [arrayData mutableCopy];
    }
    self.navigationController.navigationBarHidden = NO;
    companiesView.strVwChane = @"seachViewString";
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    [self.navigationController pushViewController:companiesView animated:NO];
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([_locationFilterTableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [_locationFilterTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([_locationFilterTableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [_locationFilterTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark- PlaceAwards

-(void)setAwardPhotos:(NSIndexPath* )index cellTable:(SearchTableViewCell*)cellTable
{
    int xValue=71;
    
    if (searchEnableBool)
    {
        for (int i=0; i<5; i++)
        {
            UIImageView *imageAwards = [[UIImageView alloc] initWithFrame:CGRectMake(xValue, 39, 12, 12)];
            xValue = xValue+16;
            
            if (i<[[[mutableArrayFilteredCompanyName valueForKey:@"allAwards"] objectAtIndex:index.row] count])
            {
                if ([[[[mutableArrayFilteredCompanyName valueForKeyPath:@"allAwards.awards"] objectAtIndex:index.row] objectAtIndex:i] isEqualToString:@"2"])
                {
                    imageAwards.image = [UIImage imageNamed:@"yellow-cup"];
                }
                else if ([[[[mutableArrayFilteredCompanyName valueForKeyPath:@"allAwards.awards"] objectAtIndex:index.row] objectAtIndex:i] isEqualToString:@"3"])
                {
                    imageAwards.image = [UIImage imageNamed:@"silverCup"];
                }
                else
                {
                    imageAwards.image = [UIImage imageNamed:@"grey-cup"];
                }
            }
            else
            {
                imageAwards.image = [UIImage imageNamed:@"grey-cup"];
            }
            
            [cellTable.contentView addSubview:imageAwards];
        }
    }
    else
    {
        if (sortEnableBool)
        {
            
            NSString *sectionTitle = [[[dicSorted allKeys]sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] objectAtIndex:index.section];
            NSArray *sectionName = [dicSorted  objectForKey:sectionTitle];
            NSString *names = [sectionName objectAtIndex:index.row];
            NSInteger anIndex = [[arrayData valueForKey:@"company_name" ] indexOfObject:names];
            
            
            for (int i=0; i<5; i++)
            {
                UIImageView *imageAwards = [[UIImageView alloc] initWithFrame:CGRectMake(xValue, 39, 12, 12)];
                xValue = xValue+16;
                
                if (i<[[[arrayData valueForKey:@"allAwards"] objectAtIndex:anIndex] count])
                {
                    if ([[[[arrayData valueForKeyPath:@"allAwards.awards"] objectAtIndex:anIndex] objectAtIndex:i] isEqualToString:@"2"])
                    {
                        imageAwards.image = [UIImage imageNamed:@"yellow-cup"];
                    }
                    else if ([[[[arrayData valueForKeyPath:@"allAwards.awards"] objectAtIndex:anIndex] objectAtIndex:i] isEqualToString:@"3"])
                    {
                        imageAwards.image = [UIImage imageNamed:@"silverCup"];
                    }
                    else if ([[[[arrayData valueForKeyPath:@"allAwards.awards"] objectAtIndex:anIndex] objectAtIndex:i] isEqualToString:@"1"])
                    {
                        imageAwards.image = [UIImage imageNamed:@"honour"];
                    }
                    else if ([[[[arrayData valueForKeyPath:@"allAwards.awards"] objectAtIndex:anIndex] objectAtIndex:i] isEqualToString:@"0"])
                    {
                        imageAwards.image = [UIImage imageNamed:@"noAward"];
                    }
                    else
                    {
                        imageAwards.image = [UIImage imageNamed:@"light_silver"];
                    }
                }
                else
                {
                    imageAwards.image = [UIImage imageNamed:@"light_silver"];
                }
                
                [cellTable.contentView addSubview:imageAwards];
            }
        }
        else
        {
            for (int i=0; i<5; i++)
            {
                UIImageView *imageAwards = [[UIImageView alloc] initWithFrame:CGRectMake(xValue, 39, 12, 12)];
                xValue = xValue+16;
                
                if (i<[[[arrayData valueForKey:@"allAwards"] objectAtIndex:index.row] count])
                {
                    if ([[[[arrayData valueForKeyPath:@"allAwards.awards"] objectAtIndex:index.row] objectAtIndex:i] isEqualToString:@"2"])
                    {
                        imageAwards.image = [UIImage imageNamed:@"yellow-cup"];
                    }
                    else if ([[[[arrayData valueForKeyPath:@"allAwards.awards"] objectAtIndex:index.row] objectAtIndex:i] isEqualToString:@"3"])
                    {
                        imageAwards.image = [UIImage imageNamed:@"silverCup"];
                    }
                    else
                    {
                        imageAwards.image = [UIImage imageNamed:@"grey-cup"];
                    }
                }
                else
                {
                    imageAwards.image = [UIImage imageNamed:@"grey-cup"];
                }
                
                [cellTable.contentView addSubview:imageAwards];
            }
        }
    }
}

#pragma mark- UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    locationFilterSearchBar.showsCancelButton = YES;
    
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    locationFilterSearchBar.showsCancelButton = NO;
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar endEditing:YES];
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [mutableArrayFilteredCompanyName removeAllObjects];
    
    if(searchBar.text.length<1)
    {
        searchEnableBool=NO;
    }
    else
    {
        searchEnableBool=YES;
        
        for (NSString *sc in arrayCompanyName)
        {
            NSRange  scrange = [sc rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (scrange.location != NSNotFound)
            {
                for ( int i=0; i<arrayData.count; i++)
                {
                    if([[[arrayData valueForKey:@"company_name"]objectAtIndex:i] isEqual:sc])
                    {
                        [mutableArrayFilteredCompanyName addObject:[arrayData objectAtIndex:i]];
                    }
                }
            }
        }
        NSSortDescriptor* brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
        NSArray* sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
        mutableArrayFilteredCompanyName = [[ mutableArrayFilteredCompanyName sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    }
    
    [self ShowDataLoader:NO];
    
    [_locationFilterTableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}


#pragma mark- ButtonActions

-(void)btnAction
{
    LocationFilterViewController *detailsView = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationFilterView"];
    [self.navigationController pushViewController:detailsView animated:NO];
}

- (IBAction)btnSortingTableAction:(id)sender
{
    if ([sender tag]==11)
    {
        [self sortCompaniesByName];
    }
    
    else if ([sender tag]==12)
    {
        sortEnableBool = NO;
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"awards" ascending:NO ];
        NSArray * stories;
        
        if (searchEnableBool)
        {
            stories = [mutableArrayFilteredCompanyName sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
            mutableArrayFilteredCompanyName = [stories mutableCopy];
        }
        else
        {
            stories = [arrayData sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
            arrayData = [stories copy];
        }
    }
    else
    {
        sortEnableBool = NO;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy/MM/dd"];
        NSArray *arrayDatesSorted;
        
        NSMutableArray*arrwithDates,*arrayWithoutDates;
        arrwithDates = [NSMutableArray array];
        arrayWithoutDates = [NSMutableArray array];
        
        if (searchEnableBool)
        {
            for (NSDictionary*dict in mutableArrayFilteredCompanyName)
            {
                if ([[dict objectForKey:@"application_due_date"] isEqualToString:@"no"]) {
                    if(![arrayWithoutDates containsObject:dict]){
                        [arrayWithoutDates addObject:dict];
                    }
                }
                else{
                    if(![arrwithDates containsObject:dict]){
                        [arrwithDates addObject:dict];
                    }
                }
            }
            arrayDatesSorted = [arrwithDates sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2)
                                {
                                    NSDate *d1 = [formatter dateFromString:obj1[@"application_due_date"]];
                                    NSDate *d2 = [formatter dateFromString:obj2[@"application_due_date"]];
                                    
                                    return [d1 compare:d2]; // ascending order
                                    //                 return [d2 compare:d1]; // descending order
                                }];
            
            arrwithDates = [arrayDatesSorted mutableCopy];
            [arrwithDates addObjectsFromArray:arrayWithoutDates];
            mutableArrayFilteredCompanyName = [arrwithDates mutableCopy];
        }
        else
        {
            for (NSDictionary*dict in arrayData)
            {
                if ([[dict objectForKey:@"application_due_date"] isEqualToString:@"no"]||[[dict objectForKey:@"application_due_date"] isEqualToString:@"Open Until Filled"])
                {
                    if(![arrayWithoutDates containsObject:dict]){
                        [arrayWithoutDates addObject:dict];
                    }
                    
                }
                else
                {
                    if(![arrwithDates containsObject:dict]){
                        [arrwithDates addObject:dict];
                    }
                }
            }
            arrayDatesSorted = [arrwithDates sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2)
                                {
                                    NSDate *d1 = [formatter dateFromString:obj1[@"application_due_date"]];
                                    NSDate *d2 = [formatter dateFromString:obj2[@"application_due_date"]];
                                    return [d1 compare:d2]; // ascending order
                                }];
            
            arrwithDates = [arrayDatesSorted mutableCopy];
            [arrwithDates addObjectsFromArray:arrayWithoutDates];
            
            arrayData = [NSArray arrayWithArray:arrwithDates];
        }
        
        [arrwithDates removeAllObjects];
        [arrayWithoutDates removeAllObjects];
        arrwithDates = nil;
        arrayWithoutDates = nil;
    }
    
    [self ShowDataLoader:NO];
    [_locationFilterTableView reloadData];
}

-(void)sortCompaniesByName
{
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"company_name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    NSArray *sortedArray;
    
    if (searchEnableBool)
    {
        sortedArray  = [mutableArrayFilteredCompanyName sortedArrayUsingDescriptors:sortDescriptors];
        
        mutableArrayFilteredCompanyName = [sortedArray mutableCopy];
    }
    else
    {
        sortedArray  = [arrayData sortedArrayUsingDescriptors:sortDescriptors];
        arrayData = [sortedArray copy];
    }
    sortEnableBool = YES;
}

#pragma mark- tabBarButtonActions
- (IBAction)buttonActionPortfolio:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[PortfolioViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    PortfolioViewController *portfolioView = [self.storyboard instantiateViewControllerWithIdentifier:@"portfolio"];
    [self.navigationController pushViewController:portfolioView animated:NO];
}

- (IBAction)buttonActionDiscip:(id)sender;
{
    stringFilterScreen = @"";
    fromLoadDisc = YES;
    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"callallCompanyWebService"];
    
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[DisciplineViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    DisciplineViewController *disciplineVw = [self.storyboard instantiateViewControllerWithIdentifier:@"disciplineView"];
    [self.navigationController pushViewController:disciplineVw animated:NO];
}

- (IBAction)buttonSearch:(id)sender;
{
}

- (IBAction)buttonMessages:(id)sender;
{
    MessagesViewController  *messageVw = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllCompanies"];
    [self.navigationController pushViewController:messageVw animated:NO];
}

- (IBAction)buttonActionSalary:(id)sender;
{
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++)
    {
        id obj=[viewControllers objectAtIndex:i];
        
        if([obj isKindOfClass:[SalaryGuideViewController class]])
        {
            [[self navigationController] popToViewController:obj animated:NO];
            return;
        }
    }
    SalaryGuideViewController *salaryVw = [self.storyboard instantiateViewControllerWithIdentifier:@"salaryVw"];
    [self.navigationController pushViewController:salaryVw animated:NO];
}

@end
